public class TipoLogradouroDAO {
    private final static TipoLogradouroDAO instance = new TipoLogradouroDAO();

    private TipoLogradouroDAO() { }

    public static TipoLogradouroDAO getInstance(){
        return instance;
    }

    public TipoLogradouro__mdt buscarPorNome(String strNome){
        List<TipoLogradouro__mdt> lstTipoLogradouro = buscarPorNome(new Set<String> { strNome });

        if (lstTipoLogradouro.isEmpty()){
            return null;
        } else {
            return lstTipoLogradouro[0];
        }
    }

    public List<TipoLogradouro__mdt> buscarPorNome(Set<String> setStrNome){
        return [
            SELECT
                Id,
                Valor__c
            FROM
                TipoLogradouro__mdt
            WHERE
                DeveloperName IN :setStrNome
        ];
    }
}