public with sharing class TabulacaoController {



  @AuraEnabled(cacheable=false)
  public static List<Codigo__c> returnCodigoDeClienteList(String codigoConta){
    try {
      List<Codigo__c> motivoList = TabulacaoDAO.getInstance().listaCodigoDeCliente(codigoConta);
      return motivoList;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled(cacheable=false)
  public static List<AggregateResult> returnMotivoList(String recordDeveloperName){
    try {
      List<AggregateResult> motivoList = TabulacaoDAO.getInstance().listaMotivoTipoRegistro(recordDeveloperName);
      return motivoList;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled(cacheable=false)
  public static List<KnowledgeBase__kav> updateCaseAssunto(String caseId, String motivo , String subMotivo){

    
    return knowledgeSearchController.getKnowledgeArticles(motivo + ' - ' + subMotivo);

  }

  @AuraEnabled(cacheable=false)
  public static List<AggregateResult> returnSubMotivoList(String recordDeveloperName, String motivo){
    try {
      List<AggregateResult> submotivoList =TabulacaoDAO.getInstance().listaSubMotivoTipoRegistro(recordDeveloperName, motivo);
      return submotivoList;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }
  
  @AuraEnabled(cacheable=false)
  public static List<AggregateResult> returnMeioDeAcessoList(String recordDeveloperName, String motivo, String subMotivo){
    try {
      List<AggregateResult> meioDeAcessoList = TabulacaoDAO.getInstance().listaMeioAcessoTipoRegistro(recordDeveloperName, motivo, subMotivo);
      return meioDeAcessoList;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage() + ' - Linha :' + e.getLineNumber());
    }
  }

  @AuraEnabled(cacheable=false)
  public static List<AggregateResult> returnSubMotivo2List(String recordDeveloperName, String motivo, String subMotivo){
    try {
      List<AggregateResult> subMotivo2List = TabulacaoDAO.getInstance().listaSubMotivo2TipoRegistro(recordDeveloperName, motivo, subMotivo);
      return subMotivo2List;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage() + ' - Linha :' + e.getLineNumber());
    }
  }

  @AuraEnabled(cacheable=false)
  public static PIDCheck returnPID(String caseId, String motivo, String subMotivo){
    try {
      Case caso=CaseDAO.getInstance().buscarPorId(caseId);
      String PID = MetadataDAO.getInstance().buscarPID( motivo, subMotivo);
      PIDCheck pidCheck=new PIDCheck();
      if (PID=='CRITICO'){
        return obterPerguntas(8, caso, PID);
      } else if (PID=='MODERADO'){
        return obterPerguntas(5, caso, PID);
      } else if (PID=='MODERADO FINANCEIRO'){
        return obterPerguntas(6, caso, PID);
      }
      return(null);
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage() + ' - Linha :' + e.getLineNumber());
    }
  }

  final static Integer NP_CNPJ=7;
@ TestVisible
  static Pergunta obterPerguntaCNPJ(Integer posicao, Account conta){

    switch on posicao{
      when 0{
        String CNPJ=conta.CnpjCpf__c;
        try{
          CNPJ='5 primeiros dígitos:'+CNPJ.substring(0,5)+'  CNPJ completo:'+CNPJ;
        } catch(Exception ex){
        }
        return(new Pergunta('CNPJ - Confirme os 5 primeiros números CNPJ da sua empresa', CNPJ,99));
      }
      when 1{
        String CNPJ=conta.CnpjCpf__c;
        try{
          CNPJ='3 primeiros dígitos:'+CNPJ.substring(0,3)+'  CNPJ completo:'+CNPJ;
        } catch(Exception ex){
        }
        return(new Pergunta('CNPJ - Confirme os 3 primeiros números CNPJ da sua empresa', CNPJ,99));
      }
      when 2{
        return(new Pergunta('CNPJ - Confirme o dígito do CNPJ da sua empresa', conta.CnpjCpf__c,99));
      }
      when 3{
        return(new Pergunta('CNPJ - Eu vou te falar os 2 nº iniciais do CNPJ, favor confirmar os demais números', conta.CnpjCpf__c,99));
      }
      when 4{
        return(new Pergunta('CNPJ - Eu vou te falar o início do CNPJ por favor confirme os demais números', conta.CnpjCpf__c,99));
      }
      when 5{
        return(new Pergunta('CNPJ - Informe o número do CNPJ de sua empresa', conta.CnpjCpf__c,99));
      }
      when 6{
        String CNPJ=conta.CnpjCpf__c;
        try{
          CNPJ='8 primeiros dígitos:'+CNPJ.substring(0,8)+'  CNPJ completo:'+CNPJ;
        } catch(Exception ex){
        }
        return(new Pergunta('CNPJ - Confirme os  8 primeiros números  do CNPJ', CNPJ,99));
      }
    }
    return(new Pergunta('Não encontrado a pergunta',''+posicao,99));
  }
  final static Integer NP_PERGUNTAS=32;

  static Pergunta obterPergunta(Integer posicao, Account conta, Contact contato){

    switch on posicao{
      when 0{
        return(new Pergunta('Endereço / Telefone - Confirme o endereço da sua empresa', conta.BillingStreet, 1));
      }
      when 1{
        return(new Pergunta('Endereço / Telefone - Confirme o CEP da sua empresa', conta.BillingPostalCode, 1));
      }
      when 2{
        return(new Pergunta('Endereço / Telefone - Confirme o bairro e o CEP da sua empresa', 'CEP:'+conta.BillingPostalCode+' Endereço:'+conta.BillingStreet, 1));
      }
      when 3{
        return(new Pergunta('Endereço / Telefone - Qual é o telefone de contato da sua empresa?', conta.Phone, 1));
      }
      when 4{
        return(new Pergunta('Endereço / Telefone - Confirme o prefixo do telefone de contato de sua empresa', conta.Phone, 1));
      }
      when 5{
        return(new Pergunta('Código - Confirme o código de serviço da sua empresa', conta.Codigo8__c,2));
      }
      when 6{
        return(new Pergunta('Código - Informe o código da sua empresa', conta.Codigo8__c,2));
      }
      when 7{
        return(new Pergunta('Código - Qual é o código de serviço da sua empresa?', conta.Codigo8__c,2));
      }
      when 8{
        return(new Pergunta('Código - Qual é o código da sua empresa para consulta?', conta.Codigo8__c,2));
      }
      when 9{
        String codigo=conta.Codigo8__c;
        try{
          codigo='3 últimos números:'+codigo.substring(codigo.length()-3)+'  Código completo:'+codigo;
        } catch(Exception ex){
        }        
        return(new Pergunta('Código - Quais são os 3 últimos números do código de serviço?', conta.Codigo8__c,2));
      }
      when 10{
        String codigo=conta.Codigo8__c;
        try{
          codigo='4 primeiros números:'+codigo.substring(0, 4)+'  Código completo:'+codigo;
        } catch(Exception ex){
        }        
        return(new Pergunta('Código - Quais são os 4 primeiros números do código de serviço de sua empresa', conta.Codigo8__c,2));
      }
      when 11{
        return(new Pergunta('Dados Transacionais - Qual foi o produto mais utilizado pela empresa no último mês?', 'PROTHEUS - Tela Inicial - Botão Consumo - Preencher o último período',3));
      }
      when 12{
        return(new Pergunta('Dados Transacionais - Informe o nome de um dos produtos utilizados pela empresa', 'PROTHEUS - Tela Inicial - Botão Consumo - Preencher o último período',3));
      }
      when 13{
        return(new Pergunta('Dados Transacionais - Qual produto mais utilizado pela empresa?', 'PROTHEUS - Tela Inicial - Botão Consumo - Preencher o último período',3));
      }
      when 14{
        return(new Pergunta('Dados Transacionais - Qual foi o produto mais utilizado pela empresa no último mês?', 'PROTHEUS - Tela Inicial - Botão Consumo - Preencher o último período',3));
      }
      when 15{
        return(new Pergunta('Dados Transacionais - Dados Transacionais - Qual foi o produto mais utilizado pela empresa no último mês?', 'PROTHEUS - Tela Inicial - Botão Consumo - Preencher o último período',3));
      }
      when 16{
        return(new Pergunta('Dados Pessoais - Informe o seu CPF', contato.CPF__c,4));
      }
      when 17{
        return(new Pergunta('Dados Pessoais - Informe o seu nome completo', contato.Name,4));
      }
      when 18{
        String data;
        try{
          data=''+contato.Birthdate.format();
        } catch(Exception ex){
          data='';
        }
        return(new Pergunta('Dados Pessoais - Informe a data do seu nascimento', data,4));
      }
      when 19{
        String idade;
        try{
          Integer days = contato.Birthdate.daysBetween(Date.Today());
          Integer age = (Integer)(math.Floor(days/365.2425));
          idade=age+' anos';
        } catch(Exception ex){
          idade='0';
        } 
        return(new Pergunta('Dados Pessoais - Qual é a sua idade?', idade,4));
      }
      when 20{
        String CPF=conta.CnpjCpf__c;
        try{
          CPF='6 primeiros dígitos:'+CPF.substring(0,6)+'  CPF completo:'+CPF;
        } catch(Exception ex){
        }
        return(new Pergunta('Dados Pessoais - Qual os 6 primeiros números do seu CPF?', CPF,4));
      }
      when 21{
        String data;
        try{
          data=''+contato.Birthdate.year();
        } catch(Exception ex){
          data='';
        }
        return(new Pergunta('Dados Pessoais - Em que ano você nasceu?', data,4));
      }
      when 22{
        String data;
        try{
          data=''+contato.Birthdate.format();
        } catch(Exception ex){
          data='';
        }
        return(new Pergunta('Dados Pessoais - Qual é a data de seu nascimento?', data,4));
      }
      when 23{
        return(new Pergunta('Dados Pessoais - Confirme o seu CPF', contato.Cpf__c,4));
      }
      when 24{
        return(new Pergunta('Faturamento / Boleto - Os boletos para pagamento são enviados para quais e-mails?', 'PROTHEUS - Tela Inicial, aba Dados do Cliente',5));
      }
      when 25{
        return(new Pergunta('Faturamento / Boleto - Qual é a data de faturamento?', 'PROTHEUS - Tela Inicial, aba Dados do cliente, Grupo faturamento',5));
      }
      when 26{
        return(new Pergunta('Contrato/Cadastro - Informe o nome do contato principal com a Boa Vista.', 'PROTHEUS - Tela Inicial, aba Cadastrais',6));
      }
      when 27{
        return(new Pergunta('Contrato/Cadastro - A empresa é cliente da Boa Vista desde que ano?', 'PROTHEUS - Tela Inicial, aba Dados do Contrato, campo Filiação',6));
      }
      when 28{
        return(new Pergunta('Contrato/Cadastro - Qual o contato da empresa cadastrado com a Boa Vista?', 'PROTHEUS - Tela Inicial, aba Cadastrais',6));
      }
      when 29{
        return(new Pergunta('Contrato/Cadastro - Desde que período a empresa é cliente da Boa Vista?', 'PROTHEUS - Tela Inicial, aba Dados do Contrato, campo Filiação',6));
      }
      when 30{
        return(new Pergunta('Contrato/Cadastro - Confirme desde que período a empresa é nossa cliente?', 'PROTHEUS - Tela Inicial, aba Dados do Contrato, campo Filiação',6));
      }
      when 31{
        return(new Pergunta('Contrato/Cadastro - Confirme o contato da empresa cadastrado com a Boa Vista', 'PROTHEUS - Tela Inicial, aba Cadastrais',6));
      }
    }
    return(new Pergunta('','',null));
  }
  @TestVisible
  static PIDCheck obterPerguntas(Integer quantidade, Case caso, String tipo){
    Account conta=AccountDAO.getInstance().buscarPorId(caso.AccountId);
    Contact contato=ContactDAO.getInstance().buscarPorId(caso.ContactId);
    List<Pergunta> perguntas=new List<Pergunta>();
    perguntas.add(obterPerguntaCNPJ((Integer)(Math.random()*NP_CNPJ),conta));  

    /*
      NRO   DESC.GRUPO                RANGE PERGUNTA  QTD.PERGUNTAS
      1     Endereço / Telefone        0  - 4               
      2     Código                     5  - 10              
      3     Dados Transacionais        11 - 15              
      4     Dados Pessoais             16 - 23              
      5     Faturamento / Boleto       24 - 25  
      6     Contrato/Cadastro          26 - 31    
    */
      
      
      Integer IndexGrupo1 = generateRandomFromRange(0,4);
      Integer IndexGrupo2 = generateRandomFromRange(5,10);
      Integer IndexGrupo3 = generateRandomFromRange(11,15);
      Integer IndexGrupo4 = generateRandomFromRange(16,23);
      Integer IndexGrupo5 = generateRandomFromRange(24,25);
      Integer IndexGrupo6 = generateRandomFromRange(26,31);
         
      Integer IndexEntreGrupos = generateRandomFromRange(1,6);
      
      System.debug('Quantidade :' + quantidade);
    
      if (quantidade == 5) 
      {
          perguntas.add(obterPergunta(IndexGrupo1,conta,contato));
          perguntas.add(obterPergunta(IndexGrupo2,conta,contato));
          perguntas.add(obterPergunta(IndexGrupo3,conta,contato));
          perguntas.add(obterPergunta(IndexGrupo4,conta,contato));
      }
      if (quantidade == 6) 
      {
          perguntas.add(obterPergunta(IndexGrupo1,conta,contato));
          perguntas.add(obterPergunta(IndexGrupo2,conta,contato));
          perguntas.add(obterPergunta(IndexGrupo3,conta,contato));
          perguntas.add(obterPergunta(IndexGrupo4,conta,contato));
          perguntas.add(obterPergunta(IndexGrupo5,conta,contato));
      }
      if (quantidade == 8) 
      {  
          perguntas.add(obterPergunta(IndexGrupo1,conta,contato));
          perguntas.add(obterPergunta(IndexGrupo2,conta,contato));
          perguntas.add(obterPergunta(IndexGrupo3,conta,contato));
          perguntas.add(obterPergunta(IndexGrupo4,conta,contato));
          perguntas.add(obterPergunta(IndexGrupo5,conta,contato));
          perguntas.add(obterPergunta(IndexGrupo6,conta,contato));
          
          
           
          //Sortear novamente
          //
          IndexGrupo1 = generateRandomFromRange(0,4);
          IndexGrupo2 = generateRandomFromRange(5,10);
          IndexGrupo3 = generateRandomFromRange(11,15);
          IndexGrupo4 = generateRandomFromRange(16,23);
          IndexGrupo5 = generateRandomFromRange(24,25);
          IndexGrupo6 = generateRandomFromRange(26,31);
          
          // Incremente de uma nova pergunta          
          switch on IndexEntreGrupos
          {
              
              when 1{
                  perguntas.add(obterPergunta(IndexGrupo1,conta,contato));
              }
              when 2{
                  perguntas.add(obterPergunta(IndexGrupo2,conta,contato));
              }
              when 3{
                  perguntas.add(obterPergunta(IndexGrupo3,conta,contato));
              }
              when 4{
                  perguntas.add(obterPergunta(IndexGrupo4,conta,contato));
              }
              when 5{
                  perguntas.add(obterPergunta(IndexGrupo5,conta,contato));
              }
              when 6{
                  perguntas.add(obterPergunta(IndexGrupo6,conta,contato));
              }
          }
      }
      
    PIDCheck pid=new PIDCheck();
    pid.tipo=tipo;
    if (perguntas.size()>0){
      pid.pergunta1=perguntas.get(0).pergunta;
      pid.resposta1=perguntas.get(0).resposta;
    } else {
      pid.pergunta1='';
      pid.resposta1='';
    }
    if (perguntas.size()>1){
      pid.pergunta2=perguntas.get(1).pergunta;
      pid.resposta2=perguntas.get(1).resposta;
    } else {
      pid.pergunta2='';
      pid.resposta2='';
    }
    if (perguntas.size()>2){
      pid.pergunta3=perguntas.get(2).pergunta;
      pid.resposta3=perguntas.get(2).resposta;
    } else {
      pid.pergunta3='';
      pid.resposta3='';
    }
    if (perguntas.size()>3){
      pid.pergunta4=perguntas.get(3).pergunta;
      pid.resposta4=perguntas.get(3).resposta;
    } else {
      pid.pergunta4='';
      pid.resposta4='';
    }
    if (perguntas.size()>4){
      pid.pergunta5=perguntas.get(4).pergunta;
      pid.resposta5=perguntas.get(4).resposta;
    } else {
      pid.pergunta5='';
      pid.resposta5='';
    }
    if (perguntas.size()>5){
      pid.pergunta6=perguntas.get(5).pergunta;
      pid.resposta6=perguntas.get(5).resposta;
    } else {
      pid.pergunta6='';
      pid.resposta6='';
    }
    if (perguntas.size()>6){
      pid.pergunta7=perguntas.get(6).pergunta;
      pid.resposta7=perguntas.get(6).resposta;
    } else {
      pid.pergunta7='';
      pid.resposta7='';
    }
    if (perguntas.size()>7){
      pid.pergunta8=perguntas.get(7).pergunta;
      pid.resposta8=perguntas.get(7).resposta;
    } else {
        pid.pergunta8='';
        pid.resposta8='';
    }
      
      return(pid);
  }
    
    public static Integer generateRandomFromRange(integer startNum, integer endNum){
        Integer returnValue;
        //Logic- first we will generate random boolean value which will decide if we want to
        //add the random number from startNum or will subtract random number from endNum
        Integer randomNumber = Integer.valueof((math.random() * 10));
        boolean addSubFlag= math.mod(randomNumber,2) == 0 ? true : false;
        
        integer diffInRange = endNum-startNum;
        //Now find random number staring from 0 to diffInRange
        Integer randomNum = Math.mod(Math.round(Math.random()*diffInRange+1),diffInRange);
        
        //If addSubFlag is true, we will add random number generated between 0 and diffInRange from start number
        //else we will subtract random number from end number
        if(addSubFlag){
            if(diffInRange > 1)
                returnValue =startNum + randomNum;
            else
                returnValue =startNum;
        }else{
            if(diffInRange > 1)
                returnValue = endNum - randomNum;
            else
                returnValue =endNum;
        }
        return returnValue;
    }  

  @AuraEnabled(cacheable=false)
  public static String retornaObservacoes(String motivo, String subMotivo, String meioDeAcesso, String recordDeveloperName){
    try {
      TabulacaoSLA__c tabulacao;
      if (recordDeveloperName == 'ITSM') {
        tabulacao = TabulacaoDAO.getInstance().dadosAprovacao(recordDeveloperName, motivo, submotivo, meioDeAcesso, '');
      } else {
        tabulacao = TabulacaoDAO.getInstance().dadosAprovacao(recordDeveloperName, motivo, submotivo, '', meioDeAcesso);
      }
      if (tabulacao==null){
        return(null);
      }
      String observacao = tabulacao.Observacoes__c;

      return observacao;
      
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  private class TabulacaoControllerException extends Exception{}


  public class responseCodigoDeCliente
  {
    @AuraEnabled public String Id{get;set;}
    @AuraEnabled public String Name{get;set;}    
  }

  public class PIDCheck {
    @AuraEnabled public String tipo{get;set;}
    @AuraEnabled public String pergunta1{get;set;}
    @AuraEnabled public String resposta1{get;set;}
    @AuraEnabled public String pergunta2{get;set;}
    @AuraEnabled public String resposta2{get;set;}
    @AuraEnabled public String pergunta3{get;set;}
    @AuraEnabled public String resposta3{get;set;}
    @AuraEnabled public String pergunta4{get;set;}
    @AuraEnabled public String resposta4{get;set;}
    @AuraEnabled public String pergunta5{get;set;}
    @AuraEnabled public String resposta5{get;set;}
    @AuraEnabled public String pergunta6{get;set;}
    @AuraEnabled public String resposta6{get;set;}
    @AuraEnabled public String pergunta7{get;set;}
    @AuraEnabled public String resposta7{get;set;}
    @AuraEnabled public String pergunta8{get;set;}
    @AuraEnabled public String resposta8{get;set;}
  }
@TestVisible
  private class Pergunta{
    String pergunta;
    String resposta;
    Integer grupo;
      
    Pergunta(String pergunta, String resposta, Integer grupo){
      this.pergunta=pergunta;
      this.resposta=resposta;
      this.grupo = grupo;
    }
  }
@TestVisible
  private class GrupoPergunta {
    Integer groupId;
    Integer nroPerguntaInicial;
    Integer nroPerguntaFinal;
    // Integer nroPerguntaAnterior;
    @TestVisible
    GrupoPergunta (Integer groupId,Integer nroPerguntaInicial,Integer nroPerguntaFinal) {
       this.groupId = groupId;
       this.nroPerguntaInicial = nroPerguntaInicial;
       this.nroPerguntaFinal = nroPerguntaFinal;
     }
     @TestVisible
     public Integer obterPergunta() {

       return (Integer) Math.floor(Math.random() * (nroPerguntaFinal - nroPerguntaInicial) + nroPerguntaInicial);
     }

  }
}