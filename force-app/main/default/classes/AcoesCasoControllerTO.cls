public class AcoesCasoControllerTO {
    
    public class ReturnTO { 
        @AuraEnabled public String motivo { get; set; }
        @AuraEnabled public String subMotivo { get; set; }
        @AuraEnabled public String subMotivo2 { get; set; }

        @AuraEnabled public String CPFCNPJ { get; set; }
        @AuraEnabled public String tipoCliente { get; set; }
        @AuraEnabled public String codigo { get; set ;}

        @AuraEnabled public String nome { get; set; }
        @AuraEnabled public String personBirthdate { get; set; }
        @AuraEnabled public String eMail { get; set; }
        @AuraEnabled public String cep {get;set;}
        @AuraEnabled public String bairro {get;set;}
        @AuraEnabled public String rua {get;set;}
        @AuraEnabled public String cidade {get;set;}
        @AuraEnabled public String uf {get;set;}
        @AuraEnabled public string numero {get;set;}






    }
    
    public class ReturnStatusTO {
        @AuraEnabled public String mensagem { get; set; }
        @AuraEnabled public Boolean erro { get; set; }
    }
    public class CadastroPositivoFormTO {
        @AuraEnabled
        public String numeroDocumento { get; set; }
        @AuraEnabled
        public String tipoCliente { get; set; }
        @AuraEnabled
        public String nome { get; set; }
        @AuraEnabled
        public String dataAniversario { get; set; }
        @AuraEnabled
        public String numeroRg { get; set; }
        @AuraEnabled
        public String orgaoExpedidor { get; set; }
        @AuraEnabled
        public String sexo { get; set; }
        @AuraEnabled
        public String email { get; set; }
        @AuraEnabled
        public String endereco { get; set; }
        @AuraEnabled
        public String bairro { get; set; }
        @AuraEnabled
        public String cidade { get; set; }
        @AuraEnabled
        public String cep { get; set; }
        @AuraEnabled
        public String unidadeFederativa { get; set; }
    }

}