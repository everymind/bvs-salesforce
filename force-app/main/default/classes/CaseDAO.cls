public class CaseDAO {
    private final static CaseDAO instance = new CaseDAO();

    private CaseDAO() { }

    public static CaseDAO getInstance(){
        return instance;
    }

    public Case buscarPorId(Id idCase){
        List<Case> lstCase = buscarPorId(new Set<Id> { idCase });

        if (lstCase.isEmpty()){
            return null;
        } else {
            return lstCase[0];
        }
    }

    public List<Case> buscarPorId(Set<Id> setIdCase){
        return [
            SELECT
                Id,
                Type,
                Status, 
                RecordTypeId,
                AccountId,
                AlertaAmarelo__c,
                AplicacaoCorrecaoProduto__c,
                AprovacaoAdicionais__c,
                AQuemPertenceEsteEmail__c,
                AQuemPertenceEsteSite__c,
                AQuemPertenceSolicitacao__c,
                Area__c,
                AreasNecessariasCall__c,
                AssetId,
                AtributoRetornoRVA__c,
                AtualmenteUtilizaVDI__c,
                CasoRelacionado__c,
                CausaRaizIdentificada__c,
                CentroCusto__c,
                ClasseMudanca__c,
                ClienteIraAlterarDesenvolverAlgo__c,
                ClosedDate,
                CodigoDeCliente__c,
                Comments,
                ContadorStatus__c,
                ContactId,
                ControleEmail__c,
                ControleRetorno__c,
                CPF__c,
                CNPJ__c,
                CPFVitima__c,
                CreatedById,
                CreatedDate,
                CreditService__c,
                Data1__c,
                Data2__c,
                DataDenuncia__c,
                Data_do_KBA__c,
                DataEstimada__c,
                Datafim__c,
                DataFinalizacaoMarco__c,
                DataHoraAutomacao__c,
                DataHoraInicio__c,
                DataInicio__c,
                DataInicioJanela__c,
                DataNascimento__c,
                DataOcorrenciaFraude__c,
                DataTerrminoJanela__c,
                DescricaoCausaRaizIdentificada__c,
                DescricaoSolucao__c,
                Description,
                DesejaGerarPostmortem__c,
                DocumentoConsulta__c,
                Duvida__c,
                Email__c,
                EmailOperador__c,
                EmailSolicitante__c,
                EmailUsuario__c,
                EmQueTivemosSorte__c,
                EntitlementId,
                EnviarProtocoloFlow__c,
                EnvolveProcessoLoginPermissaoID__c,
                EtapasMudanca__c,
                ExisteDocumentoConsultaBVS__c,
                FarolSLA__c,
                FilaAnterior__c,
                FilaAtual__c,
                Fluxodisparo__c,
                GatilhoIncidente__c,
                IDExterno__c,
                Impacto__c,
                ImpactoNegocio__c,
                ImplantacaoProdutoNovo__c,
                IndisponibilidadeDuranteJanela__c,
                InformacoesAdicionais__c,
                InformarCodigoEspelho__c,
                InformeDadosAutorFraude__c,
                IsClosedOnCreate,
                IsEscalated,
                IsStopped,
                Justificativa__c,
                JustificativaStatus__c,
                JustificativaSupervisor__c,
                KBA_Aprovado__c,
                Language,
                LinkDesativarEmail__c,
                LinkPesquisa__c,
                MarketingSolutions__c,
                MeioAcesso__c,
                MeioAcessoTxt__c,
                Mensagem__c,
                MigracaoPlataformaLocal__c,
                Modelo__c,
                MotivoNaoExecucao__c,
                MotivoPendencia__c,
                MotivoRejeicao__c,
                MotivoRejeicaoIndevida__c,
                MotivosRechamada__c,
                MotivoTxt__c,
                MudaBilhetagemFaturamentoProd__c,
                Nomebase__c,
                NomeEmpresaEnvolvidaFraude__c,
                NomeOperador__c,
                NomeSolicitante__c,
                NomeUsuario__c,
                NomeVitima__c,
                NoProdutoComoUmTodo__c,
                ObservacaoSobreData__c,
                OQueEstaSendoImplantado__c,
                OQueNaoOcorreuBem__c,
                OqueOcorreuBem__c,
                OrdemPriorizacao__c,
                OrigemIncidente__c,
                Origin,
                Outros__c,
                Ouvidoria__c,
                OwnerId,
                OwnerIdAnterior__c,
                Participantes__c,
                PeriodoIndisponibilidade__c,
                PF__c,
                PJ__c,
                PossuiCasoRelacionado__c,
                Prazo__c,
                PreOuvidoria__c,
                Priority,
                ProductId,
                Qual__c,
                QualComponenteProdutoSerAfetado__c,
                QualDataHoraEmail__c,
                QualEmailSerLiberado__c,
                QualEscopoMudanca__c,
                QualFoiArquivoAtividadeBloqueada__c,
                QualImpacto__c,
                QualJustificativaAcessoSite__c,
                QualJustificativaSolicitacao__c,
                QualRemetenteDestinatarioEmail__c,
                QualSiteVoceDesejaLiberacao__c,
                QualURLParaRemocao__c,
                QualURLPhishing__c,
                Quantidade__c,
                QuantidadeConsultas__c,
                RealizadoRollback__c,
                Reason,
                RejeicaoIndevida__c,
                RequerAprovacao__c,
                RespostaRetorno__c,
                ResultadoMudanca__c,
                Retorno_do_KBA__c,
                Risks__c,
                ServiceContractId,
                SLAAtivo__c,
                SLAVersao__c,
                Solucao__c,
                SolucaoAplicada__c,
                SolucaoIncidente__c,
                SourceId,
                StatusAcao__c,
                StatusMudanca__c,
                SuaMudancaImpactaClientes__c,
                Subject,
                Submotivo__c,
                Submotivo2__c,
                SubMotivo2Txt__c,
                SubMotivoTxt__c,
                SuppliedCompany,
                SuppliedEmail,
                SuppliedName,
                SuppliedPhone,
                Tabulacao__c,
                TelefoneContato__c,
                TempoDecorridoSLACliente__c,
                TipoPostmortem__c,
                Urgencia__c,
                VaiGerarIndisponibilidadeAcesso__c,
                Recordtype.DeveloperName,
                Account.Type,
                Account.Name,
                Account.PersonBirthdate, 
                Account.PersonEmail,
                Account.Email__c,
                Account.Recordtype.DeveloperName,
                Account.billingAddress,
                Account.NumeroEntrega__c,
                Account.BairroEntrega__c,
                Account.ShippingCity,
                Account.ShippingPostalCode,
                Account.ShippingState,
                Account.ShippingStreet,
                Contact.Birthdate,
                Contact.Name,
                Contact.Email,
                ContactCPF__c,
                PIDAprovado__c
            FROM
                Case
            WHERE
                Id IN :setIdCase
        ];
    }

    public Case buscarTabulacaoCaso(Id caseId){
        return([SELECT 
        TempoDecorridoSLACliente__c, 
        MotivoTxt__c, 
        SubMotivoTxt__c, 
        SubMotivo2Txt__c, 
        MeioAcessoTxt__c,
        Recordtype.DeveloperName,
        FilaAtual__c,
        ParentId,
        Owner.Name,
        NaoAtendido__c,
        Status,
        FilaAnterior__c,
        MotivoRejeicao__c,
        ProximaFila__c
        FROM Case WHERE Id =: caseId]);
    }
    public CaseMilestone buscarPorIdCasoCompletoEN2(string idCaso)
    {
        List<MilestoneType> idPrime = [SELECT id FROM MilestoneType Where Name = 'PRIME'];
        List<MilestoneType> idRetorno = [SELECT id FROM MilestoneType Where Name = 'RETORNO'];
        List<MilestoneType> idBackoffice = [SELECT id FROM MilestoneType Where Name = 'BACKOFFICE DE ATENDIMENTO'];
        List<MilestoneType> idConsumidor = [SELECT id FROM MilestoneType Where Name = 'CONSUMIDOR'];
        List<MilestoneType> idClass = [SELECT id FROM MilestoneType Where Name = 'CLASS'];

        if(idPrime != null && idRetorno != null && idBackoffice != null && idConsumidor != null && idClass != null)
        {
            List<CaseMilestone> milestoneList = [
            SELECT
                Id, TargetResponseInMins, StartDate, CompletionDate, ElapsedTimeInMins
            FROM 
                CaseMilestone
            WHERE
                CaseId = :idCaso 
            AND
                IsCompleted = true
            AND 
                MilestoneTypeId !=: idPrime[0].id
            AND 
                MilestoneTypeId !=: idRetorno[0].id
            AND  	
                MilestoneTypeId !=: idBackoffice[0].id
            AND
                MilestoneTypeId !=: idConsumidor[0].id
                AND
                MilestoneTypeId !=: idClass[0].id
            LIMIT 1
            ];

            if(milestoneList.size() > 0)
            {
                return milestoneList[0];  
            }
            
        }
        system.debug('Não encontrei to indo pro segundo');
           List<CaseMilestone> milestone = [ SELECT
            Id, TargetResponseInMins, StartDate, CompletionDate
        FROM 
            CaseMilestone
        WHERE
            CaseId = :idCaso 
        AND
            IsCompleted = true
        LIMIT 1
    ];
        return !milestone.isEmpty() ? milestone[0] : null;

    }

    public List<CaseMilestone> retornaMarcoAtivoByIds (Set<Id> idsCaso)
    {
        return [ SELECT
            Id, TargetResponseInMins, CaseId
            FROM 
                CaseMilestone
            WHERE
                CaseId IN: idsCaso 
            AND
                IsCompleted = false
        ];
    }
    public List<CaseMilestone> retornaMarcoAtivo (string idCaso)
    {
        return [ SELECT
            Id, TargetResponseInMins
            FROM 
                CaseMilestone
            WHERE
                CaseId =: idCaso 
            AND
                IsCompleted = false
        ];
    }
}