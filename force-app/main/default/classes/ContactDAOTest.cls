/**
 * @Autor: Everymind
 * @Data: 27/07/2022
 * @Descrição: Classe de teste para classe ContactDAO.
 */
@IsTest
public class ContactDAOTest {

    /**
	 * @Autor: Everymind
	 * @Data: 27/07/2022
	 * @Descrição: Método que testa o método  buscarPorId.
	 */
	@IsTest
	static void buscarPorIdTest() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);

		Contact objContato = ContactDAO.getInstance().buscarPorId(contato.Id);
		Test.stopTest();
		System.assertEquals(true, true);
	}

    /**
	 * @Autor: Everymind
	 * @Data: 27/07/2022
	 * @Descrição: Método que testa o método  buscarPorPersonAccount.
	 */
	@IsTest
	static void buscarPorPersonAccountTest() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);

		Contact objContato = ContactDAO.getInstance().buscarPorPersonAccount(conta.Id);
		Test.stopTest();
		System.assertEquals(true, true);
	}

    /**
	 * @Autor: Everymind
	 * @Data: 27/07/2022
	 * @Descrição: Método que testa o método  buscarPorContato.
	 */
	@IsTest
	static void buscarPorContatoTest() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);

		Contact objContato = ContactDAO.getInstance().buscarPorContato(contato.Id);
		Test.stopTest();
		System.assertEquals(true, true);
	}

    /**
	 * @Autor: Everymind
	 * @Data: 27/07/2022
	 * @Descrição: Método que testa o método  buscarRelacionamentoPorContaContato.
	 */
	@IsTest
	static void buscarRelacionamentoPorContaContatoTest() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);

		Contact objContato = ContactDAO.getInstance().buscarRelacionamentoPorContaContato(conta.Id, contato.Id);
		Test.stopTest();
		System.assertEquals(true, true);
	}

    /**
	 * @Autor: Everymind
	 * @Data: 27/07/2022
	 * @Descrição: Método que testa o método  buscarPorId.
	 */
	@IsTest
	static void buscarPorIdSetTest() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Set<Id> setId = new Set<Id>();
        setId.add(contato.Id);

		List<Contact> objContato = ContactDAO.getInstance().buscarPorId(setId);
		Test.stopTest();
		System.assertEquals(true, true);
	}

    /**
	 * @Autor: Everymind
	 * @Data: 27/07/2022
	 * @Descrição: Método que testa o método  buscarContatoFavorito.
	 */
	@IsTest
	static void buscarContatoFavoritoTest() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Set<Id> setId = new Set<Id>();
        setId.add(contato.Id);

		Contact objContato = ContactDAO.getInstance().buscarContatoFavorito(conta.Id);
		Test.stopTest();
		System.assertEquals(true, true);
	}

    /**
	 * @Autor: Everymind
	 * @Data: 27/07/2022
	 * @Descrição: Método que testa o método  buscarIdPorConta.
	 */
	@IsTest
	static void buscarIdPorContaTest() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);

		Id objContato = ContactDAO.getInstance().buscarIdPorConta(conta.Id);
		Test.stopTest();
		System.assertEquals(true, true);
	}
}