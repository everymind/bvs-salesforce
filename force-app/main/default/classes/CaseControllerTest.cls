/**
 * @Autor: Everymind
 * @Data: 22/07/2022
 * @Descrição: Classe de teste para classe IFramePortalControllerCase.
 */
@IsTest 
public class CaseControllerTest {

    /**
	 * @Autor: Everymind
	 * @Data: 25/07/2022
	 * @Descrição: Método que testa o método queryMotivoSubMotivo.
	 */
    @IsTest 
    public static void queryMotivoSubMotivoTest() { 
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
        
        Test.startTest();
            Account conta = TestDataFactory.gerarContaJuridica(true);
            Contact contato = TestDataFactory.gerarContato(true, conta);
            Case caso = TestDataFactory.gerarCasoAtendimento(false, conta, contato);
                caso.MotivoTxt__c = 'Teste';
                caso.SubMotivoTxt__c = 'Teste';
                insert caso; 

            TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(false);
            tabulacao.TipoRegistro__c = 'ITSMContaPortal';
            insert tabulacao;
            TabulacaoSLA__c tabulacao1 = TestDataFactory.gerarTabulacao(false);
            tabulacao1.TipoRegistro__c = 'ITSMContaPortal';
            insert tabulacao1;
            TabulacaoSLA__c tabulacao2 = TestDataFactory.gerarTabulacao(false);
            tabulacao2.TipoRegistro__c = 'ITSMContaPortal';
            insert tabulacao2;
            TabulacaoSLA__c tabulacao3 = TestDataFactory.gerarTabulacao(false);
            tabulacao3.TipoRegistro__c = 'ITSMContaPortal';
            insert tabulacao3;
            TabulacaoSLA__c tabulacao4 = TestDataFactory.gerarTabulacao(false);
            tabulacao4.TipoRegistro__c = 'ITSMContaPortal';
            tabulacao4.TempoSLA__c = 300;
            insert tabulacao4;
            TabulacaoSLA__c tabulacao5 = TestDataFactory.gerarTabulacao(false);
            tabulacao5.TipoRegistro__c = 'ITSMContaPortal';
            tabulacao5.SubMotivo2Txt__c = 'teste';
            insert tabulacao5;


            CaseController.queryMotivoSubMotivo();
        Test.stopTest();
        System.assertEquals(true, true);
        }

    }

    /**
	 * @Autor: Everymind
	 * @Data: 25/07/2022
	 * @Descrição: Método que testa o método salvarCaso.
	 */
    @IsTest 
    public static void salvarCasoTest() { 
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
        
        Test.startTest();
            Account conta = TestDataFactory.gerarContaJuridica(true);
            Contact contato = TestDataFactory.gerarContato(true, conta);
            Id rtITSM = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMAtendimento').getRecordTypeId();
            Case caso = TestDataFactory.gerarCasoAtendimento(false, conta, contato);
                caso.RecordTypeId = rtITSM;
                caso.MotivoTxt__c = 'ACESSOS';
                caso.SubMotivoTxt__c = 'CRIAÇÃO DE CAIXA POSTAL';
                caso.Status = 'FINALIZADO';
                caso.Solucao__c = 'FALTA DE IDENTIFICAÇÃO POSITIVA';
                caso.DescricaoSolucao__c = 'FALTA DE IDENTIFICAÇÃO POSITIVA';
                caso.Subject = 'PID BLOQUEADO';
                insert caso; 
            
            CaseController.CaseWrapper c = new CaseController.CaseWrapper();
            c.caseId = caso.Id;
            c.motivo = 'ACESSOS'; 
            c.subMotivo = 'CRIAÇÃO DE CAIXA POSTAL';
            c.meioDeAcesso = null;
            c.subMotivo2 = null;
            c.descricao = 'FINALIZADO';
            c.impacto = null;
            c.urgencia = null;
            c.prioridade = null;
            c.nomeSolicitante = null;
            c.emailSolicitante = null;
            c.centroCusto = null;
            c.justificativa = null;
            c.qualRemetenteDestinatarioEmail = null;
            c.qualDataHoraEmail = DateTime.newInstance(1997, 1, 31, 7, 8, 16);
            c.aQuemPertenceEsteEmail = null;
            c.qualFoiArquivoAtividadeBloqueada = null;
            c.existeDocumentoConsultaBVS = null;
            c.informeDadosAutorFraude = null;    
            c.dataOcorrenciaFraude = Date.newInstance(1960, 2, 19);  
            c.dataDenuncia = Date.newInstance(1960, 2, 19);
            c.cPFVitima = null;
            c.nomeVitima = null;
            c.nomeEmpresaEnvolvidaFraude = null;
            c.documentoConsulta = null;
            c.qualURLParaRemocao = null;
            c.qualURLPhishing = null;
            c.aQuemPertenceSolicitacao = null;
            c.outros = null;
            c.atualmenteUtilizaVDI = null;
            c.qualSiteVoceDesejaLiberacao = null;
            c.aQuemPertenceEsteSite = null;
            c.productId = null;
            c.quantidadeConsultas = 8;   
            c.dataInicio = Date.newInstance(1960, 2, 19);   
            c.datafim = Date.newInstance(1960, 2, 19);
            c.nomeUsuario = null;
            c.cPF = null;
            c.emailUsuario = null;
            c.nomeOperador = null;   
            c.dataNascimento = Date.newInstance(1960, 2, 19);
            c.emailOperador = null;
            c.codigoDeCliente = null;
            c.acessarPlataforma = true;
            c.PIDAprovado = true;
            c.bloquearCriacaoCasoPID = true;
            c.dataLimiteBloqueioCasoPID  = null;
            c.validouPID  = true;
            c.contato  = null;
            c.retornoKba = null;
            c.kbaAprovado = null;


            CaseController.salvarCaso(c);
        Test.stopTest();
        System.assertEquals(true, true);
        }

    }
    
    /**
	 * @Autor: Everymind
	 * @Data: 22/07/2022
	 * @Descrição: Método que testa o método CaseWrapper.
	 */
	@IsTest
	static void CaseControllerCaseWrapperTest() {
        Test.startTest();
        CaseController.CaseWrapper c = new CaseController.CaseWrapper();
            c.caseId = null;
            c.motivo = null; 
            c.subMotivo = null;
            c.meioDeAcesso = null;
            c.subMotivo2 = null;
            c.descricao = null;
            c.impacto = null;
            c.urgencia = null;
            c.prioridade = null;
            c.nomeSolicitante = null;
            c.emailSolicitante = null;
            c.centroCusto = null;
            c.justificativa = null;
            c.qualRemetenteDestinatarioEmail = null;
            c.qualDataHoraEmail = DateTime.newInstance(1997, 1, 31, 7, 8, 16);
            c.aQuemPertenceEsteEmail = null;
            c.qualFoiArquivoAtividadeBloqueada = null;
            c.existeDocumentoConsultaBVS = null;
            c.informeDadosAutorFraude = null;    
            c.dataOcorrenciaFraude = Date.newInstance(1960, 2, 19);  
            c.dataDenuncia = Date.newInstance(1960, 2, 19);
            c.cPFVitima = null;
            c.nomeVitima = null;
            c.nomeEmpresaEnvolvidaFraude = null;
            c.documentoConsulta = null;
            c.qualURLParaRemocao = null;
            c.qualURLPhishing = null;
            c.aQuemPertenceSolicitacao = null;
            c.outros = null;
            c.atualmenteUtilizaVDI = null;
            c.qualSiteVoceDesejaLiberacao = null;
            c.aQuemPertenceEsteSite = null;
            c.productId = null;
            c.quantidadeConsultas = 8;   
            c.dataInicio = Date.newInstance(1960, 2, 19);   
            c.datafim = Date.newInstance(1960, 2, 19);
            c.nomeUsuario = null;
            c.cPF = null;
            c.emailUsuario = null;
            c.nomeOperador = null;   
            c.dataNascimento = Date.newInstance(1960, 2, 19);
            c.emailOperador = null;
            c.codigoDeCliente = null;
            c.acessarPlataforma = true;
            c.PIDAprovado = true;
            c.bloquearCriacaoCasoPID = true;
            c.dataLimiteBloqueioCasoPID  = null;
            c.validouPID  = true;
            c.contato  = null;
            c.retornoKba = null;
            c.kbaAprovado = null;
        Test.stopTest();
        System.assertEquals(true, true);
	}

    /*
	 * @Autor: Everymind
	 * @Data: 02/08/2022
	 * @Descrição: Método que testa o método getCase.
	 */
	@IsTest
	static void getCaseTest() {
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Id rtITSM = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMAtendimento').getRecordTypeId();
        
        SlaProcess pp = [SELECT Id FROM SlaProcess WHERE Name = 'PRIME_Comunidade' LIMIT 1];
        
        Entitlement direito = new Entitlement();
        direito.Name = 'PRIME';
        direito.AccountId = conta.Id;
        direito.SlaProcessId = pp.Id;
        
        insert direito;     
        
        Case caso = TestDataFactory.gerarCasoAtendimento(false, conta, contato);
        caso.RecordTypeId = rtITSM;
        caso.MotivoTxt__c = 'ACESSOS';
        caso.SubMotivoTxt__c = 'CRIAÇÃO DE CAIXA POSTAL';
        caso.Status = 'FINALIZADO';
        caso.Solucao__c = 'FALTA DE IDENTIFICAÇÃO POSITIVA';
        caso.DescricaoSolucao__c = 'FALTA DE IDENTIFICAÇÃO POSITIVA';
        caso.Subject = 'PID BLOQUEADO';
        caso.EntitlementId = direito.Id;
        insert caso; 
        

        Test.startTest();
            Case retorno = CaseController.getCase(caso.Id);
        Test.stopTest();

        System.assertNotEquals(null, retorno);
    } 
}