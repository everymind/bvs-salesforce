global class SLAClienteMilestoneHandler implements Support.MilestoneTriggerTimeCalculator {   
    String caso;
    string milestoneType; 
    integer tempoSla;
    public SLAClienteMilestoneHandler (){   
    }
    public SLAClienteMilestoneHandler (string caso, string milestoneId, Integer temposla){ 
        this.caso = caso;
        this.milestoneType = milestoneId;
        this.tempoSla = temposla;
    }
    global Integer calculateMilestoneTriggerTime(String caseId, String milestoneTypeId)
    {
       TabulacaoSLA__c lstSLAMetaData;
       boolean filaN2Identica = false;
       Case caso = CaseDAO.getInstance().buscarTabulacaoCaso(caseId);
        if(caso.FilaAtual__c == 'BACKOFFICEATENDIMENTO' && caso.ProximaFila__c == 'RETORNO' || caso.FilaAtual__c == 'PRIME' && caso.ProximaFila__c == 'RETORNO'){
            lstSLAMetaData = TabulacaoDAO.getInstance().buscarTempoSLA(caso.Recordtype.DeveloperName, caso.MotivoTxt__c, caso.SubMotivoTxt__c, caso.SubMotivo2Txt__c, caso.MeioAcessoTxt__c, caso.FilaAtual__c, caso.ProximaFila__c);
            System.debug('========================================> ' + lstSLAMetaData);
        }
        else{
            lstSLAMetaData = TabulacaoDAO.getInstance().buscarTempoSLA(caso.Recordtype.DeveloperName, caso.MotivoTxt__c, caso.SubMotivoTxt__c, caso.SubMotivo2Txt__c, caso.MeioAcessoTxt__c, caso.FilaAtual__c);
            System.debug('========================================> ' + lstSLAMetaData);
        }
        
        if (caso.Status == 'ATUALIZADO') {
            lstSLAMetaData = TabulacaoDAO.getInstance().buscarTempoSLA('TB-116670');
            System.debug('========================================> ' + lstSLAMetaData);
        }
       
        System.debug('combinacaoSLA --> ' + lstSLAMetaData);
 
		Integer tempo;
        //Alteração LG 
        if (!MetadataDAO.getInstance().retornarFilasIniciaisAtendimento().contains(caso.FilaAtual__c) && caso.Recordtype.DeveloperName=='CSMAtendimento' && caso.FilaAtual__c != 'RETORNO' && caso.filaAtual__c != 'PREOUVIDORIA' && caso.filaAtual__c != 'OUVIDORIA')
        {
            system.debug('entrei aqui');
            CaseMilestone tempoRestante = CaseDAO.getInstance().buscarPorIdCasoCompletoEN2(caseId);
            system.debug(tempoRestante + ' Testando');
            if(tempoRestante != null)
            {
                system.debug('estou aqui');
                Integer dias = tempoRestante.CompletionDate.Day() - tempoRestante.StartDate.Day();
                Integer horas = dias * 24;
                system.debug(dias);
                horas += tempoRestante.CompletionDate.Hour() - tempoRestante.StartDate.Hour();
                system.debug(horas);
                Integer minutos = horas * 60;
                system.debug(minutos);
                minutos += tempoRestante.CompletionDate.minute() - tempoRestante.StartDate.minute();
                system.debug(minutos);
                tempo = tempoRestante.TargetResponseInMins - minutos;
                system.debug(tempo);
            }
        }
        // Alterado o else if abaixo, se houver colateral comunicar.
        if (tempo == null)
        {
            if(lstSLAMetaData != null)
            {
                TabulacaoSLA__c combinacaoSLA = lstSLAMetaData;
                if (combinacaoSLA.TempoSLA__c!=null)
                {
                    tempo=Integer.valueOf(combinacaoSLA.TempoSLA__c);
                }
            }
        }
/* 
        if(caso.Status == 'ATUALIZADO'){
            tempo = 4*60;
            return tempo;
        } */

        if(caso.Status == 'NÃO SOLUCIONADO')
        {
            system.debug('Entrei no não solucionado');
            TabulacaoSLA__c combinacaoSLA = lstSLAMetaData;
            tempo = Integer.valueOf(combinacaoSLA.TempoSLA__c);
            CaseMilestone tempoRestanteNaoSolucionado = CaseDAO.getInstance().buscarPorIdCasoCompletoEN2(caseId);
            if(tempoRestanteNaoSolucionado != null)
            {
                Integer tempoNaoSolucionado = tempo - tempoRestanteNaoSolucionado.ElapsedTimeInMins;
                return tempoNaoSolucionado >= 9*60 ? tempoNaoSolucionado : 9*60;

            }  
        }
        else if(caso.Recordtype.DeveloperName=='CSMAtendimento' && caso.Status == 'REJEITADO' && caso.MotivoRejeicao__c == 'DADOS INCOMPLETOS')
        {
            system.debug('entrei na regra do caso rejeitado para SLA' + tempo);
            if (tempo>3*60 && tempo < 9*60){ //SLA maior que 3 horas e menor que 9 horas
                tempo=9*60; //9 horas
                system.debug('caso rejeitado para SLA' + tempo);
                return tempo;
            } 
            else if (tempo < 3*60)
            {
                tempo = 3*60; // 3 horas
                system.debug(' caso rejeitado para SLA' + tempo);
                return tempo;
            }
        }
        else if(caso.NaoAtendido__c  && tempo > 18*60)
        {
            tempo = 1080;
            system.debug('Entrei' + tempo);
        }
        if(caso.TempoDecorridoSLACliente__c != null) {
            tempo=tempo-Integer.valueOf(caso.TempoDecorridoSLACliente__c);
        }

        System.debug('Tempo selecionado --> ' + tempo);
		return(tempo);
    } 

}