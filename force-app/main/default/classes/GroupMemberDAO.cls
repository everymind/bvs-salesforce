public class GroupMemberDAO {

    private final static GroupMemberDAO instance = new GroupMemberDAO();
    private GroupMemberDAO(){}

    public static GroupMemberDAO getInstance() {
        
        return instance;
    }

    public GroupMember buscarPorIdUsuario(id idUsuario){

        List<GroupMember> lstGroup = [SELECT Group.Name FROM GroupMember 
                                WHERE Group.Type = 'Regular' AND (Group.Name = 'CLASS' OR Group.Name = 'BACKOFFICE DE ATENDIMENTO' OR 
                                Group.Name = 'CONSUMIDOR' OR Group.Name = 'PRIME') AND UserOrGroupId = :idUsuario];

        if (lstGroup.isEmpty()){
            return null;
        } else {
            return lstGroup[0];
        }
    }
}