/**
 * @Autor: Everymind
 * @Data: 21/08/2020
 * @Descrição: Classe que orquestra os eventos da trigger para os métodos de negócio.
 */
public class UserTerritory2AssociationTriggerHandler extends TriggerHandler {

	/**
	 * @Autor: Everymind
	 * @Data: 21/08/2020
	 * @Descrição: Constante que contém a instância da classe de negócio do objeto UserTerritory2Association.
	 */
	private UserTerritory2AssociationBO objBO = UserTerritory2AssociationBO.getInstance();

	public override void afterInsert(){
		objBO.atualizarReencarteiramento(Trigger.new);
    }
        
    public override void afterDelete(){
		objBO.atualizarReencarteiramento(Trigger.old);
    }
}