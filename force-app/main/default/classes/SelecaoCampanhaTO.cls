/**
 * @Autor: Everymind
 * @Data: 11/07/2020
 * @Descrição: Classe que representa o objeto de informações do IFrame.
 */
public class SelecaoCampanhaTO {

	@AuraEnabled
	public Boolean sucesso { get; set; }

	@AuraEnabled
	public String origin { get; set; }
	
	@AuraEnabled
	public String host { get; set; }

	@AuraEnabled
	public String params { get; set; }

	@AuraEnabled
	public String url { get; set; }

	public String token { get; set; }
	public String sfId { get; set; }
	public String oportunidade { get; set; }
	public String conta { get; set; }
	public String cnpj { get; set; }
	public String codigo { get; set; }
	public String carteira { get; set; }
	public String canal_territorio { get; set; }
	public String item_receita { get; set; }
	public String regional { get; set; }
	public String regra_atual { get; set; }


	public SelecaoCampanhaTO() {
		this.sucesso = false;
		this.origin = '';
		this.host = '';
		this.params = '';
		this.url = '';
	}
}