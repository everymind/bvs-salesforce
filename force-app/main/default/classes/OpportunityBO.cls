/**
 * @Autor: Everymind
 * @Data: 18/07/2020
 * @Descrição: Classe que contém métodos de negócio referente ao domínio Opportunity.
 */
public class OpportunityBO {
    
    /**
     * @Autor: Everymind
     * @Data: 18/07/2020
     * @Descrição: Constante que representa uma instância da classe.
     */
    private static final OpportunityBO instance = new OpportunityBO();

    /**
     * @Autor: Everymind
     * @Data: 18/07/2020
     * @Descrição: Método público que retorna uma instância da classe.
     */
    public static OpportunityBO getInstance() {
        return instance;
    }

    /**
     * @Autor: Everymind
     * @Data: 25/07/2020
     * @Descrição: Método que atribui o catálogo à oportunidade de acordo com o tipo de registro
     * @Parâmetro: List<Opportunity> lstOportunidadesNovas - Lista que contém a instância dos registros de oportunidades após a criação.
     */
    public void atribuirCatalogo(List<Opportunity> lstOportunidadesNovas){
        RecordTypeInfo objTipoRegistroVendaPlano = Util.getRecordType('Opportunity', 'VendaPlano');
        
        List<RecordTypeInfo> lstTipoRegistro = new List<RecordTypeInfo>{
            Util.getRecordType('Opportunity', 'Registro'),
            Util.getRecordType('Opportunity', 'VendaCustomizada'),
            objTipoRegistroVendaPlano
        };

        List<Pricebook2> lstCatalogos = PricebookDAO.getInstance().buscarAtivos();

        Pricebook2 objCatalogoVendaPlano = (Pricebook2)CollectionUtil.buscarNaLista(lstCatalogos, objTipoRegistroVendaPlano.getName(), 'TipoRegistroOportunidade__c');

        List<PricebookEntry> lstEntradasCatalogoVendaPlano = new List<PricebookEntry>();
        if (objCatalogoVendaPlano != null){
            lstEntradasCatalogoVendaPlano = PricebookEntryDAO.getInstance().buscarPorCatalogo(objCatalogoVendaPlano.Id);
        }

        List<Opportunity> lstOportunidadesAlteradas = new List<Opportunity>();
        List<OpportunityLineItem> lstItensOportunidadeAdicionados = new List<OpportunityLineItem>();

        for (Opportunity objOportunidade : lstOportunidadesNovas){
            RecordTypeInfo objTipoRegistro;

            for (RecordTypeInfo objRecordTypeInfo : lstTipoRegistro){
                if (objRecordTypeInfo.getRecordTypeId() == objOportunidade.RecordTypeId){
                    objTipoRegistro = objRecordTypeInfo;
                    break;
                }
            }
            
            if (objTipoRegistro == null){
                continue;
            }
            
            if (!CollectionUtil.contemNaLista(lstCatalogos, objTipoRegistro.getName(), 'TipoRegistroOportunidade__c')){
                continue;
            }

            lstOportunidadesAlteradas.add(
                new Opportunity(
                    Id = objOportunidade.Id,
                    Pricebook2Id = ((Pricebook2)CollectionUtil.buscarNaLista(lstCatalogos, objTipoRegistro.getName(), 'TipoRegistroOportunidade__c')).Id
                )
            );

            if (!lstEntradasCatalogoVendaPlano.isEmpty() && (objOportunidade.RecordTypeId == Util.getRecordTypeId('Opportunity', 'VendaPlano'))){
                lstItensOportunidadeAdicionados.add(
                    new OpportunityLineItem(
                        OpportunityId = objOportunidade.Id,
                        PricebookEntryId = lstEntradasCatalogoVendaPlano[0].Id
                    )
                );
            }
        }

        if (!lstOportunidadesAlteradas.isEmpty()){
            update lstOportunidadesAlteradas;
        }

        if (!lstItensOportunidadeAdicionados.isEmpty()){
            insert lstItensOportunidadeAdicionados;
        }
    }
    
    public void obrigarPreenchimentoCampos(List<Opportunity> lstOpp,  Map<Id, Opportunity> oldMap){
        
        List<RecordType> listOppVendaCustomizada = new List<RecordType>();
        listOppVendaCustomizada = [SELECT Id FROM RecordType WHERE Name = 'Venda Customizada'];
        
        String OppVendaCustomizada;
        String profileName = [select Name from profile where id = :UserInfo.getProfileId()].Name;
        system.debug('PERFIL: '+ profileName);
        
        for(RecordType rt : listOppVendaCustomizada){
            
            if(!listOppVendaCustomizada.isEmpty()){
                
                OppVendaCustomizada = rt.id;
            }
        }
        
        for(Opportunity op : lstOpp){
            
            Opportunity oldOpp = oldMap.get(op.Id);
            
            if(!lstOpp.isEmpty() && op.TemProduto__c == true && oldOpp.TemProduto__c == true && oldOpp.StageName != op.StageName && op.RecordTypeId == OppVendaCustomizada && profileName != 'Administrador do sistema' && (op.Segmento__c == NULL || op.Ticket_M_dio__c == 0 || op.Plataforma__c == NULL || op.Pedidos_m_s__c == 0 || op.Gateway__c == NULL || op.Integra_o__c == NULL || op.In_cio_de_opera_o__c == NULL || op.Antifraude_Atual__c == NULL || op.Tem_piloto__c == NULL)){
                
                op.addError('Os campos: Segmento, Ticket Médio, Plataforma, Pedidos/mês, Gateway, Integração?, Início Operação, Antifraude Atual e Tem piloto? precisam ser preenchidos na sessão Informações Konduto.');          
                          
              }
        }
    }

    /**
	 * @Autor: Maycon
	 * @Data: 31/03/2022
	 * @Descrição: Método que atualiza a conta relacionada para ser processada como vencida
	 */
    public void marcarContasProcessamento(List<Opportunity> lstOportunidadesAtualizadas){

        List<Account> lstAccount = New List<Account>();

        for(Opportunity objOpportunity : lstOportunidadesAtualizadas){

            Account acct = New Account();
            if(objOpportunity.StageName == 'Fechada Perdida' && objOpportunity.MotivoGanhoPerda__c == 'Oportunidade sem atividades'){

                acct.Id = objOpportunity.AccountId;
                acct.ProcessarContaVencida__c = true;

                lstAccount.add(acct);
            }
        }

        update lstAccount;
    }
    
}