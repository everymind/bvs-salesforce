@isTest
public with sharing class CaseDAOTest {
    public CaseDAOTest() {}

    @IsTest
    static void buscarPorId(){
        
        Test.startTest();
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do Sistema');
        System.runAs(usr){
            
            Account conta = TestDataFactory.gerarContaJuridica(true);
            Contact contato = TestDataFactory.gerarContato(true, conta);
            Case caso =   TestDataFactory.gerarCasoAtendimento(true, conta,contato); 
            CaseDAO.getInstance().buscarPorId(caso.id);
            CaseDAO.getInstance().buscarTabulacaoCaso(caso.id);
            CaseDAO.getInstance().buscarPorIdCasoCompletoEN2(caso.id);
            Set<Id> idcase = new Set<Id>();
            idcase.add(caso.id);
            CaseDAO.getInstance().retornaMarcoAtivoByIds(idcase);
            CaseDAO.getInstance().retornaMarcoAtivo(caso.id);
        }
        Test.stopTest();
        
    }

}