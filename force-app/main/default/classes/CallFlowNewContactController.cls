public class CallFlowNewContactController {
    public Flow.Interview.Criar_contato CriarContatoFlow {get; set;}

    public PageReference getUrlcontact(){
        String contato;
        if (CriarContatoFlow!=null){
            contato=(string)CriarContatoFlow.getVariableValue('IdConta');
        } else {
            contato='Account/list?filterName=Recent';
        }
        Pagereference p = new Pagereference('/'+contato);
        p.setRedirect(true);
        return(p);
    }

}