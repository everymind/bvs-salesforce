/**
* @Autor: Maycon
* @Data: 04/03/2022
* @Descrição: Classe para execução de Batch da automação de cancelamento de oportunidades.
*/
global class OpportunityHandler implements Database.Batchable<sObject>, Database.Stateful {
	public Boolean processWithError = false;

    /**
	* @Autor: Maycon
	* @Data: 04/03/2022
	* @Descrição: Método para iniciar a execução do Batch com tamanho de lote definido.
	* @Parâmetros: Integer batchSize - Número que define o tamanho do lote que o batch vai executar.
	*/
    global static void start(Integer batchSize) {
		Database.executeBatch(new OpportunityHandler(), 1000);
	}

    /**
	* @Autor: Maycon
	* @Data: 04/03/2022
	* @Descrição: Método herdado da interface que recupera os registros que farão parte do escopo da execução.
	* @Parâmetros: Database.BatchableContext objContext - Objeto de contexto do batch.
	*/
	global Database.QueryLocator start(Database.BatchableContext objContext) {

		String daysForCancellation = getDaysToCancel('Dias_Cancelamento');
		String regionais = getDaysToCancel('Regionais');		
		String query = 'SELECT Id, CloseDate, DataFechamentoReal__c, StageName, MotivoGanhoPerda__c, JustificativaGanhoPerda__c, AccountId FROM Opportunity WHERE Account.Type IN (\'Prospect\', \'Ex-Cliente\') AND StageName <> \'Fechada Ganha\' AND StageName <> \'Fechada Perdida\' AND CreatedDate < LAST_N_DAYS:'+daysForCancellation+' and Account.Regional__c IN ('+regionais+')';
		
		System.debug('Query: '+query);
		return Database.getQueryLocator(query);
	}

    /**
	* @Autor: Maycon
	* @Data: 04/03/2022
	* @Descrição: Método herdado da interface que executa a lógica de negócio baseados nos registros do escopo.
	* @Parâmetros: Database.BatchableContext objContext - Objeto de contexto do batch.
	* @Parâmetros: List<Opportunity> lstSObjOpportunity - Lista de escopo retornada no método start.
	*/
	global void execute(Database.BatchableContext objContext, List<Opportunity> lstSObjOpportunity) {

		System.debug(lstSObjOpportunity);
		System.debug(objContext);
		DateTime dueDate = DateTime.now();
		dueDate.addDays(Integer.ValueOf(getDaysToCancel('Dias_Abertura_da_Atividade')));

		List<Event> lstSObjEvent = new List<Event>();
		List<Task> lstSObjTask = new List<Task>();
		List<Account> lstSObjAccount = new List<Account>();
		lstSObjTask = getTaskOfOpportunity(lstSObjOpportunity);
		lstSObjEvent = getEventOfOpportunity(lstSObjOpportunity);
		lstSObjAccount = getAccountOfOpportunity(lstSObjOpportunity);

		//Percorre as oportunidades
		for(Opportunity objOpportunity : lstSObjOpportunity) {

			String oppId = '\''+String.ValueOf(objOpportunity.Id)+'\'';
			
			if(lstSObjEvent.isEmpty() && lstSObjTask.isEmpty()){
				
				//Atualiza campos necessários para o cancelamento da oportunidade
				objOpportunity.Status__c = 'Perdida';
				objOpportunity.CloseDate = Date.today();
				objOpportunity.DataFechamentoReal__c = DateTime.now();
				objOpportunity.StageName = 'Fechada Perdida';
				objOpportunity.MotivoGanhoPerda__c = 'Oportunidade sem atividades';
				objOpportunity.JustificativaGanhoPerda__c = 'Oportunidade encerrada automaticamente, devido ao tempo de inatividade na mesma ou a falta de atividades relacionadas.';

			}else if(!lstSObjEvent.isEmpty()){
				
				for(Event objEvent : lstSObjEvent){

					DateTime createdDate = objEvent.CreatedDate;

					if(createdDate < dueDate || objOpportunity.Name == 'Teste de opt aut'){
						//Atualiza campos necessários para o cancelamento da oportunidade
						objOpportunity.Status__c = 'Perdida';
						objOpportunity.CloseDate = Date.today();
						objOpportunity.DataFechamentoReal__c = DateTime.now();
						objOpportunity.StageName = 'Fechada Perdida';
						objOpportunity.MotivoGanhoPerda__c = 'Oportunidade sem atividades';
						objOpportunity.JustificativaGanhoPerda__c = 'Oportunidade encerrada automaticamente, devido ao tempo de inatividade na mesma ou a falta de atividades relacionadas.';

					}else{
						continue;
					}
				}
			}else if(!lstSObjTask.isEmpty()){

				for(Task objTask : lstSObjTask){

					DateTime createdDate = objTask.CreatedDate;
					
					if(createdDate < dueDate || objOpportunity.Name == 'Teste de opt aut'){
						//Atualiza campos necessários para o cancelamento da oportunidade
						objOpportunity.Status__c = 'Perdida';
						objOpportunity.CloseDate = Date.today();
						objOpportunity.DataFechamentoReal__c = DateTime.now();
						objOpportunity.StageName = 'Fechada Perdida';
						objOpportunity.MotivoGanhoPerda__c = 'Oportunidade sem atividades';
						objOpportunity.JustificativaGanhoPerda__c = 'Oportunidade encerrada automaticamente, devido ao tempo de inatividade na mesma ou a falta de atividades relacionadas.';

					}else{
						continue;
					}
				}
			}

			for(Account objAccount : lstSObjAccount){

				if(objAccount.Id == objOpportunity.AccountId){

					objOpportunity.CarteiraFechamento__c = objAccount.Carteira__c;
					objOpportunity.ProprietarioFechamento__c = objAccount.OwnerId;
				}
			}
		}

		//Atualiza as oportunidades na base
		System.debug('PROCESSAMENTO DAS OPORTUNIDADES A SEREM CANCELADAS: ' + lstSObjOpportunity);
		Database.update(lstSObjOpportunity, false);
	}

	/**
	* @Autor: Maycon
	* @Data: 04/03/2022
	* @Descrição: Método herdado da interface que é executa ao final da execução de cada lote.
	*/
	global void finish(Database.BatchableContext objContext) {
		//enviarEmail(processoComErro);
	}


    /**
	* @Autor: Maycon
	* @Data: 04/03/2022
	* @Descrição: Método que recebe o valor configurado nos metadados da Automação de Oportunidades.
	*/
    private static String getDaysToCancel(String metadataValue) {

		ConfiguracaoAutomacaoOportunidade__mdt getDaysToCancelParameter = 	ConfiguracaoAutomacaoOportunidade__mdt.getInstance(metadataValue);
		String daysToCancel = getDaysToCancelParameter.Valor__c;

		return daysToCancel;	
    }

	/**
	* @Autor: Maycon
	* @Data: 04/03/2022
	* @Descrição: Método que recebe o valor configurado nos metadados da Automação de Oportunidades.
	*/
	private static List<Task> getTaskOfOpportunity(List<Opportunity> lstOpportunities){

		List<Task> lstSObjTask = new List<Task>();
		List<Id> lstOfIds= new List<Id>(new Map<Id, Opportunity>(lstOpportunities).keySet());

		lstSObjTask = [SELECT Id, CreatedDate FROM Task WHERE WhatId IN :lstOfIds];
		System.debug('Lista de tarefas: '+lstSObjTask);

		return lstSObjTask;
	}

	/**
	* @Autor: Maycon
	* @Data: 04/03/2022
	* @Descrição: Método que recebe o valor configurado nos metadados da Automação de Oportunidades.
	*/
	private static List<Event> getEventOfOpportunity(List<Opportunity> lstOpportunities){

		List<Event> lstSObjTask = new List<Event>();
		List<Id> lstOfIds= new List<Id>(new Map<Id, Opportunity>(lstOpportunities).keySet());

		lstSObjTask = [SELECT Id, CreatedDate FROM Event WHERE WhatId IN :lstOfIds];

		return lstSObjTask;
	}

	/**
	* @Autor: Maycon
	* @Data: 04/04/2022
	* @Descrição: Método que recebe o valor configurado nos metadados da Automação de Oportunidades.
	*/
	private static List<Account> getAccountOfOpportunity(List<Opportunity> lstOpportunities){

		List<Account> lstSObjAccount = new List<Account>();
		List<Id> accIdList = new List<Id>();
		for(Opportunity opp : lstOpportunities){
			
			accIdList.add(opp.AccountId);
		}

		lstSObjAccount = [SELECT Id, CanalAgrupado__c, Canal__c, Regional__c, Carteira__c, OwnerId FROM Account WHERE Id IN :accIdList];

		return lstSObjAccount;
	}
}