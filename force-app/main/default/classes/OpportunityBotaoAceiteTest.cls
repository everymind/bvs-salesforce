@isTest
private class OpportunityBotaoAceiteTest {
    @isTest
    static void enviarAceiteTestSuccess(){
        StaticResourceCalloutMock objMock = new StaticResourceCalloutMock();
        objMock.setStaticResource('EnviarAceiteTestSuccess');
        objMock.setStatusCode(200);

        Test.setMock(HttpCalloutMock.class, objMock);

        Account objConta = TestDataFactory.gerarContaJuridica(true);
        Contact objContato = TestDataFactory.gerarContato(true, objConta);

        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(true, objConta);

        objOportunidade.StageName = 'Negociação';
        update objOportunidade;

        TestDataFactory.gerarContatoOportunidade(true, objOportunidade, objContato, 'Cobrança');
        TestDataFactory.gerarContatoOportunidade(true, objOportunidade, objContato, 'Operador');

        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();

        System.assertEquals('success', objRetorno.Status);
        System.assertEquals('Processo efetuado com sucesso. 02 : E-mail enviado com sucesso ao GAAS, para a Reativação do cliente.', objRetorno.Message);

        objOportunidade = OpportunityDAO.getInstance().buscarPorId(objOportunidade.Id);

        System.assertEquals('Fechada Ganha', objOportunidade.StageName);
        System.assertEquals('Aceite Eletrônico', objOportunidade.MotivoGanhoPerda__c);
    }

    @isTest
    static void enviarAceiteTestError(){
        StaticResourceCalloutMock objMock = new StaticResourceCalloutMock();
        objMock.setStaticResource('EnviarAceiteTestError');
        objMock.setStatusCode(500);

        Test.setMock(HttpCalloutMock.class, objMock);

        Account objConta = TestDataFactory.gerarContaJuridica(true);
        Contact objContato = TestDataFactory.gerarContato(true, objConta);

        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(true, objConta);

        objOportunidade.StageName = 'Negociação';
        update objOportunidade;

        TestDataFactory.gerarContatoOportunidade(true, objOportunidade, objContato, 'Cobrança');

        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();

        System.assertEquals('warning', objRetorno.Status);
        System.assertEquals('Erro na conexão com os servidores da Boa Vista, tente novamente em instantes. Se o problema persistir, entre em contato com a Gestão de Vendas.', objRetorno.Message);
    }

    /*@isTest
    static void enviarAceiteTestAceiteEnviado(){
        Account objConta = TestDataFactory.gerarContaJuridica(true);

        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(false, objConta);
        objOportunidade.DataEnvioAceite__c = System.now();
        insert objOportunidade;

        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();

        System.assertEquals('warning', objRetorno.Status);
        System.assertEquals('O aceite já foi enviado para essa oportunidade', objRetorno.Message);
    }*/
    
    @isTest
    static void enviarAceiteTestDiferenteNegociacao(){
        Account objConta = TestDataFactory.gerarContaJuridica(true);
        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(true, objConta);

        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();
        
        System.assertEquals('warning', objRetorno.Status);
        System.assertEquals('Para enviar o aceite, o estágio de venda deve estar como "Negociação"', objRetorno.Message);
    }

    @isTest
    static void enviarAceiteTestSemTipoAceite(){
        Account objConta = TestDataFactory.gerarContaJuridica(true);

        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(false, objConta);
        objOportunidade.StageName = 'Negociação';
        objOportunidade.Tipo_de_aceite__c = null;
        insert objOportunidade;
    
        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();
    
        System.assertEquals('warning', objRetorno.Status);
        System.assertEquals('Antes de enviar o aceite é necessário preencher o campo "Tipo de aceite" na oportunidade', objRetorno.Message);
    }

    @isTest
    static void enviarAceiteTestSemValorPlano(){
        Account objConta = TestDataFactory.gerarContaJuridica(true);

        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(false, objConta);
        objOportunidade.StageName = 'Negociação';
        objOportunidade.ValorPlano__c = null;
        insert objOportunidade;
    
        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();
    
        System.assertEquals('warning', objRetorno.Status);
        System.assertEquals('Antes de enviar o aceite é necessário preencher o campo "Valor do plano" na oportunidade', objRetorno.Message);
    }

    @isTest
    static void enviarAceiteTestPresencial(){
        Account objConta = TestDataFactory.gerarContaJuridica(true);

        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(false, objConta);
        objOportunidade.StageName = 'Negociação';
        objOportunidade.Tipo_de_aceite__c = 'Presencial';
        insert objOportunidade;
    
        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();
    
        System.assertEquals('warning', objRetorno.Status);
        System.assertEquals('Essa funcionalidade não esta disponível para o tipo de aceite "Presencial"', objRetorno.Message);
    }

    @isTest
    static void enviarAceiteTestSemModalidadeCobranca(){
        Account objConta = TestDataFactory.gerarContaJuridica(true);

        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(false, objConta);
        objOportunidade.StageName = 'Negociação';
        objOportunidade.ModalidadeCobranca__c = null;
        insert objOportunidade;
    
        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();
    
        System.assertEquals('warning', objRetorno.Status);
        System.assertEquals('Antes de enviar o aceite é necessário preencher o campo "Modalidade de cobrança" na oportunidade', objRetorno.Message);
    }

    @isTest
    static void enviarAceiteTestSemCodigoRegraNegocio(){
        Account objConta = TestDataFactory.gerarContaJuridica(true);

        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(false, objConta);
        objOportunidade.StageName = 'Negociação';
        objOportunidade.CodigoRegraNegocio__c = null;
        insert objOportunidade;
    
        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();
    
        System.assertEquals('warning', objRetorno.Status);
        System.assertEquals('Antes de enviar o aceite é necessário preencher o campo "Código regra de negócio" na oportunidade', objRetorno.Message);
    }

    @isTest
    static void enviarAceiteTestSemEnderecoFaturamento(){
        Account objConta = TestDataFactory.gerarContaJuridica(true);

        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(false, objConta);
        objOportunidade.StageName = 'Negociação';
        objOportunidade.CEPFaturamento__c = null;
        insert objOportunidade;
    
        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();
    
        System.assertEquals('warning', objRetorno.Status);
        System.assertEquals('Antes de enviar o aceite é necessário preencher o "Endereço de faturamento" na oportunidade', objRetorno.Message);
    }

    @isTest
    static void enviarAceiteTestSemContatoCobranca(){
        Account objConta = TestDataFactory.gerarContaJuridica(true);

        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(true, objConta);
        
        objOportunidade.StageName = 'Negociação';
        update objOportunidade;
    
        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();
    
        System.assertEquals('warning', objRetorno.Status);
        System.assertEquals('Para enviar o aceite é necessário um contato na oportunidade com o papel "Cobrança"', objRetorno.Message);
    }

    @isTest
    static void enviarAceiteTestSemCnpj(){
        Account objConta = TestDataFactory.gerarContaJuridica(false);
        objConta.CnpjCpf__c = '';
        insert objConta;

        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(true, objConta);

        objOportunidade.StageName = 'Negociação';
        update objOportunidade;
    
        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();
    
        System.assertEquals('warning', objRetorno.Status);
        System.assertEquals('Antes de enviar o aceite é necessário preencher o "CNPJ/CPF" na conta da oportunidade', objRetorno.Message);
    }

    @isTest
    static void enviarAceiteCompFaturamentoAcimaLimite(){

        Account objConta = TestDataFactory.gerarContaJuridica(true);

        Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(true, objConta);
        
        objOportunidade.StageName = 'Negociação';
        objOportunidade.ComplementoFaturamento__c = '123456789123456789321312654984984654';
        
        update objOportunidade;
        
        Test.startTest();
        OpportunityBotaoAceiteController.RetornoEnviarAceiteTO objRetorno = OpportunityBotaoAceiteController.enviarAceite(objOportunidade.Id);
        Test.stopTest();

        System.assertEquals('warning', objRetorno.Status);
        System.assertEquals('O campo Complemento do endereço de faturamento, não pode ter mais que 19 caracteres. Verifique, por favor.', objRetorno.Message);
    }
}