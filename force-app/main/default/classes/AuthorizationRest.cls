@RestResource(urlMapping='/auth')
global class AuthorizationRest {

    @HttpPost
	global static void doPost() {
		AuthorizationTO.PostReturn objResponse = new AuthorizationTO.PostReturn();
		objResponse.Status = 'success';

		RestContext.response.statusCode = 200;
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponse));
	}
}