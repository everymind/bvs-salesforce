public with sharing class MetadataDAO {
    
    private static final MetadataDAO instance = new MetadataDAO();
	
    public static MetadataDAO getInstance() {
		return instance;
    }
    
    public FilaInicialPosVendas__mdt buscarFilaInicialPosVendas(String regional)
    {
        return [
            SELECT 
                Fila__c,
                SetorRegional__c
            FROM
                FilaInicialPosVendas__mdt
            WHERE
                SetorRegional__c = :regional
        ];
    }

    public List<FilaN2AtendAcionamentoComercial__mdt> buscarFilaN2AtendimentoCanal(String canal)
    {
        System.debug('Canal: ' + canal);
        return [
            SELECT 
                Fila__c
            FROM
                FilaN2AtendAcionamentoComercial__mdt
            WHERE
                Canal__c =  :canal
        ];
    }

    public List<FilaInicialAtendimento__mdt> buscarFilaInicialAtendimento(String departamento){

        return [
            SELECT 
                Fila__c,
                Departamento__c
            FROM
                FilaInicialAtendimento__mdt
            WHERE
                Departamento__c =  :departamento
        ];
    }

    public List<CaseMilestone> buscarCaseMilestone(String caseId)
    {
        return [
            select Id, TargetResponseInMins, TimeRemainingInMins, completionDate, targetDate
                          from CaseMilestone cm
                          where caseId = :caseId
                          and completionDate = null limit 1
        ];
    }

    public Id buscarIdDireito(String nome)
    {
        List<Entitlement> lstDireito= [
            SELECT Id FROM Entitlement where name=:nome
            limit 1
        ];
        if (lstDireito.isEmpty()){
            return null;
        } else {
            return lstDireito[0].id;
        }
    }
    public List<String> retornaOrigemEmailToCase()
    {
        List<String> Origens = new List<String>();
        List<OrigemEmailToCase__mdt> origem = [
            SELECT Id, origem__c 
            FROM OrigemEmailToCase__mdt
            WHERE Origem__c != null
        ];

        for(OrigemEmailToCase__mdt o : origem)
        {
            Origens.add(o.origem__c);
        }

        return origens.size() > 0 ? Origens : null;
    }
    

    public List<JustificavaStatus__c> BuscarJustificavaStatusRejeitado(String caseId)
    {
        return [
            SELECT Id FROM JustificavaStatus__c where Status__c='REJEITADO' and Caso__c=:caseId
        ];
    }

    public Integer BuscarTempoPassado(Id caseId)
    {
        try{
            CaseMilestone caseMilestoneAtual=[select MilestoneTypeId   
            from CaseMilestone 
            where Caseid=:caseId
            and IsCompleted=false
            limit 1][0];

            AggregateResult soma=([
                select sum(ActualElapsedTimeInMins)  soma
                from CaseMilestone 
                where Caseid=:caseId 
                and IsCompleted=true
                and MilestoneTypeId=:caseMilestoneAtual.MilestoneTypeId
                and id!=:caseMilestoneAtual.Id
            ]);
            return((Integer)soma.get('soma'));
        } catch(Exception ex){
            return(0);
        }
    }

    public Integer BuscarTempoSLA(Id caseId)
    {
        try{
            CaseMilestone caseMilestoneAtual=[select MilestoneTypeId   
            from CaseMilestone 
            where Caseid=:caseId
            and IsCompleted=false
            limit 1][0];

            AggregateResult soma=([
                select sum(ActualElapsedTimeInMins)  soma
                from CaseMilestone 
                where Caseid=:caseId 
                and IsCompleted=true
                and MilestoneTypeId=:caseMilestoneAtual.MilestoneTypeId
                and id!=:caseMilestoneAtual.Id
            ]);
            return((Integer)soma.get('soma'));
        } catch(Exception ex){
            return(0);
        }
    }

    public String buscarPID(String motivo, String subMotivo)
    {
        try{
            return [
                SELECT 
                    PID__c
                FROM
                    TabulacaoPID__mdt
                WHERE
                    Motivo__c = :motivo
                    and SubMotivo__c = :subMotivo
                limit 1][0].PID__c;
        } catch(Exception ex){
            return(null);
        }
    }

    @AuraEnabled(Cacheable=true)
    public static String ObterTempoDeBloqueioCasoPID() {

        try{

            String retorno = '0';

            ConfiguracoesGerais__mdt valorConfiguracao = [
                SELECT 
                    ValorDaConfiguracao__c
                FROM
                ConfiguracoesGerais__mdt 
                WHERE
                    DeveloperName  = 'Tempo_min_de_bloqueio_caso_PID'
                    
                limit 1];

            if (valorConfiguracao != null) {
                retorno = valorConfiguracao.ValorDaConfiguracao__c;
                
            }
            return retorno;
        } catch(Exception ex){
            return '0';
        }

    }


    public List<String> retornarFilasIniciaisPosVendas()
    {
        List<String> listFila = new List<String>();

        List<FilaInicialPosVendas__mdt> filas = 
        [
            SELECT 
                Fila__c
            FROM
                FilaInicialPosVendas__mdt
            WHERE Fila__c != null
        ];

        if(filas.size() > 0)
        {
            for(FilaInicialPosVendas__mdt  f : filas)
            {
                listFila.add(f.fila__c);
            }
        }

        return listFila.size() > 0 ? listFila : null;

    }

    public List<String> retornarFilasIniciaisAtendimento()
    {
        List<String> listFila = new List<String>();

        List<FilaInicialAtendimento__mdt> filas = 
        [
            SELECT 
                Fila__c
            FROM
                FilaInicialAtendimento__mdt
            WHERE Fila__c != null
        ];

        if(filas.size() > 0)
        {
            for(FilaInicialAtendimento__mdt  f : filas)
            {
                listFila.add(f.fila__c);
            }
        }

        return listFila.size() > 0 ? listFila : null;

    }
    
    public list<String> retornaUsuariosCarga()
    {
        List<UsuarioDeCarga__mdt> listUsername = [
            SELECT id, Username__c
            FROM  UsuarioDeCarga__mdt
            WHERE Username__c != null    
        ];
        List<String> lstUsernames = new List<String>();
        if(listUsername.size() > 0)
        {
            for(UsuarioDeCarga__mdt usuario : listUsername)
            {
                lstUsernames.add(usuario.Username__c);
            }
        } 
        return lstUsernames;  
    }


}