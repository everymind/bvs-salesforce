@IsTest 
public with sharing class AccountBOTest {

    @isTest
    private static void testarAtualizarDadosTerritorio(){
        Test.startTest();
        Map<Id, Account> mapNewAccount = new Map<Id, Account>();
        List<Account> lstAccount = new List<Account>();
        Account conta0 = TestDataFactory.gerarContaJuridica(true);
        Account conta1 = TestDataFactory.gerarContaJuridica(true);
     
        lstAccount.add(conta0);
        lstAccount.add(conta1);

        Map<Id, Account> mapOldAccount = new Map<Id, Account>();
        List<Account> lstOldAccount = [SELECT Id, Name, CnpjCpf__c, RecordTypeId, Carteira__c FROM Account WHERE Id = :conta0.Id];
        lstOldAccount[0].Carteira__c = 'ACSP00';
        mapOldAccount.put(lstAccount[0].Id, lstOldAccount[0]);

        conta0.Carteira__c = 'DATSET';

        mapNewAccount.put(conta0.Id, conta0);

        System.debug(mapOldAccount);
        System.debug(mapNewAccount);
        
        AccountBO.getInstance().atualizarDadosTerritorio(lstAccount);
        AccountBo.getInstance().atualizarDadosTerritorio(mapNewAccount, mapOldAccount);
        Test.stopTest();
        System.assertEquals(lstAccount.size(), 2);
    }

    @isTest
    private static void testarTratarPermissaoDeCriacaoDeCasosTrue(){
        Test.startTest();
        Account conta0 = TestDataFactory.gerarContaJuridica(true);
        Contact contato0 = TestDataFactory.gerarContato(true, conta0);
        contato0.DataLimiteBloqueio__c = DateTime.valueOf('2022-06-17 14:05:00');
        update contato0;
        AccountBO.tratarPermissaoDeCriacaoDeCasos(contato0.Id, true);
        Test.stopTest();
        System.assertNotEquals(contato0, null);
    }

    @isTest
    private static void testarTratarPermissaoDeCriacaoDeCasosFalse(){
        Test.startTest();
        Account conta0 = TestDataFactory.gerarContaJuridica(true);
        Contact contato0 = TestDataFactory.gerarContato(true, conta0);
        contato0.DataLimiteBloqueio__c = DateTime.valueOf('2022-06-17 14:05:00');
        update contato0;
        AccountBO.tratarPermissaoDeCriacaoDeCasos(contato0.Id, false);
        Test.stopTest();
        System.assertNotEquals(contato0, null);
    }
}