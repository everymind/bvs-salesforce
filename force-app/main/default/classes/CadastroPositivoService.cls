public with sharing class CadastroPositivoService {

    Map<String,String> mapMensagemRetorno = new Map<String,String>();
 

    public CadastroPositivoService() {
        
        mapMensagemRetorno.put('0','Exibir mensagem pedindo para salvar o Incidente para poder prosseguir.');
        mapMensagemRetorno.put('1','Mostrar mensagem que o Consumidor PF já está cadastrado.');
        mapMensagemRetorno.put('2','Mostrar mensagem que o Consumidor PJ já está cadastrado.');
        mapMensagemRetorno.put('3','Exibir os campos PF para realizar o cadastro.');
        mapMensagemRetorno.put('4','Exibir os campos PJ para realizar o cadastro.');
        mapMensagemRetorno.put('5','Mostrar botão para realizar o descadastro do Consumidor PF.');
        mapMensagemRetorno.put('6','Mostrar botão para realizar o descadastro do Consumidor PJ.');
        mapMensagemRetorno.put('7','Exibir mensagem que o Consumidor PF já está descadastrado.');
        mapMensagemRetorno.put('8','Exibir mensagem que o Consumidor PJ já está descadastrado.');
        mapMensagemRetorno.put('9','Exibir mensagem informando que a categoria escolhida é PF porém o cliente é um PJ.');
        mapMensagemRetorno.put('10','Exibir mensagem informando que a categoria escolhida é PJ porém o cliente é um PF.');
        mapMensagemRetorno.put('11','Categorização escolhida não permite visualizar informações.');
        mapMensagemRetorno.put('12','Sistema indisponível');
    }

    

    public class RealizarOptinRequest {

        public String numeroDocumento {get;set;}
        public String tipoCliente  {get;set;}
        public String nome {get;set;}
        public String nomeConfirmado {get;set;}
        public String dataAniversario {get;set;}
        public String numeroRg {get;set;}
        public String orgaoExpedidor {get;set;}
        public String sexo {get;set;}
        public String email {get;set;}
        public String endereco {get;set;}
        public String bairro {get;set;}
        public String cidade {get;set;}
        public String cep {get;set;}
        public String unidadeFederativa {get;set;}
    }

    public CadastroPrositivoResponse realizarOptin(RealizarOptinRequest requestOptin) {

        Map<String,String> parameters = new Map<String,String>();
        CadastroPrositivoResponse result = new CadastroPrositivoResponse();

        try {

            String baseUrl = 'https://api-prod-dadospositivos.bvsnet.com.br/bvs/cadastropositivo/autoconsulta/api-subscricoes/v1/consumidores-desabilitados';

            baseUrl += '/' + requestOptin.numeroDocumento + '?';        
            parameters.put('tipoCliente',requestOptin.tipoCliente);
            parameters.put('nome',requestOptin.nome);
            parameters.put('nomeConfirmado',requestOptin.nomeConfirmado);
            parameters.put('dataAniversario',requestOptin.dataAniversario);
            parameters.put('numeroRg',requestOptin.numeroRg);
            parameters.put('orgaoExpedidor',requestOptin.orgaoExpedidor);
            parameters.put('sexo',requestOptin.sexo);
            parameters.put('email',requestOptin.email);
            parameters.put('endereco',requestOptin.endereco);
            parameters.put('bairro',requestOptin.bairro);
            parameters.put('cidade',requestOptin.cidade);
            parameters.put('cep',requestOptin.cep);
            parameters.put('unidadeFederativa',requestOptin.unidadeFederativa);
            
            for(String paramName : parameters.keySet()) { 
                String paramValue = parameters.get(paramName);
                baseUrl += paramName + '=' + paramValue + '&';
            }
            
            baseUrl.removeEnd('&');
            baseUrl = baseUrl.replace(' ', '+');
            system.debug(baseUrl);
            Http http = new Http();       
            HttpRequest request = new HttpRequest();        
            request.setEndpoint(baseUrl);
            request.setMethod('DELETE');        
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            
                
            HttpResponse response = http.send(request);

            Integer statusCode = response.getStatusCode();

            System.debug('### statusCode: ' + statusCode);
            system.debug(response);
            if (statusCode == 200)
            {
                result.success = true;
                result.message = 'Cadastrado';
                system.debug(result + ' deu certo ');
                return result;     
                                 
            }

            result.success = false;
            result.error = 'Ocorreu um erro na chamada ao serviço de Cadastro Positivo';
            system.debug(result + ' deu errado ');

            return result;
            
        }
        catch (Exception ex) {
            result.success = false;
            result.error = 'Ocorreu um erro na chamada ao serviço de Cadastro Positivo. Erro: ' + ex.getMessage();  
            return result;          
        }
                    
    }

    public CadastroPrositivoResponse realizarOptout(String documentNumber, String personType) {  
        
        CadastroPrositivoResponse result = new CadastroPrositivoResponse();

        try {
            String baseUrl = 'https://api-prod-dadospositivos.bvsnet.com.br/bvs/cadastropositivo/autoconsulta/api-subscricoes/v1/consumidores-desabilitados';
            String bodyJson = '{"numeroDocumento": "' + documentNumber +'"'+ ', "tipoCliente": "' + personType+ '"}';
    
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(baseUrl);
            request.setMethod('POST');        
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            request.setBody(bodyJson);
                
            HttpResponse response = http.send(request);   
            system.debug(request); 
            system.debug(bodyJson); 
            system.debug(response);
            Integer statusCode = response.getStatusCode();

            if (statusCode == 201)
            {     
                result.success = true;
                result.message = 'Cadastrado';
                return result;            
            }           

            result.success = false;
            result.error = 'Ocorreu um erro na chamada ao serviço de Cadastro Positivo';
            return result;
    
            
        }
        catch (Exception ex) {
            result.success = false;
            result.error = 'Ocorreu um erro na chamada ao serviço de Cadastro Positivo. Erro: ' + ex.getMessage();  
            return result;                  
        }
    }

    public CadastroPrositivoResponse obterStatusOptinConsumidor(String numeroDocumento, String tipoPessoa) {      
        String url = 'https://api-prod-dadospositivos.bvsnet.com.br/bvs/cadastropositivo/autoconsulta/api-subscricoes/v1/consumidores-desabilitados/'+ numeroDocumento + '/status?tipoCliente=' + tipoPessoa;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(120000);

        CadastroPrositivoResponse result = new CadastroPrositivoResponse();

        try{
            Http http = new Http();
            HTTPResponse res = http.send(req);
            if (res.getStatusCode() == 200) {
                result.success =  true;
                Map<String, Object> resultsUntyped =  (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
                if (resultsUntyped != null && resultsUntyped.size() > 0) {                    
                    result.statusOptin = (Boolean)resultsUntyped.get('status');
                    return result;
                }
                result.statusOptin = false; 
                system.debug(result);
                return result;   
            }  

            result.error = 'Não foi possível consultar o status do cadastro';
            result.success = false;
            system.debug(result);
            return result;

        } catch(Exception ex) {
            result.error = 'Ocorreu um erro ao consultar o status do contato. Erro: ' +ex.getMessage();
            result.success = false;
            system.debug(result);
            return result;
        }
    }  

    public class CadastroPrositivoResponse {
        public String error;
        public Boolean statusOptin; 
        public Boolean success;
        public String message;
    }
}