public class OpportunityDAO {
    private final static OpportunityDAO instance = new OpportunityDAO();

    private OpportunityDAO() { }

    public static OpportunityDAO getInstance(){
        return instance;
    }

    public Opportunity buscarPorId(Id idOportunidade){
        List<Opportunity> lstOportunidade = buscarPorId(new Set<Id> { idOportunidade });

        if (lstOportunidade.isEmpty()){
            return null;
        } else {
            return lstOportunidade[0];
        }
    }

    public List<Opportunity> buscarPorId(Set<Id> setIdOportunidade){
        return [
            SELECT
                Id,
                AccountId,
				BairroFaturamento__c,
				CarteiraCriacao__c,
                CategoriaRegraNegocio__c,
                CEPFaturamento__c,
                CidadeFaturamento__c,
                CodigoRegraNegocio__c,
                ComplementoFaturamento__c,
                DataEnvioAceite__c,
                EstadoFaturamento__c,
                ModalidadeCobranca__c,
                MotivoGanhoPerda__c,
                NumeroFaturamento__c,
                NumeroPlano__c,
                OwnerId,
                Tipo_de_aceite__c,
                TipoLogradouroFaturamento__c,
                RuaFaturamento__c,
                StageName,
                ValorPlano__c,
                Segmento__c,
                Ticket_M_dio__c,
            	Plataforma__c,
            	Pedidos_m_s__c,
            	Gateway__c,
            	Integra_o__c,
            	In_cio_de_opera_o__c,
            	Antifraude_Atual__c,
            	Tem_piloto__c,
            	TemProduto__c,
                Type,
                Account.Codigo8__c
            FROM
                Opportunity
            WHERE
                Id IN :setIdOportunidade
        ];
    }
}