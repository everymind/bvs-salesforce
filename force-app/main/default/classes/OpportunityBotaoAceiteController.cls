public class OpportunityBotaoAceiteController {
    @AuraEnabled
    public static RetornoEnviarAceiteTO enviarAceite(Id idOportunidade){
        try{
            AceiteEletronicoService.EnviarAceiteRetornoTO objAceiteEletronicoRetornoTO = AceiteEletronicoService.enviarAceite(idOportunidade);

            return new RetornoEnviarAceiteTO('success', objAceiteEletronicoRetornoTO.Mensagem);
        } catch (AceiteEletronicoService.AceiteEletronicoServiceException objErro){
			return new RetornoEnviarAceiteTO('warning', objErro.getMessage());
		} catch (DmlException e) {
			return new RetornoEnviarAceiteTO('warning', e.getDmlMessage(0));
        } catch (Exception objErro){
            System.debug('Erro: ' + objErro.getMessage());
            return new RetornoEnviarAceiteTO('error', 'Ocorreu um erro ao tentar enviar o aceite!');
        }
    }

    public class RetornoEnviarAceiteTO{
        @AuraEnabled
        public String Status { get; set; }

        @AuraEnabled
        public String Message { get; set; }

        public RetornoEnviarAceiteTO(String strStatus, String strMessage){
            Status = strStatus;
            Message = strMessage;
        }
    }
}