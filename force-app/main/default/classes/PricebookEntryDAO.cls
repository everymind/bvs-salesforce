public class PricebookEntryDAO {
    private final static PricebookEntryDAO instance = new PricebookEntryDAO();

    private PricebookEntryDAO() { }

    public static PricebookEntryDAO getInstance(){
        return instance;
    }

    public List<PricebookEntry> buscarPorCatalogo(Id idCatalogo){
        return [
            SELECT
                Id
            FROM
                PricebookEntry
            WHERE
                Pricebook2Id = :idCatalogo
        ];
    }
}