@IsTest
private class contadorCasosControllerTest {
    @testSetup static void setup() {
        Account account = new Account(
            Name = 'Conta de teste BVS',
            CnpjCpf__c = '27777267000146',
            RecordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId()
        );
        insert account;

        /*Account accountPosVenda = new Account(
            Name = 'Conta de teste BVS Pós Venda',
            CnpjCpf__c = '11111111111111',
            RecordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId()
        );
        insert accountPosVenda;*/

        Contact testeContato = new Contact(
            FirstName = 'Contato',
            LastName = 'Contato',
            CPF__c = '36909319087',
            Phone = '11912345678',
            AccountId = account.id
        );
        insert testeContato;
        
        SlaProcess sla=[SELECT Description,Id,Name FROM SlaProcess where name='HORA08A17' order by id desc limit 1];

        Entitlement direito = new Entitlement(Name = 'HORA08A17', AccountId = account.Id, SlaProcessId = sla.id);
        insert direito;
        Entitlement direitoPRIME = new Entitlement(Name = 'PRIME', AccountId = account.Id, SlaProcessId = sla.id);
        insert direitoPRIME;

        SlaProcess slaUtil=[SELECT Description,Id,Name FROM SlaProcess where name='HORACORRIDA' order by id desc limit 1];

        Entitlement direitoUtil = new Entitlement(Name = 'HORACORRIDA', AccountId = account.Id, SlaProcessId = sla.id);
        insert direitoUtil;

        
        TabulacaoSLA__c tabulacaoACESSOS = new TabulacaoSLA__c();
        tabulacaoACESSOS.TipoRegistro__c = 'CSMAtendimento';
        tabulacaoACESSOS.MotivoTxt__c = 'ACESSOS';
        tabulacaoACESSOS.SubMotivoTxt__c = 'AED PARAMETRIZAÇÃO';
        tabulacaoACESSOS.FilaAtual__c = 'TITELECOMUNICACOESREDES';
        tabulacaoACESSOS.ProximaFILA__c = 'RETORNO';
        tabulacaoACESSOS.TempoSLA__c = 120;
        insert tabulacaoACESSOS;

        TabulacaoSLA__c tabulacaoSUPORTE = new TabulacaoSLA__c();
        tabulacaoSUPORTE.TipoRegistro__c = 'CSMAtendimento';
        tabulacaoSUPORTE.MotivoTxt__c = 'SUPORTE AO USUÁRIO';
        tabulacaoSUPORTE.SubMotivoTxt__c = 'DESBLOQUEAR / RESET->SENHA';
        tabulacaoSUPORTE.TempoSLA__c = 120;
        tabulacaoSUPORTE.FilaAtual__c = 'TITELECOMUNICACOESREDES';
        tabulacaoSUPORTE.ProximaFILA__c = 'RETORNO';
        insert tabulacaoSUPORTE;

        TabulacaoSLA__c tabulacaoRETORNO = new TabulacaoSLA__c();
        tabulacaoRETORNO.TipoRegistro__c = 'CSMAtendimento';
        tabulacaoRETORNO.MotivoTxt__c = 'SUPORTE AO USUÁRIO';
        tabulacaoRETORNO.SubMotivoTxt__c = 'DESBLOQUEAR / RESET->SENHA';
        tabulacaoRETORNO.TempoSLA__c = 120;
        tabulacaoRETORNO.FilaAtual__c = 'RETORNO';
        insert tabulacaoRETORNO;
    }
    @IsTest
	private static void testarInsercaoCasos() {
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
        caso.MotivoTxt__c = 'Teste';
        caso.SubMotivoTxt__c = 'Teste';
        update caso;
        List<Case> lstCasosCliente = contadorCasosController.buscarCasosPorCliente(conta.Id);
        Map<String, List<Case>> mapMotivoSubMotivo = contadorCasosController.montarListaMotivoSubMotivo(lstCasosCliente);
        }
      

    }
    
}