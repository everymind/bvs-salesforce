public class OpportunityAcompanhamentoAceite {
    @AuraEnabled
    public static BuscarCnpjCpfContaRetornoTO buscarCnpjCpfConta(Id idOportunidade){
        try{
            Opportunity objOportunidade = OpportunityDAO.getInstance().buscarPorId(idOportunidade);
            Account objConta = AccountDAO.getInstance().buscarPorId(objOportunidade.AccountId);

            return new BuscarCnpjCpfContaRetornoTO('success', null, objConta.CnpjCpf__c);
        } catch (Exception objErro){
            return new BuscarCnpjCpfContaRetornoTO('error', 'Não foi possível buscar informações para exibir o acompanhamento do aceite!', null);
        }
    }

    public class BuscarCnpjCpfContaRetornoTO {
        @AuraEnabled
        public String Status { get; set; }

        @AuraEnabled
        public String Mensagem { get; set; }

        @AuraEnabled
        public String CnpjCpf { get; set; }

        public BuscarCnpjCpfContaRetornoTO(String strStatus, String strMensagem, String strCnpjCpf){
            Status = strStatus;
            Mensagem = strMensagem;
            CnpjCpf = strCnpjCpf;
        }
    }
}