public class CadastrarContaService {
    
    public static String buscarContaPessoaFisica(String cpf){
        ServiceSettings__mdt servicoPF = [SELECT MasterLabel, DeveloperName, Url__c FROM ServiceSettings__mdt WHERE MasterLabel = 'PHPCorp - Busca PF'];
        System.debug('Url:'+servicoPF);
        System.debug('CPF:'+cpf);
        String url = servicoPF.Url__c + cpf;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(120000);

        try{
            Http http = new Http();
            HTTPResponse res = http.send(req);
            if (res.getStatusCode() == 200){
                system.debug(res.getBody());
                return res.getBody();
            } else {
                return(res.getBody());
            }
        } catch(Exception ex){
            System.debug('Falha ao obter dados do CPF:'+ex.getMessage());
            throw ex;
        }
    }
    
    public static String buscarContaPessoaJuridica(String cnpj){
        ServiceSettings__mdt servicoPJ = [SELECT MasterLabel, DeveloperName, Url__c FROM ServiceSettings__mdt WHERE MasterLabel = 'PHPCorp - Busca PJ'];

        String url = servicoPJ.Url__c + cnpj + '&SID=00000';
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(120000);
        
        Http http = new Http();
        HTTPResponse res = http.send(req);

        return res.getBody();
    }

    @InvocableMethod (label='Atualiza Contato')
    public static List<Boolean> buscarConsultaContato(List<String> cpf)
    {
        ServiceSettings__mdt servicoContato = [SELECT MasterLabel, DeveloperName, Url__c FROM ServiceSettings__mdt WHERE MasterLabel = 'PhpCorp-ConsultaContato'];
        string url = servicoContato.Url__c + cpf[0];
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(120000);

        Http http = new Http();
        HTTPResponse res = http.send(req);
        if (res.getStatusCode() == 200)
        {
            system.debug(res.getBody());
            ConsultaContato c = (ConsultaContato)JSON.deserialize(res.getBody(), ConsultaContato.class);
            List<Contact> contatos = [
                SELECT Name, CPF__c, BirthDate,NomeMae__c,Email,Phone,HomePhone,MobilePhone
                FROM Contact
                WHERE CPF__c =: c.cpf
            ];

            if(contatos.size() > 0)
            {
                for(Contact contato : contatos)
                {
                    Integer dia = Integer.valueOf(c.data_nascimento.substring(0,2));
                    Integer mes = Integer.valueOf(c.data_nascimento.substring(3,5));
                    Integer ano = Integer.valueOf(c.data_nascimento.substring(6));
                    contato.Birthdate = Date.newInstance(ano, mes, dia);
                    contato.nomeMae__c = c.nome_mae;
                    if(c.email != null && c.email != ''){
                        contato.email = c.email;
                    }
                    if(c.tel_cel != null && c.tel_cel != ''){
                        contato.phone = c.tel_cel;
                        contato.mobilePhone = c.tel_cel;
                    }
                    
                    contato.homePhone = c.tel_res;
                }

                update contatos;
                List<Boolean> bool = new List<Boolean>();
                bool.add(true);
                return bool;
            }
        }
        else{
            return null;
        }
        return null;
        
    }
    public class ConsultaContato{
        public String cpf;	
        public String nome;	
        public String data_nascimento;	
        public String nome_mae;	
        public String sexo;	//M
        public String email;	//
        public String tel_cel;	//
        public String tel_com;	//
        public String tel_res;	//
        public String cod_retorno;	//00
        public String msg_retorno;	//TRANSACAO EFETUADA COM SUCESSO.
    }
}