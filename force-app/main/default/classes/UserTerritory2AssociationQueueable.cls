public class UserTerritory2AssociationQueueable implements Queueable, Database.AllowsCallouts {
    
    List<ApplicationLog__c> lstApplcationLogInsert = new List<ApplicationLog__c>();
    private List<UserTerritory2Association> lstUserTerritory2AssociationNovas;

    public UserTerritory2AssociationQueueable(List<UserTerritory2Association> objLstUserTerritory2AssociationNovas) {
        this.lstUserTerritory2AssociationNovas = objLstUserTerritory2AssociationNovas;
    }

    public void execute(QueueableContext context) {
        List<Account> lstContasAlteradas = new List<Account>();
		Set<String> setCarteiras = new Set<String>();
		Set<Id> setTerritory2Id = new Set<Id>();

        for (UserTerritory2Association objUserTerritory2Association : lstUserTerritory2AssociationNovas){

			System.debug('objUserTerritory2Association.Territory2Id');
			System.debug(objUserTerritory2Association.Territory2Id);
			setTerritory2Id.add(objUserTerritory2Association.Territory2Id);
		}

		if (setTerritory2Id.size() > 0){
			List<Territory2> lstTerritory2 = [SELECT Id, DeveloperName FROM Territory2 WHERE Id IN :setTerritory2Id];
			if (lstTerritory2.size() > 0){
				for (Territory2 objTerritory2 : lstTerritory2){
					System.debug('objTerritory2.DeveloperName');
					System.debug(objTerritory2.DeveloperName);
					if (objTerritory2.DeveloperName != ''){
						setCarteiras.add(objTerritory2.DeveloperName);
					}
				}
			}
		}
		
		UserTerritory2AssociationBatch.init(setCarteiras);

		/*if (setCarteiras.size() > 0){
			List<Account> lstContas = AccountDAO.getInstance().buscarPorCarteira(setCarteiras);
			System.debug(JSON.serialize(lstContas));

			if (lstContas != null){
				if (lstContas.size() > 0){
					for (Account objConta : lstContas){
						lstContasAlteradas.add(
							new Account(Id = objConta.Id,ProcessarReencarteiramento__c = true)
						);
					}
				}
			}

			if (!lstContasAlteradas.isEmpty()){
                lstApplcationLogInsert.addAll(this.processarResultado(Database.update(lstContasAlteradas, false), 'Account', lstContasAlteradas));
            }
            
            if (!lstApplcationLogInsert.isEmpty()){
                insert lstApplcationLogInsert;
            }
		} */
    }

    /**
	 * @Autor: Everymind
	 * @Data: 24/08/2020
	 * @Descrição: Método que captura os erros nos updates de Oportunidade e Contato e grava no objeto ApplicationLog__c.
	 
	public List<ApplicationLog__c> processarResultado(List<Database.SaveResult> resultados, String origem, List<SObject> lstRegistros){
		List<ApplicationLog__c> lstApplicationLog = new List<ApplicationLog__c>();

        for (Integer i = 0; i < resultados.size(); i++) {
			Database.SaveResult databaseSaveResult = resultados[i];
			SObject registro = lstRegistros[i];

            if (!databaseSaveResult.isSuccess()) {

				for (Database.Error error : databaseSaveResult.getErrors()) {
					String IdRegistroErro = registro.Id;
					// Cria um registro de log em caso de erro
					ApplicationLog__c objLog = new ApplicationLog__c(
						ErrorMessage__c = origem + ' - ID [' + IdRegistroErro + '] - '  + error.getMessage(),
						IdTarefaRightNow__c = '',
						IdIncidenteRightNow__c = '',
						Processo__c = 'Regra de atribuição de territórios'
					);

					lstApplicationLog.add(objLog);
				}
            }
        }

        return lstApplicationLog;
	} */
}