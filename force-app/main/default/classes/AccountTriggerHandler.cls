/**
 * @Autor: Everymind
 * @Data: 21/08/2020
 * @Descrição: Classe que orquestra os eventos da trigger para os métodos de negócio.
 */
public class AccountTriggerHandler extends TriggerHandler {

	/**
	 * @Autor: Everymind
	 * @Data: 21/08/2020
	 * @Descrição: Constante que contém a instância da classe de negócio do objeto Account.
	 */
	private AccountBO objBO = AccountBO.getInstance();

	public override void afterInsert(){
		objBO.atualizarDadosTerritorio(Trigger.new);
    }
        
    public override void afterUpdate(){
		objBO.atualizarDadosTerritorio((Map<Id, Account>)Trigger.newMap, (Map<Id, Account>)Trigger.oldMap);
    }
}