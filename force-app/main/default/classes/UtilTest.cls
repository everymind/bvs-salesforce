@isTest
public with sharing class UtilTest 
{
    @isTest
    public static void getRecordTypeIdTest() 
    {
        test.startTest();
       Id teste =  Util.getRecordTypeId('Case', 'CSMAtendimento');
        Util.getRecordTypeDeveloperName(teste);

        Util.getValueOfObject(new Case(), 'Motivotxt__c');
        Util.replaceCharacterSpecial('áó');
        Util.formatterDatePtBr(Date.newInstance(23,03,2003));


        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('PostKbaAnswers');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        
        Test.setMock(HttpCalloutMock.class, mock);
        Util.executarHttpRequest('teste.com', 'teste', new Map<String,String>(), 'teste', 1000); 
        test.stopTest();
    }
}