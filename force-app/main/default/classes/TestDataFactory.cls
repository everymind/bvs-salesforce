@isTest 
public class TestDataFactory {

    public static User gerarUsuario(Boolean BoolDml, String strPerfil) {
        Profile objPerfil =[SELECT Id, Name FROM Profile WHERE Name =: strPerfil OR Name ='System Administrator' LIMIT 1 ];
        Long currentTimeMillis = System.currentTimeMillis();
        String emailUsuario = 'email+' + currentTimeMillis + '@bestminds.com';
        User usuarioComPapel = new User(
          FirstName = 'Nome',
          LastName = 'Sobrenome',
          Email = emailUsuario,
          Username = emailUsuario + '.' + currentTimeMillis,
          CompanyName = 'Everymind',
          Title = 'Desenvolvedor',
          Alias = 'nomsob',
          TimeZoneSidKey = 'America/Sao_Paulo',
          EmailEncodingKey = 'UTF-8',
          LanguageLocaleKey = 'pt_BR',
          LocaleSidKey = 'pt_BR',
          ProfileId = objPerfil.Id
     
        );
        if (BoolDml)
          insert usuarioComPapel;
        return usuarioComPapel;
      }

    public static Account gerarContaJuridica(Boolean boolDml){
        Account objConta = new Account(
            Name = 'Conta Pessoa Jurídica',
            CnpjCpf__c = gerarCNPJ(false),
            RecordTypeId = Util.getRecordTypeId('Account', 'PessoaJuridica')
        );

        if (boolDml){
            insert objConta;
        }

        return objConta;
    }

    public static Contact gerarContato(Boolean boolDml, Account objConta){
        Contact objContato = new Contact(
            AccountId = objConta.Id,
            FirstName = 'Contato',
            LastName = 'Contato',
            Email = 'teste@teste.com',
            Phone = '11912345678',
            Departamento__c = 'BI',
            CPF__c = gerarCPF(false),
            Birthdate = System.now().date()
        );

        if (boolDml){
            insert objContato;
        }

        return objContato;
    }

    public static Opportunity gerarOportunidadeVendaPlano(Boolean boolDml, Account objConta){
        Opportunity objOportunidade = new Opportunity(
            Name = 'Oportunidade de venda de plano',
            StageName = 'Prospecção',
            AccountId = objConta.Id,
            CloseDate = System.now().date(),
            DataInicioFaturamento__c = System.now().date(),
            TipoReceita__c = 'Recorrente',
            Tipo_de_aceite__c = 'Eletrônico',
            ModalidadeCobranca__c = 'E0-SOMENTE PARA USO INTERNO',
            ValorPlano__c = 50,
            CodigoRegraNegocio__c = 'TESTE',
            TipoLogradouroFaturamento__c = 'Rua',
            RuaFaturamento__c = 'Logradouro',
            NumeroFaturamento__c = '10',
            BairroFaturamento__c = 'Bairro',
            CidadeFaturamento__c = 'Cidade',
            EstadoFaturamento__c = 'Estado',
            CEPFaturamento__c = '99999999',
            RecordTypeId = Util.getRecordTypeId('Opportunity', 'VendaPlano')
        );

        if (boolDml){
            insert objOportunidade;
        }

        return objOportunidade;
    }

    public static OpportunityContactRole gerarContatoOportunidade(Boolean boolDml, Opportunity objOportunidade, Contact objContato, String strRole){
        OpportunityContactRole objContatoOportunidade = new OpportunityContactRole(
            OpportunityId = objOportunidade.Id,
            ContactId = objContato.Id,
            Role = strRole
        );

        if (boolDml){
            insert objContatoOportunidade;
        }

        return objContatoOportunidade;
    }

    /**
	 * Método utilitário para retornar um número aleatório
	 *
	 */
	public static Integer gerarNumeroAleatorio(Integer min, Integer max) {
		return (Math.floor((Math.random() * max) + min)).intValue();
	}

    /**
	 * Método utilitário para retornar um CNPJ aleatório
	 *
	 */
	public static String gerarCNPJ(Boolean comPontuacao) {
		Long n1 = gerarNumeroAleatorio(0, 9);
		Long n2 = gerarNumeroAleatorio(0, 9);
		Long n3 = gerarNumeroAleatorio(0, 9);
		Long n4 = gerarNumeroAleatorio(0, 9);
		Long n5 = gerarNumeroAleatorio(0, 9);
		Long n6 = gerarNumeroAleatorio(0, 9);
		Long n7 = gerarNumeroAleatorio(0, 9);
		Long n8 = gerarNumeroAleatorio(0, 9);
		Long n9 = 0;
		Long n10 = 0;
		Long n11 = 0;
		Long n12 = 1;

		Long d1 = n12 * 2 + n11 * 3 + n10 * 4 + n9 * 5 + n8 * 6 + n7 * 7 + n6 * 8 + n5 * 9 + n4 * 2 + n3 * 3 + n2 * 4 + n1 * 5;
		d1 = 11 - (Math.mod(d1, Long.valueOf('11')));
		if (d1 >= 10) {
			d1 = 0;
		}

		Long d2 = d1 * 2 + n12 * 3 + n11 * 4 + n10 * 5 + n9 * 6 + n8 * 7 + n7 * 8 + n6 * 9 + n5 * 2 + n4 * 3 + n3 * 4 + n2 * 5 + n1 * 6;
		d2 = 11 - (Math.mod(d2, Long.valueOf('11')));
		if (d2 >= 10) {
			d2 = 0;
		}

		String cnpj;
		if (comPontuacao) {
			cnpj = '' + n1 + n2 + '.' + n3 + n4 + n5 + '.' + n6 + n7 + n8 + '/' + n9 + n10 + n11 + n12 + '-' + d1 + d2;
		} else {
			cnpj = '' + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10 + n11 + n12 + d1 + d2;
		}

		return cnpj;
	}

	/**
	 * Método utilitário para retornar um CPF aleatório
	 *
	 */
	public static String gerarCPF(Boolean comPontuacao) {
		Long n1 = gerarNumeroAleatorio(0, 9);
		Long n2 = gerarNumeroAleatorio(0, 9);
		Long n3 = gerarNumeroAleatorio(0, 9);
		Long n4 = gerarNumeroAleatorio(0, 9);
		Long n5 = gerarNumeroAleatorio(0, 9);
		Long n6 = gerarNumeroAleatorio(0, 9);
		Long n7 = gerarNumeroAleatorio(0, 9);
		Long n8 = gerarNumeroAleatorio(0, 9);
		Long n9 = gerarNumeroAleatorio(0, 9);

		Long d1 = n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;
		d1 = 11 - (Math.mod(d1, Long.valueOf('11')));
		if (d1 >= 10) {
			d1 = 0;
		}

		Long d2 = d1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;
		d2 = 11 - (Math.mod(d2, Long.valueOf('11')));
		if (d2 >= 10) {
			d2 = 0;
		}

		String cpf;
		if (comPontuacao) {
			cpf = '' + n1 + n2 + n3 + '.' + n4 + n5 + n6 + '.' + n7 + n8 + n9 + '-' + d1 + d2;
		} else {
			cpf = '' + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + d1 + d2;
		}

		return cpf;
	}

    
    public static Case gerarCasoAtendimento(Boolean boolDml, Account objConta, Contact objContato){

        Id rtCSM = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMAtendimento').getRecordTypeId();
        Case objCaso = new Case(
            RecordTypeId = rtCSM,
            AccountId = objConta.Id,
            ContactId = objContato.Id,
            Origin = 'ATENDIMENTO',
            Description = 'TESTE',
            Priority = 'MÉDIA'
        );

        if (boolDml){
            insert objCaso;
        }

        return objCaso;
    }

    public static Case gerarCasoPosVenda(Boolean boolDml, Account objConta, Contact objContato){

        Id rtCSM = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMPosVendas').getRecordTypeId();
        Case objCaso = new Case(
            RecordTypeId = rtCSM,
            AccountId = objConta.Id,
            ContactId = objContato.Id,
            Origin = 'ATENDIMENTO',
            Description = 'TESTE',
            Priority = 'MÉDIA'
        );

        if (boolDml){
            insert objCaso;
        }

        return objCaso;
    }

    public static TabulacaoSLA__c gerarTabulacao(Boolean BoolDML){
        
        TabulacaoSLA__c objTabulacao = new TabulacaoSLA__c(
            ProximaFila__c = 'Teste',
            MotivoTxt__c = 'Teste',
            SubMotivoTxt__c = 'Teste',
            TempoSLA__c = 100,
            TipoRegistro__c = 'CSMAtendimento',
            StatusTabulacao__c = 'ATIVO'
            );

        if(boolDml){
            insert objTabulacao;
        }

        return objTabulacao;
    }

    
}