@isTest
public class ContactScheduleTest {
 
@isTest static void opportunityCalcelTest_Schedule() {

    Map<String, String> mapConfiguracoes = buscarConfiguracoes();

    String bucketAccount = mapConfiguracoes.get('BucketAccount');

    Contact objContact = new Contact(
        FirstName = 'Teste Inativo',
        LastName = 'Teste ContactChangeOwnerTest',
        Email = 'teste@teste.com',
        Updated__c = true,
        AccountId = bucketAccount,
        Status__c = 'Inativo',
        OwnerId = '0053h000003slAZAAY'
    );

    insert objContact;
 
    // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
    String CRON_EXP = '0 6 * * * ?';
 
    Test.startTest();
    
    String jobId = System.schedule('Troca proprietario contatos inativos', CRON_EXP, new ContactSchedule());
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
    System.assertEquals(CRON_EXP, ct.CronExpression);
    System.assertEquals(0, ct.TimesTriggered);
    
    Test.stopTest();
    }

    private static Map<String, String> buscarConfiguracoes() {
		Map<String, String> mapConfiguracoes = new Map<String, String>();

		CollectionUtil.carregarMap(mapConfiguracoes, 'DeveloperName', 'Valor__c', [SELECT Id, DeveloperName, MasterLabel, Valor__c FROM Contact_Metadata__mdt]);

		return mapConfiguracoes;
	}
}