public class AceiteEletronicoService {
    private static String gerarToken(Map<String, String> mapConfiguracoes) {
        String username = mapConfiguracoes.get('usuario');
        String password = mapConfiguracoes.get('senha');

        return 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(username + ':' + password));
    }
    
    public static EnviarAceiteRetornoTO enviarAceite(Id idOportunidade){
        Opportunity objOportunidade = OpportunityDAO.getInstance().buscarPorId(idOportunidade);
        List<ApplicationLog__c> lstSObjApplicationLog = new List<ApplicationLog__c>();

      
        if (objOportunidade.StageName != 'Negociação'){
            throw new AceiteEletronicoServiceException('Para enviar o aceite, o estágio de venda deve estar como "Negociação"');
        }

        if (String.isBlank(objOportunidade.Tipo_de_aceite__c)){
            throw new AceiteEletronicoServiceException('Antes de enviar o aceite é necessário preencher o campo "Tipo de aceite" na oportunidade');
        }

        if (objOportunidade.ValorPlano__c == null){
            throw new AceiteEletronicoServiceException('Antes de enviar o aceite é necessário preencher o campo "Valor do plano" na oportunidade');
        }
        
        if (objOportunidade.Tipo_de_aceite__c == 'Presencial'){
            throw new AceiteEletronicoServiceException('Essa funcionalidade não esta disponível para o tipo de aceite "Presencial"');
        }

        if (String.isBlank(objOportunidade.ModalidadeCobranca__c)){
            throw new AceiteEletronicoServiceException('Antes de enviar o aceite é necessário preencher o campo "Modalidade de cobrança" na oportunidade');
        }

        if (String.isBlank(objOportunidade.CodigoRegraNegocio__c)){
            throw new AceiteEletronicoServiceException('Antes de enviar o aceite é necessário preencher o campo "Código regra de negócio" na oportunidade');
        }

        if (
            String.isBlank(objOportunidade.TipoLogradouroFaturamento__c) ||
            String.isBlank(objOportunidade.RuaFaturamento__c) ||
            String.isBlank(objOportunidade.NumeroFaturamento__c) ||
            String.isBlank(objOportunidade.BairroFaturamento__c) ||
            String.isBlank(objOportunidade.CidadeFaturamento__c) ||
            String.isBlank(objOportunidade.EstadoFaturamento__c) ||
            String.isBlank(objOportunidade.CEPFaturamento__c)
        ) {
            throw new AceiteEletronicoServiceException('Antes de enviar o aceite é necessário preencher o "Endereço de faturamento" na oportunidade');
        }

        if(objOportunidade.ComplementoFaturamento__c != null){

            Integer complementoFaturamentoLength = 0;
            complementoFaturamentoLength += objOportunidade.ComplementoFaturamento__c.length();

            if(complementoFaturamentoLength > 19){
                throw new AceiteEletronicoServiceException('O campo Complemento do endereço de faturamento, não pode ter mais que 19 caracteres. Verifique, por favor.');
            }
        }

        Account objConta = AccountDAO.getInstance().buscarPorId(objOportunidade.AccountId);

        if (objConta.IsPersonAccount){
            throw new AceiteEletronicoServiceException('Essa funcionalidade esta disponível somente para contas PJ');
        }

        if (String.isBlank(objConta.CnpjCpf__c)){
            throw new AceiteEletronicoServiceException('Antes de enviar o aceite é necessário preencher o "CNPJ/CPF" na conta da oportunidade');
        }

        List<OpportunityContactRole> lstPapelContatoOportunidade = OpportunityContactRoleDAO.getInstance().buscarPorOportunidade(objOportunidade.Id);

        if (!CollectionUtil.contemNaLista(lstPapelContatoOportunidade, 'Cobrança', 'Role')){
            throw new AceiteEletronicoServiceException('Para enviar o aceite é necessário um contato na oportunidade com o papel "Cobrança"');
        }

        Contact objContatoCobranca = ContactDAO.getInstance().buscarPorId(((OpportunityContactRole)CollectionUtil.buscarNaLista(lstPapelContatoOportunidade, 'Cobrança', 'Role')).ContactId);
        
        if (String.isBlank(objContatoCobranca.CPF__c)){
            throw new AceiteEletronicoServiceException('O CPF do contato de cobrança é obrigatório');
        }

        if (objContatoCobranca.Birthdate == null){
            throw new AceiteEletronicoServiceException('A data de nascimento do contato de cobrança é obrigatório');
        }

        User objUsuario = UserDAO.getInstance().buscarPorId(system.UserInfo.getUserId());

        if (objOportunidade.Tipo_de_aceite__c == 'Verbal' && !FeatureManagement.checkPermission('EnviarAceiteVerbal') && objUsuario.Profile.Name == 'Comercial Televendas'){
            update new Opportunity(
                Id = objOportunidade.Id,
                AnaliseAntifraude__c = true,
                OportunidadeCongelada__c = true
            );
            throw new AceiteEletronicoServiceException('A oportunidade foi enviada para analise antifraude.');
        }

        if ((objOportunidade.Tipo_de_aceite__c == 'Eletrônico') && !FeatureManagement.checkPermission('EnviarAceiteEletronico')){
            throw new AceiteEletronicoServiceException('Você não tem permissão para enviar o aceite do tipo eletrônico');
        }

        if ((objOportunidade.Tipo_de_aceite__c == 'Verbal') && !FeatureManagement.checkPermission('EnviarAceiteVerbal')){
            throw new AceiteEletronicoServiceException('Você não tem permissão para enviar o aceite do tipo verbal');
        }

        Contact objContatoOperador = objContatoCobranca;

        if (CollectionUtil.contemNaLista(lstPapelContatoOportunidade, 'Operador', 'Role')){
            objContatoOperador = ContactDAO.getInstance().buscarPorId(((OpportunityContactRole)CollectionUtil.buscarNaLista(lstPapelContatoOportunidade, 'Operador', 'Role')).ContactId);
        }

        AceiteEletronicoTO.Contato objContatoCobrancaTO = new AceiteEletronicoTO.Contato();
        objContatoCobrancaTO.nome = objContatoCobranca.FirstName;
        objContatoCobrancaTO.sobrenome = objContatoCobranca.LastName;
        objContatoCobrancaTO.cpf = objContatoCobranca.CPF__c;
        objContatoCobrancaTO.email = objContatoCobranca.Email;
        objContatoCobrancaTO.dataNascimento = (objContatoCobranca.Birthdate == null ? '' : Util.formatterDatePtBr(objContatoCobranca.Birthdate));
        objContatoCobrancaTO.telefone = objContatoCobranca.Phone;
        objContatoCobrancaTO.ddd = objContatoCobranca.Phone != null ? objContatoCobranca.Phone.substring(3, 5) : null;
        objContatoCobrancaTO.cargo = objContatoCobranca.Cargo__c;

        AceiteEletronicoTO.Endereco objEnderecoTO = new AceiteEletronicoTO.Endereco();
        objEnderecoTO.tipo = 'R';
        objEnderecoTO.logradouro = objConta.ShippingStreet;
        objEnderecoTO.numero = objConta.NumeroFaturamento__c;
        objEnderecoTO.complemento = (String.isBlank(objConta.ComplementoFaturamento__c) ? '' : Util.replaceCharacterSpecial(objConta.ComplementoFaturamento__c).left(40));
        objEnderecoTO.bairro = (String.isBlank(objConta.BairroFaturamento__c) ? '' : objConta.BairroFaturamento__c.left(20));
        objEnderecoTO.cidade = objConta.ShippingCity;
        objEnderecoTO.uf = objConta.ShippingState;
        objEnderecoTO.cep = objConta.ShippingPostalCode;

        AceiteEletronicoTO.Contato objContatoTO = new AceiteEletronicoTO.Contato();
        objContatoTO.nome = objContatoCobranca.FirstName;
        objContatoTO.sobrenome = objContatoCobranca.LastName;
        objContatoTO.cpf = objContatoCobranca.CPF__c;
        objContatoTO.email = objContatoCobranca.Email;
        objContatoTO.dataNascimento = (objContatoCobranca.Birthdate == null ? '' : Util.formatterDatePtBr(objContatoCobranca.Birthdate));
        objContatoTO.telefone = objContatoCobranca.Phone;
        objContatoTO.ddd = objContatoCobranca.Phone != null ? objContatoCobranca.Phone.substring(3, 5) : null;
        objContatoTO.cargo = objContatoCobranca.Cargo__c;

        AceiteEletronicoTO.Endereco objEnderecoOportunidadeTO = new AceiteEletronicoTO.Endereco();
        objEnderecoOportunidadeTO.tipo = TipoLogradouroDAO.getInstance().buscarPorNome(objOportunidade.TipoLogradouroFaturamento__c).Valor__c;
        objEnderecoOportunidadeTO.logradouro = objOportunidade.RuaFaturamento__c;
        objEnderecoOportunidadeTO.numero = objOportunidade.NumeroFaturamento__c;
        objEnderecoOportunidadeTO.complemento = (String.isBlank(objOportunidade.ComplementoFaturamento__c) ? '' : Util.replaceCharacterSpecial(objOportunidade.ComplementoFaturamento__c).left(40));
        objEnderecoOportunidadeTO.bairro = (String.isBlank(objOportunidade.BairroFaturamento__c) ? '' : objOportunidade.BairroFaturamento__c.left(20));
        objEnderecoOportunidadeTO.cidade = objOportunidade.CidadeFaturamento__c;
        objEnderecoOportunidadeTO.uf = objOportunidade.EstadoFaturamento__c;
        objEnderecoOportunidadeTO.cep = objOportunidade.CEPFaturamento__c;

        AceiteEletronicoTO.Oportunidade objOportunidadeTO = new AceiteEletronicoTO.Oportunidade();
        objOportunidadeTO.categoria = objOportunidade.CategoriaRegraNegocio__c;
        objOportunidadeTO.fantasia = objConta.NomeFantasia__c;
        objOportunidadeTO.formaPagamento = objOportunidade.ModalidadeCobranca__c;
        objOportunidadeTO.regraNegocios = objOportunidade.CodigoRegraNegocio__c;
        objOportunidadeTO.valor = objOportunidade.ValorPlano__c;
        objOportunidadeTO.endereco = objEnderecoOportunidadeTO;
        
        Contact objContatoFavorito = ContactDAO.getInstance().buscarContatoFavorito(objConta.Id);

        AceiteEletronicoTO.Endereco objEnderecoComercialTO = null;
        if(objContatoFavorito != null) {
            objEnderecoComercialTO = new AceiteEletronicoTO.Endereco();
            objEnderecoComercialTO.tipo = 'R';
            objEnderecoComercialTO.logradouro = objContatoFavorito.MailingStreet;
            objEnderecoComercialTO.numero = objContatoFavorito.NumeroComercial__c;
            objEnderecoComercialTO.complemento = (String.isBlank(objContatoFavorito.ComplementoComercial__c) ? '' : Util.replaceCharacterSpecial(objContatoFavorito.ComplementoComercial__c).left(40));
            objEnderecoComercialTO.bairro = (String.isBlank(objContatoFavorito.BairroComercial__c) ? '' : objContatoFavorito.BairroComercial__c.left(20));
            objEnderecoComercialTO.cidade = objContatoFavorito.MailingCity;
            objEnderecoComercialTO.uf = objContatoFavorito.MailingState;
            objEnderecoComercialTO.cep = objContatoFavorito.MailingPostalCode;
        }

        AceiteEletronicoTO.Operador objOperadorTO = new AceiteEletronicoTO.Operador();
        objOperadorTO.nome = objContatoOperador.Name;
        objOperadorTO.email = objContatoOperador.Email;
        objOperadorTO.cpf = objContatoOperador.CPF__c;
        objOperadorTO.dataNascimento = (objContatoOperador.Birthdate == null ? '' : Util.formatterDatePtBr(objContatoOperador.Birthdate));

        AceiteEletronicoTO.AceiteEletronico objAceiteEletronicoTO = new AceiteEletronicoTO.AceiteEletronico();

        if (objOportunidade.Tipo_de_aceite__c == 'Verbal'){
            objAceiteEletronicoTO.aceite = 'N';
            objAceiteEletronicoTO.canal = 'TELEVENDAS';
        } else if (objOportunidade.Tipo_de_aceite__c == 'Eletrônico'){
            objAceiteEletronicoTO.aceite = 'S';
            objAceiteEletronicoTO.canal = 'PRESENCIAL';
        }

        objAceiteEletronicoTO.idOportunidade = objOportunidade.Id;
        objAceiteEletronicoTO.conta = objConta.Id;
        objAceiteEletronicoTO.usuario = objOportunidade.OwnerId;
        objAceiteEletronicoTO.documento = objConta.CnpjCpf__c;
        objAceiteEletronicoTO.razaoSocial = objConta.RazaSocial__c;
        objAceiteEletronicoTO.fantasia = objConta.NomeFantasia__c;
        objAceiteEletronicoTO.carteira = objOportunidade.CarteiraCriacao__c;
        objAceiteEletronicoTO.modalidadeCobranca = objOportunidade.ModalidadeCobranca__c;
        objAceiteEletronicoTO.campanha = objOportunidade.CodigoRegraNegocio__c;
        objAceiteEletronicoTO.pme = 'Y';
        objAceiteEletronicoTO.endereco = objEnderecoComercialTO != null ? objEnderecoComercialTO : objEnderecoOportunidadeTO;
        objAceiteEletronicoTO.contatoCobranca = objContatoCobrancaTO;
        objAceiteEletronicoTO.enderecoCobranca = objEnderecoOportunidadeTO;
        objAceiteEletronicoTO.contato = objContatoTO;
        objAceiteEletronicoTO.oportunidade = objOportunidadeTO;
        objAceiteEletronicoTO.operador = objOperadorTO;
        objAceiteEletronicoTO.tipoCliente = objOportunidade.Type;
        objAceiteEletronicoTO.codigoConta = objOportunidade.Account.Codigo8__c;

        Map<String, String> mapConfiguracoes = CollectionUtil.retornarMap('DeveloperName', 'Valor__c', ConfiguracaoEnvioAceiteDAO.getInstance().buscarTodas());

        Map<String, String> mapHeader = new Map<String, String>();
        mapHeader.put('Authorization', gerarToken(mapConfiguracoes));
        mapHeader.put('Content-Type', 'text/json');
        
        System.debug('Envio: ' + JSON.serializePretty(objAceiteEletronicoTO));

        HttpResponse objHttpResponse = Util.executarHttpRequest(mapConfiguracoes.get('endpoint'), 'POST', mapHeader, JSON.serialize(objAceiteEletronicoTO), 20000);

        if (objHttpResponse.getStatusCode() != 200){
            throw new AceiteEletronicoServiceException('Erro na conexão com os servidores da Boa Vista, tente novamente em instantes. Se o problema persistir, entre em contato com a Gestão de Vendas.');
        }

        AceiteEletronicoTO.AceiteEletronicoRetorno objAceiteEletronicoRetornoTO = (AceiteEletronicoTO.AceiteEletronicoRetorno)JSON.deserialize(objHttpResponse.getBody(), AceiteEletronicoTO.AceiteEletronicoRetorno.class);

        // Logs do retorno
        System.debug('Retorno: ' + objAceiteEletronicoRetornoTO);
        ApplicationLog__c objLog = new ApplicationLog__c(
                    ErrorMessage__c = objAceiteEletronicoRetornoTO.toString(),
                    Processo__c = 'Envio de Aceite'
                );
        lstSObjApplicationLog.add(objLog);
        insert lstSObjApplicationLog;

        String returnCode = objAceiteEletronicoRetornoTO.codigoRetorno;
        String returnMessage = objAceiteEletronicoRetornoTO.mensagemRetorno;
        String returnStatus = returnCode + ' : ' + returnMessage;


        if (returnCode.contains('00') || returnMessage.contains('00')){

            update new Opportunity(
                Id = objOportunidade.Id,
                StatusAceite__c = returnCode + ': ' + returnMessage,
                DataEnvioAceite__c = System.now()
            );
            
            insert new ApplicationLog__c(
                ErrorMessage__c = returnCode +' - '+ returnMessage,
                Processo__c = 'Pendente'

            );
            return new EnviarAceiteRetornoTO('Processo efetuado com sucesso. ' + returnStatus);
        } 
        
        else if (returnCode.contains('02') || returnMessage.contains('02') || returnMessage.contains('ACEITE VERBAL REALIZADO COM SUCESSO')){

            update new Opportunity(
                Id = objOportunidade.Id,
                StatusAceite__c = returnStatus,
                StageName = 'Fechada Ganha',
                MotivoGanhoPerda__c = 'Aceite Eletrônico',
                JustificativaGanhoPerda__c = 'Aceite Eletrônico',
                DataEnvioAceite__c = System.now()
            );

            update new Account(
                Id = objConta.Id,
                Codigo8__c = objAceiteEletronicoRetornoTO.codigoCliente,
                Type = 'Cliente'
            );

            update new Contact(
                Id = objContatoCobranca.Id,
                OperadorAC__c = objAceiteEletronicoRetornoTO.operadorAC
            );

            insert new ApplicationLog__c(
                ErrorMessage__c = returnCode +' - '+ returnMessage,
                Processo__c = 'Pendente'

            );

            return new EnviarAceiteRetornoTO('Processo efetuado com sucesso. ' + returnStatus);
        } 
        
        else if ((returnCode.contains('12') || returnMessage.contains('12')) && new List<String> { 'F', 'D', 'J' }.contains(objAceiteEletronicoRetornoTO.statusCliente)){
            update new Opportunity(
                Id = objOportunidade.Id,
                StatusAceite__c = returnStatus + '.' + (objAceiteEletronicoRetornoTO.statusCliente == 'D' ? 'Debito Pendente.' : (objAceiteEletronicoRetornoTO.statusCliente == 'J' ? 'Juridico.' : 'Fraude.')),
                DataEnvioAceite__c = System.now()
            );

            throw new AceiteEletronicoServiceException('Não foi possivel efetuar o aceite. Cliente com restrição, por Débito Pendente, Fraude ou Processo no Juridico.');
        } 
        
        else if ((returnCode.contains('12') || returnMessage.contains('12')) && new List<String> { 'I', 'R' }.contains(objAceiteEletronicoRetornoTO.statusCliente)){
            update new Opportunity(
                Id = objOportunidade.Id,
                StatusAceite__c = returnStatus,
                StageName = 'Fechada Ganha',
                MotivoGanhoPerda__c = 'Aceite Eletrônico',
                JustificativaGanhoPerda__c = 'Aceite Eletrônico',
                DataEnvioAceite__c = System.now()
            );

            update new Account(
                Id = objConta.Id,
                Codigo8__c = objAceiteEletronicoRetornoTO.codigoCliente,
                Type = 'Cliente'
            );

            update new Contact(
                Id = objContatoCobranca.Id,
                OperadorAC__c = objAceiteEletronicoRetornoTO.operadorAC
            );

            return new EnviarAceiteRetornoTO('Aceite de Reativação enviado com sucesso. ' + objAceiteEletronicoRetornoTO.codigoRetorno + ' : ' + objAceiteEletronicoRetornoTO.mensagemRetorno);
        } 
        
        else {
            update new Opportunity(
                Id = objOportunidade.Id,
                StatusAceite__c = returnStatus + '. ' + objAceiteEletronicoRetornoTO.statusCliente
            );

            throw new AceiteEletronicoServiceException(returnStatus);
        }
    }

    public class EnviarAceiteRetornoTO {
        public String Mensagem { get; set; }

        public EnviarAceiteRetornoTO(String strMensagem){
            Mensagem = strMensagem;
        }
    }

    public class AceiteEletronicoServiceException extends Exception { }
}