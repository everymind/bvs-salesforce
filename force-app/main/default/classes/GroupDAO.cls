public class GroupDAO {
    private final static GroupDAO instance = new GroupDAO();

    private GroupDAO() { }

    public static GroupDAO getInstance(){
        return instance;
    }

    public Group buscarPorFila(String nomeFila){
        List<Group> lstGroup = [SELECT 
                                    Id, 
                                    Name 
                                FROM 
                                    Group 
                                WHERE 
                                    Type    = 'Queue' 
                                AND DeveloperName    = :nomeFila];

        if (lstGroup.isEmpty()){
            return null;
        } else {
            return lstGroup[0];
        }
    }

    public Group buscarPorFilaId(Id idFila){
        List<Group> lstGroup = [SELECT 
                                    Id, 
                                    Name 
                                FROM 
                                    Group 
                                WHERE 
                                    Type    = 'Queue' 
                                AND Id    = :idFila];

        if (lstGroup.isEmpty()){
            return null;
        } else {
            return lstGroup[0];
        }
    }

    public Group buscarPorFilaAtendimento(String nomeFila){
        List<Group> lstGroup = [SELECT 
                                    Id, 
                                    Name 
                                FROM 
                                    Group 
                                WHERE 
                                    Type    = 'Queue' 
                                AND Name    = :nomeFila];

        if (lstGroup.isEmpty()){
            return null;
        } else {
            return lstGroup[0];
        }
    }

    public List<Group> buscarPorFila(Set<String> setNomeFila){
        return [SELECT 
                                    Id, 
                                    Name 
                                FROM 
                                    Group 
                                WHERE 
                                    Type    = 'Queue' 
                                AND NAME    IN :setNomeFila];
    }
}