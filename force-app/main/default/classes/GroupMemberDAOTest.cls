@isTest
public with sharing class GroupMemberDAOTest {
    public GroupMemberDAOTest() {}

    @isTest
    public static void buscarPorUsuarioTest()
    {
        Test.startTest();
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do Sistema');
        GroupMemberDAO.getInstance().buscarPorIdUsuario(usr.id);
        Test.stopTest();
    }
}