@isTest
private class CaseTest
{
    
    @testSetup
    private static void setup()
    {
        Account conta = TestDataFactory.gerarContaJuridica(true);
    }

    @isTest
    private static void testarInclusaoPosVendas(){
        Test.startTest();
        Account conta = [SELECT Id FROM Account LIMIT 1];
        Id rtCSMPosVendas = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMPosVendas').getRecordTypeId();

        Case caso = new Case();
        caso.AccountId = conta.Id;
        caso.Area__c = 'Pós-vendas';
        caso.Origin = 'ATENDIMENTO';
        caso.RecordTypeId = rtCSMPosVendas;
        caso.Reason = 'ACESSOS';
        caso.Submotivo__c = 'BLOQUEIO/CANCELAMENTO CÓD. OPERADOR';
        caso.Submotivo2__c = 'APP';
        caso.SuppliedEmail = 'teste@teste.com.br';
        caso.SuppliedName = 'CASO TESTE';
        caso.Area__c = 'CSM';
        caso.Description = 'TESTE';
        insert caso;

        //Incluir caso filho
        Case casoFilho = new Case();
        casoFilho.AccountId = conta.Id;
        casoFilho.Area__c = 'Pós-vendas';
        casoFilho.Origin = 'ATENDIMENTO';
        casoFilho.RecordTypeId = rtCSMPosVendas;
        casoFilho.Reason = 'ACESSOS';
        casoFilho.Submotivo__c = 'BLOQUEIO/CANCELAMENTO CÓD. OPERADOR';
        casoFilho.Submotivo2__c = 'APP';
        casoFilho.SuppliedEmail = 'teste@teste.com.br';
        casoFilho.SuppliedName = 'CASO TESTE';
        casoFilho.Area__c = 'CSM';
        casoFilho.Description = 'TESTE DE CASO FILHO';
        casoFilho.ParentId=caso.Id;
        insert casoFilho;

        Test.stopTest();

        Case casoIncluido = [SELECT Id, Description, OwnerId FROM Case LIMIT 1];
        System.assertEquals(false, false);
    }

}