@isTest 
public with sharing class GroupDAOTest {
    public GroupDAOTest() {}

    @isTest
    public static void buscarFilaTest()
    {
        Test.startTest();
        GroupDAO.getInstance().buscarPorFila('PRIME');
        GroupDAO.getInstance().buscarPorFilaAtendimento('PRIME');

        Set<string> fila = new Set<String>();
        fila.add('PRIME');

        List<Group> g = GroupDAO.getInstance().buscarPorFila(fila);
        GroupDAO.getInstance().buscarPorFilaId(g[0].id);
        Test.stopTest();
    }
}