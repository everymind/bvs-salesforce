@isTest
public with sharing class CadastroContaTOTest {
    public CadastroContaTOTest() {}

    @isTest
    public static void CadastroContaTO(){
        Test.startTest();
        CadastroContaTO.ContaPF contaPF = new CadastroContaTO.ContaPF();
        contaPF.nome = '';
        contaPF.data_nascimento = '';
        contaPF.nome_mae = '';
        contaPF.sexo = '';
        contaPF.email = '';
        contaPF.tel_cel = '';
        contaPF.tel_com = '';
        contaPF.tel_res = '';
        contaPF.cod_retorno = '';
        contaPF.msg_retorno = '';
        CadastroContaTO.ContaPJ contaPJ = new CadastroContaTO.ContaPJ();
        contaPJ.razao = '';
        contaPJ.endereco = '';
        contaPJ.bairro = '';
        contaPJ.cep = '';
        contaPJ.cidade = '';
        contaPJ.uf = '';
        contaPJ.cod_retorno = '';
        contaPJ.msg_retorno = '';
        Test.stopTest();
    }
}