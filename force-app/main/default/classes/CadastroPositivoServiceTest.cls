@isTest
public with sharing class CadastroPositivoServiceTest
{
    public CadastroPositivoServiceTest() {}

    @isTest
    public static void realizarOptinTest()
    {
        CadastroPositivoService cadastrar = new CadastroPositivoService();
        CadastroPositivoService.RealizarOptinRequest teste = new CadastroPositivoService.RealizarOptinRequest();
        test.startTest();
        teste.numeroDocumento = '11111111111';
        teste.tipoCliente = 'PF';
        teste.nome = 'teste';
        teste.nomeConfirmado ='teste';
        teste.dataAniversario = '2022-12-11';
        teste.bairro = 'teste';
        teste.cep = '00000000';
        teste.numeroRg='111111111';
        teste.sexo = 'Masculino';
        teste.email = 'teste@teste.com';
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('PositivoRequest');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        
        Test.setMock(HttpCalloutMock.class, mock);
        cadastrar.obterStatusOptinConsumidor('11111111111','PF');
        cadastrar.realizarOptin(teste);
        cadastrar.realizarOptout('11111111111','PF');
        test.stopTest();   
        
    }
}