/**
 * @Autor: Maycon
 * @Data: 12/07/2022
 * @Descrição: Classe que manipula objetos de contatos
 * @Parâmetro: List<Contact> lstContacts - Lista que contém a instância dos registros de contatos após a criação ou alteração.
 */
public without sharing class ContactBO{

    /**
     * @Autor: Maycon
     * @Data: 12/07/2022
     * @Descrição: Constante que representa uma instância da classe.
     */
    private static final ContactBO instance = new ContactBO();

    /**
     * @Autor: Maycon
     * @Data: 12/07/2022
     * @Descrição: Método público que retorna uma instância da classe.
     */
    public static ContactBO getInstance() {
        return instance;
    }

    public void archiveContact(List<Contact> lstContacts){

        Map<String, String> mapConfiguracoes = buscarConfiguracoes();

        //User inactiveContactOwner = new User(Id = mapConfiguracoes.get('InactiveContactOwner'));
        //system.debug(inactiveContactOwner);
        //String inactiveContactOwner = mapConfiguracoes.get('InactiveContactOwner');
		String bucketAccount = mapConfiguracoes.get('BucketAccount');
        List<Contact> updatedContacts = new List<Contact>();

        for(Contact objContact : lstContacts){

            if(objContact.Status__c == 'Inativo' && objContact.Updated__c == false){

                String accountId = objContact.AccountId;

                updatedContacts.add(
                    new Contact(
                        Id = objContact.Id,
                        //OwnerId = inactiveContactOwner,
                        IdContaAnterior__c = accountId,
                        AccountId = bucketAccount,
                        Updated__c = true
                    )
                );
            }
        }

        if(!updatedContacts.isEmpty()){
            system.debug(updatedContacts);
            update updatedContacts;
        }

    }

    private static Map<String, String> buscarConfiguracoes() {
		Map<String, String> mapConfiguracoes = new Map<String, String>();

		CollectionUtil.carregarMap(mapConfiguracoes, 'DeveloperName', 'Valor__c', [SELECT Id, DeveloperName, MasterLabel, Valor__c FROM Contact_Metadata__mdt]);

		return mapConfiguracoes;
	}
}