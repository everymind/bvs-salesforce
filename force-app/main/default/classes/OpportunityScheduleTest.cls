@isTest
public class OpportunityScheduleTest {
 
@isTest static void opportunityCalcelTest_Schedule() {

    // Recupera Id do tipo de registro de pessoa jurídica da conta
	Id tipoRegistroPessoaJuridicaId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId();

    // Cria conta de teste
    Account objAccount = new Account(
        Name = 'Teste Account',
        Carteira__c = 'CALATV',
        CnpjCpf__c = '45406480000123',
        RecordTypeId = tipoRegistroPessoaJuridicaId
    );

    insert objAccount;

    Opportunity opp = new Opportunity(
        Name = 'Teste de opt aut',
        StageName = 'Proposta',
        CloseDate = date.newInstance(2022,04,22),
        DataInicioFaturamento__c = date.today(),
        TipoReceita__c = 'Pontual',
        NumeroMeses__c = 1,
        AccountId = objAccount.Id,
        OwnerId = '0053h000001pNQoAAM',
        CreatedDate = DateTime.now().addDays(-15)
    );

    insert opp;
 
    // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
    String CRON_EXP = '0 6 * * * ?';
 
    Test.startTest();
    
    String jobId = System.schedule('Cancel opportunities', CRON_EXP, new OpportunitySchedule());
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
    System.assertEquals(CRON_EXP, ct.CronExpression);
    System.assertEquals(0, ct.TimesTriggered);
    
    Test.stopTest();
    }
}