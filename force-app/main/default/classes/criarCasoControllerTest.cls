@isTest
private class criarCasoControllerTest {

    


    @isTest
    private static void testSalvarCaso()
    {      
        Test.startTest();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);      
        String AccountId = conta.Id;
        String ContactId = contato.Id;
        String Origin = 'ATENDIMENTO';
        String Description = 'TESTE';
        String Priority = 'MÉDIA';
        Entitlement direito = new Entitlement(Name = 'PRIME', AccountId = conta.Id);
        insert direito;


        Case caso = criarCasoController.salvarCaso(AccountId, null, Origin, Priority, 'NOVO', Description, null, ContactId, 'CSM Atendimento');
        Case caso2 = criarCasoController.salvarCaso(AccountId, null, 'GC', Priority, 'AGUARDANDO TRATAMENTO', Description, null, ContactId, 'CSM Pós Vendas');
        Case caso1 = criarCasoController.salvarCaso(AccountId, null, 'GC', Priority, 'NOVO', Description, null, ContactId, 'CSM Atendimento');
        List<Contact> teste = criarCasoController.buscaContatosRelacionados(conta.id);
        system.assertNotEquals(caso.id, null);

        Test.stopTest();
    }
}