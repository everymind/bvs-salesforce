public class ConfiguracaoEnvioAceiteDAO {
    private final static ConfiguracaoEnvioAceiteDAO instance = new ConfiguracaoEnvioAceiteDAO();

    private ConfiguracaoEnvioAceiteDAO() { }

    public static ConfiguracaoEnvioAceiteDAO getInstance(){
        return instance;
    }

    public List<ConfiguracaoEnvioAceite__mdt> buscarTodas(){
        return [
            SELECT
                Id,
                DeveloperName,
                Valor__c
            FROM
                ConfiguracaoEnvioAceite__mdt
        ];
    }
}