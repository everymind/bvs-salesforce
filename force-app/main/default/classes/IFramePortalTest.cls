/**
 * @Autor: Everymind
 * @Data: 28/07/2020
 * @Descrição: Classe de teste para o método do controlador de IFrame.
 */
@IsTest
public class IFramePortalTest {

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método que prepara a configuração e cenário necessários para os testes.
	 */
	@TestSetup
	static void setup() {
		// Recupera Id do tipo de registro de pessoa jurídica da conta
		Id tipoRegistroPessoaJuridicaId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId();

		// Cria conta de teste
		Account objAccount = new Account(
			Name = 'Teste Account',
			CnpjCpf__c = '45406480000123',
			RecordTypeId = tipoRegistroPessoaJuridicaId
		);

		insert objAccount;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método que testa a busca de URL de Portal de Relatórios.
	 */
	@IsTest
	static void testarIFramePortalRelatorio() {
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalController.buscarUrl(recordId, 'Portal de Relatórios', null);
		Test.stopTest();
	}

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método que testa a busca de URL de Portal de Vendas.
	 */
	@IsTest
	static void testarIFramePortalVendas() {
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalController.buscarUrl(recordId, 'Portal de Vendas', null);
		Test.stopTest();
	}

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método que testa a busca de URL de Painel.
	 */
	@IsTest
	static void testarIFramePainel() {
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalController.buscarUrl(recordId, 'Portal de Vendas', null);
		Test.stopTest();
	}

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método que testa a busca de URL de Reencarteiramento.
	 */
	@IsTest
	static void testarIFrameReencarteiramento() {
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalController.buscarUrl(recordId, 'Reencarteiramento', null);
		Test.stopTest();
	}

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método que testa a busca de URL de Visão 360.
	 */
	@IsTest
	static void testarIFrameVisao360() {
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalController.buscarUrl(recordId, 'Visão 360', null);
		Test.stopTest();
	}

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método que testa a busca de URL de Painel de Incidentes.
	 */
	@IsTest
	static void testarIFramePainelIncidentes() {
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalController.buscarUrl(recordId, 'Painel de Incidentes', null);
		Test.stopTest();
	}

	/**
	 * @Autor: Maycon
	 * @Data: 10/10/2022
	 * @Descrição: Método que testa a busca de URL do Novo Painel de Incidentes.
	 */
	@IsTest
	static void testarIFrameNovoPainelIncidentes() {
		Test.startTest();
		Account objConta = TestDataFactory.gerarContaJuridica(true);
		String recordId = objConta.Id;

		String url = IFramePortalController.buscarUrl(recordId, 'Novo Painel Incidentes', null);
		Test.stopTest();
	}

	/**
	 * @Autor: Maycon
	 * @Data: 10/10/2022
	 * @Descrição: Método que testa a busca de URL do Painel de Oficios.
	 */
	@IsTest
	static void testarIFrameOficios() {
		Test.startTest();
		String url = IFramePortalController.buscarUrl(null, 'Oficios', null);
		Test.stopTest();
	}

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método que testa a busca de URL de Intenção de Cancelamento.
	 */
	@IsTest
	static void testarIFrameIntencaoCancelamento() {
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalController.buscarUrl(recordId, 'Intenção de Cancelamento', null);
		Test.stopTest();
	}

	@IsTest
	static void testarIFrameSuaFatura() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
		Contact contato = TestDataFactory.gerarContato(true, conta);
		String recordId = conta.Id;	

		String url = IFramePortalController.buscarUrl(recordId, 'Sua Fatura', recordId);
		Test.stopTest();
	}

	@IsTest
	static void testarIFrameGuiaResetSenha() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(false);
		conta.CnpjCpf__c = '98471135000130';
		insert conta;

		Contact contato = TestDataFactory.gerarContato(false, conta);
        contato.cpf__c = '83242872800';
		contato.Email = 'test@test.com';
		contato.OperadorAC__c = null;
        insert contato;

		String recordContactId = contato.Id;
		String recordId = conta.Id;
		
		String url = IFramePortalController.buscarUrl(recordId, 'GuiaResetSenha', conta.Id);
		Test.stopTest();
	}

	@IsTest
	static void testarIFrameGuiaReenvioSenha() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(false);
		conta.CnpjCpf__c = '98471135000130';
		insert conta;

		Contact contato = TestDataFactory.gerarContato(false, conta);
        contato.cpf__c = '83242872800';
		contato.Email = 'test@test.com';
		contato.OperadorAC__c = null;
        insert contato;

		String recordContactId = contato.Id;
		String recordId = conta.Id;

		String url = IFramePortalController.buscarUrl(recordId, 'GuiaReenvioSenha', conta.Id);
		Test.stopTest();
	}

	@IsTest
	static void testarIFrameBuscarUrlcomIdContatoPortalVendas() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
		Contact contato = TestDataFactory.gerarContato(true, conta);
		String recordId = conta.Id;
		String recordIdContato = contato.Id;

		String url = IFramePortalController.buscarUrlcomIdContato(recordId, 'Portal de Vendas', null, recordIdContato);
		Test.stopTest();
	}

	@IsTest
	static void testarIFrameBuscarUrlcomIdContatoReencarteiramento() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
		Contact contato = TestDataFactory.gerarContato(true, conta);
		String recordId = conta.Id;
		String recordIdContato = contato.Id;

		String url = IFramePortalController.buscarUrlcomIdContato(recordId, 'Reencarteiramento', null, recordIdContato);
		Test.stopTest();
	}

	@IsTest
	static void testarIFrameBuscarUrlcomIdContatoVisao360() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
		Contact contato = TestDataFactory.gerarContato(true, conta);
		String recordId = conta.Id;
		String recordIdContato = contato.Id;

		String url = IFramePortalController.buscarUrlcomIdContato(recordId, 'Visão 360', null, recordIdContato);
		Test.stopTest();
	}

	@IsTest
	static void testarIFrameBuscarUrlcomIdContatoPainelDeIncidentes() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
		Contact contato = TestDataFactory.gerarContato(true, conta);
		String recordId = conta.Id;
		String recordIdContato = contato.Id;

		String url = IFramePortalController.buscarUrlcomIdContato(recordId, 'Painel de Incidentes', null, recordIdContato);
		Test.stopTest();
	}

	@IsTest
	static void testarIFrameBuscarUrlcomIdContatoIntencaoDeCancelamento() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
		Contact contato = TestDataFactory.gerarContato(true, conta);
		String recordId = conta.Id;
		String recordIdContato = contato.Id;

		String url = IFramePortalController.buscarUrlcomIdContato(recordId, 'Intenção de Cancelamento', null, recordIdContato);
		Test.stopTest();
	}

	@IsTest
	static void testarIFrameBuscarUrlcomIdContatoGuiaResetSenha() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(false);
		conta.CnpjCpf__c = '98471135000130';
		insert conta;

		Contact contato = TestDataFactory.gerarContato(false, conta);
        contato.cpf__c = '83242872800';
		contato.Email = 'test@test.com';
		contato.OperadorAC__c = null;
        insert contato;

		String recordIdContato = contato.Id;
		String recordId = conta.Id;

		String url = IFramePortalController.buscarUrlcomIdContato(recordId, 'GuiaResetSenha', conta.Id , recordIdContato);
		Test.stopTest();
	}

	@IsTest
	static void testarIFrameBuscarUrlcomIdContatoGuiaReenvioSenha() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(false);
		conta.CnpjCpf__c = '98471135000130';
		insert conta;

		Contact contato = TestDataFactory.gerarContato(false, conta);
        contato.cpf__c = '83242872800';
		contato.Email = 'test@test.com';
		contato.OperadorAC__c = null;
        insert contato;

		String recordIdContato = contato.Id;
		String recordId = conta.Id;
		
		String url = IFramePortalController.buscarUrlcomIdContato(recordId, 'GuiaReenvioSenha', conta.Id , recordIdContato);
		Test.stopTest();
	}

	@IsTest
	static void testarIFrameBuscarUrlcomIdContatoSuaFatura() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
		Contact contato = TestDataFactory.gerarContato(true, conta);
		String recordId = conta.Id;
		String recordIdContato = contato.Id;

		String url = IFramePortalController.buscarUrlcomIdContato(recordId, 'Sua Fatura', recordId, recordIdContato);
		Test.stopTest();
	}

	@IsTest
	static void testarIFrameFormataDocumentoMaiorQue14() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
		Contact contato = TestDataFactory.gerarContato(true, conta);
		String doc = conta.CnpjCpf__c = '12345678901234';

		String url = IFramePortalController.formataDocumento(doc);
		Test.stopTest();
		System.assertEquals(doc.length() >= 14, true);
	}

	@IsTest
	static void testarIFrameFormataEmail() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
		Contact contato = TestDataFactory.gerarContato(true, conta);
		String email = contato.Email = 'teste@teste.com';

		String url = IFramePortalController.formataEmail(email);
		Test.stopTest();
		System.assertEquals(email.length() <= 100, true);
	}

	@IsTest
	static void testarIFrameFormataAccountId() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
		Contact contato = TestDataFactory.gerarContato(true, conta);
		String accountId = conta.Id;

		String url = IFramePortalController.formataAccountId(accountId);
		Test.stopTest();
		System.assertEquals(accountId.length() > 10, true);
	}

	@IsTest
	static void testarIFrameformataOperadorId() {
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
		Contact contato = TestDataFactory.gerarContato(true, conta);
		String operadorId = '';

		String url = IFramePortalController.formataOperadorId(operadorId);
		Test.stopTest();
		System.assertEquals(operadorId.length() < 15, true);
	}
}