/**
 * @Autor: Everymind
 * @Data: 20/07/2020
 * @Descrição: Classe que contém métodos de negócio referente ao domínio Task.
 */
public class TaskBO {

	/**
	 * @Autor: Everymind
	 * @Data: 20/07/2020
	 * @Descrição: Constante que representa uma instância da classe.
	 */
	private static final TaskBO instance = new TaskBO();

	/**
	 * @Autor: Everymind
	 * @Data: 20/07/2020
	 * @Descrição: Método público que retorna uma instância da classe.
	 */
    public static TaskBO getInstance() {
        return instance;
	}
	
	/**
	 * @Autor: Everymind
	 * @Data: 20/07/2020
	 * @Descrição: Método que valida se a tarefa criada pelo RightNow foi encerrada para devolutiva.
	 * @Parâmetro: Map<Id, Task> mapSObjNewTasks - Mapa que contém a instância dos registros de tarefas após a atualização.
	 * @Parâmetro: Map<Id, Task> mapSObjOldTasks - Mapa que contém a instância dos registros de tarefas antes a atualização.
	 */
	public void enviarTarefaRightNow(Map<Id, Task> mapSObjNewTasks, Map<Id, Task> mapSObjOldTasks) {
		Set<Id> setTaskIds = new Set<Id>();

		// Percorre as tarefas da trigger para validar o envio
		for(Task objNewTask : mapSObjNewTasks.values()) {
			Task objOldTask = mapSObjOldTasks.get(objNewTask.Id);

			// Verifica se o Id de Incidente do Right Now está preenchido
			if(String.isBlank(objNewTask.IdIncidenteRightNow__c)) {
				continue;
			}

			// Verifica se o Id da Tarefa do Right Now está preenchido
			if(String.isBlank(objNewTask.IdTarefaRightNow__c)) {
				continue;
			}

			// Verifica se a Data da conclusão foi preenchida
			if(objNewTask.DataConclusao__c != null) {
				continue;
			}

			// Verifica se houve alteração no fechamento da tarefa
			if(objNewTask.IsClosed == objOldTask.IsClosed) {
				continue;
			}
			
			// Verifica se a tarefa ainda está aberta
			if(!objNewTask.IsClosed) {
				continue;
			}

			// Adiciona na lista para enviar para RightNow
			setTaskIds.add(objNewTask.Id);
		}

		// Verifica se alguma das tarefas atualizadas entrou no critério de envio para o RightNow
		if(!setTaskIds.isEmpty()) {
			RightNowService.enviar(setTaskIds);
		}
	}

	public void bloquearExclusaoTarefas (List<Task> lstTask){

		  String Perfil = UserInfo.getProfileId();
		  User usuario = UserDAO.getInstance().buscarPorId(UserInfo.getUserId());
		  String perfil15 = String.valueOf(usuario.ProfileId).substring(0,15);

		  System.debug('### PERFIL ID ' + perfil15);

		  for (Task tarefa : lstTask)
		  {
			  //USANDO RÓRULO PERSONALIZADO
			if(!Label.PerfilExcecao.contains(perfil15)){
				tarefa.addError('Somente o administrador do sistema pode excluir tarefas!');
		 
			}
		}        
    }
			
}