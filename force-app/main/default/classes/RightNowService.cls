/**
 * @Autor: Everymind
 * @Data: 22/07/2020
 * @Descrição: Classe que contém métodos de serviço do RightNow.
 */
public class RightNowService {
	
	/**
	 * @Autor: Everymind
	 * @Data: 20/07/2020
	 * @Descrição: Método futuro que chama o serviço de encerramento de tarefas no RightNow.
	 * @Parâmetro: Set<String> - Set que contém os Ids RightNow das tarefas a serem encerradas.
	 */
	@Future(callout=true)
	public static void enviar(Set<Id> setTarefaIds) {
		Http objHttp = new Http();
		List<ApplicationLog__c> lstSObjApplicationLog = new List<ApplicationLog__c>();

		// Busca as tarefas
		List<Task> lstSObjTask = [SELECT Id, IdIncidenteRightNow__c, IdTarefaRightNow__c FROM Task WHERE Id IN :setTarefaIds];

		// Busca as configurações no custom metadata
		Map<String, String> mapConfiguracoes = buscarConfiguracoes();

		// Valida se possui configurações suficientes
		if(	!mapConfiguracoes.containsKey('ENDPOINT_INCIDENTS') || 
			!mapConfiguracoes.containsKey('ENDPOINT_TASKS') || 
			!mapConfiguracoes.containsKey('USERNAME') || 
			!mapConfiguracoes.containsKey('PASSWORD') || 
			!mapConfiguracoes.containsKey('TASK_STATUS_ID') ||
			!mapConfiguracoes.containsKey('INCIDENT_STATUS_ID')) {
				// Cria um registro de log para mapear as configurações necessárias
				ApplicationLog__c objLog = new ApplicationLog__c(
					ErrorMessage__c = 'Configurações necessárias: (ENDPOINT_INCIDENTS, USERNAME, PASSWORD e STATUS_ID)',
					Processo__c = 'Right Now');
				insert objLog;
				return;
		}

		// Gerar o token de autenticação
		String token = gerarToken(mapConfiguracoes);

		// Montar corpo da requisição
		String bodyTask = montarCorpoRequisicaoTarefa(mapConfiguracoes);
		String bodyIncident = montarCorpoRequisicaoIncidente(mapConfiguracoes);

		// Recupera template do endpoint
		String endpointTemplateTask = mapConfiguracoes.get('ENDPOINT_TASKS');
		String endpointTemplateIncident = mapConfiguracoes.get('ENDPOINT_INCIDENTS');

		// Percorre o set de Ids do RightNow para envio
		for(Task objTask : lstSObjTask) {
			// Preenche parâmetros da requisição
			HttpRequest objRequestTask = montarRequisicao(objTask.Id, token, bodyTask);
			HttpRequest objRequestIncident = montarRequisicao(objTask.Id, token, bodyIncident);
			
			// Substitui na query string o id do RightNow
			objRequestTask.setEndpoint(String.format(endpointTemplateTask, new List<Object>{ objTask.IdTarefaRightNow__c }));
			objRequestIncident.setEndpoint(String.format(endpointTemplateIncident, new List<Object>{ objTask.IdIncidenteRightNow__c }));

			// Realiza a requisição
			HttpResponse objResponseTask = objHttp.send(objRequestTask);

			// Verifica se houve erro
			if(objResponseTask.getStatusCode() >= 300) {
				// Cria um registro de log em caso de erro
				ApplicationLog__c objLog = new ApplicationLog__c(
					ErrorMessage__c = objResponseTask.getBody(),
					IdTarefaRightNow__c = objTask.IdTarefaRightNow__c,
					IdIncidenteRightNow__c = objTask.IdIncidenteRightNow__c,
					Processo__c = 'Right Now'
				);

				lstSObjApplicationLog.add(objLog);
			}

			HttpResponse objResponseIncident = objHttp.send(objRequestIncident);

			// Verifica se houve erro
			if(objResponseIncident.getStatusCode() >= 300) {
				// Cria um registro de log em caso de erro
				ApplicationLog__c objLog = new ApplicationLog__c(
					ErrorMessage__c = objResponseIncident.getBody(),
					IdTarefaRightNow__c = objTask.IdTarefaRightNow__c,
					IdIncidenteRightNow__c = objTask.IdIncidenteRightNow__c,
					Processo__c = 'Right Now'
				);

				lstSObjApplicationLog.add(objLog);
			}

			// Verifica se atingiu o limite de callouts
			if(Limits.getCallouts() >= Limits.getLimitCallouts()) {
				// Cria um registro de log em caso de erro
				ApplicationLog__c objLog = new ApplicationLog__c(
					ErrorMessage__c = 'Limite de callouts atingido.',
					Processo__c = 'Right Now'
				);

				lstSObjApplicationLog.add(objLog);

				// Encerra o loop para não estourar o limite
				break;
			}
		}

		// Insere os logs de aplicação
		insert lstSObjApplicationLog;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 22/07/2020
	 * @Descrição: Método que busca as configurações cadastradas no metadado ConfiguracaoRightNow__mdt.
	 */
	public static Map<String, String> buscarConfiguracoes() {
		Map<String, String> mapConfiguracoes = new Map<String, String>();

		CollectionUtil.carregarMap(mapConfiguracoes, 'DeveloperName', 'Valor__c', [SELECT Id, DeveloperName, MasterLabel, Valor__c FROM ConfiguracaoRightNow__mdt]);

		return mapConfiguracoes;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 22/07/2020
	 * @Descrição: Método que gera o token de autenticação.
	 */
	private static String gerarToken(Map<String, String> mapConfiguracoes) {
		String username = mapConfiguracoes.get('USERNAME');
		String password = mapConfiguracoes.get('PASSWORD');

		return EncodingUtil.base64Encode(Blob.valueOf(username + ':' + password));
	}

	/**
	 * @Autor: Maycon
	 * @Data: 18/02/2022
	 * @Descrição: Método que resgata token dinamico no Rightnow
	 */
	public static String gerarTokenDinamico(Map<String, String> mapConfiguracoes) {
		String username = mapConfiguracoes.get('USERNAME');
		String password = mapConfiguracoes.get('PASSWORD');
		String tokenUrl = mapConfiguracoes.get('ENDPOINT_TOKEN');
		String sessionToken = '';

		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(tokenUrl);
		request.setMethod('POST');    
		request.setHeader('Content-Type', 'application/x-www-form-urlencoded');

		String payload = 'username='+EncodingUtil.urlEncode(username,'UTF-8')+'&password='+EncodingUtil.urlEncode(password,'UTF-8');
	    System.debug(payload);

		request.setBody(payload);
		HttpResponse response = http.send(request);
		// Parse the JSON response
		if (response.getStatusCode() != 200) {
		System.debug('Ocorreu um erro ao tentar recuperar o token: ' + response.getStatusCode() + ' ' + response.getStatus());
		} else {
		System.debug(response.getBody());
		}

		RightNowTO.Token objSessionToken = (RightNowTO.Token)JSON.deserialize(response.getBody(), RightNowTO.Token.class);
		sessionToken = objSessionToken.session;
		return sessionToken;

	}

	/**
	 * @Autor: Everymind
	 * @Data: 22/07/2020
	 * @Descrição: Método que monta as configurações iniciais da requisição.
	 */
	private static HttpRequest montarRequisicao(String idTarefa, String token, String body) {
		HttpRequest objRequest = new HttpRequest();
		objRequest.setMethod('POST');
		objRequest.setHeader('X-HTTP-Method-Override', 'PATCH');
		objRequest.setHeader('OSvC-CREST-Application-Context', idTarefa);
		objRequest.setHeader('Authorization', 'Basic ' + token);
		objRequest.setHeader('Content-Type', 'application/json');
		objRequest.setBody(body);

		return objRequest;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 22/07/2020
	 * @Descrição: Método que monta os objetos de TO e serializa o objeto em JSON para envio da chamada.
	 * @Parâmetro: Map<String, String> mapConfiguracoes. Mapa que contém as configurações do serviço.
	 */
	private static String montarCorpoRequisicaoTarefa(Map<String, String> mapConfiguracoes) {
		RightNowTO.Status objStatus = new RightNowTO.Status();
		RightNowTO.StatusWithType objStatusWithType = new RightNowTO.StatusWithType();
		RightNowTO.Incident objIncident = new RightNowTO.Incident();

		// Recupera o status id do RightNow
		String statusId =  mapConfiguracoes.containsKey('TASK_STATUS_ID') ? mapConfiguracoes.get('TASK_STATUS_ID') : null;

		// Cria objetos para a serialização
		objStatus.id = statusId != null ? Integer.valueOf(statusId) : -1;
		objStatusWithType.status = objStatus;
		objIncident.statusWithType = objStatusWithType;

		return JSON.serializePretty(objIncident);
	}

	/**
	 * @Autor: Everymind
	 * @Data: 23/07/2020
	 * @Descrição: Método que monta os objetos de TO e serializa o objeto em JSON para envio da chamada.
	 * @Parâmetro: Map<String, String> mapConfiguracoes. Mapa que contém as configurações do serviço.
	 */
	private static String montarCorpoRequisicaoIncidente(Map<String, String> mapConfiguracoes) {
		RightNowTO.Status objStatus = new RightNowTO.Status();
		RightNowTO.StatusWithType objStatusWithType = new RightNowTO.StatusWithType();
		RightNowTO.Incident objIncident = new RightNowTO.Incident();

		// Recupera o status id do RightNow
		String statusId =  mapConfiguracoes.containsKey('INCIDENT_STATUS_ID') ? mapConfiguracoes.get('INCIDENT_STATUS_ID') : null;

		// Cria objetos para a serialização
		objStatus.id = statusId != null ? Integer.valueOf(statusId) : -1;
		objStatusWithType.status = objStatus;
		objIncident.statusWithType = objStatusWithType;

		return JSON.serializePretty(objIncident);
	}


}