/**
 * @Autor: Maycon
 * @Data: 13/07/2022
 * @Descrição: Classe que orquestra os eventos da trigger para os métodos de negócio.
 */
public class ContactTriggerHandler extends TriggerHandler {

	/**
	 * @Autor: Maycon
	 * @Data: 13/07/2022
	 * @Descrição: Constante que contém a instância da classe de negócio do objeto Contact.
	 */
	private ContactBO objBO = ContactBO.getInstance();

	public override void afterUpdate(){
		objBO.archiveContact(Trigger.new);
	}
}