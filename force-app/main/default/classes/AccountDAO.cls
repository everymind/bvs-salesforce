public class AccountDAO {
    private final static AccountDAO instance = new AccountDAO();

    private AccountDAO() { }

    public static AccountDAO getInstance(){
        return instance;
    }
    public Account buscarPorContactId(Id idContato){
       List<Contact> listContact = [
           SELECT id, AccountId
           FROM Contact
           WHERE id =: idContato
           ];
           
        if (listContact.isEmpty()){
            return null;
        }
        
        List<Account> lstConta = buscarPorId(new Set<Id> { listContact[0].AccountId });

        if (lstConta.isEmpty()){
            return null;
        } else {
            return lstConta[0];
        }
    }
    

    public Account buscarPorId(Id idConta){
        List<Account> lstConta = buscarPorId(new Set<Id> { idConta });

        if (lstConta.isEmpty()){
            return null;
        } else {
            return lstConta[0];
        }
    }

    public List<Account> buscarPorId(Set<Id> setIdConta){
        return [
            SELECT
                Id,
                BairroFaturamento__c,
                Carteira__c,
                CnpjCpf__c,
                ComplementoFaturamento__c,
                IsPersonAccount,
                NomeFantasia__c,
                NumeroFaturamento__c,
                RazaSocial__c,
                ShippingCity,
                ShippingPostalCode,
                ShippingState,
                ShippingStreet,
                FirstName,
                LastName,
                PersonEmail,
                PersonBirthdate,
                Type,
                Name,
                Recordtype.DeveloperName,
                Regional__c,
                Canal__c,
                
                Codigo8__c,
                BillingAddress,
                Phone,
                BillingPostalCode,
                BillingStreet
            FROM
                Account
            WHERE
                Id IN :setIdConta
        ];
    }

    public List<Account> buscarPorCarteira(Set<String> setCarteiras){
        return [
            SELECT
                Id,
                BairroFaturamento__c,
                Carteira__c,
                CnpjCpf__c,
                ComplementoFaturamento__c,
                IsPersonAccount,
                NomeFantasia__c,
                NumeroFaturamento__c,
                RazaSocial__c,
                ShippingCity,
                ShippingPostalCode,
                ShippingState,
                ShippingStreet
            FROM
                Account
            WHERE
                Carteira__c IN :setCarteiras
        ];
    }
}