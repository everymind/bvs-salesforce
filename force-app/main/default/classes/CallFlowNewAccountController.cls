global class CallFlowNewAccountController {
    public Flow.Interview.CriarConta CriarContaFlow {get; set;}

    public PageReference getUrlAccount(){
        String conta;
        if (CriarContaFlow!=null){
            conta=(string)CriarContaFlow.getVariableValue('IdConta');
        } else {
            conta='Account/list?filterName=Recent';
        }
        Pagereference p = new Pagereference('/'+conta);
        p.setRedirect(true);
        return(p);
    }
   
}