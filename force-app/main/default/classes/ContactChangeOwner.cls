/**
 * @Autor: Maycon
 * @Data: 08/08/2022
 * @Descrição: Classe para execução de Batch da automação de cancelamento de oportunidades, que envia as contas relacionadas para o Bolsão Interno
 */
global class ContactChangeOwner implements Database.Batchable<sObject>, Database.Stateful {
	public Boolean processWithError = false;

    /**
	 * @Autor: Maycon
	 * @Data: 08/08/2022
	 * @Descrição: Método para iniciar a execução do Batch com tamanho de lote definido.
	 * @Parâmetros: Integer batchSize - Número que define o tamanho do lote que o batch vai executar.
	 */
    global static void start(Integer batchSize) {
		Database.executeBatch(new ContactChangeOwner(), batchSize);
	}

    /**
	 * @Autor: Maycon
	 * @Data: 08/08/2022
	 * @Descrição: Método herdado da interface que recupera os registros que farão parte do escopo da execução.
	 * @Parâmetros: Database.BatchableContext objContext - Objeto de contexto do batch.
	 */
	global Database.QueryLocator start(Database.BatchableContext objContext) {

		String bucketAccount = getRuleParameter('BucketAccount');
        String inactiveContactOwner = getRuleParameter('InactiveContactOwner');

		String query = 'SELECT Id, OwnerId, AccountId FROM Contact WHERE AccountId = \''+bucketAccount+'\''+' and OwnerId <> \''+inactiveContactOwner+'\'';
		System.debug('PROCESSAMENTO DOS CONTATOS INATIVADOS: ' + query);
		
		return Database.getQueryLocator(query);
	}

    /**
	 * @Autor: Maycon
	 * @Data: 08/08/2022
	 * @Descrição: Método herdado da interface que executa a lógica de negócio baseados nos registros do escopo.
	 * @Parâmetros: Database.BatchableContext objContext - Objeto de contexto do batch.
	 * @Parâmetros: List<Account> lstSObjAccount - Lista de escopo retornada no método start.
	 */
	global void execute(Database.BatchableContext objContext, List<Contact> lstSObjContact) {

        if(!lstSObjContact.isEmpty()){

            String inactiveContactOwner = getRuleParameter('InactiveContactOwner');

            for(Contact objContact : lstSObjContact){
					objContact.OwnerId = inactiveContactOwner;
            }
        }

        //Atualiza as contas na base
		System.debug('Contatos a serem reencarteirados: ' + lstSObjContact);
		Database.update(lstSObjContact, false);
    }

    /**
	 * @Autor: Maycon
	 * @Data: 08/08/2022
	 * @Descrição: Método herdado da interface que é executa ao final da execução de cada lote.
	 */
	global void finish(Database.BatchableContext objContext) {
		//enviarEmail(processoComErro);
		if(!test.isRunningTest()) System.scheduleBatch(new ContactChangeOwner(),'Run again in 5',5);
	}

    /**
	* @Autor: Maycon
	* @Data: 08/08/2022
	* @Descrição: Método que recebe o valor configurado nos metadados da Automação de Oportunidades.
	*/
    private static String getRuleParameter(String metadataValue) {

		Contact_Metadata__mdt getDaysToCancelParameter = 	Contact_Metadata__mdt.getInstance(metadataValue);
		String daysToCancel = getDaysToCancelParameter.Valor__c;

		return daysToCancel;	
    }
}