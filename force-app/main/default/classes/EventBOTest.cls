@IsTest
private class EventBOTest {

 /*   @testSetup
       private static void setup() {
       //  Account objConta = TestDataFactory.gerarContaJuridica(true);
       Account objConta = new Account(
        Name = 'Teste Account',
        Carteira__c = 'CALATV'
      //  CnpjCpf__c = '344.005.940-50' 
              
        );

    insert objConta;
     //   Contact objContato = TestDataFactory.gerarContato(true, objConta); 

     Contact objContato = new Contact(
        AccountId = objConta.Id,
        FirstName = 'Contato',
        LastName = 'Contato',
        Email = 'teste@teste.com',
        Phone = '11912345678',
        Departamento__c = 'BI',
      //  CPF__c = '344.005.940-50',
        Birthdate = System.now().date()
     );
      insert  objContato;
       // Opportunity objOportunidade = TestDataFactory.gerarOportunidadeVendaPlano(true, objConta);
       Event compromisso3 = new Event();
       compromisso3.WhatId = objConta.Id;
       compromisso3.WhoId = objContato.Id;
       compromisso3.EndDateTime = DateTime.now().addHours(1);
       compromisso3.StartDateTime = DateTime.now();
       compromisso3.Type = 'Visita Presencial';

       insert compromisso3;
       }
*/


static testMethod void afterInsertTest(){


        Account objConta = new Account(
            Name = 'Teste Account',
            Carteira__c = 'CALATV',
            CnpjCpf__c = '30539030000131'        
        );
        insert objConta;
        
       // Account objConta = TestDataFactory.gerarContaJuridica(true);
        
        Contact objContato = TestDataFactory.gerarContato(true, objConta);

        Test.startTest();

        Test.enableChangeDataCapture();

        List<Event> lstcompromisso = new List<Event>();
        Event compromisso = new Event();
        compromisso.WhatId = objConta.Id;
        compromisso.StatusCompromisso__c = 'Não iniciada';
        compromisso.WhoId = objContato.Id;
        compromisso.StartDateTime = DateTime.now();
        compromisso.EndDateTime = DateTime.now().addHours(1);
        compromisso.Type = 'Visita Presencial';
        compromisso.Objetivo__c = 'Retenção';
        compromisso.Subject = 'Call';
        lstcompromisso.add(compromisso);

    
        insert lstcompromisso;

       
        Test.getEventBus().deliver();

      //  compromisso.Type = 'Visita Remota';
      //  update compromisso;

      Test.enableChangeDataCapture();
        
        Test.stopTest();

        system.debug('compromisso ' + compromisso);

        

        Account contaResultado = [SELECT Id FROM Account WHERE Id =: objConta.Id LIMIT 1];
        System.assertEquals(contaResultado.Id, compromisso.WhatId);
            
    }
}