@IsTest 
private class CaseBOTest {
    
    @testSetup static void setup() {
        Account account = new Account(
            Name = 'Conta de teste BVS',
            CnpjCpf__c = '27777267000146',
            RecordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId()
        );
        insert account;

        /*Account accountPosVenda = new Account(
            Name = 'Conta de teste BVS Pós Venda',
            CnpjCpf__c = '11111111111111',
            RecordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId()
        );
        insert accountPosVenda;*/

        Contact testeContato = new Contact(
            FirstName = 'Contato',
            LastName = 'Contato',
            CPF__c = '36909319087',
            Phone = '11912345678',
            AccountId = account.id
        );
        insert testeContato;
        
        SlaProcess sla=[SELECT Description,Id,Name FROM SlaProcess where name='HORA08A17' order by id desc limit 1];

        Entitlement direito = new Entitlement(Name = 'HORA08A17', AccountId = account.Id, SlaProcessId = sla.id);
        insert direito;
        Entitlement direitoPRIME = new Entitlement(Name = 'PRIME', AccountId = account.Id, SlaProcessId = sla.id);
        insert direitoPRIME;

        SlaProcess slaUtil=[SELECT Description,Id,Name FROM SlaProcess where name='HORACORRIDA' order by id desc limit 1];

        Entitlement direitoUtil = new Entitlement(Name = 'HORACORRIDA', AccountId = account.Id, SlaProcessId = sla.id);
        insert direitoUtil;

        
        TabulacaoSLA__c tabulacaoACESSOS = new TabulacaoSLA__c();
        tabulacaoACESSOS.TipoRegistro__c = 'CSMAtendimento';
        tabulacaoACESSOS.MotivoTxt__c = 'ACESSOS';
        tabulacaoACESSOS.SubMotivoTxt__c = 'AED PARAMETRIZAÇÃO';
        tabulacaoACESSOS.FilaAtual__c = 'TITELECOMUNICACOESREDES';
        tabulacaoACESSOS.ProximaFILA__c = 'RETORNO';
        tabulacaoACESSOS.TempoSLA__c = 120;
        insert tabulacaoACESSOS;

        TabulacaoSLA__c tabulacaoSUPORTE = new TabulacaoSLA__c();
        tabulacaoSUPORTE.TipoRegistro__c = 'CSMAtendimento';
        tabulacaoSUPORTE.MotivoTxt__c = 'SUPORTE AO USUÁRIO';
        tabulacaoSUPORTE.SubMotivoTxt__c = 'DESBLOQUEAR / RESET->SENHA';
        tabulacaoSUPORTE.TempoSLA__c = 120;
        tabulacaoSUPORTE.FilaAtual__c = 'TITELECOMUNICACOESREDES';
        tabulacaoSUPORTE.ProximaFILA__c = 'RETORNO';
        insert tabulacaoSUPORTE;

        TabulacaoSLA__c tabulacaoRETORNO = new TabulacaoSLA__c();
        tabulacaoRETORNO.TipoRegistro__c = 'CSMAtendimento';
        tabulacaoRETORNO.MotivoTxt__c = 'SUPORTE AO USUÁRIO';
        tabulacaoRETORNO.SubMotivoTxt__c = 'DESBLOQUEAR / RESET->SENHA';
        tabulacaoRETORNO.TempoSLA__c = 120;
        tabulacaoRETORNO.FilaAtual__c = 'RETORNO';
        insert tabulacaoRETORNO;

    }

    @isTest 
    public static void testandoOperacao(){
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case casoPai = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
    }

    @IsTest 
    public static void testDefinirFila() { 

        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
        List<Case> lstOldCase = new List<Case>();
        List<Case> lstCase = new List<Case>(); 
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
            caso.MotivoTxt__c = 'Teste';
            caso.SubMotivoTxt__c = 'Teste';
            update caso;

        lstCase.add(caso);

        Case caso2 = [SELECT Id, MotivoTxt__c, SubmotivoTxt__c, SubMotivo2Txt__c, MeioAcessoTxt__c FROM Case WHERE Id =: caso.Id];
        caso2.MeioAcessoTxt__c = 'Teste2';
        lstOldCase.add(caso2);

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(true);
        
        Test.startTest();
            CaseBO.getInstance().DefinirFila(lstCase, lstOldCase);
            System.assertEquals('Teste', tabulacao.ProximaFila__c);
        Test.stopTest();
        }

    }

    @IsTest 
    public static void testDefinirFilaInicialPosVenda() { 
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){ 
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Id rtPosVendas = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMPosVendas').getRecordTypeId();
        Entitlement direito = new Entitlement();
        direito.Name = 'POSVENDASSALESOPS';
        direito.AccountId = conta.Id;
        insert direito;
        Case casePosVendas = new Case(TempoDecorridoSLACliente__c = 0, 
                          MotivoTxt__c='SUPORTE AO USUÁRIO',
                          Description='Teste',
                          SubMotivoTxt__c='DESBLOQUEAR / RESET->SENHA',
                          EmailSolicitante__c = 'alguem@servidor.com',
                          NomeSolicitante__c = 'Alguém teste',
                          SLAAtivo__c = true,
                          AccountId = conta.id,
                          Status = 'AGUARDANDO TRATAMENTO',
                          ContactId = contato.id,
                          RecordTypeId = rtPosVendas
                         );
        insert casePosVendas;
        
        Case caso=[select FIELDS(STANDARD) from Case u where MotivoTxt__c='SUPORTE AO USUÁRIO' and  SubMotivoTxt__c='DESBLOQUEAR / RESET->SENHA' and RecordTypeId=:rtPosVendas limit 1];
        List<Case> lstCase = new List<Case>(); 
        lstCase.add(casePosVendas);
        CaseBO.getInstance().definirFilaInicialPosVendas(lstCase);
        System.assertEquals(true, caso.id != null);
        
        //Testar rotina de pausarSLA
        caso.SLAAtivo__c=false;

        update caso;
        
        System.assertEquals(false, caso.SLAAtivo__c); 
        
        //Testar rotina de finalizar caso
        caso.DescricaoSolucao__c = 'TESTE';
        caso.Status='FINALIZADO';

        //update caso;
        
        System.assertEquals(true, caso.Status=='FINALIZADO'); 
    }
    }

    @IsTest 
    public static void testDefinirFilaInicialAtendimento() {  
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){ 
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);

        Id rtCSM = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMAtendimento').getRecordTypeId();
        Case caseCSM = new Case(TempoDecorridoSLACliente__c = 0, 
                          MotivoTxt__c='SUPORTE AO USUÁRIO',
                          Description='Teste',
                          SubMotivoTxt__c='DESBLOQUEAR / RESET->SENHA',
                          EmailSolicitante__c = 'alguem@servidor.com',
                          NomeSolicitante__c = 'Alguém teste',
                          SLAAtivo__c = true,
                          AccountId = conta.id,
                          Status = 'NOVO',
                          ContactId = contato.id,
                          RecordTypeId = rtCSM);
        insert caseCSM;

        Case caso=[select FIELDS(STANDARD) from Case u where MotivoTxt__c='SUPORTE AO USUÁRIO' and  SubMotivoTxt__c='DESBLOQUEAR / RESET->SENHA' and RecordTypeId=:rtCSM limit 1];
        
        System.assertEquals(true, caso.id != null);
        List<Case> lstCase = new List<Case>(); 
        lstCase.add(caseCSM);
        CaseBO.getInstance().definirFilaInicialAtendimento(lstCase);
        CaseBO.getInstance().atualizarPrazoLista(lstCase);

        //Testar rotina de pausarSLA
        caso.SLAAtivo__c=false;

        update caso;
        
        System.assertEquals(false, caso.SLAAtivo__c); 
        }
        
    }
    @IsTest 
    public static void testVerificaStatusFinalizadoRegra() { 
        List<Case> lstCase = new List<Case>(); 
        List<Case> lstCaseOld = new List<Case>();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){

            Entitlement direito = new Entitlement();
            direito.Name = 'PREOUVIDORIA';
            direito.AccountId = conta.Id;
            insert direito;

            Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
            lstCase.add(caso);
            caso.Status = 'PREOUVIDORIA';
            caso.EntitlementId = direito.Id;
            lstCaseOld.add(caso);
            
            CaseBO.getInstance().verificaStatusFinalizadoRegra(lstCase,lstCaseOld);
             



        }
    }
    @IsTest 
    public static void testCriarCasoFilhoPreOuvidoria() { 
        List<Case> lstCase = new List<Case>(); 
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
        Entitlement direito = new Entitlement();
        direito.Name = 'PREOUVIDORIA';
        direito.AccountId = conta.Id;

        insert direito;

        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
            caso.Status = 'PREOUVIDORIA';
            caso.EntitlementId = direito.Id;
            update caso;

        Account conta2 = TestDataFactory.gerarContaJuridica(true);
        Contact contato2 = TestDataFactory.gerarContato(true, conta);
        Case caso2 = TestDataFactory.gerarCasoAtendimento(true, conta2, contato2);
        Id rtCSM = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMAtendimento').getRecordTypeId();
        Case objCaso = new Case(
            RecordTypeId = rtCSM,
            AccountId = conta2.Id,
            ContactId = contato2.Id,
            Origin = 'ATENDIMENTO',
            Description = 'TESTE',
            Priority = 'MÉDIA',
            ParentId = caso.id
        );
            caso2.ParentId = caso.Id;
            update caso2;

        lstCase.add(objCaso);
        system.debug('Aqui está a lista de caso'+ caso2.CreatedById + caso2.CreatedDate);
        Test.startTest();
        CaseBO.getInstance().criarCasoFilhoPreOuvidoria(lstCase);
        //CaseBO.getInstance().criarCasoFilhoPreOuvidoriaTest(lstCase);
        System.assertEquals(true, true);
        Test.stopTest();
        }
        
        
    }

    @isTest
    public static void testPausarSLA(){
        
        List<Case> lstOldCase = new List<Case>();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
        caso.SLAAtivo__c = false;
        update caso;

        lstOldCase.add(caso);

        List<Case> lstCase = new List<Case>();
        lstCase.add(caso);

        Test.startTest();
        CaseBo.getInstance().pausarSLA(lstCase, lstOldCase);
        CaseBo.getInstance().definirTipoCaso(lstCase, lstOldCase);
        CaseBo.getInstance().definirTipoCaso(lstCase, null);

        CaseBo.getInstance().impedirFecharCaso(lstCase);
        CaseBo.getInstance().requeraprovacao(lstCase);
        CaseBo.getInstance().definirFilaAnterior(lstCase, lstOldCase);
        CaseBo.getInstance().gerarLinkPesquisaProtocolosAtendimento(lstCase, lstOldCase);
        System.assertEquals(true, true);
        Test.stopTest();

    }
    @isTest
    public static void testFilAnterior(){
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){ 
        Map<Id,Case> mapOldCase = new Map<Id,Case>();
        List<Case> lstOldCase = new List<Case>();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
        mapOldCase.put(caso.id,caso);
        lstOldCase.add(caso);
        caso.Status = 'REJEITADO';
        caso.KBA_Aprovado__c = 'Não';
        List<Case> lstCase = new List<Case>();
        lstCase.add(caso);

        CaseBo.getInstance().definirFilaAnterior(lstCase, lstOldCase);
		CaseBo.getInstance().encerrarMarco(lstCase, mapOldCase);
    
        CaseBo.getInstance().gerarLinkPesquisaProtocolosAtendimento(lstCase,lstOldCase);
        }

    }

    @isTest
    public static void testDefinirSLA()
    {
        Test.StartTest();
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
            List<Case> lstCaseAtual = new List<Case>();
            List<Case> lstCaseOld = new List<Case>();
            Account conta = TestDataFactory.gerarContaJuridica(true);
            Contact contato = TestDataFactory.gerarContato(true, conta);
            Entitlement direito = new Entitlement();
            direito.Name = 'CASOREJEITADO3HORAS';
            direito.AccountId = conta.Id;
            insert direito;
            Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
            Case casoOld = CaseDAO.getInstance().buscarPorId(caso.Id);
            caso.MotivoTxt__c = 'Teste';
            caso.SubmotivoTxt__c = 'Teste';
            caso.Status = 'REJEITADO';
            caso.MotivoRejeicao__c = 'DADOS INCOMPLETOS';
            TestDataFactory.gerarTabulacao(true);
            lstCaseAtual.add(caso);
            lstCaseOld.add(casoOld);
            Map<Id,Case> mapCase = new Map<Id,Case>();
            mapCase.put(casoOld.id, casoOld);
            CaseBO.getInstance().definirSLA(lstCaseAtual,mapCase);
            Test.stopTest();
        }

    }
    
}