@isTest
public with sharing class CadastrarContaTest {
    public CadastrarContaTest() {}
    
    @testSetup static void setup() {
        
        Account acc = new Account();
        acc = TestDataFactory.gerarContaJuridica(true);
        Contact con = new Contact();
        con = TestDataFactory.gerarContato(false, acc);
        con.cpf__c = '04188351012';
        insert con;

    }

    @isTest 
    public static void buscarContaTest(){
        Test.startTest();
            List<String> cnpjList = new List<String>();
            String cnpj = TestDataFactory.gerarCNPJ(false);
            cnpjList.add(cnpj);

            CadastrarConta.buscarConta(cnpjList);
        Test.stopTest();
    }

    @isTest 
    public static void buscarContaPJTest(){
        Test.startTest();
            String cnpj = TestDataFactory.gerarCNPJ(false);
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('CNPJ');
            mock.setStatusCode(200);
            mock.setHeader('Content-Type', 'application/json;charset=UTF-8');

            Test.setMock(HttpCalloutMock.class, mock);
            CadastrarConta.buscarContaPJ(cnpj);
        Test.stopTest();
    }
    
    @isTest 
    public static void buscarContaPFTest(){

        Test.startTest();
            CadastrarContaService.ConsultaContato teste = new CadastrarContaService.ConsultaContato();
            teste.cpf = '04188351012';
            String cpf = TestDataFactory.gerarCPF(false);
            List<String> listCpf = new List<String>();
            listCpf.add(cpf);
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('CPF');
            mock.setStatusCode(200);
            mock.setHeader('Content-Type', 'application/json;charset=UTF-8');

            Test.setMock(HttpCalloutMock.class, mock);
            CadastrarConta.buscarContaPF(cpf);
            CadastrarContaService.buscarContaPessoaFisica(cpf);
        Test.stopTest();
    }

    
    @isTest
    public static void buscarContatoTest()
    {
        Test.startTest();
            List<String> cpfList = new List<String>();
            String cpf = TestDataFactory.gerarCPF(false);
            cpfList.add(cpf);
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('CPF');
            mock.setStatusCode(200);
            mock.setHeader('Content-Type', 'application/json;charset=UTF-8');

            Test.setMock(HttpCalloutMock.class, mock);
            CadastrarContato.buscarContato(cpfList);
            
        Test.stopTest();
    }
    @isTest
    public static void buscarContatoRelacoonadoTest()
    {     
		        
        Test.startTest();
        
        List<String> cpfList = new List<String>();
            String cpf = TestDataFactory.gerarCPF(false);
            cpfList.add('04188351012');
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('CPF');
            mock.setStatusCode(200);
            mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
            Test.setMock(HttpCalloutMock.class, mock);
           
            CadastrarContaService.buscarConsultaContato(cpfList);
        
          Test.stopTest();

    }
}