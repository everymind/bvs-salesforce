public class ContactDAO {
    private final static ContactDAO instance = new ContactDAO();

    private ContactDAO() { }

    public static ContactDAO getInstance(){
        return instance;
    }

    public Contact buscarPorId(Id idContato){
        List<Contact> lstContato = buscarPorId(new Set<Id> { idContato });

        if (lstContato.isEmpty()){
            return null;
        } else {
            return lstContato[0];
        }
    }

    public Contact buscarPorPersonAccount(Id idConta){
        List<Contact> lstContato = [SELECT Id, Email FROM Contact WHERE AccountId = :idConta];

        if (lstContato.isEmpty()){
            return null;
        } else {
            return lstContato[0];
        }
    }
    
    public Contact buscarPorContato (Id idContato){
        List<Contact> lstContato = [SELECT Id, Email FROM Contact WHERE Id = :idContato];

        if (lstContato.isEmpty()){
            return null;
        } else {
            return lstContato[0];
        }
    }

    public Contact buscarRelacionamentoPorContaContato(Id conta, Id contato){
        List<Contact> lstContato = [SELECT Email, OperadorAc__c FROM Contact WHERE id = :contato and AccountId=:conta limit 1];

        if (lstContato.isEmpty()){
            return null;
        } else {
            return lstContato[0];
        }
    }

    public List<Contact> buscarPorId(Set<Id> setIdContato){
        return [
            SELECT
                Id,
                BairroComercial__c,
                Birthdate,
                Cargo__c,
                ComplementoComercial__c,
                CPF__c,
                DataLimiteBloqueio__c,
                Email,
                FirstName,
                LastName,
                MailingCity,
                MailingPostalCode,
                MailingState,
                MailingStreet,
                Name,
                NumeroComercial__c,
                Phone,
                HabilitarPesquisa__c

            FROM
                Contact
            WHERE
                Id IN :setIdContato
        ];
	}
	
	public Contact buscarContatoFavorito(Id idConta) {
		List<Contact> lst = [
			SELECT Id, ContatoFavorito__c, MailingStreet, BairroComercial__c, NumeroComercial__c, ComplementoComercial__c, MailingCity, MailingState, MailingPostalCode
			FROM Contact
			WHERE AccountId = :idConta
			AND ContatoFavorito__c = true
		];
		return !lst.isEmpty() ? lst.get(0) : null;
	}

    public Id buscarIdPorConta(Id idConta){
        List<Contact> lstContato = [SELECT
                Id
            FROM
                Contact
            WHERE
                AccountId= :idConta
        ];
        if (lstContato.isEmpty()){
            return null;
        } else {
            return lstContato[0].Id;
        }
	}
	

}