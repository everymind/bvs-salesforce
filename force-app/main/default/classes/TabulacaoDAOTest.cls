@IsTest 
public with sharing class TabulacaoDAOTest {

    @isTest
    private static void testBuscarFila(){
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){

        Test.startTest();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
        caso.MotivoTxt__c = 'Teste';
        caso.SubMotivoTxt__c = 'Teste';
        update caso;

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(true);

        List<TabulacaoSLA__c> tabulacao1 = TabulacaoDAO.getInstance().buscarFila(
                                                                            'CSMAtendimento', 
                                                                            caso.MotivoTxt__c, 
                                                                            caso.SubMotivoTxt__c, 
                                                                            null, 
                                                                            null
                                                                            );
        Test.stopTest();

        System.assertEquals(tabulacao.ProximaFila__c, tabulacao1[0].ProximaFila__c);
        }
    }

    @isTest
    private static void testRequerAprovacao(){
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){

        Test.startTest();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
        caso.MotivoTxt__c = 'Teste';
        caso.SubMotivoTxt__c = 'Teste';
        update caso;

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(true);
        tabulacao.RequerAprovacao__c = true;
        update tabulacao;

        Boolean tabulacao1 = TabulacaoDAO.getInstance().RequerAprovacao(
                                                                    'CSMAtendimento', 
                                                                    caso.MotivoTxt__c, 
                                                                    caso.SubMotivoTxt__c, 
                                                                    null, 
                                                                    null
                                                                    );
        Test.stopTest();

        System.assertEquals(tabulacao.RequerAprovacao__c, tabulacao1);
        }
    }

    @isTest
    private static void testDadosAprovacao(){
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){

        Test.startTest();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
        caso.MotivoTxt__c = 'Teste';
        caso.SubMotivoTxt__c = 'Teste';
        update caso;

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(true);
        tabulacao.RequerAprovacao__c = true;
        tabulacao.Observacoes__c = 'Teste';
        update tabulacao;

        TabulacaoSLA__c tabulacao1 = TabulacaoDAO.getInstance().dadosAprovacao(
                                                                            'CSMAtendimento', 
                                                                            caso.MotivoTxt__c, 
                                                                            caso.SubMotivoTxt__c, 
                                                                            null, 
                                                                            null
                                                                            );
        Test.stopTest();

        System.assertEquals(tabulacao.RequerAprovacao__c, tabulacao1.RequerAprovacao__c);
        System.assertEquals(tabulacao.Observacoes__c, tabulacao1.Observacoes__c);
        System.assertEquals(tabulacao.MotivoTxt__c, tabulacao1.MotivoTxt__c);
        System.assertEquals(tabulacao.SubMotivoTxt__c, tabulacao1.SubMotivoTxt__c);
        System.assertEquals(tabulacao.SubMotivo2Txt__c, tabulacao1.SubMotivo2Txt__c);
        System.assertEquals(tabulacao.TipoCasoTxt__c, tabulacao1.TipoCasoTxt__c);
        }
    }

    @isTest
    private static void testDadosAprovacaoNulo(){
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){

        Test.startTest();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
        caso.MotivoTxt__c = 'Teste';
        caso.SubMotivoTxt__c = 'Teste';
        update caso;

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(true);
        tabulacao.RequerAprovacao__c = true;
        tabulacao.Observacoes__c = 'Teste';
        tabulacao.MeioAcessoTxt__c = 'Teste';
        update tabulacao;

        TabulacaoSLA__c tabulacao1 = TabulacaoDAO.getInstance().dadosAprovacao(
                                                                            'CSMAtendimento', 
                                                                            caso.MotivoTxt__c, 
                                                                            caso.SubMotivoTxt__c, 
                                                                            null, 
                                                                            null
                                                                            );
        Test.stopTest();

        System.assertEquals(tabulacao1, null);
        }
    }

    @isTest
    private static void testBuscarTempoSLA(){
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){

        Test.startTest();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        
        Entitlement direito = new Entitlement();
        direito.Name = 'Teste';
        direito.AccountId = conta.Id;
        insert direito;

        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
        caso.MotivoTxt__c = 'Teste';
        caso.SubMotivoTxt__c = 'Teste';
        caso.MeioAcessoTxt__c = 'Teste';
        caso.FilaAtual__c = 'Teste';
        caso.ProximaFila__c = 'Teste1';
    
        update caso;

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(true);
        tabulacao.RequerAprovacao__c = true;
        tabulacao.Observacoes__c = 'Teste';
        tabulacao.MeioAcessoTxt__c = 'Teste';
        tabulacao.FilaAtual__c = 'Teste';
        tabulacao.ProximaFila__c = 'Teste1';
        update tabulacao;

        TabulacaoSLA__c tabulacao1 = TabulacaoDAO.getInstance().buscarTempoSLA(
                                                                            'CSMAtendimento', 
                                                                            caso.MotivoTxt__c, 
                                                                            caso.SubMotivoTxt__c, 
                                                                            null, 
                                                                            caso.MeioAcessoTxt__c, 
                                                                            caso.FilaAtual__c, 
                                                                            caso.ProximaFila__c
                                                                            );

        TabulacaoSLA__c tabulacao2 = TabulacaoDAO.getInstance().buscarTempoSLA(
                                                                            'CSMAtendimento', 
                                                                            caso.MotivoTxt__c, 
                                                                            caso.SubMotivoTxt__c, 
                                                                            null, 
                                                                            caso.MeioAcessoTxt__c, 
                                                                            caso.FilaAtual__c
                                                                            );
        Test.stopTest();

        System.assertEquals(tabulacao.TempoSLA__c, tabulacao1.TempoSLA__c);
        System.assertEquals(tabulacao.TempoSLA__c, tabulacao2.TempoSLA__c);
        }
    }

    @isTest
    private static void testListaMotivoTipoRegistro(){
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){

        Test.startTest();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
        caso.MotivoTxt__c = 'Teste';
        caso.SubMotivoTxt__c = 'Teste';
        update caso;

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(true);
        tabulacao.RequerAprovacao__c = true;
        tabulacao.Observacoes__c = 'Teste';
        tabulacao.MeioAcessoTxt__c = 'Teste';
        update tabulacao;
        List<String> Motivo = new List<String>();
        List<String> Submotivo = new List<String>();
        motivo.add('teste');
        submotivo.add('teste');
        List<String> Fila = new List<String>();

        TabulacaoDAO.getInstance().listaCodigoDeCliente('00002');
        TabulacaoDAO.getInstance().listaSubMotivo2TipoRegistro('CSMAtendimento', 'teste', 'teste');
        TabulacaoDAO.getInstance().listaMeioAcessoTipoRegistro('CSMAtendimento', 'teste', 'teste');
        TabulacaoDAO.getInstance().listaTabulacao('CSMAtendimento');
        TabulacaoDAO.getInstance().buscarTempoSLAByList('CSMAtendimento',Motivo,Submotivo,Fila,Fila,Fila);
        List<AggregateResult> tabulacao1 = TabulacaoDAO.getInstance().listaMotivoTipoRegistro(
                                                                            'CSMAtendimento'
                                                                            );
        Test.stopTest();

        System.assertEquals(tabulacao1.size(), 1);
        }
    }

    @isTest
    private static void testListaSubMotivoTipoRegistro(){
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){

        Test.startTest();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
        caso.MotivoTxt__c = 'Teste';
        caso.SubMotivoTxt__c = 'Teste';
        update caso;

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(true);
        tabulacao.RequerAprovacao__c = true;
        tabulacao.Observacoes__c = 'Teste';
        tabulacao.MeioAcessoTxt__c = 'Teste';
        update tabulacao;

        List<AggregateResult> tabulacao1 = TabulacaoDAO.getInstance().listaSubMotivoTipoRegistro(
                                                                            'CSMAtendimento',
                                                                            caso.MotivoTxt__c
                                                                            );
        Test.stopTest();

        System.assertEquals(tabulacao1.size(), 1);
        }
    }
}