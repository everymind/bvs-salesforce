@IsTest
public class OpportunityHandlerTest {

    @isTest
    static void opportunitiesCancellationSuccessWithoutAtv(){
        
        // Recupera Id do tipo de registro de pessoa jurídica da conta
		Id tipoRegistroPessoaJuridicaId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId();

        // Cria conta de teste
		Account objAccount = new Account(
			Name = 'Teste Account',
			Carteira__c = 'CALATV',
			CnpjCpf__c = '45406480000123',
            Type = 'Prospect',
			RecordTypeId = tipoRegistroPessoaJuridicaId
		);

		insert objAccount;
        System.debug('Conta incluida: '+objAccount);

        Account accountIncluded = [SELECT Id, Regional__c, Type FROM Account WHERE Name = 'Teste Account' AND CnpjCpf__c = '45406480000123' LIMIT 1];
        System.debug('Conta de teste: '+accountIncluded);

        Opportunity opp = new Opportunity(
            Name = 'Teste de opt aut',
            StageName = 'Proposta',
            CloseDate = date.newInstance(2025,04,22),
            DataInicioFaturamento__c = date.today(),
            TipoReceita__c = 'Pontual',
            NumeroMeses__c = 1,
            AccountId = accountIncluded.Id,
            OwnerId = '0053h000002xsXRAAY',
            CreatedDate = DateTime.now().addDays(-15)
        );

        insert opp;
        System.debug('Oportunidade incluida: '+opp);

        Test.startTest();
        OpportunityHandler batchRun = new OpportunityHandler();
        ID batchprocessid = Database.executeBatch(batchRun);
        Test.stopTest();

        Opportunity opportunityIncluded = [SELECT Id, StageName FROM Opportunity WHERE Name = 'Teste de opt aut' LIMIT 1];

        System.assertEquals('Fechada Perdida', opportunityIncluded.StageName, 'A oportunidade não foi cancelada');
    }

    @isTest
    static void opportunitiesCancellationSuccessWithAtv(){

        // Recupera Id do tipo de registro de pessoa jurídica da conta
		Id tipoRegistroPessoaJuridicaId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId();

        // Cria conta de teste
        Account objAccount = new Account(
            Name = 'Teste Account',
            Carteira__c = 'CALATV',
            CnpjCpf__c = '45406480000123',
            Type = 'Prospect',
            RecordTypeId = tipoRegistroPessoaJuridicaId
        );

        insert objAccount;

        Account accountIncluded = [SELECT Id FROM Account WHERE Name = 'Teste Account' AND CnpjCpf__c = '45406480000123' LIMIT 1];

        Opportunity opp = new Opportunity(
            Name = 'Teste de opt aut',
            StageName = 'Proposta',
            CloseDate = date.newInstance(2025,04,22),
            DataInicioFaturamento__c = date.today(),
            TipoReceita__c = 'Pontual',
            NumeroMeses__c = 1,
            AccountId = accountIncluded.Id,
            OwnerId = '0053h000002xsXRAAY',
            CreatedDate = DateTime.now().addDays(-15)
        );

        insert opp;

        Opportunity opportunityUpdate = [SELECT Id FROM Opportunity WHERE Name LIKE '%Teste de opt aut%' LIMIT 1];

        Task tsk = new Task(
        Subject = 'Teste de Cancelamento de opt',
        WhatId = opportunityUpdate.Id,
        Priority = 'Normal',
        Status = 'Aberta'
        );

        insert tsk;

        Test.startTest();
        OpportunityHandler batchRun = new OpportunityHandler();
        ID batchprocessid = Database.executeBatch(batchRun);
        Test.stopTest();

        Opportunity opportunitySuccessful = [SELECT Id, StageName FROM Opportunity WHERE Name = 'Teste de opt aut' LIMIT 1];
        
        System.assertEquals('Fechada Perdida', opportunitySuccessful.StageName, 'A oportunidade não foi cancelada');
    }
}