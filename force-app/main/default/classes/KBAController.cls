public with sharing class KBAController {
    public KBAController() {}

    @AuraEnabled(cacheable = true)
    public static ResultadoRequisicao PostQuestionsKBA(string CPF)
     {
        //ResultadoRequisicao resultado2;
       system.debug('CPF' + CPF);
        ResultadoRequisicao resultado;
        //ResultadoRequisicao resultado2;
        Http http = new Http();
        string authorization = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6IkJPQVZJU1RBX0FQSSIsIm5iZiI6MTU2NTIwNzE3MCwiZXhwIjoxODgwMjA3MTcwLCJpYXQiOjE1NjUyMDcxNzAsImlzcyI6IkJpZyBEYXRhIENvcnAuIiwicHJvZHVjdHMiOltdLCJkb21haW4iOiJCT0FWSVNUQSJ9.-apSGeQYl889Bi-hxnzrlMAp9TbwJ6faPkmTb9ut83s';
       
        HttpRequest request = new HttpRequest();
        Boolean VerificaCPF = false;
        request.setEndpoint('https://bigid.bigdatacorp.com.br/Questions');
        request.setMethod('POST');
        request.setHeader('Authorization', authorization);
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody('{ "Group\":\"BVS_API\","Parameters":['+
                        ' "CPF='+CPF+'"]}');
            
        HttpResponse response = http.send(request);
        system.debug(resultado);
        system.debug(response.getStatusCode());
        if (response.getStatusCode() == 200)
        {
            System.debug('Resposta da API ' + response.getBody());   
                
        }
        else 
        {
            System.debug('The status code returned was not expected: ' +
            response.getStatusCode() + ' ' + response.getStatus());
                
        }
        system.debug(resultado);
        
        
        //string mockado ='{"DocInfo":{},"EstimatedInfo":{},"TicketId":"628b90ba9e43e896afbd03e8","ResultCode":0,"ResultMessage":"Generated","Questions":[{"Question":"Quantos anos você tem?","CorrectAnswers":null,"Answers":["19","20","21","18","NENHUMA DAS OUTRAS ALTERNATIVAS"]},{"Question":"Qual destas pessoas é da sua família?","CorrectAnswers":null,"Answers":["FERNANDA GONZAGA DORES MACHADO DE SOUZA","MARIA DE FATIMA VIDAL DE SOUZA","LEONARDO DE SALDANHA CRUZA DE SOUZA","YURI SARAIVA FATIMA ALCANTARA DE SOUZA","NENHUMA DAS OUTRAS ALTERNATIVAS"]},{"Question":"Em qual estado seu CPF foi emitido?","CorrectAnswers":null,"Answers":["MG","PA/AM/AC/AP/RO/RR","SP","RJ/ES","NENHUMA DAS OUTRAS ALTERNATIVAS"]}]}';
        resultado = (ResultadoRequisicao)JSON.deserialize(response.getBody(), ResultadoRequisicao.class);
        system.debug(resultado);
        
        /*
        try {
                if(String.isNotBlank(resultado2.TicketId))
                {
                    ApplicationLog__c log = new ApplicationLog__c ();
                    log.ErrorMessage__c = resultado2.TicketId;
                    insert log;
                }
            }
            catch(Exception er)
            {
                ApplicationLog__c log = new ApplicationLog__c ();
                log.ErrorMessage__c = er.getMessage();
                insert log;
            }
            */
        return resultado;
    }


    @AuraEnabled(cacheable = true)
    public static ResultadoRequisicao PostAnswersKBA(string ticket, string[] respostas)
    {
    
       string authorization = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6IkJPQVZJU1RBX0FQSSIsIm5iZiI6MTU2NTIwNzE3MCwiZXhwIjoxODgwMjA3MTcwLCJpYXQiOjE1NjUyMDcxNzAsImlzcyI6IkJpZyBEYXRhIENvcnAuIiwicHJvZHVjdHMiOltdLCJkb21haW4iOiJCT0FWSVNUQSJ9.-apSGeQYl889Bi-hxnzrlMAp9TbwJ6faPkmTb9ut83s';       
       string parameters = '{"TicketId":"'+ticket+'", "Parameters":[';
       for(string r : respostas)
       {
            parameters += ' "'+r+'",';
       }
       parameters = parameters.substring(0, parameters.length() -1);
       parameters += ']}';
       ResultadoRequisicao resultado;
       Http http = new Http();
       HttpRequest request = new HttpRequest();
       request.setEndpoint('https://bigid.bigdatacorp.com.br/Answers');
       request.setMethod('POST');
       request.setHeader('Authorization', authorization);
       request.setHeader('Content-Type', 'application/json;charset=UTF-8');
       request.setBody(parameters);
           
       HttpResponse response = http.send(request);
       resultado = (ResultadoRequisicao)JSON.deserialize(response.getBody(), ResultadoRequisicao.class);
       
       if (response.getStatusCode() == 200)
       {
           System.debug('Resposta da API ' + response.getBody());   
               
       }
       else 
       {
           System.debug('The status code returned was not expected: ' +
           response.getStatusCode() + ' ' + response.getStatus());
               
       }

       return resultado;
       
      /*ResultadoRequisicao resultado2;
       string mockado = '{"DocInfo": {},"EstimatedInfo": {},"TicketId": "629697e99902e6c4533c7e08","ResultCode": 1,"ResultMessage": "Validated","Questions": []}';
       resultado2 = (ResultadoRequisicao)JSON.deserialize(mockado, ResultadoRequisicao.class);
        system.debug(resultado2);
        return resultado2;
        */

   }


   public class Questions {
    @AuraEnabled public String Question {get;set;} 
    @AuraEnabled public Object CorrectAnswers {get;set;} 
    @AuraEnabled public List<String> Answers {get;set;} 
   }
   public class DocInfo {
     String text {get;set;}
   }
   
   public class ResultadoRequisicao {
    @AuraEnabled public DocInfo DocInfo {get;set;} 
    @AuraEnabled public DocInfo EstimatedInfo {get;set;} 
    @AuraEnabled public String TicketId {get;set;} 
    @AuraEnabled public Integer ResultCode {get;set;} 
    @AuraEnabled public String ResultMessage {get;set;} 
    @AuraEnabled public List<Questions> Questions {get;set;} 
   }

   @AuraEnabled(cacheable = true)
   public static string buscaCPF(String id)
   {
        List<Account> listAccount;
       List<Contact> listContact =[
           SELECT id, CPF__c, AccountId
           FROM Contact
           Where Id =: id
       ];
        system.debug(listContact);
       if(listContact[0].CPF__c == null)
       {
            listAccount =[
                SELECT id, CNPJCPF__C
                FROM Account
                 Where Id =: listContact[0].AccountId
            ];
       }
       system.debug(listAccount);


        return listContact[0].CPF__c != null ? listContact[0].CPF__c : listAccount[0].CNPJCPF__C ;

   }

   @AuraEnabled
    public static Decimal getBuscarTempoKBA(string id)
    {
       
         List<Contact> Contact = [
             SELECT Id, Regra_de_Validacao_kba__c
             FROM Contact
             WHERE Id =: id
         ];
         return Contact[0].Regra_de_Validacao_kba__c;
 

    }

}