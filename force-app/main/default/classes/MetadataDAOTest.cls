@isTest
public with sharing class MetadataDAOTest {
    @isTest
    public static void buscarFilaInicialPosVendasTest()
    {
        Test.startTest();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta,contato );
        MetadataDAO.getInstance().buscarFilaInicialPosVendas('TELECOM');
        MetadataDAO.getInstance().buscarFilaN2AtendimentoCanal('TELECOM');
        MetadataDAO.getInstance().buscarFilaInicialAtendimento('PRIME');
        MetadataDAO.getInstance().buscarIdDireito('teste');
        MetadataDAO.getInstance().BuscarTempoPassado(caso.id);
        MetadataDAO.getInstance().buscarPID('ACESSOS', 'CRIAÇÃO CÓDIGO OPERADOR CLIENTE');
        MetadataDAO.ObterTempoDeBloqueioCasoPID();
        MetadataDAO.getInstance().retornaOrigemEmailToCase();
        MetadataDAO.getInstance().retornarFilasIniciaisPosVendas();
        MetadataDAO.getInstance().retornarFilasIniciaisAtendimento();
        MetadataDAO.getInstance().retornaUsuariosCarga();

        MetadataDAO.getInstance().BuscarTempoSLA(caso.id);        
       // MetadataDAO.getInstance().buscarFilaInicialAtendimento('teste');



        JustificavaStatus__c jusStatus = new JustificavaStatus__c ();
        jusStatus.Status__c = 'REJEITADO';
        jusStatus.Caso__c = caso.id;
        jusStatus.Motivo__c = 'ACESSOS';
        insert jusStatus;


        MetadataDAO.getInstance().BuscarJustificavaStatusRejeitado(caso.id);
        Test.stopTest();
    }
}