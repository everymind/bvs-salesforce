/**
 * @Autor: Maycon
 * @Data: 28/03/2022
 * @Descrição: Classe para execução de Batch da automação de cancelamento de oportunidades, que envia as contas relacionadas para o Bolsão Interno
 */
global class ReencarteiramentoContasVencidas implements Database.Batchable<sObject>, Database.Stateful {
	public Boolean processWithError = false;

    /**
	 * @Autor: Maycon
	 * @Data: 28/03/2022
	 * @Descrição: Método para iniciar a execução do Batch com tamanho de lote definido.
	 * @Parâmetros: Integer batchSize - Número que define o tamanho do lote que o batch vai executar.
	 */
    global static void start(Integer batchSize) {
		Database.executeBatch(new ReencarteiramentoContasVencidas(), batchSize);
	}

    /**
	 * @Autor: Maycon
	 * @Data: 28/03/2022
	 * @Descrição: Método herdado da interface que recupera os registros que farão parte do escopo da execução.
	 * @Parâmetros: Database.BatchableContext objContext - Objeto de contexto do batch.
	 */
	global Database.QueryLocator start(Database.BatchableContext objContext) {

		String regionais = getRuleParameter('RegionaisProcessamentoContas');

		String query = 'SELECT Id, Type, Regional__c, Carteira__c, ProcessarContaVencida__c, ProcessarReencarteiramento__c FROM Account WHERE ProcessarContaVencida__c = true AND Type = \'Prospect\' AND Regional__c IN ('+regionais+')';
		System.debug('PROCESSAMENTO DAS OPORTUNIDADES ABERTAS: ' + query);
		
		return Database.getQueryLocator(query);
	}

    /**
	 * @Autor: Maycon
	 * @Data: 23/03/2022
	 * @Descrição: Método herdado da interface que executa a lógica de negócio baseados nos registros do escopo.
	 * @Parâmetros: Database.BatchableContext objContext - Objeto de contexto do batch.
	 * @Parâmetros: List<Account> lstSObjAccount - Lista de escopo retornada no método start.
	 */
	global void execute(Database.BatchableContext objContext, List<Account> lstSObjAccount) {

        if(!lstSObjAccount.isEmpty()){

            String carteiraDestinoProspect = getRuleParameter('CarteiraProspect');
            String carteiraDestinoCliente = getRuleParameter('CarteiraCliente');
			String regra1 = getRuleParameter('RegraUm');
			String regra2 = getRuleParameter('RegraDois');
			String regionalRegra2 = getRuleParameter('RegionaisRegraDois');

            for(Account objAccount : lstSObjAccount){

				if(objAccount.Regional__c <> regionalRegra2){
					//Atualiza campos necessários para o cancelamento da oportunidade
					if(objAccount.Type == 'Prospect'){
						objAccount.Carteira__c = carteiraDestinoProspect;
					}else{
						objAccount.Carteira__c = carteiraDestinoCliente;
					}
					objAccount.ProcessarContaVencida__c = false;
					objAccount.ProcessarReencarteiramento__c = true;
				}
            }
        }

        //Atualiza as contas na base
		System.debug('Contas a serem reencarteiradas: ' + lstSObjAccount);
		Database.update(lstSObjAccount, false);
    }

    /**
	 * @Autor: Maycon
	 * @Data: 28/03/2022
	 * @Descrição: Método herdado da interface que é executa ao final da execução de cada lote.
	 */
	global void finish(Database.BatchableContext objContext) {
		//enviarEmail(processoComErro);
	}

    /**
	* @Autor: Maycon
	* @Data: 28/03/2022
	* @Descrição: Método que recebe o valor configurado nos metadados da Automação de Oportunidades.
	*/
    private static String getRuleParameter(String metadataValue) {

		ConfiguracaoAutomacaoOportunidade__mdt getDaysToCancelParameter = 	ConfiguracaoAutomacaoOportunidade__mdt.getInstance(metadataValue);
		String daysToCancel = getDaysToCancelParameter.Valor__c;

		return daysToCancel;	
    }
}