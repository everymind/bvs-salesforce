/**
 * @Autor: Maycon
 * @Data: 29/03/2022
 * @Descrição: Classe de teste que envia as contas relacionadas para o Bolsão Interno
 */
@IsTest
public class ReencarteiramentoContasVencidasTest{

    @isTest
    static void reencarteraContasProspect(){
        // Recupera Id do tipo de registro de pessoa jurídica da conta
        Id tipoRegistroPessoaJuridicaId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId();

        // Cria conta de teste
        Account objAccount = new Account(
            Name = 'Teste Account - Vencida - Prospect',
            Carteira__c = 'CALATV',
            CnpjCpf__c = '45406480000123',
            RecordTypeId = tipoRegistroPessoaJuridicaId,
            Type = 'Prospect',
            ProcessarContaVencida__c = true
        );

        insert objAccount;

        Test.startTest();
        ReencarteiramentoContasVencidas batchRun = new ReencarteiramentoContasVencidas();
        ID batchprocessid = Database.executeBatch(batchRun);
        Test.stopTest();

        Account accountIncluded = [SELECT Id, Carteira__c FROM Account WHERE Name = 'Teste Account - Vencida - Prospect' LIMIT 1];

        System.assertEquals(accountIncluded.Carteira__c, 'BOPROS', 'a conta não foi reencarteirada');
    }
}