public class OpportunityContactRoleDAO {
    private final static OpportunityContactRoleDAO instance = new OpportunityContactRoleDAO();

    private OpportunityContactRoleDAO() { }

    public static OpportunityContactRoleDAO getInstance(){
        return instance;
    }

    public List<OpportunityContactRole> buscarPorOportunidade(Id idOportunidade){
        return [
            SELECT
                Id,
                ContactId,
                OpportunityId,
                Role
            FROM
                OpportunityContactRole
            WHERE
                OpportunityId = :idOportunidade
        ];
    }
}