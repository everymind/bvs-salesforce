@IsTest
private class ContactBOTest{

    static testMethod void afterUpdateTest(){

        Contact objContact = new Contact(
            FirstName = 'Test',
            LastName = 'Automatic',
            Status__c = 'Ativo',
            AccountId = '0013h00001kJHgLAAW',
            OwnerId = '0053h000002xsSaAAI',
            Email = 'test.automatic@test.com'
        );
        insert objContact;

        Contact insertedContact = [Select Id, AccountId, OwnerId, Status__c from Contact where Email = 'test.automatic@test.com'];

        Test.startTest();

        Test.enableChangeDataCapture();

        Contact updatedContact = new Contact(
            Id = insertedContact.Id,
            Status__c = 'Inativo'
        );
        update updatedContact;

        Test.getEventBus().deliver();
        Test.enableChangeDataCapture();
        Test.stopTest();

        Contact resultContact = [SELECT Id, AccountId, OwnerId, Status__c FROM Contact WHERE Id =: updatedContact.Id LIMIT 1];
        System.assertEquals(resultContact.Status__c, 'Inativo');
    }
}