public class EventBO {
    
    private static final EventBO instance = new EventBO();

	public static EventBO getInstance() {
		return instance;
    }
    
    public void preencheObjeto(List<Event> lstEvent){
	
		        
        List<Event> atualizaEventos = new List<Event>();
		List<Event> eventos = new List<Event>();
		//eventos = [SELECT Id, WhatId, WhoId FROM Event Where (WhatId != null OR WhoId != null) AND IsChild = false LIMIT 1];
        for(Event evento : lstEvent) {
            if(evento.WhatId != null && evento.WhoId == null){
            	if(evento.WhatId.getsObjectType() == Account.sObjectType){
                	system.debug('sim eu sou uma conta');
                    Event atualizaevento = new Event();
                    atualizaevento.Id = evento.Id;
                    atualizaevento.Conta__c = evento.WhatId;
                    atualizaEventos.add(atualizaevento);
            	}  
                if(evento.WhatId.getsObjectType() == Opportunity.sObjectType){
                	system.debug('sim eu sou uma Oportunidade');
                    Event atualizaevento = new Event();
                    atualizaevento.Id = evento.Id;
                    atualizaevento.Oportunidade__c = evento.WhatId;                 
                    Opportunity EventAccount = [SELECT AccountId FROM Opportunity Where ID =: evento.WhatId]; 
                    atualizaevento.Conta__c = EventAccount.AccountId;
                    atualizaEventos.add(atualizaevento);
            	}
            }
            
		}
        for(Event evento : lstEvent) {
            if(evento.WhoId != null && evento.WhatId == null){
               if(evento.WhoId.getsObjectType() == Contact.sObjectType){
                	system.debug('sim eu sou um contato');
                    Event atualizaevento = new Event();
                    atualizaevento.Id = evento.Id;
                    atualizaevento.Contato__c = evento.WhoId;
                    contact EventAccount = [SELECT AccountId FROM contact Where ID =: evento.WhoId];  
                    atualizaevento.conta__c = EventAccount.AccountId;
                    atualizaEventos.add(atualizaevento);
            	}  
                
            }
            if(evento.WhoId != null && evento.WhatId != null){
               if(evento.WhoId.getsObjectType() == Contact.sObjectType && evento.WhatId.getsObjectType() == Account.sObjectType){
                	system.debug('Sou um contato e uma conta');
                    Event atualizaevento = new Event();
                    atualizaevento.Id = evento.Id;
                    atualizaevento.Contato__c = evento.WhoId;
                   	atualizaevento.Conta__c = evento.WhatId;
                    atualizaEventos.add(atualizaevento);
            	}  
                if(evento.WhoId.getsObjectType() == Contact.sObjectType && evento.WhatId.getsObjectType() == Opportunity.sObjectType){
                	system.debug('Sou um contato e uma Oportunidade');
                    Event atualizaevento = new Event();
                    atualizaevento.Id = evento.Id;
                    atualizaevento.Contato__c = evento.WhoId;
                    atualizaevento.Oportunidade__c = evento.WhatId;
                    Opportunity  EventAccount = [SELECT AccountId FROM Opportunity Where ID =: evento.WhatId];  
                    atualizaevento.conta__c = EventAccount.AccountId;
                    atualizaEventos.add(atualizaevento);
            	}
                
            }
            
        }
        system.debug('atualizaEventos' + atualizaEventos);
        if (atualizaEventos.size() > 0){
            update atualizaEventos;
        }        
        
    }
}