@isTest
private class SelecaoCampanhaTOTest {
	
	@isTest static void test_method_one() {

		Test.startTest();

		
		SelecaoCampanhaTO slcTO = new SelecaoCampanhaTO();
		slcTO.sucesso = true;
		slcTO.origin = 'TESTE';
		slcTO.host = 'TESTE';
		slcTO.params = 'TESTE';
        slcTO.url = 'TESTE';

        slcTO.token = 'TESTE';
		slcTO.sfId = 'TESTE';
		slcTO.oportunidade = 'TESTE';
        slcTO.conta = 'TESTE';
        slcTO.cnpj = 'TESTE';
        slcTO.codigo = 'TESTE';
        slcTO.carteira = 'TESTE';
        slcTO.canal_territorio = 'TESTE';
        slcTO.item_receita = 'TESTE';
        slcTO.regional = 'TESTE';
        slcTO.regra_atual = 'TESTE';

		Test.stopTest();


	}
	
	
}