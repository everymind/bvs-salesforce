/**
 * @Autor: Everymind
 * @Data: 21/08/2020
 * @Descrição: Classe que contém métodos de negócio referente ao domínio Account.
 */
public class AccountBO {
	
	/**
	 * @Autor: Everymind
	 * @Data: 21/08/2020
	 * @Descrição: Constante que representa uma instância da classe.
	 */
	private static final AccountBO instance = new AccountBO();

	/**
	 * @Autor: Everymind
	 * @Data: 21/08/2020
	 * @Descrição: Método público que retorna uma instância da classe.
	 */
    public static AccountBO getInstance() {
        return instance;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 21/08/2020
	 * @Descrição: Método que atribui o novo registro ao processo de reencarteiramento
	 * @Parâmetro: List<UserTerritory2Association> lstUserTerritory2AssociationNovas - Lista que contém a instância dos registros de contas após a criação.
	 */
	public void atualizarDadosTerritorio(List<Account> lstAccountNovas){
		atualizarDadosTerritorioFuture(JSON.serialize(lstAccountNovas));
    }
    
    /**
	 * @Autor: Everymind
	 * @Data: 21/08/2020
	 * @Descrição: Método que atribui o novo registro ao processo de reencarteiramento
	 * @Parâmetro: List<UserTerritory2Association> lstUserTerritory2AssociationNovas - Lista que contém a instância dos registros de contas após a criação.
	 */
	public void atualizarDadosTerritorio(Map<Id, Account> mapSObjNewAccounts, Map<Id, Account> mapSObjOldAccounts){
        List<Account> lstAccount = new List<Account>();

        for(Account objNewAccount : mapSObjNewAccounts.values()) {
			Account objOldAccount = mapSObjOldAccounts.get(objNewAccount.Id);

			// Verifica se a Data da conclusão foi preenchida
			if(objNewAccount.Carteira__c != objOldAccount.Carteira__c) {
				lstAccount.add(objNewAccount);
			}
		}

		atualizarDadosTerritorioFuture(JSON.serialize(lstAccount));
	}

	/**
	 * @Autor: Everymind
	 * @Data: 21/08/2020
	 * @Descrição: Método que atribui o novo registro ao processo de reencarteiramento mas com chamada Futura
	 * @Parâmetro: String jsonLstUserTerritory2AssociationNovas - Lista que contém a instância dos registros de contas após a criação.
	 */
    public static void atualizarDadosTerritorioFuture(String jsonLstAccountNovas){
		List<Account> lstAccountNovas = (List<Account>)JSON.deserialize(jsonLstAccountNovas, List<Account>.class);

        List<Account> lstContasAlteradas = new List<Account>();
		Set<String> setCarteiras = new Set<String>();
		Set<Id> setTerritory2Id = new Set<Id>();
		Set<Id> setUserId = new Set<Id>();

        for (Account objAccount : lstAccountNovas){

			setCarteiras.add(objAccount.Carteira__c);
		}

		if (setCarteiras.size() > 0){
            List<Territory2> lstTerritory2 = [SELECT Id, DeveloperName, Canal__c, Canal_Agrupado__c, Regional__c FROM Territory2 WHERE DeveloperName IN :setCarteiras];
            Map<String,Territory2> mapTerritory2 = new Map<String, Territory2>();
            for(Territory2 territorio : lstTerritory2){
                mapTerritory2.put(territorio.DeveloperName, territorio);
            }

            List<UserTerritory2Association> lstUserTerritory2Association = [SELECT Id, UserId, Territory2.DeveloperName, RoleInTerritory2 FROM UserTerritory2Association WHERE RoleInTerritory2 = 'Proprietário' AND Territory2.DeveloperName IN :setCarteiras];
            Map<String,UserTerritory2Association> mapUserTerritory2Association = new Map<String, UserTerritory2Association>();
            for(UserTerritory2Association userTerritory2Association : lstUserTerritory2Association){
				mapUserTerritory2Association.put(userTerritory2Association.Territory2.DeveloperName, userTerritory2Association);
				setUserId.add(userTerritory2Association.UserId);
			}
			
			List<User> lstUser = [SELECT Id, Name, Username, IsActive FROM User WHERE Id IN :setUserId];
			Map<String,User> mapUser = new Map<String, User>();
            for(User user : lstUser){
				mapUser.put(user.Id, user);
			}

			List<ConfiguracaoDadosTerritorio__mdt> configuracaoDadosTerritorio = [SELECT Id, DeveloperName, MasterLabel FROM ConfiguracaoDadosTerritorio__mdt WHERE DeveloperName = 'CarteiraVaga'];

			if (lstTerritory2.size() > 0){
				for (Account objAccount : lstAccountNovas){
                    Id proprietarioId = null;
                    if (mapUserTerritory2Association.containsKey(objAccount.Carteira__c)){
						
						if (mapUser.containsKey(mapUserTerritory2Association.get(objAccount.Carteira__c).UserId)){
							System.debug(mapUser.get(mapUserTerritory2Association.get(objAccount.Carteira__c).UserId));
							proprietarioId = mapUserTerritory2Association.get(objAccount.Carteira__c).UserId;
						}
                    }
                    if (mapTerritory2.containsKey(objAccount.Carteira__c)){
                        Territory2 territorio = mapTerritory2.get(objAccount.Carteira__c);

                        Account contaAlterada = new Account();
                        contaAlterada.Id = objAccount.Id;
                        contaAlterada.Canal__c = territorio.Canal__c;
                        contaAlterada.CanalAgrupado__c = territorio.Canal_Agrupado__c;
                        contaAlterada.Regional__c = territorio.Regional__c;
                        if (proprietarioId != null){
                            contaAlterada.OwnerId = proprietarioId;
						}else{
							contaAlterada.OwnerId = configuracaoDadosTerritorio[0].MasterLabel;
						}
                        lstContasAlteradas.add(contaAlterada);
                    }
                }
            }
            
            if (!lstContasAlteradas.isEmpty()){
				update lstContasAlteradas;
			}
		}
    }

	public static void tratarPermissaoDeCriacaoDeCasos(Id contactId, Boolean PermiteCriar) {

		Integer tempoBloqueioMinutos = 30;
		String tempoConfigurado = MetadataDAO.ObterTempoDeBloqueioCasoPID();

		if (string.isNotBlank(tempoConfigurado)) {

			try {
				tempoBloqueioMinutos = Integer.valueOf(tempoConfigurado);	
			}
			catch (Exception ex) {
				System.debug('Erro na conversão do valor configurado: ' + tempoConfigurado);
			}
			
		}

		

		Contact contact = [SELECT  DataLimiteBloqueio__c FROM Contact WHERE Id = :contactId];

		if (contact != null) {

			if (PermiteCriar) {
				
				contact.DataLimiteBloqueio__c = null;				
			} else {
				
				contact.DataLimiteBloqueio__c = Datetime.now().addMinutes(tempoBloqueioMinutos);
			}
			try{
				update contact;	
			}catch(Exception ex){
				system.debug(ex.getMessage());
			}
				
		}		
	}

}