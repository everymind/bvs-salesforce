@isTest
private class CollectionUtilTest {

    @testSetup
    private static void setup() {
		List<Account> lstAccounts = new List<Account>();
		
		// Recupera Id do tipo de registro de pessoa jurídica da conta
		Id tipoRegistroPessoaJuridicaId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId();

		lstAccounts.add(
			new Account(
				Name = 'Test Account1', 
				CnpjCpf__c = '30539030000131', 
				NumberOfEmployees = 10,
				RecordTypeId = tipoRegistroPessoaJuridicaId,
				BillingStreet = 'Rua teste, 100',
				BillingCity = 'São Paulo',
				BillingState = 'SP',
				BillingPostalCode = '09700-000',
				BillingCountry = 'Brasil',
				NumeroFaturamento__c = '1000'
			)
		);
		lstAccounts.add(
			new Account(
				Name = 'Test Account2', CnpjCpf__c = '35363831000102', NumberOfEmployees = 10,
				RecordTypeId = tipoRegistroPessoaJuridicaId,
				BillingStreet = 'Rua teste, 100',
				BillingCity = 'São Paulo',
				BillingState = 'SP',
				BillingPostalCode = '09700-000',
				BillingCountry = 'Brasil',
				NumeroFaturamento__c = '1000'
			)
		);
		lstAccounts.add(
			new Account(
				Name = 'Test Account3', CnpjCpf__c = '79996464000150', NumberOfEmployees = 10,
				RecordTypeId = tipoRegistroPessoaJuridicaId,
				BillingStreet = 'Rua teste, 100',
				BillingCity = 'São Paulo',
				BillingState = 'SP',
				BillingPostalCode = '09700-000',
				BillingCountry = 'Brasil',
				NumeroFaturamento__c = '1000'
			)
		);

        insert lstAccounts;
    }
    
    @isTest 
    private static void testCarregarSet() {
        List<Account> lstAccounts = [Select Id, CreatedDate, NumberOfEmployees, Name FROM Account];
        
        Set<Id> setIds = new Set<Id>();
        CollectionUtil.carregarSet(setIds, 'Id', lstAccounts);
        System.assert(setIds.contains(lstAccounts[0].Id));

        Set<Integer> setInteger = new Set<Integer>();
        CollectionUtil.carregarSet(setInteger, 'NumberOfEmployees', lstAccounts);
        System.assert(setInteger.contains(lstAccounts[0].NumberOfEmployees));

        Set<String> setString = new Set<String>();
        CollectionUtil.carregarSet(setString, 'Name', lstAccounts);
        System.assert(setString.contains(lstAccounts[0].Name));
    }

    @isTest 
    private static void testCarregarMap() {
        List<Account> lstAccounts = [Select Id, CreatedDate, NumberOfEmployees, Name, Owner.Name FROM Account];

        Map<Id, Account> mapIdAccount = new Map<Id, Account>();
        CollectionUtil.carregarMap(mapIdAccount, 'Id', lstAccounts);
        System.assert(mapIdAccount.containsKey(lstAccounts[0].Id));

        Map<Integer, Account> mapIntegerAccount = new Map<Integer, Account>();
        CollectionUtil.carregarMap(mapIntegerAccount, 'NumberOfEmployees', lstAccounts);
        System.assert(mapIntegerAccount.containsKey(lstAccounts[0].NumberOfEmployees));

        Map<String, Account> mapStringAccount = new Map<String, Account>();
        CollectionUtil.carregarMap(mapStringAccount, 'Name', lstAccounts);
        System.assert(mapStringAccount.containsKey(lstAccounts[0].Name));

        Map<String, Account> mapParentAccount = new Map<String, Account>();
        CollectionUtil.carregarMap(mapParentAccount, 'Owner.Name', lstAccounts);
        System.assert(mapParentAccount.containsKey(lstAccounts[0].Owner.Name));

        Map<String, Account> mapCompoundKey = new Map<String, Account>();
        CollectionUtil.carregarMap(mapCompoundKey, new Set<String>{ 'Id', 'Name' }, lstAccounts);
        System.assert(mapCompoundKey.containsKey(lstAccounts[0].Id + '_' + lstAccounts[0].Name));
    }

    @isTest 
    private static void testCarregarMapList() {
        List<Account> lstAccounts = [Select Id, CreatedDate, NumberOfEmployees, Name FROM Account];

        Map<Id, List<Account>> mapIdListAccount = new Map<Id, List<Account>>();
        CollectionUtil.carregarMap(mapIdListAccount, 'Id', lstAccounts);
        System.assert(mapIdListAccount.containsKey(lstAccounts[0].Id));

        Map<Integer, List<Account>> mapIntegerListAccount = new Map<Integer, List<Account>>();
        CollectionUtil.carregarMap(mapIntegerListAccount, 'NumberOfEmployees', lstAccounts);
        System.assert(mapIntegerListAccount.containsKey(lstAccounts[0].NumberOfEmployees));

        Map<String, List<Account>> mapStringListAccount = new Map<String, List<Account>>();
        CollectionUtil.carregarMap(mapStringListAccount, 'Name', lstAccounts);
        System.assert(mapStringListAccount.containsKey(lstAccounts[0].Name));

        Map<String, List<Account>> mapCompoundKey = new Map<String, List<Account>>();
        CollectionUtil.carregarMap(mapCompoundKey, new Set<String>{ 'Id', 'Name' }, lstAccounts);
        System.assert(mapCompoundKey.containsKey(lstAccounts[0].Id + '_' + lstAccounts[0].Name));
    }

    @isTest
    private static void testCarregarMapValue() {
        List<Account> lstAccounts = [Select Id, CreatedDate, NumberOfEmployees, AnnualRevenue, Name FROM Account];

        Map<String, String> mapNameByName = new Map<String, String>();
        CollectionUtil.carregarMap(mapNameByName, 'Name', 'Name', lstAccounts);
        System.assert(mapNameByName.containsKey(lstAccounts[0].Name));

        Map<Id, String> mapNameById = new Map<Id, String>();
        CollectionUtil.carregarMap(mapNameById, 'Id', 'Name', lstAccounts);
        System.assert(mapNameById.containsKey(lstAccounts[0].Id));

        Map<Id, Integer> mapEmployeesById = new Map<Id, Integer>();
        CollectionUtil.carregarMap(mapEmployeesById, 'Id', 'NumberOfEmployees', lstAccounts);
        System.assert(mapEmployeesById.containsKey(lstAccounts[0].Id));

        Map<Id, Decimal> mapAnnualRevenueById = new Map<Id, Decimal>();
        CollectionUtil.carregarMap(mapAnnualRevenueById, 'Id', 'AnnualRevenue', lstAccounts);
        System.assert(mapAnnualRevenueById.containsKey(lstAccounts[0].Id));

        Map<String, Integer> mapEmployeesByName = new Map<String, Integer>();
        CollectionUtil.carregarMap(mapEmployeesByName, 'Name', 'NumberOfEmployees', lstAccounts);
        System.assert(mapEmployeesByName.containsKey(lstAccounts[0].Name));

        Map<String, Decimal> mapAnnualRevenueByName = new Map<String, Decimal>();
        CollectionUtil.carregarMap(mapAnnualRevenueByName, 'Name', 'AnnualRevenue', lstAccounts);
        System.assert(mapAnnualRevenueByName.containsKey(lstAccounts[0].Name));
    }

    @isTest 
    private static void testCarregarList() {
    	List<Account> lstAccounts = [Select Id, CreatedDate, NumberOfEmployees, Name FROM Account];

		List<Id> lstAccountIds = new List<Id>();
		CollectionUtil.carregarList(lstAccountIds, 'Id', lstAccounts);
		System.assert(lstAccountIds[0] == lstAccounts[0].Id);

        List<String> lstAccountStrIds = new List<String>();
        CollectionUtil.carregarList(lstAccountStrIds, 'Id', lstAccounts);
        System.assert(lstAccountStrIds[0] == lstAccounts[0].Id);
    }
}