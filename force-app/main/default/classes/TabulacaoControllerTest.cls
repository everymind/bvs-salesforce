@isTest
public class TabulacaoControllerTest {
/*
    @testSetup
    static void setup()
    {
        TabulacaoSLA__c tabulacaoCSMAtendimento = new TabulacaoSLA__c();
        tabulacaoCSMAtendimento.TipoRegistro__c = 'CSMPosVendas';
        tabulacaoCSMAtendimento.MotivoTxt__c = 'FALHA / ERRO';
        tabulacaoCSMAtendimento.SubMotivoTxt__c = 'AVALIE';
        tabulacaoCSMAtendimento.MeioAcessoTxt__c = 'STRING';
        tabulacaoCSMAtendimento.RequerAprovacao__c = false;
        tabulacaoCSMAtendimento.TipoCasoTxt__c = 'TESTE TIPO';
        tabulacaoCSMAtendimento.Observacoes__c = 'TESTE OBSERVAÇÕES';        
        insert tabulacaoCSMAtendimento;

        TabulacaoSLA__c tabulacaoCSMAtendimentoSemMeioDeAcesso = new TabulacaoSLA__c();
        tabulacaoCSMAtendimentoSemMeioDeAcesso.TipoRegistro__c = 'CSMPosVendas';
        tabulacaoCSMAtendimentoSemMeioDeAcesso.MotivoTxt__c = 'FALHA';
        tabulacaoCSMAtendimentoSemMeioDeAcesso.SubMotivoTxt__c = 'AVALIE TESTE';
        tabulacaoCSMAtendimentoSemMeioDeAcesso.MeioAcessoTxt__c = null;
        tabulacaoCSMAtendimentoSemMeioDeAcesso.RequerAprovacao__c = false;
        tabulacaoCSMAtendimentoSemMeioDeAcesso.TipoCasoTxt__c = 'TESTE TIPO';
        tabulacaoCSMAtendimentoSemMeioDeAcesso.Observacoes__c = 'TESTE OBSERVAÇÕES';
        insert tabulacaoCSMAtendimentoSemMeioDeAcesso;
        
        TabulacaoSLA__c tabulacaoITSM = new TabulacaoSLA__c();
        tabulacaoITSM.TipoRegistro__c = 'ITSM';
        tabulacaoITSM.MotivoTxt__c = 'FALHA / ERRO';
        tabulacaoITSM.SubMotivoTxt__c = 'AVALIE';
        tabulacaoITSM.SubMotivo2Txt__c = 'STRING';
        tabulacaoITSM.RequerAprovacao__c = false;
        tabulacaoITSM.TipoCasoTxt__c = 'TESTE TIPO';
        tabulacaoITSM.Observacoes__c = 'TESTE OBSERVAÇÕES';
        insert tabulacaoITSM;
        
        TabulacaoSLA__c tabulacaoITSMSemSubmotivo2 = new TabulacaoSLA__c();
        tabulacaoITSMSemSubmotivo2.TipoRegistro__c = 'ITSM';
        tabulacaoITSMSemSubmotivo2.MotivoTxt__c = 'FALHA';
        tabulacaoITSMSemSubmotivo2.SubMotivoTxt__c = 'AVALIE TESTE';
        tabulacaoITSMSemSubmotivo2.SubMotivo2Txt__c = null;
        tabulacaoITSMSemSubmotivo2.RequerAprovacao__c = false;
        tabulacaoITSMSemSubmotivo2.TipoCasoTxt__c = 'TESTE TIPO';
        tabulacaoITSMSemSubmotivo2.Observacoes__c = 'TESTE OBSERVAÇÕES';
        insert tabulacaoITSMSemSubmotivo2;
    }
    
    @isTest
    static void testaRetornoMotivoSubmotivoEMeioDeAcesso(){
        String recordDeveloperName = 'CSM Pós Vendas';

        Test.startTest();
        List<AggregateResult> motivos = TabulacaoController.returnMotivoList(recordDeveloperName);
        List<AggregateResult> submotivos = TabulacaoController.returnSubmotivoList((String)motivos[1].get('MotivoTxt__c'), recordDeveloperName);
        //List<AggregateResult> meiodeAcessos = TabulacaoController.returnMeioDeAcessoList((String)submotivos[0].get('SubMotivoTxt__c'), recordDeveloperName);
        //String observacoes = TabulacaoController.retornaObservacoes((String)motivos[1].get('MotivoTxt__c'), (String)submotivos[0].get('SubMotivoTxt__c'), (String)meioDeAcessos[0].get('MeioAcessoTxt__c'), recordDeveloperName);
        Test.stopTest();

        System.assertEquals('FALHA / ERRO', (String)motivos[1].get('MotivoTxt__c'));
        System.assertEquals('AVALIE', (String)submotivos[0].get('SubMotivoTxt__c'));
        //System.assertEquals('STRING', (String)meiodeAcessos[0].get('MeioAcessoTxt__c'));
        //System.assertEquals('TESTE OBSERVAÇÕES', observacoes);
    }
    
    @isTest
    static void testaRetornoMotivoSubmotivoSemMeioDeAcesso(){
        String recordDeveloperName = 'CSM Pós Vendas';

        Test.startTest();
        List<AggregateResult> motivos = TabulacaoController.returnMotivoList(recordDeveloperName);
        List<AggregateResult> submotivos = TabulacaoController.returnSubmotivoList((String)motivos[0].get('MotivoTxt__c'), recordDeveloperName);
       // List<AggregateResult> meiodeAcessos = TabulacaoController.returnMeioDeAcessoList((String)submotivos[0].get('SubMotivoTxt__c'), recordDeveloperName);
        //String observacoes = TabulacaoController.retornaObservacoes((String)motivos[0].get('MotivoTxt__c'), (String)submotivos[0].get('SubMotivoTxt__c'), (String)meioDeAcessos[0].get('MeioAcessoTxt__c'), recordDeveloperName);
        Test.stopTest();

        System.assertEquals('FALHA', (String)motivos[0].get('MotivoTxt__c'));
        System.assertEquals('AVALIE TESTE', (String)submotivos[0].get('SubMotivoTxt__c'));
        //System.assertEquals(null, (String)meiodeAcessos[0].get('MeioAcessoTxt__c'));
        //System.assertEquals('TESTE OBSERVAÇÕES', observacoes);
    }

    @isTest
    static void testaRetornoMotivoSubmotivoESubmotivo2(){
        String recordDeveloperName = 'ITSM';

        Test.startTest();
        List<AggregateResult> motivos = TabulacaoController.returnMotivoList(recordDeveloperName);
        List<AggregateResult> submotivos = TabulacaoController.returnSubmotivoList((String)motivos[1].get('MotivoTxt__c'), recordDeveloperName);
        //List<AggregateResult> submotivos2 = TabulacaoController.returnMeioDeAcessoList((String)submotivos[0].get('SubMotivoTxt__c'), recordDeveloperName);
        //String observacoes = TabulacaoController.retornaObservacoes((String)motivos[1].get('MotivoTxt__c'), (String)submotivos[0].get('SubMotivoTxt__c'), (String)submotivos2[0].get('SubMotivo2Txt__c'), recordDeveloperName);
        Test.stopTest();

        System.assertEquals('FALHA / ERRO', (String)motivos[1].get('MotivoTxt__c'));
        System.assertEquals('AVALIE', (String)submotivos[0].get('SubMotivoTxt__c'));
        //System.assertEquals('STRING', (String)submotivos2[0].get('SubMotivo2Txt__c'));
        //System.assertEquals('TESTE OBSERVAÇÕES', observacoes);
    }

    @isTest
    static void testaRetornoMotivoSubmotivoSemSubmotivo2(){
        String recordDeveloperName = 'ITSM';

        Test.startTest();
        List<AggregateResult> motivos = TabulacaoController.returnMotivoList(recordDeveloperName);
        List<AggregateResult> submotivos = TabulacaoController.returnSubmotivoList((String)motivos[0].get('MotivoTxt__c'), recordDeveloperName);
        //List<AggregateResult> submotivos2 = TabulacaoController.returnMeioDeAcessoList((String)submotivos[0].get('SubMotivoTxt__c'), recordDeveloperName);
        //String observacoes = TabulacaoController.retornaObservacoes((String)motivos[0].get('MotivoTxt__c'), (String)submotivos[0].get('SubMotivoTxt__c'), (String)submotivos2[0].get('SubMotivo2Txt__c'), recordDeveloperName);
        Test.stopTest();

        System.assertEquals('FALHA', (String)motivos[0].get('MotivoTxt__c'));
        System.assertEquals('AVALIE TESTE', (String)submotivos[0].get('SubMotivoTxt__c'));
        //System.assertEquals(null, (String)submotivos2[0].get('SubMotivo2Txt__c'));
        //System.assertEquals('TESTE OBSERVAÇÕES', observacoes);
    }

    @isTest
    static void testaSetarValorNoCaso(){

        Case caso = new Case();

        RecordType recordType = [
            SELECT Id, Name
            FROM RecordType
            WHERE Name = 'CSM Pós Vendas'
        ];

        caso.RecordTypeId = recordType.Id;
        caso.Status = 'NOVO';
        caso.Subject = 'Is a case test';
        caso.Area__c = 'CSM';
        caso.Description = 'TESTE';
        insert caso;

        String motivo = 'FALHA / ERRO';
        String submotivo = 'AVALIE';
        String meioDeAcesso = 'STRING';

        Case selectCase = [
            SELECT Id, Description
            FROM Case
            WHERE Subject =: caso.Subject
        ];

        //Test.startTest();
        //TabulacaoController.setValuesInCase(motivo, submotivo, meioDeAcesso, recordType.Name, selectCase.Id);
        //Test.stopTest();

        Case casoComMotivo = [
            SELECT Id, MotivoTxt__c, SubMotivoTxt__c, MeioAcessoTxt__c
            FROM Case
            WHERE Subject =: caso.Subject 
        ];

        System.assertEquals('FALHA / ERRO', casoComMotivo.MotivoTxt__c);
        System.assertEquals('AVALIE', casoComMotivo.SubMotivoTxt__c);
        System.assertEquals('STRING', casoComMotivo.MeioAcessoTxt__c);
        
    }

    @isTest
    static void testaSetarValorNoCasoITSM(){

        Case caso = new Case();

        RecordType recordType = [
            SELECT Id, Name
            FROM RecordType
            WHERE Name = 'ITSM'
        ];

        caso.RecordTypeId = recordType.Id;
        caso.Status = 'NOVO';
        caso.Subject = 'Is a case test';
        caso.Description = 'TESTE';
        caso.EmailSolicitante__c = 'email@solicitante.com.br';
        caso.NomeSolicitante__c = 'SOLICITANTE';
        insert caso;

        String motivo = 'FALHA / ERRO';
        String submotivo = 'AVALIE';
        String meioDeAcesso = 'STRING';

        Case selectCase = [
            SELECT Id, Description
            FROM Case
            WHERE Subject =: caso.Subject
        ];

       // Test.startTest();
       // TabulacaoController.setValuesInCase(motivo, submotivo, meioDeAcesso, recordType.Name, selectCase.Id);
       // Test.stopTest();

        Case casoComMotivo = [
            SELECT Id, MotivoTxt__c, SubMotivoTxt__c, SubMotivo2Txt__c
            FROM Case
            WHERE Subject =: caso.Subject 
        ];

        System.assertEquals('FALHA / ERRO', casoComMotivo.MotivoTxt__c);
        System.assertEquals('AVALIE', casoComMotivo.SubMotivoTxt__c);
        System.assertEquals('STRING', casoComMotivo.SubMotivo2Txt__c);
        
    }

    @isTest
    static void testaSetarValorNoCasoITSMTodosMotivosSubMotivos(){

        Case caso = new Case();

        RecordType recordType = [
            SELECT Id, Name
            FROM RecordType
            WHERE Name = 'ITSM'
        ];

        caso.RecordTypeId = recordType.Id;
        caso.Status = 'NOVO';
        caso.Subject = 'Is a case test';
        caso.Description = 'TESTE';
        caso.EmailSolicitante__c = 'email@solicitante.com.br';
        caso.NomeSolicitante__c = 'SOLICITANTE';
        insert caso;

        String motivo = 'SEGURANÇA DA INFORMAÇÃO';
        String submotivo = 'LIBERAÇÃO DE E-MAIL/SPAM ENVIADO/RECEBIDO';
        String meioDeAcesso = 'CONFIGURAÇÃO NO ANTISPAM';

        Case selectCase = [
            SELECT Id, Description
            FROM Case
            WHERE Subject =: caso.Subject
        ];

        Test.startTest();
        //String retn = TabulacaoController.setValuesInCase(motivo, submotivo, meioDeAcesso, recordType.Name, selectCase.Id);

        //System.assertEquals('Os campos são obrigatórios. Após o preenchimento dos campos clique no botão SALVAR.', retn);

        motivo = 'ALERTA';
        submotivo = 'TENTATIVA DE GOLPE';
        meioDeAcesso = '';

        //retn = TabulacaoController.setValuesInCase(motivo, submotivo, meioDeAcesso, recordType.Name, selectCase.Id);

        motivo = 'SEGURANÇA DA INFORMAÇÃO';
        submotivo = 'AVALIAÇÃO DE BLOQUEIOS DO ANTIVÍRUS';
        meioDeAcesso = 'ANÁLISE NO ANTIVIRUS';

        //retn = TabulacaoController.setValuesInCase(motivo, submotivo, meioDeAcesso, recordType.Name, selectCase.Id);

        motivo = 'SEGURANÇA DA INFORMAÇÃO';
        submotivo = 'REMOÇÃO DE CONTEÚDO DA INTERNET';
        meioDeAcesso = 'AXUR - PEDIDO DE TAKEDOWN';

       // retn = TabulacaoController.setValuesInCase(motivo, submotivo, meioDeAcesso, recordType.Name, selectCase.Id);

        motivo = 'SEGURANÇA DA INFORMAÇÃO';
        submotivo = 'DENÚNCIA DE PHISHING';
        meioDeAcesso = 'AVALIAÇÃO DE PHISHING EM NOME DA BVS PARA CLIENTES';

        //retn = TabulacaoController.setValuesInCase(motivo, submotivo, meioDeAcesso, recordType.Name, selectCase.Id);

        motivo = 'SEGURANÇA DA INFORMAÇÃO';
        submotivo = 'AVALIAÇÃO DE CONFIGURAÇÕES DO WAF';
        meioDeAcesso = 'ANÁLISE NO WAF';

        //retn = TabulacaoController.setValuesInCase(motivo, submotivo, meioDeAcesso, recordType.Name, selectCase.Id);

        motivo = 'SEGURANÇA DA INFORMAÇÃO';
        submotivo = 'AUTORIZAÇÃO DE BYOD';
        meioDeAcesso = 'ANÁLISE PARA INCLUSÃO NO GRUPO BYOD';

        //retn = TabulacaoController.setValuesInCase(motivo, submotivo, meioDeAcesso, recordType.Name, selectCase.Id);

        motivo = 'SEGURANÇA DA INFORMAÇÃO';
        submotivo = 'LIBERAÇÃO DE SITES';
        meioDeAcesso = 'CONFIGURAÇÃO NO PROXY';

       // retn = TabulacaoController.setValuesInCase(motivo, submotivo, meioDeAcesso, recordType.Name, selectCase.Id);
        Test.stopTest();

       // System.assertEquals('Os campos são obrigatórios. Após o preenchimento dos campos clique no botão SALVAR.', retn);

        
    }
    */
    @isTest
    public static void obterCodigo()
    {
        Account conta = TestDataFactory.gerarContaJuridica(true);
        TabulacaoController.returnCodigoDeClienteList(conta.Codigo8__c);
    }
    @isTest
    public static void ObterPerguntas()
    {
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
        

        List<Case> lstOldCase = new List<Case>();
        List<Case> lstCase = new List<Case>(); 
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(false, conta, contato);
        caso.MotivoTxt__c = 'Teste';
        caso.SubMotivoTxt__c = 'Teste';
        insert caso;
        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(false);
        tabulacao.MeioAcessoTxt__c = 'Teste';
        tabulacao.Observacoes__c = 'Teste';
        TabulacaoController.returnPID(caso.id,'teste','teste');
        TabulacaoController.obterPerguntas(10, caso, 'teste');
        TabulacaoController.obterPerguntas(6, caso, 'teste');
        TabulacaoController.obterPerguntas(8, caso, 'teste');
        TabulacaoController.obterPerguntas(5, caso, 'teste');
        TabulacaoController.obterPerguntas(19, caso, 'teste');
        TabulacaoController.obterPerguntas(9, caso, 'teste');
        TabulacaoController.obterPerguntas(18, caso, 'teste');
        TabulacaoController.obterPerguntas(22, caso, 'teste');
        TabulacaoController.obterPerguntaCNPJ(10,conta );
        TabulacaoController.obterPerguntaCNPJ(6,conta );
        TabulacaoController.obterPerguntaCNPJ(8,conta);
        TabulacaoController.obterPerguntaCNPJ(5,conta );
        TabulacaoController.obterPerguntaCNPJ(19,conta );
        TabulacaoController.obterPerguntaCNPJ(9,conta );
        TabulacaoController.obterPerguntaCNPJ(18, conta);
        TabulacaoController.obterPerguntaCNPJ(22, conta);


        TabulacaoController.responseCodigoDeCliente teste2 = new TabulacaoController.responseCodigoDeCliente();
            teste2.id = 'teste';
            teste2.Name ='teste';
        TabulacaoController.GrupoPergunta teste = new TabulacaoController.GrupoPergunta(2,3,4);
        teste.obterPergunta();
        TabulacaoController.returnMotivoList('CSMAtendimento');
        TabulacaoController.returnSubMotivoList('CSMAtendimento','Teste');
        TabulacaoController.returnMeioDeAcessoList('CSMAtendimento','Teste','Teste');
        TabulacaoController.returnSubMotivo2List('CSMAtendimento','Teste','Teste');
        TabulacaoController.retornaObservacoes(tabulacao.motivoTxt__c,tabulacao.submotivoTxt__c, tabulacao.MeioAcessoTxt__c, 'CSMAtendimento');
    }
}

}