/**
 * @Autor: Everymind
 * @Data: 25/07/2020
 * @Descrição: Classe para execução de Batch de Reencarteiramento.
 */
global class ReencarteiramentoBatch implements Database.Batchable<sObject>, Database.Stateful {
	public Boolean processoComErro = false;
	public Boolean processarTodaBase = false;

	/**
	 * @Autor: Everymind
	 * @Data: 25/07/2020
	 * @Descrição: Método para iniciar a execução do Batch sem tamanho de lote definido.
	 */
	global static void start() {
		Database.executeBatch(new ReencarteiramentoBatch());
	}

	/**
	 * @Autor: Everymind
	 * @Data: 25/07/2020
	 * @Descrição: Método para iniciar a execução do Batch com tamanho de lote definido.
	 * @Parâmetros: Integer batchSize - Número que define o tamanho do lote que o batch vai executar.
	 */
	global static void start(Integer batchSize) {
		Database.executeBatch(new ReencarteiramentoBatch(), batchSize);
	}

	/**
	 * @Autor: Everymind
	 * @Data: 25/07/2020
	 * @Descrição: Método para iniciar a execução do Batch com tamanho de lote definido.
	 * @Parâmetros: Integer batchSize - Número que define o tamanho do lote que o batch vai executar.
	 */
	global static void start(Boolean blnProcessarTodaBase) {
		Database.executeBatch(new ReencarteiramentoBatch());
	}
	
	/**
	 * @Autor: Everymind
	 * @Data: 25/07/2020
	 * @Descrição: Método herdado da interface que recupera os registros que farão parte do escopo da execução.
	 * @Parâmetros: Database.BatchableContext objContext - Objeto de contexto do batch.
	 */
	global Database.QueryLocator start(Database.BatchableContext objContext) {
		processoComErro = false;
		String query = ''; 
		if (processarTodaBase){
			query = 'SELECT Id, Carteira__c FROM Account';
			System.debug('PROCESSAMENTO DE TODA A BASE');
		}else{
			query = 'SELECT Id, Carteira__c FROM Account where ProcessarReencarteiramento__c = true';
			System.debug('PROCESSAMENTO SOMENTE DAS CONTAS FLEGADAS');
		}
		
		return Database.getQueryLocator(query);
	}

	/**
	 * @Autor: Everymind
	 * @Data: 25/07/2020
	 * @Descrição: Método herdado da interface que executa a lógica de negócio baseados nos registros do escopo.
	 * @Parâmetros: Database.BatchableContext objContext - Objeto de contexto do batch.
	 * @Parâmetros: List<Account> lstSObjAccount - Lista de escopo retornada no método start.
	 */
	global void execute(Database.BatchableContext objContext, List<Account> lstSObjAccount) {
		Set<String> setCarteiras = new Set<String>();
		Set<Id> setIdAccount = new Set<Id>();
		Map<String, List<UserTerritory2Association>> mapUserTerritory2Association = new Map<String, List<UserTerritory2Association>>();
		Map<Id, List<Opportunity>> mapOpportunity = new Map<Id, List<Opportunity>>();
		Map<Id, List<Contact>> mapContact = new Map<Id, List<Contact>>();
		List<Opportunity> lstSObjOpportunity = new List<Opportunity>();
		List<Contact> lstSObjContact = new List<Contact>();
		List<ApplicationLog__c> lstApplcationLogInsert = new List<ApplicationLog__c>();
		Set<Id> setUserId = new Set<Id>();

		// Guardar as carteiras em um set
		CollectionUtil.carregarSet(setCarteiras, 'Carteira__c', lstSObjAccount);

		// Buscar os territórios por carteira
		CollectionUtil.carregarMap(mapUserTerritory2Association, 'Territory2.DeveloperName', [SELECT Id, UserId, RoleInTerritory2, Territory2Id, Territory2.DeveloperName FROM UserTerritory2Association WHERE Territory2.DeveloperName = :setCarteiras AND IsActive = true]);
		
		List<UserTerritory2Association> lstUserTerritory2Association = [SELECT Id, UserId, Territory2.DeveloperName, RoleInTerritory2 FROM UserTerritory2Association WHERE RoleInTerritory2 = 'Proprietário' AND Territory2.DeveloperName IN :setCarteiras];
		Map<String,UserTerritory2Association> mapUserTerritory2Association2 = new Map<String, UserTerritory2Association>();
		for(UserTerritory2Association userTerritory2Association : lstUserTerritory2Association){
			setUserId.add(userTerritory2Association.UserId);
			mapUserTerritory2Association2.put(userTerritory2Association.Territory2.DeveloperName, userTerritory2Association);
		}

		// Guardar as contas em um set
		CollectionUtil.carregarSet(setIdAccount, 'Id', lstSObjAccount);

		// Buscar oportunidades da conta
		CollectionUtil.carregarMap(mapOpportunity, 'AccountId', [SELECT Id, AccountId, OwnerId FROM Opportunity WHERE AccountId IN :setIdAccount]);
		
		// Buscar contatos da conta
		CollectionUtil.carregarMap(mapContact, 'AccountId', [SELECT Id, AccountId, OwnerId FROM Contact WHERE AccountId IN :setIdAccount]);

		List<Territory2> lstTerritory2 = [SELECT Id, DeveloperName, Canal__c, Canal_Agrupado__c, Regional__c FROM Territory2 WHERE DeveloperName IN :setCarteiras];
		Map<String,Territory2> mapTerritory2 = new Map<String, Territory2>();
		for(Territory2 territorio : lstTerritory2){
			mapTerritory2.put(territorio.DeveloperName, territorio);
		}

		List<User> lstUser = [SELECT Id, Name, Username, IsActive FROM User WHERE Id IN :setUserId];
		Map<String,User> mapUser = new Map<String, User>();
		for(User user : lstUser){
			mapUser.put(user.Id, user);
		}

		// Percorrer as contas
		for(Account objAccount : lstSObjAccount) {
			// Buscar usuários do território no mapa
			List<UserTerritory2Association> lstAssociation = mapUserTerritory2Association.containsKey(objAccount.Carteira__c) ? mapUserTerritory2Association.get(objAccount.Carteira__c) : null;

			// Verifica se o território da conta tem usuários
			if((lstAssociation == null) || lstAssociation.isEmpty()) {
				continue;
			}

			// Buscar proprietário do território
			UserTerritory2Association objAssociationProprietario = (UserTerritory2Association)CollectionUtil.buscarNaLista(lstAssociation, 'Proprietário', 'RoleInTerritory2');

			// Verificar se o proprietário do território foi encontrado
			if (objAssociationProprietario == null){
				continue;
			}
			
			if (mapOpportunity.containsKey(String.valueOf(objAccount.Id))){
				// Percorre as oportunidades abertas da conta
				for(Opportunity objOpportunity : mapOpportunity.get(String.valueOf(objAccount.Id))) {

					// Verificar se o proprietário da oportunidade está no território
					if(CollectionUtil.contemNaLista(lstAssociation, objOpportunity.OwnerId, 'UserId')) {
						continue;
					}

					// Atualizar o proprietário da oportunidade aberta quando não for o mesmo usuário do território da conta
					objOpportunity.OwnerId = objAssociationProprietario.UserId;

					// Adiciona na lista de atualização
					lstSObjOpportunity.add(objOpportunity);
				}
			}
			
			if (mapContact.containsKey(String.valueOf(objAccount.Id))){
				// Percorre os contatos da conta
				for(Contact objContact : mapContact.get(String.valueOf(objAccount.Id))) {

					// Verificar se o proprietário do contato está no território
					if(CollectionUtil.contemNaLista(lstAssociation, objContact.OwnerId, 'UserId')) {
						continue;
					}

					// Adiciona na lista de atualização
					lstSObjContact.add(
						new Contact(
							Id = objContact.Id,
							OwnerId = objAssociationProprietario.UserId
						)
					);
				}
			}

			Id proprietarioId = null;
			if (mapUserTerritory2Association2.containsKey(objAccount.Carteira__c)){
				
				if (mapUser.containsKey(mapUserTerritory2Association2.get(objAccount.Carteira__c).UserId)){
					System.debug(mapUser.get(mapUserTerritory2Association2.get(objAccount.Carteira__c).UserId));
					if (mapUser.get(mapUserTerritory2Association2.get(objAccount.Carteira__c).UserId).IsActive){
						proprietarioId = mapUserTerritory2Association2.get(objAccount.Carteira__c).UserId;
					}else{
						List<ConfiguracaoDadosTerritorio__mdt> configuracaoDadosTerritorio = [SELECT Id, DeveloperName, MasterLabel FROM ConfiguracaoDadosTerritorio__mdt WHERE DeveloperName = 'CarteiraVaga'];
						proprietarioId = !configuracaoDadosTerritorio.isEmpty() ? configuracaoDadosTerritorio[0].MasterLabel : null;
					}
				
				}
			}
			if (mapTerritory2.containsKey(objAccount.Carteira__c)){
				Territory2 territorio = mapTerritory2.get(objAccount.Carteira__c);

				objAccount.Canal__c = territorio.Canal__c;
				objAccount.CanalAgrupado__c = territorio.Canal_Agrupado__c;
				objAccount.Regional__c = territorio.Regional__c;
				if (proprietarioId != null){
					objAccount.OwnerId = proprietarioId;
				} else {
					continue;
				}
			}

			// Retira o registro da fila de Reencarteiramento
			objAccount.ProcessarReencarteiramento__c = false;
		}

		// Atualiza a lista de oportunidades com a troca dos proprietários
		if(!lstSObjOpportunity.isEmpty()) {
			lstApplcationLogInsert.addAll(this.processarResultado(Database.update(lstSObjOpportunity, false), 'Oportunidade', lstSObjOpportunity));
		}

		// Atualiza a lista de contatos com a troca dos proprietários
		if(!lstSObjContact.isEmpty()) {
			lstApplcationLogInsert.addAll(this.processarResultado(Database.update(lstSObjContact, false), 'Contato', lstSObjContact));
		}

		// Atualiza a lista de contas com a troca dos proprietários
		if(!lstSObjAccount.isEmpty()) {
			lstApplcationLogInsert.addAll(this.processarResultado(Database.update(lstSObjAccount, false), 'Conta', lstSObjAccount));
		}

		if (!lstApplcationLogInsert.isEmpty()){
			processoComErro = true;
			insert lstApplcationLogInsert;
		}
	}

	/**
	 * @Autor: Everymind
	 * @Data: 25/07/2020
	 * @Descrição: Método herdado da interface que é executa ao final da execução de cada lote.
	 */
	global void finish(Database.BatchableContext objContext) {
		enviarEmail(processoComErro);
	}

	/**
	 * @Autor: Everymind
	 * @Data: 24/08/2020
	 * @Descrição: Método que captura os erros nos updates de Oportunidade e Contato e grava no objeto ApplicationLog__c.
	 */
	public List<ApplicationLog__c> processarResultado(List<Database.SaveResult> resultados, String origem, List<SObject> lstRegistros){
		List<ApplicationLog__c> lstApplicationLog = new List<ApplicationLog__c>();

        for (Integer i = 0; i < resultados.size(); i++) {
			Database.SaveResult databaseSaveResult = resultados[i];
			SObject registro = lstRegistros[i];

            if (!databaseSaveResult.isSuccess()) {

				for (Database.Error error : databaseSaveResult.getErrors()) {
					String IdRegistroErro = registro.Id;
					// Cria um registro de log em caso de erro
					ApplicationLog__c objLog = new ApplicationLog__c(
						ErrorMessage__c = origem + ' - ID [' + IdRegistroErro + '] - '  + error.getMessage(),
						IdTarefaRightNow__c = '',
						IdIncidenteRightNow__c = '',
						Processo__c = 'Reencarteiramento'
					);

					lstApplicationLog.add(objLog);
				}
            }
        }

        return lstApplicationLog;
	}
	
	public void enviarEmail(Boolean mailError){
		
		List<ConfiguracaoEmailReencarteiramento__mdt> configuracaoEmailReencarteiramento = [SELECT Id, DeveloperName, MasterLabel, Assunto__c, EMail__c, MensagemErro__c, MensagemSucesso__c FROM ConfiguracaoEmailReencarteiramento__mdt WHERE DeveloperName = 'CONFIG'];
		DateTime dataHoje = DateTime.now();
		String dateOutput = dataHoje.format('dd/MM/yyyy hh:mm:ss');

		String mails = configuracaoEmailReencarteiramento[0].EMail__c;
		String[] arrMails = mails.split(',');

		Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
		message.toAddresses = arrMails;
		message.subject = configuracaoEmailReencarteiramento[0].Assunto__c;
		if (mailError){
			message.plainTextBody = configuracaoEmailReencarteiramento[0].MensagemErro__c + ' em ' + dateOutput;
		}else{
			message.plainTextBody = configuracaoEmailReencarteiramento[0].MensagemSucesso__c + ' em ' + dateOutput;
		}
		Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
		Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

		if (results[0].success) {
			System.debug('The email was sent successfully.');
		} else {
			System.debug('The email failed to send: ' + results[0].errors[0].message);
		}
	}
}