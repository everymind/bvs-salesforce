public class CaseTriggerHandler extends TriggerHandler {
    private CaseBO objBO = CaseBO.getInstance();

    public override void beforeInsert(){
        Boolean usuarioDeCarga = MetadataDAO.getInstance().retornaUsuariosCarga().contains(UserDAO.getInstance().obterUsername(UserInfo.getUserId()));
        if(!usuarioDeCarga)
        {
            objBO.criarCasoFilhoPreOuvidoria(Trigger.new);
            objBO.definirFila(Trigger.new, null);
            objBO.definirFilaInicialPosVendas(Trigger.new);
            objBO.definirFilaInicialAtendimento(Trigger.new);
            objBO.definirSLA(Trigger.new, null);
            objBO.requerAprovacao(Trigger.new);
            objBO.definirTipoCaso(Trigger.new, null);
        }
        
       
    }
    public override void beforeUpdate(){
        Boolean usuarioDeCarga = MetadataDAO.getInstance().retornaUsuariosCarga().contains(UserDAO.getInstance().obterUsername(UserInfo.getUserId()));
        if(!usuarioDeCarga)
        {
            objBO.definirFila(Trigger.new, Trigger.old);
            objBO.definirSLA(Trigger.new, Trigger.oldMap);
            objBO.impedirFecharCaso(Trigger.new);
            objBO.definirFilaAnterior(Trigger.new, Trigger.old);
            objBO.pausarSLA(Trigger.new, Trigger.old);
            objBO.requerAprovacao(Trigger.new);
            objBO.definirTipoCaso(Trigger.new, Trigger.old);
            objBO.encerrarMarco(Trigger.new, Trigger.OldMap);
            objBO.verificaStatusFinalizadoRegra(Trigger.new, Trigger.old);
        }
    }

    public override void afterInsert(){

    }

    public override void afterUpdate()
    {
        objBO.gerarLinkPesquisaProtocolosAtendimento(Trigger.new, Trigger.old);

    }

}