public class contadorCasosController {
    
    @AuraEnabled
    public static List<Case> buscarCasosPorCliente(Id contaId) {
        return([SELECT Id, Area__c, CaseNumber, CreatedDate, Description, MotivoTxt__c, Owner.Name, SubMotivoTxt__c, SubMotivo2Txt__c, Status FROM Case WHERE AccountId = :contaId AND CreatedDate = LAST_N_DAYS:30 ORDER BY MotivoTxt__c]);
    }
    @AuraEnabled
    public static List<Case> buscarCasosPorClienteContato(Id contatoId) {
        return([SELECT Id, Area__c, CaseNumber, CreatedDate, Description, MotivoTxt__c, Owner.Name, SubMotivoTxt__c, SubMotivo2Txt__c, Status FROM Case WHERE ContactId = :contatoId AND CreatedDate = LAST_N_DAYS:30 ORDER BY MotivoTxt__c]);
    }

    @AuraEnabled
    public static Map<String, List<Case>> montarListaMotivoSubMotivo(List<Case> casos) {
        Map<String, List<Case>> mapMotivoSubmotivo = new Map<String, List<Case>>();
        Map<String, List<Case>> mapMotivoSubmotivoContador = new Map<String, List<Case>>();
        Map<String, List<Case>> mapMotivoSubmotivoFinal = new Map<String, List<Case>>();
        List<Integer> ordem = new List<Integer>();
        List<Integer> ordemInversa = new List<Integer>();

        for (Case caso : casos) {
            if(caso.MotivoTxt__c != null && caso.SubMotivoTxt__c != null){
                if(mapMotivoSubmotivo.containsKey(caso.MotivoTxt__c + ' - ' + caso.SubMotivoTxt__c)){
                    mapMotivoSubmotivo.get(caso.MotivoTxt__c + ' - ' + caso.SubMotivoTxt__c).Add(caso);
                } else{
                    mapMotivoSubmotivo.put(caso.MotivoTxt__c + ' - ' + caso.SubMotivoTxt__c, new List<Case>());
                    mapMotivoSubmotivo.get(caso.MotivoTxt__c + ' - ' + caso.SubMotivoTxt__c).Add(caso);
                }
            } else{
                if(mapMotivoSubmotivo.containsKey('NÃO TABULADO')){
                    mapMotivoSubmotivo.get('NÃO TABULADO').Add(caso);
                } else{
                    mapMotivoSubmotivo.put('NÃO TABULADO', new List<Case>());
                    mapMotivoSubmotivo.get('NÃO TABULADO').Add(caso);
                }
            }
        }

        for(String chave : mapMotivoSubmotivo.keySet()){
            List<Case> listCasos = mapMotivoSubmotivo.get(chave);
            String chaveMap = chave + ' (' + listCasos.size() + ')';
            mapMotivoSubmotivoContador.put(chaveMap, listCasos);
            ordem.add(listCasos.size());
        }

        ordem.sort();

        for(Integer i = ordem.size() - 1; i >= 0; i--){
            ordemInversa.add(ordem[i]);
        }

        for(Integer ordemItem : ordemInversa){
            for(String chave : mapMotivoSubmotivoContador.keySet()){
                List<Case> listCasos = mapMotivoSubmotivoContador.get(chave);
                String numeroChave = chave.subStringBeforeLast(')');
                if(ordemItem == Integer.valueOf(numeroChave.subStringAfterLast('('))){
                    mapMotivoSubmotivoFinal.put(chave, listCasos);
                }
            }
        }

        return mapMotivoSubmotivoFinal;
    }
    
}