/**
 * @Autor: Everymind
 * @Data: 18/07/2020
 * @Descrição: Classe que orquestra os eventos da trigger para os métodos de negócio.
 */
public class OpportunityTriggerHandler extends TriggerHandler {

	/**
	 * @Autor: Everymind
	 * @Data: 18/07/2020
	 * @Descrição: Constante que contém a instância da classe de negócio do objeto Opportunity.
	 */
	private OpportunityBO objBO = OpportunityBO.getInstance();

	public override void afterInsert(){
		objBO.atribuirCatalogo(Trigger.new);
 	}
    
    public override void beforeUpdate(){
		objBO.obrigarPreenchimentoCampos(Trigger.new,(Map<Id, Opportunity>) Trigger.oldMap);
 	}

	public override void afterUpdate(){
		objBO.marcarContasProcessamento(Trigger.new);
	}
}