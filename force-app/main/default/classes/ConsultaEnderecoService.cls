/**
 * @Autor: Everymind
 * @Data: 04/07/2020
 * @Descrição: Classe de serviço do BVS para consulta de endereço.
 */
public class ConsultaEnderecoService {
	
	/**
	 * @Autor: Everymind
	 * @Data: 11/07/2020
	 * @Descrição: Método responsável por chamar o serviço de consulta de endereço do BVS.
	 * @Parâmetro: String cep - CEP do endereço a ser consultado.
	 * @Parâmetro: String configuracaoId - Id da configuração selecionada.
	 * @Retorno: ConsultaEnderecoTO - Objeto que representa o resultado da consulta.
	 */
	public static ConsultaEnderecoTO consultar(String configuracaoId, String cep) {
		// Busca os parâmetros para requisição de endereço
		ConfiguracaoConsultaEndereco__mdt objConfiguracao = buscarConfiguracao(configuracaoId);

		// Valida parâmetros cadastrados
		if(String.isBlank(objConfiguracao.NomeServico__c) || String.isBlank(objConfiguracao.Endpoint__c) || String.isBlank(objConfiguracao.HashAutenticacao__c)) {
			throw new ConsultaEnderecoException('Problemas de configuração da consulta. Por favor, contate o administrador.');
		}

		// Monta o documento para o corpo da requisição
		String documento = montarDocumento(objConfiguracao.NomeServico__c, objConfiguracao.HashAutenticacao__c, cep);

		// Realiza chamada para o serviço de endereço
		Http http = new Http();
		HttpRequest objRequisicao = montarRequisicao(objConfiguracao.Endpoint__c, documento);
		HttpResponse objResponse = http.send(objRequisicao);

		// Parse do objeto de resposta
		ConsultaEnderecoTO objEnderecoTO = carregarDocumento(objResponse.getBodyDocument().toXmlString());

		// Valida resultado da chamada
		if(objEnderecoTO.CodigoRetorno != '00') {
			throw new ConsultaEnderecoException(objEnderecoTO.MensagemRetorno);
		}

		return objEnderecoTO;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 11/07/2020
	 * @Descrição: Método responsável por buscar os parâmetros cadastrados no metadado ConfiguracaoConsultaEndereco__mdt.
	 * @Parâmetro: objId - Id da configuração selecionada.
	 * @Retorno: ConfiguracaoConsultaEndereco__mdt - Objeto com chave e valor dos parâmetros configurados.
	 */
	private static ConfiguracaoConsultaEndereco__mdt buscarConfiguracao(String configuracaoId) {
		return [SELECT Id, DeveloperName, Objeto__c, NomeServico__c, HashAutenticacao__c, Endpoint__c, CampoCEP__c, CampoRua__c, CampoBairro__c, CampoCidade__c, CampoEstado__c, CampoNumero__c, CampoComplemento__c
				FROM ConfiguracaoConsultaEndereco__mdt
				WHERE Id = :configuracaoId];
	}

	/**
	 * @Autor: Everymind
	 * @Data: 04/07/2020
	 * @Descrição: Método responsável por montar o XML de requisição para o serviço.
	 * @Parâmetro: String - Hash de autenticação do serviço de consulta.
	 * @Parâmetro: String - CEP do endereço a ser consultado.
	 * @Retorno: String - XML que representa o corpo da requisição da chamada.
	 */
	private static String montarDocumento(String servico, String hash, String cep) {
		XmlStreamWriter objWriter = new XmlStreamWriter();
		// Inicia o documento
		objWriter.writeStartDocument(null, '1.0');

		// Inicia o envelope
		objWriter.writeStartElement(null, 'soapenv:Envelope', null); 
		objWriter.writeAttribute(null, null, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
		objWriter.writeAttribute(null, null, 'xmlns:xsd', 'http://www.w3.org/2001/XMLSchema');
		objWriter.writeAttribute(null, null, 'xmlns:urn', 'urn:CRMIntf-ICRM');
		objWriter.writeAttribute(null, null, 'xmlns:soapenv', 'http://schemas.xmlsoap.org/soap/envelope/');

		// Inicia o Body
		objWriter.writeStartElement(null, 'soapenv:Body', null);

		// Inicia o PesquisaCep
		objWriter.writeStartElement(null, servico, null);
		objWriter.writeAttribute(null, null, 'soapenv:encodingStyle', 'http://schemas.xmlsoap.org/soap/encoding/');

		// Inicia o HashAutenticacao
		objWriter.writeStartElement(null, 'HashAutenticacao', null);
		objWriter.writeCharacters(hash);

		// Finaliza o HashAutenticacao
		objWriter.writeEndElement();

		// Inicia o Value
		objWriter.writeStartElement(null, 'Value', null); 
		objWriter.writeCharacters(cep);

		// Finaliza o Value
		objWriter.writeEndElement();

		// Finaliza o PesquisaCep
		objWriter.writeEndElement();

		// Finaliza o Body
		objWriter.writeEndElement();

		// Finaliza o envelope
		objWriter.writeEndElement();
		
		// Finaliza o documento
		objWriter.writeEndDocument();

		// Converte o XML gerado em String
		String documento = objWriter.getXmlString();

		// Finaliza o objeto de Writer
		objWriter.close();

		return documento;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 04/07/2020
	 * @Descrição: Método responsável por montar a requisição HTTP do Apex.
	 * @Parâmetro: String - Endpoint do serviço de consulta de endereço.
	 * @Parâmetro: String - XML que representa o corpo da requisição.
	 * @Retorno: ConsultaEnderecoTO - Objeto que representa a requisição HTTP do Apex.
	 */
	private static HttpRequest montarRequisicao(String endpoint, String documento) {
        HttpRequest objRequisicao = new HttpRequest();
		objRequisicao.setMethod('POST');
		objRequisicao.setEndpoint(endpoint);
		objRequisicao.setHeader('Content-Type', 'text/xml');
		objRequisicao.setBody(documento);

		return objRequisicao;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 04/07/2020
	 * @Descrição: Método responsável por interpretar e converter o resultado da consulta.
	 * @Parâmetro: String - XML que representa o corpo da resposta do serviço de consulta.
	 * @Retorno: ConsultaEnderecoTO - Objeto que representa o resultado da consulta.
	 */
	private static ConsultaEnderecoTO carregarDocumento(String documento) {
		ConsultaEnderecoTO objEnderecoTO = new ConsultaEnderecoTO();
		XmlStreamReader objReader = new XmlStreamReader(documento);
		Boolean podeIrParaProximaTagXml = true;

		// Percorre o XML para montar o objeto de retorno
		while(podeIrParaProximaTagXml) {
            if (objReader.getEventType() == XmlTag.START_ELEMENT && objReader.hasNext()) {
                switch on objReader.getLocalName() {
					when 'CodigoRetorno' {
						objReader.next();
						if(objReader.getEventType() == XmlTag.CHARACTERS) {
							objEnderecoTO.CodigoRetorno = objReader.getText();
						}
					}
					when 'MensagemRetorno' {
						objReader.next();
						if(objReader.getEventType() == XmlTag.CHARACTERS) {
							objEnderecoTO.MensagemRetorno = objReader.getText();
						}
					}
					when 'Logradouro' {
						objReader.next();
						if(objReader.getEventType() == XmlTag.CHARACTERS) {
							objEnderecoTO.Logradouro = objReader.getText();
						}
					}
					when 'Bairro' {
						objReader.next();
						if(objReader.getEventType() == XmlTag.CHARACTERS) {
							objEnderecoTO.Bairro = objReader.getText();
						}
					}
					when 'Cidade' {
						objReader.next();
						if(objReader.getEventType() == XmlTag.CHARACTERS) {
							objEnderecoTO.Cidade = objReader.getText();
						}
			   		}
					when 'UF' {
						objReader.next();
						if(objReader.getEventType() == XmlTag.CHARACTERS) {
							objEnderecoTO.UF = objReader.getText();
						}
					}
				}
            }
            if (objReader.hasNext()) {
                objReader.next();
            } else {
                podeIrParaProximaTagXml = false;
                break;
            }
        }

		return objEnderecoTO;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 04/07/2020
	 * @Descrição: Classe Exception para representar exceções oriundas do serviço de consulta de endereço.
	 */
	public class ConsultaEnderecoException extends Exception {}
}