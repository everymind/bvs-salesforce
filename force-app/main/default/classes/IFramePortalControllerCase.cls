/**
 * @Autor: Everymind
 * @Data: 28/07/2020
 * @Descrição: Classe controladora do componente de exibição dos IFrames.
 */
public class IFramePortalControllerCase {

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método responsável por construir a url de acesso do IFrame.
	 * @Parâmetros: String recordId - Id do registro injetado pelo lightning.
	 * @Parâmetros: String portal - nome do portal selecionado no design do componente.
	 */
	public Case caso {get;set;}
	public String  token {get;set;}
	public ConfiguracaoIFrame__mdt objConfiguracao {get;set;}

	public IFramePortalControllerCase(ApexPages.StandardController controller){
	
		this.caso = (Case)controller.getRecord();
        Account conta = buscarContaCaso(this.caso.Id);
		token = buscarUrl(conta.Id , 'Sua Fatura');
		
		this.objConfiguracao = new ConfiguracaoIFrame__mdt();
		this.objConfiguracao = buscarConfiguracao('Sua Fatura');
    }   

	@AuraEnabled
	public static String buscarUrl(String recordId, String portal) {
		String url = '';

		// Busca registro de configuração
		ConfiguracaoIFrame__mdt objConfiguracao = buscarConfiguracao(portal);
		
		// Verifica se encontrou o registro de configuração
		if(objConfiguracao == null) {
			return null;
		}

		// Busca informações da conta
		Account objAccount = buscarConta(recordId);

		// Verifica os portais que dependem da conta, possuem todos os parâmetros
		if(objAccount == null && (portal == 'Visão 360' || portal == 'Painel de Incidentes' || portal == 'Intenção de Cancelamento')) {
			return null;
		}

		// Atribui URL de retorno
		url += objConfiguracao.Url__c;
        System.debug('url+'+url);

		String content = '';
		if (Test.isRunningTest()){
			content = 'Start_Of_Session_IdTesteEnd_Of_Session_Id';
		} else {
			PageReference vf = Page.SessionVF;
			content = vf.getContent().toString();
		}

		Integer s = content.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
                e = content.indexOf('End_Of_Session_Id');
		String token = objConfiguracao.Token__c;
		String ssId = content.substring(s, e);

		// Realiza substituições de parâmetros dependendo do portal selecionado
		switch on portal {
			when 'Portal de Vendas', 'Painel' {
				url = url.replace('{token}', ssId);
			}
			when 'Reencarteiramento'{
				url = url.replace('{token}', ssId);
				url = url.replace('{userId}', UserInfo.getUserId());
			}
			when 'Visão 360' {
				url = url.replace('{token}', ssId);
				url = url.replace('{accountId}', recordId);
			}
			when 'Painel de Incidentes', 'Intenção de Cancelamento' {
				url = url.replace('{token}', token);
				url = url.replace('{AccountCNPJCPF}', objAccount.CnpjCpf__c);
			}
			when 'Sua Fatura'{
			//	String params = '';
			//	params += 'origem=SalesForce';
			//	params += '&codigoCliente=' + objAccount.Codigo8__c;
			//	params += '&sfid=' + ssId;
				//url += params;
				//url = url.replace('{token}', ssId);
				//url = url.replace('{CodigoDe8}', objAccount.Codigo8__c);
				url = ssId;
			}
		}

		// Retorna url com os parâmetros devidamente substituídos
		return String.isNotBlank(url) ? url : null;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método responsável por buscar a configuração do portal selecionado.
	 * @Parâmetros: String portal - Nome do portal selecionado no design do componente.
	 */
	
	@AuraEnabled
	public static ConfiguracaoIFrame__mdt buscarConfiguracao(String portal) {
		System.debug('Nome: ' + portal);
		List<ConfiguracaoIFrame__mdt> lstConfiguracao = [SELECT Id, MasterLabel, DeveloperName, Token__c, Url__c FROM ConfiguracaoIFrame__mdt WHERE MasterLabel = :portal];
		System.debug(lstConfiguracao);
		return !lstConfiguracao.isEmpty() ? lstConfiguracao.get(0) : null;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método responsável por buscar as informações da conta.
	 * @Parâmetros: String recordId - Id do registro injetado pelo lightning.
	 */
	@TestVisible private static Account buscarContaCaso(String recordId) {
        Case caso = [SELECT Id, AccountId FROM Case WHERE Id = :recordId];

		List<Account> lstAccount = [SELECT Id, CnpjCpf__c, Codigo8__c FROM Account WHERE Id = :caso.AccountId];
		return !lstAccount.isEmpty() ? lstAccount.get(0) : null;
	}

	private static Account buscarConta(String recordId) {
		List<Account> lstAccount = [SELECT Id, CnpjCpf__c, Codigo8__c FROM Account WHERE Id = :recordId];
		return !lstAccount.isEmpty() ? lstAccount.get(0) : null;
	}
}