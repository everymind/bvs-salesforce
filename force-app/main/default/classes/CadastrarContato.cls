public class CadastrarContato {
    
    @InvocableMethod
    public static List<RetornoServico> buscarContato(List<String> CPFs){
        List<RetornoServico> retornoList = new List<RetornoServico>();

        for(String CPF : CPFs){
            retornoList.add(BuscaContatoPorCPF(CPF));
        }
        
        return retornoList;
    }
    
    public static RetornoServico BuscaContatoPorCPF(String CPF){
        RetornoServico retornoList = new RetornoServico();
        
        try{

            //Chama serviço
            String jsonPF = CadastrarContaService.buscarContaPessoaFisica(CPF);
            CadastroContaTO.contaPF contaPessoaFisica = (CadastroContaTO.contaPF)JSON.deserialize(jsonPF, CadastroContaTO.contaPF.class);
            
            retornoList.Nome = contaPessoaFisica.nome;
            retornoList.PrimeiroNome = retornoList.Nome.substringBefore(' ');
            retornoList.Sobrenome = retornoList.Nome.substringAfter(' ');
            retornoList.NomeMae = contaPessoaFisica.nome_mae;
            retornoList.DataNascimento = contaPessoaFisica.data_nascimento != null ? Date.newInstance(Integer.valueOf(contaPessoaFisica.data_nascimento.substring(6,10)), Integer.valueOf(contaPessoaFisica.data_nascimento.substring(3,5)), Integer.valueOf(contaPessoaFisica.data_nascimento.substring(0,2)))  : null;
            retornoList.OperadorAC = '12345678';
            retornoList.StatusOperadorAC = 'Ativo';
            retornoList.EmailContato = contaPessoaFisica.email;
            retornoList.TelefoneComercialCont = contaPessoaFisica.tel_com;
            retornoList.TelefoneResidencialCont = contaPessoaFisica.tel_res;
            retornoList.TelefoneCelularCont = contaPessoaFisica.tel_cel;
            retornoList.CodigoRetorno = contaPessoaFisica.cod_retorno;

        } catch (Exception ex) {

            retornoList.CodigoRetorno = '99';

        }
        
        return retornoList;
    }
    
    
    public class RetornoServico{

        @InvocableVariable
        public String Nome;
        
        @InvocableVariable
        public String PrimeiroNome;
        
        @InvocableVariable
        public String Sobrenome;
        
        @InvocableVariable
        public String NomeMae;

        @InvocableVariable
        public String EmailContato;

        @InvocableVariable
        public String TelefoneComercialCont;
        
        @InvocableVariable
        public String TelefoneResidencialCont;

        @InvocableVariable
        public String TelefoneCelularCont;
        
        @InvocableVariable
        public Date DataNascimento;   
        
        @InvocableVariable
        public String OperadorAC;
        
        @InvocableVariable
        public String StatusOperadorAC; 
        
        @InvocableVariable
        public String CodigoRetorno;

    }
}