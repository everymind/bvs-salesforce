/**
 * @Autor: Everymind
 * @Data: 23/07/2020
 * @Descrição: Classe que contém métodos de teste do domínio Task.
 */
@IsTest
public class TaskTest {

	/**
	 * @Autor: Everymind
	 * @Data: 23/07/2020
	 * @Descrição: Método cria tarefa para o contexto do teste.
	 */
	@TestSetup
	static void setup() 
	{
		List<Task> lstTarefa = new List<Task>();
		Task task = new Task();
		task.Subject = 'Task teste';
		task.Priority = 'Normal';
		task.Status = 'Aberta';
		task.IdTarefaRightNow__c = '1';
		task.IdIncidenteRightNow__c = '1';
		lstTarefa.add(task);

		User usuario = new User ();
		usuario.IsActive = true;
		usuario.Username = 'user@testMock.com';
		usuario.LastName = 'UserMock';
		usuario.Email = 'teste@mail.com';
		usuario.Alias = 'Alias';
		usuario.CommunityNickname = 'nickname';
		usuario.TimeZoneSidKey = 'America/Chicago';
		usuario.LocaleSidKey = 'en_US';
		usuario.EmailEncodingKey = 'UTF-8';
		usuario.LanguageLocaleKey = 'en_US';
		usuario.ProfileId = [SELECT Id from Profile where Name = 'Auditoria' LIMIT 1][0].Id;
		insert usuario;
		
		Task taskDelete = new Task();
		taskDelete.Subject = 'Task teste delete';
		taskDelete.Priority = 'Normal';
		taskDelete.Status = 'Aberta';
		taskDelete.IdTarefaRightNow__c = '1';
		taskDelete.IdIncidenteRightNow__c = '1';
		taskDelete.OwnerId = usuario.Id;

		lstTarefa.add(taskDelete);

		insert lstTarefa;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 23/07/2020
	 * @Descrição: Método que testa o erro na chamada de atualização de incidente do RightNow.
	 */
    @IsTest
    static void testarErroEnvioTarefaRightNow() {
		Map<String, String> mapConfiguracoes = new Map<String, String>();

		// Busca os endpoints das chamadas para configuração do mock
		CollectionUtil.carregarMap(mapConfiguracoes, 'DeveloperName', 'Valor__c', [SELECT Id, DeveloperName, MasterLabel, Valor__c FROM ConfiguracaoRightNow__mdt]);

		// Recupera da configuração os endpoints
		String endpointTask = mapConfiguracoes.get('ENDPOINT_TASKS');
		String endpointIncident = mapConfiguracoes.get('ENDPOINT_INCIDENTS');

		// Busca a tarefa criada no setup
		Task objTask = [SELECT Id, Status, IdTarefaRightNow__c, IdIncidenteRightNow__c FROM Task WHERE Subject = 'Task teste' LIMIT 1];

		// Atualiza o path dos endpoints com os Ids salvos na tarefa
		endpointTask = String.format(endpointTask, new List<Object>{ objTask.IdTarefaRightNow__c });
		endpointIncident = String.format(endpointIncident, new List<Object>{ objTask.IdTarefaRightNow__c });

		// Prepara os mocks de resposta
		MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
		multimock.setStaticResource(endpointTask, 'RightNowResposta');
		multimock.setStaticResource(endpointIncident, 'RightNowResposta');
		multimock.setStatusCode(404);
		multimock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multimock);
		
		// Altera o status da tarefa
		objTask.Status = 'Concluído';

		// Atualiza a tarefa para disparo da trigger
        Test.StartTest();
        update objTask;
        Test.StopTest();

        // Busca logs gerados por causa do erro da chamada
		List<ApplicationLog__c> lstLog = [SELECT Id, IdTarefaRightNow__c, IdIncidenteRightNow__c FROM ApplicationLog__c];
        
        // Valida os resultados
		System.assertEquals(false, lstLog.isEmpty());
		System.assertEquals(lstLog.size(), 2);
        System.assertEquals(lstLog[0].IdTarefaRightNow__c, objTask.IdTarefaRightNow__c);
		System.assertEquals(lstLog[0].IdIncidenteRightNow__c, objTask.IdIncidenteRightNow__c);
	}
	
	/**
	 * @Autor: Everymind
	 * @Data: 23/07/2020
	 * @Descrição: Método que testa o sucesso da chamada de atualização do incidente do RightNow.
	 */
	@IsTest
    static void testarSucessoEnvioTarefaRightNow() {
		Map<String, String> mapConfiguracoes = new Map<String, String>();

		// Busca os endpoints das chamadas para configuração do mock
		CollectionUtil.carregarMap(mapConfiguracoes, 'DeveloperName', 'Valor__c', [SELECT Id, DeveloperName, MasterLabel, Valor__c FROM ConfiguracaoRightNow__mdt]);

		// Recupera da configuração os endpoints
		String endpointTask = mapConfiguracoes.get('ENDPOINT_TASKS');
		String endpointIncident = mapConfiguracoes.get('ENDPOINT_INCIDENTS');

		// Busca a tarefa criada no setup
		Task objTask = [SELECT Id, Status, IdTarefaRightNow__c, IdIncidenteRightNow__c FROM Task  WHERE Subject = 'Task teste' LIMIT 1];

		// Atualiza o path dos endpoints com os Ids salvos na tarefa
		endpointTask = String.format(endpointTask, new List<Object>{ objTask.IdTarefaRightNow__c });
		endpointIncident = String.format(endpointIncident, new List<Object>{ objTask.IdTarefaRightNow__c });

		// Prepara os mocks de resposta
		MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
		multimock.setStaticResource(endpointTask, 'RightNowResposta');
		multimock.setStaticResource(endpointIncident, 'RightNowResposta');
		multimock.setStatusCode(200);
		multimock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, multimock);
		
		// Altera o status da tarefa
		objTask.Status = 'Concluído';

		// Atualiza a tarefa para disparo da trigger
        Test.StartTest();
        update objTask;
        Test.StopTest();

        // Busca de logs que mas não deve ter nenhum resultado
		List<ApplicationLog__c> lstLog = [SELECT Id FROM ApplicationLog__c];
        
        // Valida se a lista está vazia devido ao sucesso da chamada
        System.assertEquals(lstLog.isEmpty(), true);
	}

	@IsTest
	static void testarDelecaoTarefaSucess()
	{
		Task objTask = [SELECT Id, Status, IdTarefaRightNow__c, IdIncidenteRightNow__c FROM Task  WHERE Subject = 'Task teste delete' LIMIT 1];

		Test.startTest();
		delete objTask;
		Test.stopTest();
	}

	@IsTest
	static void testarDelecaoTarefaErro()
	{
		Task objTask = [SELECT Id, Status, IdTarefaRightNow__c, IdIncidenteRightNow__c FROM Task  WHERE Subject = 'Task teste delete' LIMIT 1];
		User usuario = [select id from user where Username = 'user@testMock.com'];
		
		Test.startTest();

		System.runAs(usuario)
		{
			try 
			{
				delete objTask;	
			}
			catch (Exception ex) 
			{
				System.assertEquals(true, ex.getMessage().containsIgnoreCase('Somente o administrador do sistema pode excluir tarefas!'));
			}
			
			
			Test.stopTest();
		}
		
	}
}