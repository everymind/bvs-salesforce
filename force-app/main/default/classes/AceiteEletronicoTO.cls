public class AceiteEletronicoTO {
    public class Endereco{
        public String tipo { get; set; }
        public String logradouro { get; set; }
        public String numero { get; set; }
        public String complemento { get; set; }
        public String bairro { get; set; }
        public String cidade { get; set; }
        public String uf { get; set; }
        public String cep { get; set; }
    }

    public class Contato{
        public String nome { get; set; }
        public String sobrenome { get; set; }
        public String cpf { get; set; }
        public String email { get; set; }
        public String dataNascimento { get; set; }
        public String ddd { get; set; }
        public String telefone { get; set; }
        public String cargo { get; set; }
    }

    public class Oportunidade{
        public String categoria { get; set; }
        public String fantasia { get; set; }
        public String formaPagamento { get; set; }
        public String regraNegocios { get; set; }
        public Double valor { get; set; }
        public Endereco endereco { get; set; }
    }

    public class Operador{
        public String nome { get; set; }
        public String email { get; set; }
        public String cpf { get; set; }
        public String dataNascimento { get; set; }
    }

    public class AceiteEletronico{
        public String aceite { get; set; }
        public Id idOportunidade { get; set; }
        public Id conta { get; set; }
        public Id usuario { get; set; }
        public String documento { get; set; }
        public String razaoSocial { get; set; }
        public String fantasia { get; set; }
        public String carteira { get; set; }
        public String inscricaoMunicipal { get; set; }
        public String inscricaoEstadual { get; set; }
        public String optanteSimples { get; set; }
        public String acNFE { get; set; }
        public String observacaoNF { get; set; }
        public String observacaoFaturamento { get; set; }
        public String modalidadeCobranca { get; set; }
        public String campanha { get; set; }
        public String canal { get; set; }
        public String pme { get; set; }
        public Endereco endereco { get; set; }
        public Contato contatoCobranca { get; set; }
        public Endereco enderecoCobranca { get; set; }
        public Contato contato { get; set; }
        public Oportunidade oportunidade { get; set; }
        public Operador operador { get; set; }
        public String tipoCliente { get; set; }
        public String codigoConta { get; set; }
    }

    public class AceiteEletronicoRetorno{
        public String oportunidade { get; set; }
        public String codigoCliente { get; set; }
        public String operadorAC { get; set; }
        public String statusCliente { get; set; }
        public String codigoRetorno { get; set; }
        public String mensagemRetorno { get; set; }
        public String dados { get; set; }
    }
}