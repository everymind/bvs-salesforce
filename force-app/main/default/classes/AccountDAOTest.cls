@IsTest 
public with sharing class AccountDAOTest {

    @isTest
    private static void testBuscarPorContactId(){
        Test.startTest();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = testDataFactory.gerarContato(true, conta);

        Account conta1 = AccountDAO.getInstance().buscarPorContactId(contato.Id);
        Test.stopTest();
        System.assertEquals(conta.Id, conta1.Id);
    }



}