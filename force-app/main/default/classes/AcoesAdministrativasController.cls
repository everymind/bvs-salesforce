public with sharing class AcoesAdministrativasController {
    
    @AuraEnabled
    public static void iniciarReencarteiramento(){
        ReencarteiramentoBatch.start();
    }
}