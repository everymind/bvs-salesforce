public class OpportunityLineItemTriggerHandler extends TriggerHandler {
    
    private OpportunityLineItemBO objBO = OpportunityLineItemBO.getInstance();
    
    public override void afterInsert(){
		objBO.marcarFlagOpp(Trigger.new);
 	}
    
    public override void beforeDelete(){
		objBO.desmarcarFlagOpp(Trigger.old);
        
    }

}