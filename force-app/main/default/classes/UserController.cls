public with sharing class UserController {
    @AuraEnabled(cacheable=true)
    public static User usuarioLogado(){
        String userId = UserInfo.getUserId();
        User activeUser = UserDAO.getInstance().buscarPorId(userId);
        return(activeUser);
    }
}