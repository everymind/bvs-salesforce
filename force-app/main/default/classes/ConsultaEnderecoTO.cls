/**
 * @Autor: Everymind
 * @Data: 04/07/2020
 * @Descrição: Classe que representa o objeto de retorno da consulta de endereço da BVS.
 */
public class ConsultaEnderecoTO {
	
	@AuraEnabled
	public String CodigoRetorno { get; set; }
	@AuraEnabled
	public String MensagemRetorno { get; set; }
	@AuraEnabled
	public String CEP { get; set; }
	@AuraEnabled
	public String Logradouro { get; set; }
	@AuraEnabled
	public String Bairro { get; set; }
	@AuraEnabled
	public String Cidade { get; set; }
	@AuraEnabled
	public String UF { get; set; }
	@AuraEnabled
	public String Numero { get; set; }
	@AuraEnabled
	public String Complemento { get; set; }

	public ConsultaEnderecoTO() {
		this.CodigoRetorno = '';
		this.MensagemRetorno = '';
		this.CEP = '';
		this.Logradouro = '';
		this.Bairro = '';
		this.Cidade = '';
		this.UF = '';
		this.Numero = '';
		this.Complemento = '';
	}
}