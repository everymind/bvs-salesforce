@IsTest
public class SelecaoCampanhaTest {
	
	@TestSetup
	static void setup() {
		// Busca o tipo de território para associação no usuário de contexto
		Territory2Type objTerritory2Type = [SELECT Id FROM Territory2Type LIMIT 1];

		// Busca o modelo de território para associação no usuário de contexto
		Territory2Model objTerritory2Model = [SELECT Id FROM Territory2Model WHERE State = 'Active' LIMIT 1];

		// Cria um território de teste para associar ao usuário de contexto
		Territory2 objTerritory2 = new Territory2(
			Name = 'Teste Territory',
			DeveloperName = 'TesteTerritorio',
			Territory2TypeId = objTerritory2Type.Id,
			Territory2ModelId = objTerritory2Model.Id,
			Canal__c = 'CanalTeste',
			Regional__c = 'RegionalTeste'
		);

		insert objTerritory2;

		// Cria a associação do território 1 ao usuário que executa o teste
		UserTerritory2Association objUserTerritory2Association = new UserTerritory2Association(
			UserId = UserInfo.getUserId(),
			Territory2Id = objTerritory2.Id
		);

		insert objUserTerritory2Association;
	}

	@IsTest
	static void testarSelecaoCampanhaComSucesso() {
		// Busca território criado no Setup
		Territory2 objTerritory2 = [SELECT Id FROM Territory2 WHERE DeveloperName = 'TesteTerritorio' LIMIT 1];

		// Recupera Id do tipo de registro de pessoa jurídica da conta
		Id tipoRegistroPessoaJuridicaId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId();

		// Cria conta de teste
		Account objAccount = new Account(
			Name = 'Teste Account',
			CnpjCpf__c = '45406480000123',
			RecordTypeId = tipoRegistroPessoaJuridicaId
		);

		insert objAccount;

		// Cria associação da conta com o território
		ObjectTerritory2Association objObjectTerritory2Association = new ObjectTerritory2Association(
			ObjectId = objAccount.Id,
			Territory2Id = objTerritory2.Id,
			AssociationCause = 'Territory2Manual'
		);

		insert objObjectTerritory2Association;

		// Recupera Id do tipo de registro de venda de plano de oportunidade
		Id vendaPlanoId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('VendaPlano').getRecordTypeId();

		// Cria oportunidade para exibição da tela de seleção de planos
		Opportunity objOpportunity = new Opportunity(
			Name = 'Teste de oportunidade',
			StageName = 'Prospecção',
			CloseDate = System.today(),
			RecordTypeId = vendaPlanoId,
			AccountId = objAccount.Id,
			OwnerId = UserInfo.getUserId()
		);

		insert objOpportunity;

		// Chama controller para validar se trouxe a URL preenchida
		SelecaoCampanhaTO objTO = SelecaoCampanhaController.buscarIFrame(objOpportunity.Id);

		// Valida resultados
		System.assertEquals(objTO.sucesso, true);
	}
}