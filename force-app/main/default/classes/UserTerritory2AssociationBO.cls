/**
 * @Autor: Everymind
 * @Data: 21/08/2020
 * @Descrição: Classe que contém métodos de negócio referente ao domínio Account.
 */
public class UserTerritory2AssociationBO {
	
	/**
	 * @Autor: Everymind
	 * @Data: 21/08/2020
	 * @Descrição: Constante que representa uma instância da classe.
	 */
	private static final UserTerritory2AssociationBO instance = new UserTerritory2AssociationBO();

	/**
	 * @Autor: Everymind
	 * @Data: 21/08/2020
	 * @Descrição: Método público que retorna uma instância da classe.
	 */
    public static UserTerritory2AssociationBO getInstance() {
        return instance;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 21/08/2020
	 * @Descrição: Método que atribui o novo registro ao processo de reencarteiramento
	 * @Parâmetro: List<UserTerritory2Association> lstUserTerritory2AssociationNovas - Lista que contém a instância dos registros de contas após a criação.
	 */
	public void atualizarReencarteiramento(List<UserTerritory2Association> lstUserTerritory2AssociationNovas){
		// Queueble
		System.debug('lstUserTerritory2AssociationNovas.size()');
		System.debug(lstUserTerritory2AssociationNovas.size());
		List<UserTerritory2Association> lstLoteUserTerritory2Association = new List<UserTerritory2Association>();
		Integer numeroMaximoLote = 0;
		List<ConfiguracaoDadosTerritorio__mdt> configuracaoDadosTerritorio = [SELECT Id, DeveloperName, MasterLabel FROM ConfiguracaoDadosTerritorio__mdt WHERE DeveloperName = 'QueuebleMaxSize'];
		if (configuracaoDadosTerritorio.size() > 0){
			numeroMaximoLote = Integer.valueOf(configuracaoDadosTerritorio[0].MasterLabel);
		}else{
			numeroMaximoLote = 50;
		}
		Integer numeroLotesQueueable = lstUserTerritory2AssociationNovas.size() / numeroMaximoLote;

		Integer registrosPorLote = 0;
		Integer registrosEnviados = 0;
		for (UserTerritory2Association objUserTerritory2Association : lstUserTerritory2AssociationNovas){

			if (registrosPorLote == numeroMaximoLote){
				registrosPorLote = 0;
				UserTerritory2AssociationQueueable job = new UserTerritory2AssociationQueueable(lstLoteUserTerritory2Association);
            	System.enqueueJob(job);
				lstLoteUserTerritory2Association.clear();
			}

			lstLoteUserTerritory2Association.add(objUserTerritory2Association);
			registrosPorLote++;
			registrosEnviados++;
		}
		if (lstLoteUserTerritory2Association.size() > 0){
			UserTerritory2AssociationQueueable job = new UserTerritory2AssociationQueueable(lstLoteUserTerritory2Association);
			System.enqueueJob(job);
			lstLoteUserTerritory2Association.clear();
		}
		System.debug('registrosEnviados');
		System.debug(registrosEnviados);
	}

}