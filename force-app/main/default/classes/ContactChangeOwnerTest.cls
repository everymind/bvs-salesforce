/**
 * @Autor: Maycon
 * @Data: 08/08/2022
 * @Descrição: Classe de teste que envia as contas relacionadas para o Bolsão Interno
 */
@IsTest
public class ContactChangeOwnerTest{

    @isTest
    static void alteraProprietarioContato(){

        Map<String, String> mapConfiguracoes = buscarConfiguracoes();

        String bucketAccount = mapConfiguracoes.get('BucketAccount');

        Contact objContact = new Contact(
            FirstName = 'Teste Inativo',
            LastName = 'Teste ContactChangeOwnerTest',
            Email = 'teste@teste.com',
            Updated__c = true,
            AccountId = bucketAccount,
            Status__c = 'Inativo',
            OwnerId = '0053h000003slAZAAY'
        );

        insert objContact;

        Test.startTest();
        ContactChangeOwner batchRun = new ContactChangeOwner();
        ID batchprocessid = Database.executeBatch(batchRun);
        Test.stopTest();

        Contact contactIncluded = [SELECT Id, OwnerId, Status__c FROM Contact WHERE LastName = 'Teste ContactChangeOwnerTest' LIMIT 1];

        System.assertEquals(contactIncluded.Status__c, 'Inativo', 'o contato não teve o proprietario alterado.');
    }

    private static Map<String, String> buscarConfiguracoes() {
		Map<String, String> mapConfiguracoes = new Map<String, String>();

		CollectionUtil.carregarMap(mapConfiguracoes, 'DeveloperName', 'Valor__c', [SELECT Id, DeveloperName, MasterLabel, Valor__c FROM Contact_Metadata__mdt]);

		return mapConfiguracoes;
	}
}