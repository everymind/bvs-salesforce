/**
 * @Autor: Everymind
 * @Data: 20/07/2020
 * @Descrição: Classe que orquestra os eventos da trigger para os métodos de negócio.
 */
public class TaskTriggerHandler extends TriggerHandler {

	/**
	 * @Autor: Everymind
	 * @Data: 20/07/2020
	 * @Descrição: Constante que contém a instância da classe de negócio do objeto Task.
	 */
	private TaskBO objBO = TaskBO.getInstance();

	/**
	 * @Autor: Everymind
	 * @Data: 20/07/2020
	 * @Descrição: Método que é invocado quando o evento "after update" acontece na trigger.
	 */
	public override void afterUpdate() {
		objBO.enviarTarefaRightNow((Map<Id, Task>)Trigger.newMap, (Map<Id, Task>)Trigger.oldMap);
	}
	
	public override void beforeDelete() {
		objBO.bloquearExclusaoTarefas(Trigger.old);
	}

}