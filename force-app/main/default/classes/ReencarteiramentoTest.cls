/**
 * @Autor: Everymind
 * @Data: 25/07/2020
 * @Descrição: Classe de teste para o Batch de Reencarteiramento.
 */
@IsTest
public class ReencarteiramentoTest {

	/**
	 * @Autor: Everymind
	 * @Data: 25/07/2020
	 * @Descrição: Método que prepara a configuração de território necessária para os testes.
	 */
	@TestSetup
	static void setup() {
		// Cria usuário para associação do território
		User objUser = new User(
			FirstName = 'Usuário',
			LastName = 'Teste',
			UserName = 'usuario.teste@boavistascpc.com.br',
			Email = 'usuario.teste@boavistascpc.com.br',
			Alias = 'teste',
			LanguageLocaleKey = 'pt_BR',
			LocaleSidKey = 'pt_BR',
			TimeZoneSidKey = 'America/Sao_Paulo',
			EmailEncodingKey = 'UTF-8',
			Title = 'Developer',
			CompanyName = 'Boa Vista',
			Department = 'TI',
			PostalCode = '00000000',
			Street = 'Rua Teste',
			City = 'São Paulo',
			State = 'SP',
			Country = 'Brasil',
			ProfileId = UserInfo.getProfileId()
		);

		insert objUser;

		// Busca o tipo de território para associação no usuário de contexto
		Territory2Type objTerritory2Type = [SELECT Id FROM Territory2Type LIMIT 1];

		// Busca o modelo de território para associação no usuário de contexto
		Territory2Model objTerritory2Model = [SELECT Id FROM Territory2Model WHERE State = 'Active' LIMIT 1];

		// Cria um território de teste para associar ao usuário de contexto
		Territory2 objTerritory2 = [SELECT Id FROM Territory2 WHERE DeveloperName = 'CALATV' LIMIT 1];
 
		// Cria a associação do território ao usuário que executa o teste
		UserTerritory2Association objUserTerritory2Association = new UserTerritory2Association(
			UserId = UserInfo.getUserId(),
			Territory2Id = objTerritory2.Id
		);

		insert objUserTerritory2Association;

		// Cria a associação do território ao novo usuário
		UserTerritory2Association objUserTerritory2Association2 = new UserTerritory2Association(
			UserId = objUser.Id,
			Territory2Id = objTerritory2.Id,
			RoleInTerritory2 = 'Proprietário'
		);

		insert objUserTerritory2Association2;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 25/07/2020
	 * @Descrição: Método que testa o reencarteiramento de uma oportunidade.
	 */
	@IsTest
	static void testarReencarteiramentoComSucesso() {
		// Busca território criado no Setup
		Territory2 objTerritory2 = [SELECT Id FROM Territory2 WHERE DeveloperName = 'CALATV' LIMIT 1];

		// Recupera Id do tipo de registro de pessoa jurídica da conta
		Id tipoRegistroPessoaJuridicaId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId();

		// Cria conta de teste
		Account objAccount = new Account(
			Name = 'Teste Account',
			Carteira__c = 'CALATV',
			CnpjCpf__c = '45406480000123',
			RecordTypeId = tipoRegistroPessoaJuridicaId
		);

		insert objAccount;

		TestDataFactory.gerarContato(true, objAccount);

		System.runAs([SELECT Id FROM User WHERE Id = :UserInfo.getUserId()][0]){

			// Cria associação da conta com o território
			ObjectTerritory2Association objObjectTerritory2Association = new ObjectTerritory2Association(
				ObjectId = objAccount.Id,
				Territory2Id = objTerritory2.Id,
				AssociationCause = 'Territory2Manual'
			);

			insert objObjectTerritory2Association;

			// Cria oportunidade onde o proprietário está no território
			Opportunity objOpportunity = new Opportunity(
				Name = 'Teste Oportunidade',
				StageName = 'Qualification',
				CloseDate = System.today(),
				AccountId = objAccount.Id,
				OwnerId = UserInfo.getUserId()
			);

			insert objOpportunity;

			// Remover o proprietário da oportunidade do território
			delete new UserTerritory2Association(
				Id = [SELECT Id FROM UserTerritory2Association WHERE Territory2.DeveloperName = 'CALATV' AND UserId = :UserInfo.getUserId()][0].Id
			);

			// Executa teste para reatribuição da carteira
			Test.startTest();
			ReencarteiramentoBatch.start();
			Test.stopTest();

			// Busca oportunidade pra saber se a reatribuição foi concluída
			Opportunity objOpportunityPosReencarteiramento = [SELECT Id, OwnerId, Owner.Name FROM Opportunity LIMIT 1];

			//UserTerritory2Association objUserTerritoryAssociationOpportunity = [SELECT Id, RoleInTerritory2 FROM UserTerritory2Association WHERE Territory2.DeveloperName = 'CALATV' AND UserId = :objOpportunityPosReencarteiramento.OwnerId][0];

			// Valida resultados
			//System.assertEquals('Proprietário', objUserTerritoryAssociationOpportunity.RoleInTerritory2);
		}
	}
}