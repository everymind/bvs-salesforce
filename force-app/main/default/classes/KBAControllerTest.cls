@isTest
public with sharing class KBAControllerTest {
    public KBAControllerTest() {}

    @isTest 
    public static void PostQuestionsKBATest()
    {
        test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('PostKba');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        
        Test.setMock(HttpCalloutMock.class, mock);
        KBAController.ResultadoRequisicao teste = KBAController.PostQuestionsKBA('11111111111');
        test.stopTest();


        system.assertEquals(teste.ResultMessage,'Generated');
    }   


    @isTest 
    public static void PostAnswersKBATest()
    {
        test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('PostKbaAnswers');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        
        Test.setMock(HttpCalloutMock.class, mock);
        KBAController.ResultadoRequisicao teste = KBAController.PostAnswersKBA('11111111111', new string[]{});
        test.stopTest();


        system.assertEquals(teste.ResultMessage,'Validated');
    }   

    
    @isTest 
    public static void buscaCPFTest()
    {
        test.startTest();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(false, conta);
        contato.cpf__c = '11111111111';
        insert contato;

        string teste = KBAController.buscaCPF(contato.id);
        test.stopTest();


        system.assertEquals(teste,'11111111111');
    }   
    @isTest
    public static void getBuscarTempoKBATest()
    {
        test.startTest();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(false, conta);
        contato.cpf__c = '11111111111';
        insert contato;

        decimal teste = KBAController.getBuscarTempoKBA(contato.id);
        test.stopTest();


        system.assertEquals(teste,1);
    }


}