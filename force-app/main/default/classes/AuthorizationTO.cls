global class AuthorizationTO {
    global class PostReturn {
        global String Status { get; set; }
        global String Message { get; set; }
    }
}