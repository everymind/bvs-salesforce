/**
 * @Autor: Everymind
 * @Data: 11/07/2020
 * @Descrição: Classe controladora do componente que exibe o IFrame de seleção de campanha na oportunidade.
 */
public class SelecaoCampanhaController {

	/**
	 * @Autor: Everymind
	 * @Data: 11/07/2020
	 * @Descrição: Método responsável por buscar as informações necessárias para exibir o IFrame de seleção de campanha.
	 * @Parâmetro: String - recordId. Id da oportundiade injetado pelo componente.
	 * @Retorno: SelecaoCampanhaTO - Objeto que representa as informações de exibição e funcionamento do IFrame.
	 */
		public Opportunity opportunity {get;set;}
		public SelecaoCampanhaTO objTO {get;set;} 
	
			
		public SelecaoCampanhaController(ApexPages.StandardController controller)
		{
		
			this.opportunity = (Opportunity)controller.getRecord();
			objTO = buscarIFrame(this.opportunity.id);
	
		 }   	

	@AuraEnabled
	public static SelecaoCampanhaTO buscarIFrame(String recordId) {
		// Instância de objeto de retorno
		SelecaoCampanhaTO objTO = new SelecaoCampanhaTO();

		// Buscar configurações da URL
		Map<String, String> mapConfiguracoes = buscarConfiguracoes();
		
		// Buscar dados da oportunidade
		Opportunity objOportunidade = buscarOportunidade(recordId);

		// Valida se a oportunidade tem conta
		if(String.isBlank(objOportunidade.AccountId) || String.isBlank(objOportunidade.Account.CnpjCpf__c)) {
			return objTO;
		}

		// Verifica configurações cadastradas
		if(mapConfiguracoes.containsKey('host') && mapConfiguracoes.containsKey('token')) {
			objTO.sucesso = true;
		}

		// Valida se a oportunidade tem conta
		if(String.isBlank(objOportunidade.AccountId) || String.isBlank(objOportunidade.Account.CnpjCpf__c)) {
			return objTO;
		}

		// Buscar dados do produto da oportunidade
		OpportunityLineItem objItemOportunidade = buscarItemOportunidade(recordId);

		// Valida se existe o produto da oportunidade
		if(objItemOportunidade == null) {
			return objTO;
		}

		// Buscar informações do território associado a conta
		ObjectTerritory2Association objAssociation = buscarObjectTerritory2Association(objOportunidade.AccountId);

		// Valida se existe o território
		if(objAssociation == null || String.isBlank(objAssociation.Territory2.Canal__c)) {
			return objTO;
		}

		// Recupera host configurado
		objTO.host += mapConfiguracoes.get('host');

		// Recupera origin configurado
		objTO.origin += mapConfiguracoes.get('origin');

		// Recupera o token da sessão ativa
		String content = '';
		PageReference vf = Page.SessionVF;
		content = vf.getContent().toString();

		// quebra a string de sessão, pegando apenas as infos necessárias
		Integer s = content.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
		e = content.indexOf('End_Of_Session_Id');

		String sID = content.substring(s, e);

		//Variaveis
		objTO.token = mapConfiguracoes.get('token'); //sID; 
		objTO.sfId=  sID; //'00D2h0000000q6c!AQMAQIJXR_0LYba4pXXs8FsR2Vb2yWvYWrc8f.cbF0pEYYyHadc1TEa5aZSVaShbNsXszVjptpxlg9ikc47fSEGcDuA6fWJp';
		objTO.oportunidade=  objOportunidade.Id;
		objTO.conta= objOportunidade.AccountId;
		objTO.cnpj= objOportunidade.Account.CnpjCpf__c ;
		objTO.codigo= (String.isNotBlank(objOportunidade.Account.Codigo8__c) ? objOportunidade.Account.Codigo8__c : '');
		objTO.carteira= (String.isNotBlank(objOportunidade.Account.Carteira__c) ? objOportunidade.Account.Carteira__c : '');
		objTO.canal_territorio= objAssociation.Territory2.Canal__c;
		objTO.item_receita= objItemOportunidade.Id;
		objTO.regional= (String.isNotBlank(objAssociation.Territory2.Regional__c) ? objAssociation.Territory2.Regional__c : '');
		objTO.regra_atual=(String.isNotBlank(objOportunidade.NumeroPlano__c) ? objOportunidade.NumeroPlano__c : '');

		// Monta os parâmetros da URL
		/*
		objTO.params += 'token=' + mapConfiguracoes.get('token') + '&';
		objTO.params += 'sfId=' + sID + '&';
		objTO.params += 'oportunidade=' + objOportunidade.Id + '&';
		objTO.params += 'conta=' + objOportunidade.AccountId + '&';
		objTO.params += 'cnpj=' + objOportunidade.Account.CnpjCpf__c + '&';
		objTO.params += 'codigo=' + (String.isNotBlank(objOportunidade.Account.Codigo8__c) ? objOportunidade.Account.Codigo8__c : '') + '&';
		objTO.params += 'carteira=' + (String.isNotBlank(objOportunidade.Account.Carteira__c) ? objOportunidade.Account.Carteira__c : '') + '&';
		objTO.params += 'canal_territorio=' + objAssociation.Territory2.Canal__c + '&';
		objTO.params += 'item_receita=' + objItemOportunidade.Id + '&';
		objTO.params += 'regional=' + (String.isNotBlank(objAssociation.Territory2.Regional__c) ? objAssociation.Territory2.Regional__c : '') + '&';
		objTO.params += 'regra_atual=' + (String.isNotBlank(objOportunidade.NumeroPlano__c) ? objOportunidade.NumeroPlano__c : '');

		// Converte os parâmetros em base64
		objTO.params = EncodingUtil.base64Encode(Blob.valueOf(objTO.params));

		// Monta a URL completa que será chamada no IFrame
		objTO.url += objTO.host + '?params=';
		objTO.url += objTO.params;

		// Imprime resultado do objeto
		System.debug(':objTO: ' + objTO);
		*/
		return objTO;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 11/07/2020
	 * @Descrição: Método responsável por buscar as configurações cadastradas no metadado ConfiguracaoSelecaoCampanha__mdt.
	 * @Parâmetro: N/A.
	 * @Retorno: Map<String, String> - Objeto com chave e valor das configurações retornadas.
	 */
	private static Map<String, String> buscarConfiguracoes() {
		Map<String, String> mapConfiguracoes = new Map<String, String>();

		List<ConfiguracaoSelecaoCampanha__mdt> lstConfiguracoes = [
			SELECT Id, DeveloperName, Valor__c 
			FROM ConfiguracaoSelecaoCampanha__mdt
		];

		for(ConfiguracaoSelecaoCampanha__mdt obj : lstConfiguracoes) {
			mapConfiguracoes.put(obj.DeveloperName, obj.Valor__c);
		}

		return mapConfiguracoes;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 11/07/2020
	 * @Descrição: Método responsável por buscar as informações da oportunidade em questão.
	 * @Parâmetro: String - oppId. Id da oportundiade injetado pelo componente.
	 * @Retorno: Opportunity - Objeto de oportunidade.
	 */
	private static Opportunity buscarOportunidade(String oppId) {
		return [SELECT Id, AccountId, Account.CnpjCpf__c, Account.Carteira__c, Account.Codigo8__c, NumeroPlano__c
				FROM Opportunity 
				WHERE Id = :oppId];
	}

	/**
	 * @Autor: Everymind
	 * @Data: 25/07/2020
	 * @Descrição: Método responsável por buscar as informações do item da oportunidade em questão.
	 * @Parâmetro: String - oppId. Id da oportundiade injetado pelo componente.
	 * @Retorno: OpportunityLineItem - Objeto de item da oportunidade.
	 */
	private static OpportunityLineItem buscarItemOportunidade(String oppId) {
		List<OpportunityLineItem> lst = new List<OpportunityLineItem>();
		lst = [	SELECT Id, OpportunityId
				FROM OpportunityLineItem 
				WHERE OpportunityId = :oppId];
		return !lst.isEmpty() ? lst.get(0) : null;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 25/07/2020
	 * @Descrição: Método responsável por buscar as informações do território da conta.
	 * @Parâmetro: String - accountId. Id da conta da oportunidade.
	 * @Retorno: ObjectTerritory2Association - Objeto de associação da conta com o território.
	 */
	private static ObjectTerritory2Association buscarObjectTerritory2Association(String accountId) {
		List<ObjectTerritory2Association> lst = new List<ObjectTerritory2Association>();
		lst = [SELECT Id, ObjectId, Territory2Id, Territory2.Canal__c, Territory2.Regional__c FROM ObjectTerritory2Association WHERE ObjectId = :accountId];
		return !lst.isEmpty() ? lst.get(0) : null;
	}
}