@isTest
public class OppLineItemTriggerTest {

    static testMethod void myUnitTest() {
     
        test.startTest();
        Account c = new Account(Name = 'Teste Conta');
        insert c;
        update c;
        
        Opportunity opp = new Opportunity(	Name = 'teste',
        									StageName = 'Prospecção',
                                            TemProduto__c = false,
        									DataInicioFaturamento__c = date.parse('20/10/2021'),
       										CloseDate = date.parse('23/10/2021'),
        									TipoReceita__c = 'Recorrente',
        									AccountId = c.Id);
        
        insert opp;
        update opp;
        
        Product2 produto = new Product2(Name = 'ACERTA COMPLETO POSITIVO',
                                        family = 'ANTIFRAUDE');
        insert produto;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        Pricebook2 pricebook = new Pricebook2(Name = 'Standard Price Book');
        insert pricebook;
        
        PricebookEntry tabela = new PricebookEntry(UnitPrice = 0,
                                                   Product2Id = produto.Id,
                                                   isActive = true,
                                                   Pricebook2Id = pricebookId);
        insert tabela;
        
       
        OpportunityLineItem p = new OpportunityLineItem(Quantity = 2,
                                                       	UnitPrice = 0,
        												OpportunityId = opp.Id,
                                                        PricebookEntryId = tabela.Id);
        
        insert p;
        update p;
        
        List<OpportunityLineItem> lst = new List<OpportunityLineItem>();
		lst.add(p);
    	system.debug('lst ' + lst);
        

        
       OpportunityLineItemBO.getInstance().marcarFlagOpp(lst);
        
       delete p; 
       OpportunityLineItemBO.getInstance().desmarcarFlagOpp(lst);
       //OpportunityLineItemTriggerHandler t = new OpportunityLineItemTriggerHandler();
       //t.afterInsert();
	   //t.beforeDelete();
		
        
        test.stopTest();
    }
}