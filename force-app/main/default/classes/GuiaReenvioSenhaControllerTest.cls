@isTest
public with sharing class GuiaReenvioSenhaControllerTest {
    public GuiaReenvioSenhaControllerTest() {}

@isTest
    public static void buscaContatosRelacionadosTest()
    {
        Account conta = TestDataFactory.gerarContaJuridica(true);
        TestDataFactory.gerarContato(true, conta);
        TestDataFactory.gerarContato(true, conta);
        List<Contact> lstContact = GuiaReenvioSenhaController.buscaContatosRelacionados(conta.id);
        system.assertEquals(lstContact.size(), 2);

    }
}