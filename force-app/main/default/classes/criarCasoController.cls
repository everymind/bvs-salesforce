public without sharing class criarCasoController {

    @AuraEnabled
    public static String pegarTipoConta(String idConta){
        
        Account conta = AccountDAO.getInstance().buscarPorId(idConta);
        System.debug('Tipo da conta: ' + conta.Recordtype.DeveloperName);
        return conta.Recordtype.DeveloperName;
    }

    
    @AuraEnabled(cacheable = true)
    public static List<Contact> buscaContatosRelacionados(Id accId)
    {

        Set<Id> Ids = new Set<Id>();
        Set<Id> IdsContact = new Set<Id>();

        List<Contact> listContacts = [
            SELECT Id, Name
            FROM Contact
            WHERE AccountId =: accId
        ];
        List<AccountContactRelation> listAccountRelation = [
            SELECT ContactId, Contact.Name
            FROM AccountContactRelation
            WHERE AccountId =: accId
        ];

        for(Contact contactAcc : listContacts)
        {
            if(!IdsContact.contains(contactAcc.Id))
            {
                 IdsContact.add(contactAcc.Id);
            }
        }
        for(AccountContactRelation accountContactRelationAcc : listAccountRelation)
        {
               
            if(!IdsContact.contains(accountContactRelationAcc.ContactId))
            {
                IdsContact.add(accountContactRelationAcc.ContactId);
            }
        }
        List<Contact> lstResult = [
            SELECT Id, Name
            FROM Contact
            WHERE ID IN: IdsContact
        ];

        return lstResult;
    }
    @AuraEnabled
    public static Case salvarCaso(
        String conta,
        String area,
        String origem,
        String prioridade,
        String status,
        String descricao,
        String produto,
        String contato,
        String tipoRegistro
    ){

        id idTipoRegistro;

        if(tipoRegistro == 'CSM Atendimento'){
            idTipoRegistro = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMAtendimento').getRecordTypeId();
        } else{
            idTipoRegistro = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMPosVendas').getRecordTypeId();
        }
        if(tipoRegistro == 'CSM Atendimento' && origem != 'GC'){
            origem = 'ATENDIMENTO';
        } else if (tipoRegistro == 'CSM Pós Vendas' && origem != 'GC'){
            origem = 'PÓS VENDAS';
        } 

        Case caso = new Case();
        caso.Description = descricao;
        caso.AccountId = conta;
        caso.Area__c = area;
        caso.Priority = prioridade;
        caso.Status = status;
        caso.ProductId = produto;
        caso.ContactId = contato;
        caso.RecordTypeId = idTipoRegistro;
        caso.Origin = origem;

        if(caso.Origin == 'GC' && tipoRegistro == 'CSM Atendimento')
        {
            String userId = UserInfo.getUserId();
            User usuario = UserDAO.getInstance().buscarPorIdGrupo(userId);
            if(usuario.Grupo__c != null)
            {
                List<FilaInicialAtendimento__mdt> lstFila = MetadataDAO.getInstance().buscarFilaInicialAtendimento(usuario.Grupo__c);
                if (lstFila != null && !lstFila.isEmpty()){
                    Group objGroup = GroupDAO.getInstance().buscarPorFilaAtendimento(lstFila[0].Departamento__c);
                    if (objGroup != null){
                        caso.OwnerId = objGroup.Id;
                    }
                }
            }

        }else if(caso.Origin == 'GC' && tipoRegistro == 'CSM Pós Vendas')
        {
            String regional;
            if (caso.AccountId!=null){
                Account contaAcc =AccountDAO.getInstance().buscarPorId(conta);
				regional=contaAcc.Regional__c;
            } else{
                regional='';
            }
            System.debug('regional = ' + regional);
            FilaInicialPosVendas__mdt filaInicialPosVendas = MetadataDAO.getInstance().buscarFilaInicialPosVendas(regional);     
            if (filaInicialPosVendas != null){
                Group grupo = GroupDAO.getInstance().buscarPorFila(filaInicialPosVendas.Fila__c);
                if (grupo != null){
                    caso.OwnerId = grupo.Id;
                }
            }
        }
        INSERT caso;
        return(caso);
    }
}