/**
 * @Autor: Everymind
 * @Data: 21/08/2020
 * @Descrição: Classe que contém métodos de teste do UserTerritory2Association.
 */
@IsTest
public class UserTerritory2AssociationTest {

	/**
	 * @Autor: Everymind
	 * @Data: 21/08/2020
	 * @Descrição: Método que testa o erro de trigger de inclusão / Exclusão no objeto UserTerritory2Association.
	 */
    @IsTest
    static void testarTriggerInclusaoExclusao() {
        Territory2Type territory2Type = [SELECT id FROM Territory2Type LIMIT 1];
        Territory2Model territory2Model = [SELECT id FROM Territory2Model LIMIT 1];
        Territory2 territory2 = new Territory2(
             Name = 'TESTE'
            ,DeveloperName = 'TESTE'
            ,Description = 'TESTE'
            ,Territory2TypeId = territory2Type.Id
            ,Territory2ModelId = territory2Model.Id
            ,Canal__c = 'CANALTESTE'
            ,Canal_Agrupado__c = 'CANALAGRUPADOTESTE'
            ,Regional__c = 'REGIONALTESTE'
        );
        insert territory2;

        Profile profile = [SELECT Id FROM Profile WHERE PermissionsContentAdministrator = true LIMIT 1];

        User user = new User(
             FirstName = 'Usuario'
            ,LastName = 'Teste'
            ,Username = 'usuario.teste@boavistascpc.com.br.test'
            ,Email = 'usuario.teste@teste.com.br'
            ,Alias = 'uteste'
            ,TimeZoneSidKey = 'America/Sao_Paulo'
            ,LocaleSidKey = 'pt_BR'
            ,EmailEncodingKey = 'UTF-8'
            ,ProfileId = profile.Id
            ,LanguageLocaleKey = 'pt_BR'
        );
        insert user;

        UserTerritory2Association userTerritory2Association = new UserTerritory2Association(
             Territory2Id = territory2.Id
            ,UserId = user.Id
        );
        insert userTerritory2Association;

        System.assertEquals(true, userTerritory2Association != null);

        delete userTerritory2Association;
    }
}