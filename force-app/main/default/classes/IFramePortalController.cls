/**
 * @Autor: Everymind
 * @Data: 28/07/2020
 * @Descrição: Classe controladora do componente de exibição dos IFrames.
 */
public class IFramePortalController {

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método responsável por construir a url de acesso do IFrame.
	 * @Parâmetros: String recordId - Id do registro injetado pelo lightning.
	 * @Parâmetros: String portal - nome do portal selecionado no design do componente.
	 */
	public Account account {get;set;}
	public String  token {get;set;}
	public ConfiguracaoIFrame__mdt objConfiguracao {get;set;}

	public IFramePortalController(ApexPages.StandardController controller){
	
		this.account = (Account)controller.getRecord();
		token = buscarUrl(this.account.id , 'Sua Fatura',null);
		
		this.objConfiguracao = new ConfiguracaoIFrame__mdt();
		this.objConfiguracao = buscarConfiguracao('Sua Fatura');
    }  

	@AuraEnabled
	public static String buscarUrl(String recordId, String portal, String conta) {
		String url = '';

		// Busca registro de configuração
		ConfiguracaoIFrame__mdt objConfiguracao = buscarConfiguracao(portal);
		
		// Verifica se encontrou o registro de configuração
		if(objConfiguracao == null) {
			return null;
		}

		// Busca informações da conta
		Account objAccount = AccountDAO.getInstance().buscarPorId(recordId);

		// Verifica os portais que dependem da conta, possuem todos os parâmetros
		if(objAccount == null && (portal == 'Visão 360' || portal == 'Painel de Incidentes' || portal == 'Intenção de Cancelamento' || portal == 'Novo Painel Incidentes')) {
			return null;
		}

		// Atribui URL de retorno
		url += objConfiguracao.Url__c;

		String content = '';
		if (Test.isRunningTest()){
			content = 'Start_Of_Session_IdTesteEnd_Of_Session_Id';
		} else {
			PageReference vf = Page.SessionVF;
            System.debug('vf: ' + vf);
			content = vf.getContent().toString();
            System.debug('content: ' + content);
		}

		Integer s = content.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
                e = content.indexOf('End_Of_Session_Id');
		String token = objConfiguracao.Token__c;
		String ssId = content.substring(s, e); //UserInfo.getSessionId()
        System.debug('ssid: ' + ssid);
        System.debug('recordId: ' + recordId);

		// Realiza substituições de parâmetros dependendo do portal selecionado
		switch on portal {
			when 'Portal de Vendas', 'Painel' {
				url = url.replace('{token}', ssId);
			}
			when 'Reencarteiramento'{
				url = url.replace('{token}', ssId);
				url = url.replace('{userId}', UserInfo.getUserId());
			}
			when 'Visão 360' {
				url = url.replace('{token}', ssId);
				url = url.replace('{accountId}', recordId);
			}
			when 'Painel de Incidentes', 'Intenção de Cancelamento' {
				url = url.replace('{token}', token);
				url = url.replace('{AccountCNPJCPF}', objAccount.CnpjCpf__c);
			}
			when 'Novo Painel Incidentes'{ // 10/10/2022, Maycon: Incluido contexto de teste, para a execução da classe de teste

				String dynamicToken = '';

				if(Test.isRunningTest()){
					dynamicToken = tokenDinamico(true);
				}else{
					dynamicToken = tokenDinamico(false);
				}
					url = url.replace('{token}', dynamicToken);
					url = url.replace('{AccountCNPJCPF}', objAccount.CnpjCpf__c);
			}
			when 'GuiaResetSenha' {
				Contact contato = ContactDAO.getInstance().buscarPorPersonAccount(recordId);

				Contact contatoRel = ContactDAO.getInstance().buscarRelacionamentoPorContaContato(conta, contato.id);
				url += '?lk_codig='+contatoRel.OperadorAc__c;
				url += '&lk_email='+contatoRel.Email;
				url += '&sid='+ssId;

				Account contaEmpresa = AccountDAO.getInstance().buscarPorId(conta);
				url += '&org_id='+contaEmpresa.Codigo8__c;
			}
			when 'GuiaReenvioSenha' {
				System.debug('Entrou no reenvio de senha');
				Contact contato = ContactDAO.getInstance().buscarPorPersonAccount(recordId);
				Account contaEmpresa = AccountDAO.getInstance().buscarPorId(conta);

				String documento = contaEmpresa.CnpjCpf__c;
				String email = contato.Email;
				String accountId = ' ';
				String operadorId = ' ';

				String documentoFormatado = formataDocumento(documento);
				String emailFormatado = formataEmail(email);
				String accountIdFormatado = formataAccountId(accountId);
				String operadorIdFormatado = formataOperadorId(operadorId);

				url += '?param=BVSTVSNH';
				url += documentoFormatado+emailFormatado;
				url += 'TELEVENDAS';
				url += accountIdFormatado+operadorIdFormatado;

				EncodingUtil.URLENCODE(url,'UTF-8');

				// String urlFixa = 'https://phpcorp.boavistaservicos.com.br/acspnet/programas/siaph121.php?param=BVSTVSNH05199105000135gabriel.azeredo@boavistascpc.com.br%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20TELEVENDAS%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20';
			}
			when 'Sua Fatura'{
				//	String params = '';
				//	params += 'origem=SalesForce';
				//	params += '&codigoCliente=' + objAccount.Codigo8__c;
				//	params += '&sfid=' + ssId;
				//url += params;
				//url = url.replace('{token}', ssId);
				//url = url.replace('{CodigoDe8}', objAccount.Codigo8__c);
				url = ssId;
			}

			when 'Oficios'{ // 10/10/2022, Maycon: Incluido contexto de teste, para a execução da classe de teste
				
				String dynamicToken = '';

				if(Test.isRunningTest()){
					dynamicToken = tokenDinamico(true);
				}else{
					dynamicToken = tokenDinamico(false);
				}
				url = url.replace('{token}', dynamicToken);
			}
		}
        System.debug('url: ' + url);

		// Retorna url com os parâmetros devidamente substituídos
		return String.isNotBlank(url) ? url : null;
	}

	@AuraEnabled
	public static String buscarUrlcomIdContato(String recordId, String portal, String conta, String recordIdContato) {
		String url = '';

		// Busca registro de configuração
		ConfiguracaoIFrame__mdt objConfiguracao = buscarConfiguracao(portal);
		
		// Verifica se encontrou o registro de configuração
		if(objConfiguracao == null) {
			return null;
		}

		// Busca informações da conta
		Account objAccount = AccountDAO.getInstance().buscarPorId(recordId);

		// Verifica os portais que dependem da conta, possuem todos os parâmetros
		if(objAccount == null && (portal == 'Visão 360' || portal == 'Painel de Incidentes' || portal == 'Intenção de Cancelamento')) {
			return null;
		}

		// Atribui URL de retorno
		url += objConfiguracao.Url__c;

		String content = '';
		if (Test.isRunningTest()){
			content = 'Start_Of_Session_IdTesteEnd_Of_Session_Id';
		} else {
			PageReference vf = Page.SessionVF;
            System.debug('vf: ' + vf);
			content = vf.getContent().toString();
            System.debug('content: ' + content);
		}

		Integer s = content.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
                e = content.indexOf('End_Of_Session_Id');
		String token = objConfiguracao.Token__c;
		String ssId = content.substring(s, e); //UserInfo.getSessionId()
        System.debug('ssid: ' + ssid);
        System.debug('recordId: ' + recordId);

		// Realiza substituições de parâmetros dependendo do portal selecionado
		switch on portal {
			when 'Portal de Vendas', 'Painel' {
				url = url.replace('{token}', ssId);
			}
			when 'Reencarteiramento'{
				url = url.replace('{token}', ssId);
				url = url.replace('{userId}', UserInfo.getUserId());
			}
			when 'Visão 360' {
				url = url.replace('{token}', ssId);
				url = url.replace('{accountId}', recordId);
			}
			when 'Painel de Incidentes', 'Intenção de Cancelamento' {
				url = url.replace('{token}', token);
				url = url.replace('{AccountCNPJCPF}', objAccount.CnpjCpf__c);
			}
			when 'GuiaResetSenha' {
				Contact contato = ContactDAO.getInstance().buscarPorPersonAccount(recordId);

				Contact contatoRel = ContactDAO.getInstance().buscarRelacionamentoPorContaContato(conta, contato.id);
				url += '?lk_codig='+contatoRel.OperadorAc__c;
				url += '&lk_email='+contatoRel.Email;
				url += '&sid='+ssId;

				Account contaEmpresa = AccountDAO.getInstance().buscarPorId(conta);
				url += '&org_id='+contaEmpresa.Codigo8__c;
			}
			when 'GuiaReenvioSenha' {
				System.debug('Entrou no reenvio de senha');
				Contact contato = ContactDAO.getInstance().buscarPorContato(recordIdContato);
				Account contaEmpresa = AccountDAO.getInstance().buscarPorId(conta);

				String documento = contaEmpresa.CnpjCpf__c;
				String email = contato.Email;
				String accountId = ' ';
				String operadorId = ' ';

				String documentoFormatado = formataDocumento(documento);
				String emailFormatado = formataEmail(email);
				String accountIdFormatado = formataAccountId(accountId);
				String operadorIdFormatado = formataOperadorId(operadorId);

				url += '?param=BVSTVSNH';
				url += documentoFormatado+emailFormatado;
				url += 'TELEVENDAS';
				url += accountIdFormatado+operadorIdFormatado;

				EncodingUtil.URLENCODE(url,'UTF-8');

				// String urlFixa = 'https://phpcorp.boavistaservicos.com.br/acspnet/programas/siaph121.php?param=BVSTVSNH05199105000135gabriel.azeredo@boavistascpc.com.br%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20TELEVENDAS%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20';
			}
			when 'Sua Fatura'{
				//	String params = '';
				//	params += 'origem=SalesForce';
				//	params += '&codigoCliente=' + objAccount.Codigo8__c;
				//	params += '&sfid=' + ssId;
				//url += params;
				//url = url.replace('{token}', ssId);
				//url = url.replace('{CodigoDe8}', objAccount.Codigo8__c);
				url = ssId;
			}

		}
        System.debug('url: ' + url);

		// Retorna url com os parâmetros devidamente substituídos
		return String.isNotBlank(url) ? url : null;
	}


	public static String formataDocumento(String documento){
		while(documento.length() < 14){
			documento = '0' + documento;
		}
		return documento;
	}

	public static String formataEmail(String email){
		while (email.length() < 100){
			email = email + ' ';
		}
		return email;
	}

	public static String formataAccountId(String accountId){
		while (accountId.length() < 10) {
			accountId = accountId + ' ';
		}
		return accountId;
	}

	public static String formataOperadorId(String operadorId){
		while (operadorId.length() < 15) {
			operadorId = operadorId + ' ';
		}
		return operadorId;
	}

	public static Map<String, String> buscarConfiguracoesRightnow() {
		Map<String, String> mapConfiguracoes = new Map<String, String>();

		CollectionUtil.carregarMap(mapConfiguracoes, 'DeveloperName', 'Valor__c', [SELECT Id, DeveloperName, MasterLabel, Valor__c FROM ConfiguracaoRightNow__mdt]);

		return mapConfiguracoes;
	}

	private static String tokenDinamico(Boolean test){ // 10/10/2022, Maycon: Incluido contexto de teste, para a execução da classe de teste

		Map<String, String> objConfiguracao = buscarConfiguracoesRightnow();

		if(test == true){
			String token = 'ag5r1gerg1S165sdasdwagtrj$gAEf98';
			return token;
		}else{
			String token = RightNowService.gerarTokenDinamico(objConfiguracao);
			return token;
		}
	}

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método responsável por buscar a configuração do portal selecionado.
	 * @Parâmetros: String portal - Nome do portal selecionado no design do componente.
	 */
	private static ConfiguracaoIFrame__mdt buscarConfiguracao(String portal) {
		List<ConfiguracaoIFrame__mdt> lstConfiguracao = [SELECT Id, MasterLabel, DeveloperName, Token__c, Url__c FROM ConfiguracaoIFrame__mdt WHERE MasterLabel = :portal];
		return !lstConfiguracao.isEmpty() ? lstConfiguracao.get(0) : null;
	}
}