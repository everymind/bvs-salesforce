public class CadastrarConta{

    @InvocableMethod
    public static List<RetornoServico> buscarConta(List<String> valorEntradaList){
        List<RetornoServico> retornoList = new List<RetornoServico>();

        for(String valorEntrada : valorEntradaList){
            if(valorEntrada.length() == 14){
                retornoList.add(buscarContaPJ(valorEntrada));
            } else{
                retornoList.add(buscarContaPF(valorEntrada));
            }
        }

        return retornoList;
    }

    public static RetornoServico buscarContaPJ(String cnpj){
        RetornoServico retornoServ = new RetornoServico();
        String jsonPJ;
        CadastroContaTO.contaPJ contaPessoaJuridica;
        try {

            //Chama serviço
             jsonPJ = CadastrarContaService.buscarContaPessoaJuridica(cnpj);
             contaPessoaJuridica = (CadastroContaTO.contaPJ)JSON.deserialize(jsonPJ, CadastroContaTO.contaPJ.class);

            retornoServ.Razao_Social_PJ = contaPessoaJuridica.razao;
            retornoServ.Endereco_PJ = contaPessoaJuridica.endereco;
            retornoServ.Bairro_PJ = contaPessoaJuridica.bairro;
            retornoServ.CEP_PJ = contaPessoaJuridica.cep;
            retornoServ.Cidade_PJ = contaPessoaJuridica.cidade;
            retornoServ.UF_PJ = contaPessoaJuridica.uf;
            retornoServ.RecordTypeId = [SELECT Id FROM Recordtype WHERE DeveloperName = 'PessoaJuridica' And SobjectType = 'Account' LIMIT 1].Id;
            retornoServ.CodigoRetorno = contaPessoaJuridica.cod_retorno;
            
        } catch (Exception ex) {

            retornoServ.CodigoRetorno = '99';

        }
        
        return retornoServ;
    }

    public static RetornoServico buscarContaPF(String cpf){
        RetornoServico retornoList = new RetornoServico();
		String jsonPF;
        CadastroContaTO.contaPF contaPessoaFisica;
        try{

            //Chama serviço
            jsonPF = CadastrarContaService.buscarContaPessoaFisica(cpf);
            contaPessoaFisica = (CadastroContaTO.contaPF)JSON.deserialize(jsonPF, CadastroContaTO.contaPF.class);
            system.debug(jsonPF + ' teste ' + contaPessoaFisica);
            retornoList.NomePF = contaPessoaFisica.nome;
            retornoList.PrimeiroNomePF = retornoList.NomePF.substringBefore(' ');
            retornoList.SobrenomePF = retornoList.NomePF.substringAfter(' ');
            retornoList.NomeMaePF = contaPessoaFisica.nome_mae;
            retornoList.DataNascimentoPF = contaPessoaFisica.data_nascimento != null ? Date.newInstance(Integer.valueOf(contaPessoaFisica.data_nascimento.substring(6,10)), Integer.valueOf(contaPessoaFisica.data_nascimento.substring(3,5)), Integer.valueOf(contaPessoaFisica.data_nascimento.substring(0,2)))  : null;
            retornoList.EmailPF = contaPessoaFisica.email;
            retornoList.RecordTypeIdPF = [SELECT Id FROM Recordtype WHERE DeveloperName = 'PessoaFisica' And SobjectType = 'Account' LIMIT 1].Id;
            retornoList.CodigoRetorno = contaPessoaFisica.cod_retorno;
            retornoList.MensagemRetorno = contaPessoaFisica.msg_retorno;

        } catch (Exception ex) {
            retornoList.CodigoRetorno = contaPessoaFisica.cod_retorno;
            retornoList.MensagemRetorno = contaPessoaFisica.msg_retorno;
        }
        
        return retornoList;
    }

    public class RetornoServico{
        
        //PESSOA JURÍDICA
        @InvocableVariable
        public String Razao_Social_PJ;
        
        @InvocableVariable
        public String Endereco_PJ;
        
        @InvocableVariable
        public String RecordTypeId;

        @InvocableVariable
        public String Bairro_PJ;

        @InvocableVariable
        public String CEP_PJ;

        @InvocableVariable
        public String Cidade_PJ;

        @InvocableVariable
        public String UF_PJ;
        
        //PESSOA FÍSICA
        @InvocableVariable
        public String RecordTypeIdPF;
        
        @InvocableVariable
        public String NomePF;
        
        @InvocableVariable
        public String NomeMaePF;
        
        @InvocableVariable
        public Date DataNascimentoPF;
        
        @InvocableVariable
        public String EmailPF;
        
        @InvocableVariable
        public String PrimeiroNomePF;
        
        @InvocableVariable
        public String SobrenomePF;   

        @InvocableVariable
        public String CodigoRetorno;
        @InvocableVariable
        public String MensagemRetorno;
    }
}