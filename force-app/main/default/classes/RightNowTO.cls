/**
 * @Autor: Everymind
 * @Data: 20/07/2020
 * @Descrição: Classe que representa o objeto de envio do serviço do RightNow.
 */
public class RightNowTO {

	public class Incident {
		public StatusWithType statusWithType {get; set;}
	}

	public class StatusWithType {
		public Status status { get; set; }
	}
	
	public class Status {
		public Integer id { get; set; }
	}

	public class Token{
		public String session { get; set; }
	}
}