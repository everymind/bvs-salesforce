@isTest
public with sharing class UserTerritory2AssociationBatchTest {
    
    @isTest
    public static void batchTest(){
        Test.startTest();
        List<Account> lstAccount = new List<Account>();
        Set<String> setTeste = new Set<String>();
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Database.BatchableContext bc;

        lstAccount.add(conta);
        UserTerritory2AssociationBatch instance = new UserTerritory2AssociationBatch(setTeste);
        UserTerritory2AssociationBatch.init(setTeste);
        instance.start(bc);
        instance.execute(bc, lstAccount);

        Test.stopTest();
    }
}