public class CadastroContaTO {

    public class ContaPF{

        public String nome { get; set; }
        public String data_nascimento { get; set; }
        public String nome_mae { get; set; }
        public String sexo { get; set; }
        public String email { get; set; }
        public String tel_cel { get; set; }
        public String tel_com { get; set; }
        public String tel_res { get; set; }
        public String cod_retorno { get; set; }
        public String msg_retorno { get; set; }
        
    }

    public class ContaPJ{

        public String razao { get; set; }
        public String endereco { get; set; }
        public String bairro { get; set; }
        public String cep { get; set; }
        public String cidade { get; set; }
        public String uf { get; set; }
        public String cod_retorno { get; set; }
        public String msg_retorno { get; set; }

    }

}