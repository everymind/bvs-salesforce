/**
 * @Autor: Everymind
 * @Data: 22/07/2022
 * @Descrição: Classe de teste para classe IFramePortalControllerCase.
 */
@IsTest
public class IFramePortalControllerCaseTest {

    /**
	 * @Autor: Everymind
	 * @Data: 22/07/2022
	 * @Descrição: Método que prepara a configuração e cenário necessários para os testes.
	 */
	@TestSetup
	static void setup() {
		// Recupera Id do tipo de registro de pessoa jurídica da conta
		Id tipoRegistroPessoaJuridicaId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId();

		// Cria conta de teste
		Account objAccount = new Account(
			Name = 'Teste Account',
			CnpjCpf__c = '45406480000123',
			RecordTypeId = tipoRegistroPessoaJuridicaId
		);
        insert objAccount;

        Contact contato = TestDataFactory.gerarContato(true, objAccount);
	}

    /**
	 * @Autor: Everymind
	 * @Data: 22/07/2022
	 * @Descrição: Método que testa o método  buscaUrl.
	 */
	@IsTest
	static void testIFramePortalControllerCaseVisao360() {
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalControllerCase.buscarUrl(recordId, 'Visão 360');
		Test.stopTest();
		System.assertNotEquals(url, null, 'URL retornada.');
        }
	}

    /**
	 * @Autor: Everymind
	 * @Data: 22/07/2022
	 * @Descrição: Método que testa o método  buscaUrl.
	 */
	@IsTest
	static void testIFramePortalControllerCasePainelIncidentes() {
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalControllerCase.buscarUrl(recordId, 'Painel de Incidentes');
		Test.stopTest();
		System.assertNotEquals(url, null, 'URL retornada.');
        }
	}

    /**
	 * @Autor: Everymind
	 * @Data: 22/07/2022
	 * @Descrição: Método que testa o método  buscaUrl.
	 */
	@IsTest
	static void testIFramePortalControllerCaseIntencaoCancelamento() {
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalControllerCase.buscarUrl(recordId, 'Intenção de Cancelamento');
		Test.stopTest();
		System.assertNotEquals(url, null, 'URL retornada.');
        }
	}

    /**
	 * @Autor: Everymind
	 * @Data: 22/07/2022
	 * @Descrição: Método que testa o método  buscaUrl.
	 */
	@IsTest
	static void testIFramePortalControllerCaseSuaFatura() {
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalControllerCase.buscarUrl(recordId, 'Sua Fatura');
		Test.stopTest();
		System.assertNotEquals(url, null, 'URL retornada.');
        }
	}

    /**
	 * @Autor: Everymind
	 * @Data: 22/07/2022
	 * @Descrição: Método que testa o método  buscaUrl.
	 */
	@IsTest
	static void testIFramePortalControllerCasePortaldeVendas() {
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalControllerCase.buscarUrl(recordId, 'Portal de Vendas');
		Test.stopTest();
		System.assertNotEquals(url, null, 'URL retornada.');
        }
	}

    @IsTest
	static void testIFramePortalControllerCasePainel() {
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalControllerCase.buscarUrl(recordId, 'Painel');
		Test.stopTest();
		System.assertEquals(true, true);
        }
	}

    @IsTest
	static void testIFramePortalControllerCaseReencarteiramento() {
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
		Test.startTest();
		Account objConta = [SELECT Id FROM Account LIMIT 1];
		String recordId = objConta.Id;

		String url = IFramePortalControllerCase.buscarUrl(recordId, 'Reencarteiramento');
		Test.stopTest();
		System.assertNotEquals(url, null, 'URL retornada.');
        }
	}

    @IsTest
	static void testIFramePortalControllerCaseBuscarContaCaso() {
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){
		Test.startTest();
		Account conta = TestDataFactory.gerarContaJuridica(true);
		Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(true, conta, contato);
        String recordId = caso.Id;

		Account conta1 = IFramePortalControllerCase.buscarContaCaso(recordId);
		Test.stopTest();
		System.assertEquals(caso.AccountId, conta1.Id);
        }
	}
}