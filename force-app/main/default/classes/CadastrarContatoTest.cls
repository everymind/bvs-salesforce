@isTest
public with sharing class CadastrarContatoTest {
    public CadastrarContatoTest() {}

    @isTest 
    public static void buscarContatoTest(){
        Test.startTest();
            List<String> cpfList = new List<String>();
            String cpf = TestDataFactory.gerarCPF(false);
            cpfList.add(cpf);
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('CPF');
            mock.setStatusCode(200);
            mock.setHeader('Content-Type', 'application/json;charset=UTF-8');

            Test.setMock(HttpCalloutMock.class, mock);
            CadastrarContato.buscarContato(cpfList);
        Test.stopTest();
    }
}