public class OpportunityLineItemBO {
    
    private static final OpportunityLineItemBO instance = new OpportunityLineItemBO();

	public static OpportunityLineItemBO getInstance() {
        return instance;
	}

    
    public void marcarFlagOpp(List<OpportunityLineItem> lstOppLineItem){
        
        System.debug('lstOppLineItem: ' + lstOppLineItem);
        Set<Id> setOpp = new Set<Id>();
        List<Opportunity> lstOpp = new List<Opportunity>();
		List<Product2>  idProdutoAntifraude = new List<Product2> ();
        idProdutoAntifraude = [SELECT Id,Name,Family FROM Product2 WHERE Family = 'ANTIFRAUDE'];
		String familyProduct;
        List<Product2> produtoFamilia = new List<Product2>();
		
        for(Product2 p : idProdutoAntifraude){
            
            if(!idProdutoAntifraude.isEmpty()){
               familyProduct = p.Family;
            }
        }        
        
        for(OpportunityLineItem oppLineItem : lstOppLineItem){
            
            if(!lstOppLineItem.isEmpty() && oppLineItem.FamiliaProduto__c == familyProduct){            
			setOpp.add(oppLineItem.OpportunityId);        
    		System.debug('Passei no primeiro if');
            }
        }
        
           
        lstOpp = OpportunityDAO.getInstance().buscarPorId(setOpp);
        System.debug('LISTA OPP: ' + lstOpp);
        
         for(Opportunity op : lstOpp){
                          
            if(!lstOpp.isEmpty()){
                op.TemProduto__c = true;
             }
        
         }
        
        update lstOpp;
        system.debug('ATUALIZEI');

   }
    
    public void desmarcarFlagOpp(List<OpportunityLineItem> lstOppLineItem){
        
        system.debug('lista produto: '+ lstOppLineItem);
        Set<Id> setOpp = new Set<Id>();
        List<Opportunity> lstOpp = new List<Opportunity>();
       
         for(OpportunityLineItem oppLineItem : lstOppLineItem){
            
            if(!lstOppLineItem.isEmpty()){            
			setOpp.add(oppLineItem.OpportunityId);        
    		System.debug('Passei no primeiro if');
            }
        }
        
        
        lstOpp = OpportunityDAO.getInstance().buscarPorId(setOpp);
        
        list<Opportunity> lstOppUpdate = new list<Opportunity>();
        
        if(!lstOpp.isEmpty()){
            for(Opportunity op : lstOpp){
                if(op.TemProduto__c == true){
                    op.TemProduto__c = false;
                    lstOppUpdate.add(op);
                }
            }
    		    
    	}
        
        if(!lstOppUpdate.isEmpty()){
            update lstOppUpdate;
        }        
        
    }
    
    
}