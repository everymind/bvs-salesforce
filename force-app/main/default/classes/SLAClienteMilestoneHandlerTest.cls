@isTest 
private class SLAClienteMilestoneHandlerTest {
    static  void testMilestoneTimeCalculatorSemTempoDecorrido() {   
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){

        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];      
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];

        Account conta = TestDataFactory.gerarContaJuridica(true);
        
        Entitlement direito = new Entitlement();
        direito.Name = 'RETORNONAOSOLUCIONADO';
        direito.AccountId = conta.Id;
        insert direito;

        TabulacaoSLA__c objTabulacao = new TabulacaoSLA__c(
            TipoRegistro__c = 'CSMPosVendas',
            MotivoTxt__c = 'ACESSOS',
            SubMotivoTxt__c = 'CRIAÇÂO DE CAIXA POSTAL',
            SubMotivo2Txt__c = '',
            MeioAcessoTxt__c = '',
            FilaAtual__c = 'PRIME', 
            ProximaFila__c = 'RETORNO',
            TempoSLA__c = 100,
            StatusTabulacao__c = 'ATIVO'
            );
        insert objTabulacao;

        Id rtCSM = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMPosVenda').getRecordTypeId();
        Case c = new Case(
            OwnerId = usr.Id,
            RecordTypeId = rtCSM,	
            TempoDecorridoSLACliente__c = 0, 
            EntitlementId = direito.Id, 
            Status = 'NÃO SOLUCIONADO',
            MotivoTxt__c = 'ACESSOS', 
            SubMotivoTxt__c = 'CRIAÇÂO DE CAIXA POSTAL',
            SubMotivo2Txt__c = '',
            MeioAcessoTxt__c = '',
            FilaAtual__c = 'PRIME', 
            ProximaFila__c = 'RETORNO', 
            Area__c = 'CSM', 
            Description = 'TESTE'
            );
        TriggerHandler.bypass('CaseTriggerHandler');
        insert c;
        TriggerHandler.clearBypass('CaseTriggerHandler');
        
        SLAClienteMilestoneHandler calculator = new SLAClienteMilestoneHandler();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(c.Id, mt.Id);
        
        System.assertEquals(true, true);
        
        }  
    }

    static testmethod void testMilestoneTimeCalculatorTempoCerto() {  
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){      
      
        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];      
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];
        
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(false, conta, contato);
            caso.MotivoTxt__c = 'Teste';
            caso.SubMotivoTxt__c = 'Teste';
            caso.Status = 'REJEITADO';
            caso.MotivoRejeicao__c = 'DADOS INCOMPLETOS';
            TriggerHandler.bypass('CaseTriggerHandler');
            insert caso;
            TriggerHandler.clearBypass('CaseTriggerHandler');

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(false);
            tabulacao.TempoSLA__c = 200;
            insert tabulacao;
        
        SLAClienteMilestoneHandler calculator = new SLAClienteMilestoneHandler();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(caso.Id, mt.Id);
        
        System.assertEquals(true, true);
        
        }
    }

    static testmethod void testMilestoneTimeCalculatorTempoMaior() {  
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){      
      
        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];      
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];
        
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(false, conta, contato);
            caso.MotivoTxt__c = 'Teste';
            caso.SubMotivoTxt__c = 'Teste';
            caso.Status = 'REJEITADO';
            caso.MotivoRejeicao__c = 'DADOS INCOMPLETOS';
            caso.NaoAtendido__c = true;
            TriggerHandler.bypass('CaseTriggerHandler');
            insert caso;
            TriggerHandler.clearBypass('CaseTriggerHandler');

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(false);
            tabulacao.TempoSLA__c = 19*60;
            insert tabulacao;
        
        SLAClienteMilestoneHandler calculator = new SLAClienteMilestoneHandler();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(caso.Id, mt.Id);
        
        
        }
    }
    
    static testmethod void testMilestoneTimeCalculatorTempoMenor() {  
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){      
      
        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];      
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];
        
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(false, conta, contato);
            caso.MotivoTxt__c = 'Teste';
            caso.SubMotivoTxt__c = 'Teste';
            caso.Status = 'REJEITADO';
            caso.MotivoRejeicao__c = 'DADOS INCOMPLETOS';
            caso.NaoAtendido__c = true;
            TriggerHandler.bypass('CaseTriggerHandler');
            insert caso;
            TriggerHandler.clearBypass('CaseTriggerHandler');

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(false);
            tabulacao.TempoSLA__c = 2*60;
            insert tabulacao;
        
        SLAClienteMilestoneHandler calculator = new SLAClienteMilestoneHandler();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(caso.Id, mt.Id);
        
        
        }
    }

    static testmethod void testMilestoneTimeCalculatorTempoNulo() {  
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){      
      
        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];      
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];
        
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(false, conta, contato);
            caso.MotivoTxt__c = 'Teste';
            caso.SubMotivoTxt__c = 'Teste';
            caso.Status = 'REJEITADO';
            caso.MotivoRejeicao__c = 'DADOS INCOMPLETOS';
            caso.NaoAtendido__c = true;
            TriggerHandler.bypass('CaseTriggerHandler');
            insert caso;
            TriggerHandler.clearBypass('CaseTriggerHandler');

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(false);
            tabulacao.TempoSLA__c = null;
            insert tabulacao;
        
        SLAClienteMilestoneHandler calculator = new SLAClienteMilestoneHandler();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(caso.Id, mt.Id);
        
        
        }
    }

    static testmethod void testMilestoneTimeCalculatorComTempoDecorrido() {  
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){      
      
        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];      
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];
        
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(false, conta, contato);
            caso.MotivoTxt__c = 'Teste';
            caso.SubMotivoTxt__c = 'Teste';
            caso.Status = 'NÂO SOLUCIONADO';
            insert caso;

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(true);
        
        SLAClienteMilestoneHandler calculator = new SLAClienteMilestoneHandler();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(caso.Id, mt.Id);
        
        
        }
    }
    
    static testmethod void testMilestoneTimeCalculatorNULLTempoDecorrido() {  
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){      
      
        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];      
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];

        Integer tempo = null;
        
        Account conta = TestDataFactory.gerarContaJuridica(true);
        Entitlement direito = new Entitlement();
            direito.Name = 'PRIME';
            direito.AccountId = conta.Id;
        insert direito;
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoAtendimento(false, conta, contato);
            caso.EntitlementId = direito.Id;
            caso.MotivoTxt__c = 'Teste';
            caso.SubMotivoTxt__c = 'Teste';
            caso.Status = 'NÂO SOLUCIONADO';
            caso.FilaAtual__c = 'PRIME';
            caso.ProximaFila__c = 'RETORNO';
            insert caso;

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(false);
            tabulacao.FilaAtual__c = 'PRIME';
            tabulacao.ProximaFila__c = 'RETORNO';
        insert tabulacao;
   
    SLAClienteMilestoneHandler calculator = new SLAClienteMilestoneHandler();
    Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(caso.Id, mt.Id);
    
    
    }
}


    static testmethod void testNaoSolucionado() {  
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do sistema');
        System.runAs(usr){      
            Account conta = TestDataFactory.gerarContaJuridica(true);
        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];      
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];
        Entitlement direito = new Entitlement();
        direito.Name = 'RETORNONAOSOLUCIONADO';
        direito.AccountId = conta.Id;
        insert direito;
        
        SLAClienteMilestoneHandler calculator2 = new SLAClienteMilestoneHandler('teste','teste', 1);
        Contact contato = TestDataFactory.gerarContato(true, conta);
        Case caso = TestDataFactory.gerarCasoPosVenda(false, conta, contato);
            caso.MotivoTxt__c = 'Teste';
            caso.SubMotivoTxt__c = 'Teste';
            caso.Status = 'Novo';
            
            TriggerHandler.bypass('CaseTriggerHandler');
            insert caso;
            TriggerHandler.clearBypass('CaseTriggerHandler');

        TabulacaoSLA__c tabulacao = TestDataFactory.gerarTabulacao(false);
            tabulacao.TipoRegistro__c = 'CSMPosVendas';
            tabulacao.TempoSLA__c = 200;
            insert tabulacao;
        
        SLAClienteMilestoneHandler calculator = new SLAClienteMilestoneHandler();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(caso.Id, mt.Id);

            caso.DescricaoSolucao__c = 'Teste';
            caso.Status = 'FINALIZADO';
            caso.Solucao__c = 'Atendido';
            TriggerHandler.bypass('CaseTriggerHandler');
            update caso;
            TriggerHandler.clearBypass('CaseTriggerHandler');


            caso.Status = 'NÃO SOLUCIONADO';
            caso.OwnerId = usr.id;
            TriggerHandler.bypass('CaseTriggerHandler');
            update caso;
        TriggerHandler.clearBypass('CaseTriggerHandler');
        Integer actualTriggerTime2 = calculator.calculateMilestoneTriggerTime(caso.Id, mt.Id);
        
        }
    }
}