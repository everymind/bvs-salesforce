public class UserTerritory2AssociationBatch implements Database.Batchable<sObject> {
    
    private Set<String> setCarteira = new Set<String>();
    
    public UserTerritory2AssociationBatch(Set<String> setRecebido){
        setCarteira = setRecebido;
    }
    
    public static void init(Set<String> setRecebido) {
        Database.executeBatch(new UserTerritory2AssociationBatch(setRecebido), 500);
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {		   
        String query = 'SELECT Id FROM Account WHERE Carteira__c IN :setCarteira';
         
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<Account> lstContas){
        System.debug('lstContas -> ' + lstContas);
        List<Account> lstContasAlteradas = new List<Account>();
        List<ApplicationLog__c> lstApplcationLogInsert = new List<ApplicationLog__c>();
        
        for (Account objConta : lstContas){
            lstContasAlteradas.add(
                new Account(Id = objConta.Id,ProcessarReencarteiramento__c = true)
            );
        }
        
        if (!lstContasAlteradas.isEmpty()){
            lstApplcationLogInsert.addAll(this.processarResultado(Database.update(lstContasAlteradas, false), 'Account', lstContasAlteradas));

           /* try{
                update lstContasAlteradas;
            } catch(System.DmlException e){
                this.processarResultado(e, 'Account', lstContasAlteradas); 
            }  */
        }
            
        if (!lstApplcationLogInsert.isEmpty()){
            insert lstApplcationLogInsert;
        }
    }
    
    public List<ApplicationLog__c> processarResultado(List<Database.SaveResult> resultados, String origem, List<SObject> lstRegistros){
		List<ApplicationLog__c> lstApplicationLog = new List<ApplicationLog__c>();

        /*for (Integer i = 0; i < resultados.getNumDml(); i++) {
			SObject registro = lstRegistros[i];

            String IdRegistroErro = registro.Id;
            // Cria um registro de log em caso de erro
            ApplicationLog__c objLog = new ApplicationLog__c(
                ErrorMessage__c = origem + ' - ID [' + IdRegistroErro + '] - '  + resultados.getDmlMessage(i),
                IdTarefaRightNow__c = '',
                IdIncidenteRightNow__c = '',
                Processo__c = 'Regra de atribuição de territórios'
            );
            
            lstApplicationLog.add(objLog);
        } */

        for (Integer i = 0; i < resultados.size(); i++) {
			Database.SaveResult databaseSaveResult = resultados[i];
			SObject registro = lstRegistros[i];

            if (!databaseSaveResult.isSuccess()) {

				for (Database.Error error : databaseSaveResult.getErrors()) {
					String IdRegistroErro = registro.Id;
					// Cria um registro de log em caso de erro
					ApplicationLog__c objLog = new ApplicationLog__c(
						ErrorMessage__c = origem + ' - ID [' + IdRegistroErro + '] - '  + error.getMessage(),
						IdTarefaRightNow__c = '',
						IdIncidenteRightNow__c = '',
						Processo__c = 'Regra de atribuição de territórios'
					);

					lstApplicationLog.add(objLog);
				}
            }
        }

        return lstApplicationLog;
	}
    
    public void finish(Database.BatchableContext bc){
        
    }
}