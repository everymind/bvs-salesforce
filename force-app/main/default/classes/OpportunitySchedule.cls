global class OpportunitySchedule implements Schedulable{
    global void execute (SchedulableContext ctx) {

        Database.executeBatch(new OpportunityHandler());
        
    }
}