public with sharing class CaseController {
    @AuraEnabled(cacheable=true)
    public static List<String[]> queryMotivoSubMotivo(){
        List<TabulacaoSLA__c> lstTabulacao = TabulacaoDAO.getInstance().listaTabulacao('ITSMContaPortal');
        List<String[]> lstRetorno=new List<String[]>();
        for(TabulacaoSLA__c tabulacao : lstTabulacao) {
            String[] linha= new String[6];
            linha[0]=tabulacao.MotivoTxt__c + '->'+tabulacao.SubMotivoTxt__c;
            if (tabulacao.SubMotivo2Txt__c!=null){
                linha[0]+=+ '->'+tabulacao.SubMotivo2Txt__c;
            }
            linha[1]=tabulacao.MotivoTxt__c;
            linha[2]=tabulacao.SubMotivoTxt__c;
            linha[3]=(tabulacao.SubMotivo2Txt__c==null?'':tabulacao.SubMotivo2Txt__c);
            
            try{
                Decimal tempoSLA=tabulacao.TempoSLA__c;
                Integer horas=integer.valueOf(tempoSLA/60);
                String retorno= String.valueof(horas);
                if (horas==1){
                    retorno=retorno+' hora';
                } else {
                    retorno=retorno+' horas';
                }
                linha[4]=retorno;
            } catch (Exception ex){
                linha[4]='SLA não determinado';
            }
            linha[5]=tabulacao.Observacoes__c;
    
            lstRetorno.add(linha);
        }
        return(lstRetorno);
    }

    @AuraEnabled(cacheable=false)
    public static Case salvarCaso(CaseWrapper casoWrapper){
        if (casoWrapper.motivo==null || casoWrapper.motivo.trim()==''){
            throw new CaseException('Informe o motivo da abertura do caso');
        } else {
            Case caso;
            boolean novo=false;
            if (casoWrapper.caseId==null || casoWrapper.caseId==''){
                caso=new Case();
                novo=true;
                Id rtITSM = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ITSMContaPortal').getRecordTypeId();
                caso.RecordTypeId = rtITSM;
                caso.Description = casoWrapper.descricao;
                caso.Impacto__c = casoWrapper.impacto;
                caso.Urgencia__c = casoWrapper.urgencia;
                caso.Priority = casoWrapper.prioridade;
                caso.NomeSolicitante__c = casoWrapper.nomeSolicitante;
                caso.EmailSolicitante__c = casoWrapper.emailSolicitante;
            } else {
                caso = CaseDAO.getInstance().buscarPorId(casoWrapper.caseId);
            }
            System.debug(caso);
            caso.MotivoTxt__c=casoWrapper.motivo;
            caso.SubMotivoTxt__c=casoWrapper.subMotivo;
            caso.SubMotivo2Txt__c=casoWrapper.subMotivo2;
            caso.MeioAcessoTxt__c=casoWrapper.meioDeAcesso;
            caso.CentroCusto__c = casoWrapper.centroCusto;
            caso.Justificativa__c = casoWrapper.justificativa;
            caso.QualRemetenteDestinatarioEmail__c = casoWrapper.qualRemetenteDestinatarioEmail;
            caso.QualDataHoraEmail__c = casoWrapper.qualDataHoraEmail;
            caso.AQuemPertenceEsteEmail__c = casoWrapper.aQuemPertenceEsteEmail;
            caso.QualFoiArquivoAtividadeBloqueada__c = casoWrapper.qualFoiArquivoAtividadeBloqueada;
            caso.ExisteDocumentoConsultaBVS__c = casoWrapper.existeDocumentoConsultaBVS;
            caso.InformeDadosAutorFraude__c = casoWrapper.informeDadosAutorFraude;
            caso.DataOcorrenciaFraude__c = casoWrapper.dataOcorrenciaFraude;
            caso.DataDenuncia__c = casoWrapper.dataDenuncia;
            caso.CPFVitima__c = casoWrapper.cPFVitima;
            caso.NomeVitima__c = casoWrapper.nomeVitima;
            caso.NomeEmpresaEnvolvidaFraude__c = casoWrapper.nomeEmpresaEnvolvidaFraude;
            caso.DocumentoConsulta__c = casoWrapper.documentoConsulta;
            caso.QualURLParaRemocao__c = casoWrapper.qualURLParaRemocao;
            caso.QualURLPhishing__c = casoWrapper.qualURLPhishing;
            caso.AQuemPertenceSolicitacao__c = casoWrapper.aQuemPertenceSolicitacao;
            caso.Outros__c = casoWrapper.outros;
            caso.AtualmenteUtilizaVDI__c = casoWrapper.atualmenteUtilizaVDI;
            caso.QualSiteVoceDesejaLiberacao__c = casoWrapper.qualSiteVoceDesejaLiberacao;
            caso.AQuemPertenceEsteSite__c = casoWrapper.aQuemPertenceEsteSite;
            Id posVendaId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMPosVendas').getRecordTypeId();
            // if (caso.RecordTypeId == posVendaId) {
            caso.productId = casoWrapper.productId;
             // }
             system.debug(' caso.productId' +  caso.productId);
            caso.quantidadeConsultas__c = casoWrapper.quantidadeConsultas;
            caso.DataInicio__c = casoWrapper.dataInicio;
            caso.DataFim__c = casoWrapper.datafim;
            caso.NomeUsuario__c = casoWrapper.nomeUsuario;
            caso.CPF__c = casoWrapper.cPF;
            caso.EmailUsuario__c = casoWrapper.emailUsuario;

            caso.nomeOperador__c = casoWrapper.nomeOperador;
            caso.dataNascimento__c = casoWrapper.dataNascimento;
            caso.emailOperador__c = casoWrapper.emailOperador;
            caso.codigoDeCliente__c = casoWrapper.codigoDeCliente;
            caso.PIDAprovado__c = casoWrapper.PIDAprovado;
            caso.ContactId = casoWrapper.contato;
            if(casoWrapper.kbaAprovado != null)
            {
                caso.Retorno_do_KBA__c = casoWrapper.retornoKBA;
                caso.KBA_Aprovado__c = casoWrapper.kbaAprovado;
                caso.Data_do_KBA__c = Datetime.now();
                if(casoWrapper.kbaAprovado == 'Não')
                {
                    caso.Status = 'FINALIZADO';
                    caso.Solucao__c = 'FALTA DE IDENTIFICAÇÃO POSITIVA';
                    caso.DescricaoSolucao__c = 'FALTA DE IDENTIFICAÇÃO POSITIVA';
                    caso.Subject = 'KBA BLOQUEADO';
                }
            }
            

            if (casoWrapper.validouPID) {
                caso.DataPID__c= System.now();
                if (!casoWrapper.PIDAprovado){
                    
                    caso.Status = 'FINALIZADO';
                    caso.Solucao__c = 'FALTA DE IDENTIFICAÇÃO POSITIVA';
                    caso.DescricaoSolucao__c = 'FALTA DE IDENTIFICAÇÃO POSITIVA';
                    caso.Subject = 'PID BLOQUEADO';
                    system.debug('passei aqui');
                    // if (novo) {
                       
                        
                    // } else {
                    //     caso.Subject = 'PID BLOQUEADO';
                    // }                   
                    AccountBO.tratarPermissaoDeCriacaoDeCasos(caso.ContactId, false);                  
                }                     
            }               
            
            if (!casoWrapper.bloquearCriacaoCasoPID && casoWrapper.dataLimiteBloqueioCasoPID != null) {
                AccountBO.tratarPermissaoDeCriacaoDeCasos(caso.ContactId, true);                                                                                                      
            }
      
            if (casoWrapper.acessarPlataforma){
                caso.Status='FINALIZADO';
                caso.RespostaRetorno__c = 'OCI';
            }
            if (novo){
                INSERT caso;
            } else {
                caso.CategorizacaoAutomatica__c = false;
                UPDATE caso;
            }
            return(caso);
        }
    }

    @AuraEnabled
    public static Case getCase(String caseId){
        Case retorno = [SELECT CategorizacaoAutomatica__c FROM Case WHERE Id = :caseId];
        return retorno;
    }

    public class CaseException extends Exception { }

    public class CaseWrapper {
        @AuraEnabled public String caseId{get;set;}
        @AuraEnabled public String motivo{get;set;} 
        @AuraEnabled public String subMotivo{get;set;}
        @AuraEnabled public String meioDeAcesso{get;set;}
        @AuraEnabled public String subMotivo2{get;set;}
        @AuraEnabled public String descricao{get;set;}
        @AuraEnabled public String impacto{get;set;}
        @AuraEnabled public String urgencia{get;set;}
        @AuraEnabled public String prioridade{get;set;}
        @AuraEnabled public String nomeSolicitante{get;set;}
        @AuraEnabled public String emailSolicitante{get;set;}
        @AuraEnabled public String centroCusto{get;set;}
        @AuraEnabled public String justificativa{get;set;}
        @AuraEnabled public String qualRemetenteDestinatarioEmail{get;set;}
        @AuraEnabled public DateTime qualDataHoraEmail{get;set;}
        @AuraEnabled public String aQuemPertenceEsteEmail{get;set;}
        @AuraEnabled public String qualFoiArquivoAtividadeBloqueada{get;set;}
        @AuraEnabled public String existeDocumentoConsultaBVS{get;set;}
        @AuraEnabled public String informeDadosAutorFraude{get;set;}
        @AuraEnabled public Date dataOcorrenciaFraude{get;set;}
        @AuraEnabled public Date dataDenuncia{get;set;}
        @AuraEnabled public String cPFVitima{get;set;}
        @AuraEnabled public String nomeVitima{get;set;}
        @AuraEnabled public String nomeEmpresaEnvolvidaFraude{get;set;}
        @AuraEnabled public String documentoConsulta{get;set;}
        @AuraEnabled public String qualURLParaRemocao{get;set;}
        @AuraEnabled public String qualURLPhishing{get;set;}
        @AuraEnabled public String aQuemPertenceSolicitacao{get;set;}
        @AuraEnabled public String outros{get;set;}
        @AuraEnabled public String atualmenteUtilizaVDI{get;set;}
        @AuraEnabled public String qualSiteVoceDesejaLiberacao{get;set;}
        @AuraEnabled public String aQuemPertenceEsteSite{get;set;}
        @AuraEnabled public String productId{get;set;}
        @AuraEnabled public Integer quantidadeConsultas{get;set;}
        @AuraEnabled public Date dataInicio{get;set;}
        @AuraEnabled public Date datafim{get;set;}
        @AuraEnabled public String nomeUsuario{get;set;}
        @AuraEnabled public String cPF{get;set;}
        @AuraEnabled public String emailUsuario{get;set;}
        @AuraEnabled public String nomeOperador{get;set;}
        @AuraEnabled public Date dataNascimento{get;set;}
        @AuraEnabled public String emailOperador{get;set;}
        @AuraEnabled public String codigoDeCliente{get;set;}
        @AuraEnabled public Boolean acessarPlataforma{get;set;}
        @AuraEnabled public Boolean PIDAprovado{get;set;}
        @AuraEnabled public Boolean bloquearCriacaoCasoPID{get;set;}
        @AuraEnabled public String dataLimiteBloqueioCasoPID {get;set;}
        @AuraEnabled public Boolean validouPID {get;set;}
        @AuraEnabled public String contato {get;set;}
        @AuraEnabled public String retornoKba{get;set;}
        @AuraEnabled public String kbaAprovado{get;set;}
    }

}