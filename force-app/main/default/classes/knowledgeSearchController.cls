public with sharing class knowledgeSearchController {
    
    //Get knowledge articles which contains input text
    @AuraEnabled(cacheable=false)
    public static List<KnowledgeBase__kav> getKnowledgeArticles(String subject) {

        List<KnowledgeBase__kav> knowledgeArticlesList = new List<KnowledgeBase__kav>();
                
            if (subject != '' && subject != null) {

                List<KnowledgeBase__kav> listArticle = [SELECT Id, Title, Summary, Descricao__c FROM KnowledgeBase__kav WHERE PublishStatus='Online'];

                for(KnowledgeBase__kav kav : listArticle) {
                    if(kav.Title.toUpperCase().contains(subject.toUpperCase()) || kav.Summary.toUpperCase().contains(subject.toUpperCase()))
                    {
                        //if(knowledgeArticlesList.size() <= 0)
                        //{
                            knowledgeArticlesList.add(kav);
                        //}
                    }
                }
            }
            
            System.debug('Total de Registros :' + knowledgeArticlesList.size());

        return knowledgeArticlesList;
    }
    
    
    //Assign articles to case
    @AuraEnabled
    public static boolean assignArticleToCase(List<CaseArticle> articles) {
        if(articles!=null)
        {
            upsert articles;
        }
        return true;
    }
}