public class AcoesCasoController {
    
    @AuraEnabled
    public static AcoesCasoControllerTO.ReturnTO retornaDadosCaso(String caseId) {

        Case caso = CaseDAO.getInstance().buscarPorId(Id.valueOf(caseId));
        AcoesCasoControllerTO.ReturnTO retn = new AcoesCasoControllerTO.ReturnTO();  


        if (caso != null) {

            String tipoPessoa = defineTipoPessoa(caso.MotivoTxt__c, caso.SubMotivoTxt__c);           

            retn.CPFCNPJ = defineNumeroDocumento(caso.CNPJ__c,caso.ContactCPF__c,tipoPessoa);
            retn.tipoCliente =  tipoPessoa == '1' ? 'Pessoa física' : 'Pessoa jurídica';
            retn.motivo = caso.MotivoTxt__c;
            retn.subMotivo = caso.SubMotivoTxt__c;
            retn.nome = tipoPessoa == '1'  ? caso.contact.name : caso.account.name;
            retn.eMail = tipoPessoa == '1'  ? caso.contact.Email : caso.account.Email__c;
            retn.personBirthdate = tipoPessoa == '1' ?  String.ValueOf(caso.contact.Birthdate) : null;
            if(tipoPessoa == '3')
            {
                retn.bairro = caso.Account.BairroEntrega__c;
                retn.cidade = caso.Account.ShippingCity;
                retn.rua = caso.Account.ShippingStreet;
                retn.cep = caso.Account.ShippingPostalCode;
                retn.uf = caso.Account.ShippingState;
            }
            return retn;
        }

        return retn; 
    }

    @AuraEnabled
    public static AcoesCasoControllerTO.ReturnStatusTO obterStatusCadastroPositivo(String caseId) {

        AcoesCasoControllerTO.ReturnStatusTO retn = new AcoesCasoControllerTO.ReturnStatusTO();
        Case caso = CaseDAO.getInstance().buscarPorId(Id.valueOf(caseId));

        try {

            if (caso != null) {            
            
                String tipoPessoa = defineTipoPessoa(caso.MotivoTxt__c, caso.SubMotivoTxt__c);
                String documento = defineNumeroDocumento(caso.CNPJ__c,caso.ContactCPF__c,tipoPessoa);

                CadastroPositivoService.CadastroPrositivoResponse response;
                CadastroPositivoService service = new CadastroPositivoService();
    
                response = service.obterStatusOptinConsumidor(documento, tipoPessoa);
    
                retn.erro = response.error != null ? true : false;
                retn.mensagem = string.valueOf(response.statusOptin);
                return retn;
            }

            retn.erro = true;
            retn.mensagem = 'Nenhum caso encontrado com o id: ' + caseId;
            return retn;            
        } catch (Exception e) {            
            throw new AuraHandledException(e.getMessage());
        }    
    }

    private static String defineTipoPessoa(String motivo, String submotivo) {       
        return motivo != 'Cadastro Positivo' ? '0' : submotivo.containsIgnoreCase('pf') ? '1' : '3';
    }

    private static String defineNumeroDocumento(String cnpj, String cpf, String tipoPessoa) {
        return tipoPessoa == '0' ? '' : (tipoPessoa == '1' ? cpf == null ? cnpj : cpf : cnpj.substring(0, 8));
    }

    @AuraEnabled
    public static AcoesCasoControllerTO.ReturnStatusTO cancelamentoCadastroPositivo(String caseId) {

        Case caso = CaseDAO.getInstance().buscarPorId(Id.valueOf(caseId));
        AcoesCasoControllerTO.ReturnStatusTO retn = new AcoesCasoControllerTO.ReturnStatusTO();

        if (caso != null) {            
            
            String tipoPessoa = defineTipoPessoa(caso.MotivoTxt__c, caso.SubMotivoTxt__c);
            String documento = defineNumeroDocumento(caso.CNPJ__c,caso.ContactCPF__c,tipoPessoa);

            CadastroPositivoService.CadastroPrositivoResponse response;
            CadastroPositivoService service = new CadastroPositivoService();

            response = service.realizarOptout(documento, tipoPessoa);

            retn.erro = response.error != null ? true : false;
            retn.mensagem = string.valueOf(response.message);
            return retn;
        }

        retn.erro = true;
        retn.mensagem = 'Nenhum caso encontrado com o id: ' + caseId;        
        return retn;
    }

    @AuraEnabled
    public static AcoesCasoControllerTO.ReturnStatusTO reautorizacaoCadastroPositivo(String cadastroForm){
        
        AcoesCasoControllerTO.ReturnStatusTO retn = new AcoesCasoControllerTO.ReturnStatusTO();

        //TODO: Verificar com o LG se ele consegue enviar o tipo do cliente que obtem do método retornaDadosCaso
        // Case caso = CaseDAO.getInstance().buscarPorId(Id.valueOf(idCase));
        system.debug('cadastroForm ' + cadastroForm);
        AcoesCasoControllerTO.CadastroPositivoFormTO form = (AcoesCasoControllerTO.CadastroPositivoFormTO) JSON.deserialize(cadastroForm, AcoesCasoControllerTO.CadastroPositivoFormTO.class);
        system.debug('form ' + form);
        CadastroPositivoService.RealizarOptinRequest  request = new CadastroPositivoService.RealizarOptinRequest();

        // String tipoPessoa = defineTipoPessoa(caso.MotivoTxt__c, caso.SubMotivoTxt__c);
        // String documento = defineNumeroDocumento(caso.CNPJ__c,caso.ContactCPF__c,tipoPessoa);

        request.nome = form.nome;
        request.nomeConfirmado = form.nome;
        request.numeroDocumento = form.numeroDocumento;
        request.numeroRg = form.numeroRg;
        request.tipoCliente = form.tipoCliente;
        request.email = form.email;
        request.dataAniversario = form.dataAniversario;
        request.sexo =form.sexo;
        request.orgaoExpedidor = form.orgaoExpedidor;
        request.cidade = form.cidade;
        request.endereco = form.endereco;
        request.bairro = form.bairro;
        request.cep = form.cep;
        request.unidadeFederativa = form.unidadeFederativa;

        CadastroPositivoService.CadastroPrositivoResponse response ;        
        CadastroPositivoService service = new CadastroPositivoService();
        
        response = service.realizarOptin(request);
      

        
        system.debug(response + 'response');
        retn.mensagem = response.message;
        retn.erro = response.error != null ? true : false;

        return retn;
    }


}