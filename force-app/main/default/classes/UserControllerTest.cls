@isTest
public with sharing class UserControllerTest {

    @isTest
    public static void usuarioLogadoTest()
    {
        Test.startTest();
        User usr = TestDataFactory.gerarUsuario(true, 'Administrador do Sistema');
        System.runAs(usr)
        {
            UserController usrC = new UserController();
            UserController.usuarioLogado();
        }
        Test.stopTest();
    }

}