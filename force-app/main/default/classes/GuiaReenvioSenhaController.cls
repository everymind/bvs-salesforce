public with sharing class GuiaReenvioSenhaController {
    public GuiaReenvioSenhaController() {}

    @AuraEnabled(cacheable = true)
    public static List<Contact> buscaContatosRelacionados(Id accId)
    {
        Set<Id> Ids = new Set<Id>();
        List<Contact> listContacts = [
            SELECT Id, Name, OperadorAC__c
            FROM Contact
            WHERE AccountId =: accId
        ];
        return listContacts;
    }
}