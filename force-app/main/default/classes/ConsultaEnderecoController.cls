/**
 * @Autor: Everymind
 * @Data: 05/07/2020
 * @Descrição: Classe responsável por orquestrar as chamadas do componente com o servidor (Apex).
 */
public class ConsultaEnderecoController {

	/**
	 * @Autor: Everymind
	 * @Data: 11/07/2020
	 * @Descrição: Método que recupera a configuração de endereço do objeto.
	 * @Parâmetro: String objApiName - Atributo do nome API do objeto que está chamando a ação de consulta de endereço.
	 * @Retorno: ConfiguracaoConsultaEndereco__mdt - Objeto que de configuração da consulta de endereço.
	 */
	@AuraEnabled
	public static ConfiguracaoConsultaEndereco__mdt buscarConfiguracao(String objApiName) {
		return [SELECT Id, DeveloperName, Objeto__c, NomeServico__c, HashAutenticacao__c, Endpoint__c, CampoCEP__c, CampoRua__c, CampoBairro__c, CampoCidade__c, CampoEstado__c, CampoNumero__c, CampoComplemento__c
				FROM ConfiguracaoConsultaEndereco__mdt
				WHERE Objeto__c = :objApiName];
	}

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método que recupera a o registro de contexto.
	 * @Parâmetro: String recordId - Id do registro injetado pelo componente.
	 * @Retorno: ConfiguracaoConsultaEndereco__mdt objConfiguracao - Objeto que de configuração da consulta de endereço.
	 */
	@AuraEnabled
	public static ConsultaEnderecoTO buscarRegistro(String recordId, ConfiguracaoConsultaEndereco__mdt objConfiguracao) {
		ConsultaEnderecoTO objTO = new ConsultaEnderecoTO();
		String query = '';
		SObject obj;

		// Verifica se veio o objeto de configuração
		if(objConfiguracao == null) {
			return objTO;
		}

		// Monta a query dinamica baseada nos campos do metadado
		query += 'SELECT Id, ';
		query += objConfiguracao.CampoCEP__c + ', ';
		query += objConfiguracao.CampoRua__c + ', ';
		query += objConfiguracao.CampoBairro__c + ', ';
		query += objConfiguracao.CampoCidade__c + ', ';
		query += objConfiguracao.CampoEstado__c + ', ';
		query += objConfiguracao.CampoNumero__c + ', ';
		query += objConfiguracao.CampoComplemento__c + ' ';
		query += 'FROM ';
		query += objConfiguracao.Objeto__c + ' ';
		query += 'WHERE Id = \'' + recordId + '\'';

		System.debug(':query: ' + query);
		
		// Realiza a query montada dinamicamente
		try {
			obj = Database.query(query);
		} catch(QueryException e) {
			return objTO;
		}

		// Verifica se houve resultado da query
		if(obj == null) {
			return objTO;
		}
		
		// Tenta fazer a atribuição dos valores do campos configurados no metadado e retornados da query
		try {
			objTO.CEP = (String) obj.get(objConfiguracao.CampoCEP__c);
			objTO.Logradouro = (String) obj.get(objConfiguracao.CampoRua__c);
			objTO.Bairro = (String) obj.get(objConfiguracao.CampoBairro__c);
			objTO.Cidade = (String) obj.get(objConfiguracao.CampoCidade__c);
			objTO.UF = (String) obj.get(objConfiguracao.CampoEstado__c);
			objTO.Numero = (String) obj.get(objConfiguracao.CampoNumero__c);
			objTO.Complemento = (String) obj.get(objConfiguracao.CampoComplemento__c);
		} catch(SObjectException e) {
			return objTO;
		}

		return objTO;
	}

	/**
	 * @Autor: Everymind
	 * @Data: 05/07/2020
	 * @Descrição: Método que consulta a classe de serviço de consulta de endereço por CEP.
	 * @Parâmetro: String configuracaoId - Atributo que representa o Id da configuração selecionada.
	 * @Parâmetro: String cep - Atributo que representa o CEP digitado no componente para consulta de endereço.
	 * @Retorno: ConsultaEnderecoTO - Objeto que representa o resultado da consulta de endereço.
	 */
	@AuraEnabled
	public static ConsultaEnderecoTO consultarEndereco(String configuracaoId, String cep) {
		return ConsultaEnderecoService.consultar(configuracaoId, cep);
	}

	/**
	 * @Autor: Everymind
	 * @Data: 28/07/2020
	 * @Descrição: Método que recupera a o registro de contexto.
	 * @Parâmetro: String recordId - Id do registro injetado pelo componente.
	 * @Parâmetro: String cep - Valor de cep digitada no formulário.
	 * @Parâmetro: String rua - Valor de rua digitada no formulário.
	 * @Parâmetro: String bairro - Valor de bairro digitada no formulário.
	 * @Parâmetro: String cidade - Valor de cidade digitada no formulário.
	 * @Parâmetro: String estado - Valor de estado digitada no formulário.
	 * @Parâmetro: String numero - Valor de numero digitada no formulário.
	 * @Parâmetro: String complemento - Valor de complemento digitada no formulário.
	 * @Retorno: ConfiguracaoConsultaEndereco__mdt objConfiguracao - Objeto que de configuração da consulta de endereço.
	 */
	@AuraEnabled
	public static void salvarRegistro(String recordId, String cep, String rua, String bairro, String cidade, String estado, String numero, String complemento, ConfiguracaoConsultaEndereco__mdt objConfiguracao) {
		SObject obj;

		// Verifica se veio o objeto de configuração
		if(objConfiguracao == null) {
			throw new AuraHandledException('Ocorreu um erro inesperado. Por favor, contate o administrador.');
		}

		// Recupera o tipo de objeto configurado no metadado
		Schema.SObjectType type = Schema.getGlobalDescribe().get(objConfiguracao.Objeto__c);

		// Verifica se encontrou o tipo do objeto configurado
		if(type == null) {
			throw new AuraHandledException('Ocorreu um erro inesperado. Por favor, contate o administrador.');
		}

		// Atribui os valores nos devidos campos
		try {
			obj = type.newSObject();
			obj.Id = recordId;
			obj.put(objConfiguracao.CampoCEP__c, cep);
			obj.put(objConfiguracao.CampoRua__c, rua);
			obj.put(objConfiguracao.CampoBairro__c, bairro);
			obj.put(objConfiguracao.CampoCidade__c, cidade);
			obj.put(objConfiguracao.CampoEstado__c, estado);
			obj.put(objConfiguracao.CampoNumero__c, numero);
			obj.put(objConfiguracao.CampoComplemento__c, complemento);
		} catch(SObjectException e) {
			throw new AuraHandledException(e.getDmlMessage(0));
		}

		// Atualiza o registro
		try {
			update obj;
		} catch(DmlException e) {
			throw new AuraHandledException(e.getDmlMessage(0));
		}
	}
}