public with sharing class NetworkDAO {
    private final static NetworkDAO instance = new NetworkDAO();

    private NetworkDAO() { }

    public static NetworkDAO getInstance(){
        return instance;
    }

    public Network buscarPorName(String networkName){
        List<Network> lstNetwork = [
            SELECT 
                Id 
            FROM 
                Network 
            WHERE 
                Name = : networkName];
        return lstNetwork.size() > 0 ? lstNetwork[0] : null;
    }
}