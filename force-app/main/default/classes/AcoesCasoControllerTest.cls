@IsTest
public class AcoesCasoControllerTest {
    
    @TestSetup
    static void setup(){
        Account objContaJuridica = new Account(
            Name = 'Teste Account',
            Carteira__c = 'CALATV',
            CnpjCpf__c = '30539030000131',
            RecordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaJuridica').getRecordTypeId(),
            Vip__c = true
        );
        insert objContaJuridica;

        Codigo__c codigoClientePJ = new Codigo__c(
            Name = 'TESTE CODIGO',
            Origem__c = 'Manual',
            Status__c = 'Ativo',
            Conta__c = objContaJuridica.Id
        );
        insert codigoClientePJ;

        Case objCasoPJ = new Case(
            AccountId = objContaJuridica.Id,
            Reason = 'ACESSOS',
            Submotivo__c = 'CRIAÇÃO CÓD. OPERADOR CLIENTE',
            Area__c = 'Central de Relacionamento',
            Description = 'TESTE',
            CodigoDeCliente__c = codigoClientePJ.Id,
            Mensagem__c = 'TESTE',
            RecordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMAtendimento').getRecordTypeId()
        );
        insert objCasoPJ;

        Account objContaFisica = new Account(
            FirstName = 'Teste Account',
            LastName = 'PF',
            RecordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PessoaFisica').getRecordTypeId()
        );
        insert objContaFisica;

        /*Contact contatoPF = new Contact(
            CPF__c = '80078151040',
            AccountId = objContaFisica.Id
        );
        insert contatoPF;*/

        Codigo__c codigoClientePF = new Codigo__c(
            Name = 'TESTE CODIGO PF',
            Origem__c = 'Manual',
            Status__c = 'Ativo',
            Conta__c = objContaFisica.Id
        );
        insert codigoClientePF;

        Case objCasoPF = new Case(
            AccountId = objContaFisica.Id,
            Reason = 'ACESSOS',
            Submotivo__c = 'CRIAÇÃO CÓD. OPERADOR CLIENTE',
            Area__c = 'Central de Relacionamento',
            Description = 'TESTE',
            CodigoDeCliente__c = codigoClientePF.Id,
            Mensagem__c = 'TESTE',
            RecordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CSMAtendimento').getRecordTypeId()

        );
        insert objCasoPF;
    }

    @IsTest
    private static void testRetornaDadosCaso(){
        Case caso = [SELECT Id FROM Case WHERE Account.CnpjCpf__c = '30539030000131' LIMIT 1];

        AcoesCasoControllerTO.ReturnTO retn = AcoesCasoController.retornaDadosCaso(String.valueOf(caso.Id));

        System.assertEquals(true, retn != null);
    }

    @IsTest
    private static void testCancelamentoCadastroPositivoPJ(){
        Case caso = [SELECT Id FROM Case WHERE Account.CnpjCpf__c = '30539030000131' LIMIT 1];

        AcoesCasoControllerTO.ReturnStatusTO retn = AcoesCasoController.cancelamentoCadastroPositivo(String.valueOf(caso.Id));

        System.assertEquals(true, retn != null);
    }

    @IsTest
    private static void testReautorizacaoCadastroPositivoPJ(){
        Case caso = [SELECT Id FROM Case WHERE Account.CnpjCpf__c = '30539030000131' LIMIT 1];

        //AcoesCasoControllerTO.ReturnStatusTO retn = AcoesCasoController.reautorizacaoCadastroPositivo(String.valueOf(caso.Id), 'TESTE', 'TESTE@TESTE.COM','2000-01-01');

        //System.assertEquals(true, retn != null);
    }

    @IsTest
    private static void testCancelamentoCadastroPositivoPF(){
        Case caso = [SELECT Id FROM Case WHERE Account.FirstName = 'Teste Account' LIMIT 1];

        AcoesCasoControllerTO.ReturnStatusTO retn = AcoesCasoController.cancelamentoCadastroPositivo(String.valueOf(caso.Id));

        System.assertEquals(true, retn != null);
    }

    @IsTest
    private static void testReautorizacaoCadastroPositivoPF(){
        Case caso = [SELECT Id FROM Case WHERE Account.FirstName = 'Teste Account' LIMIT 1];
        AcoesCasoControllerTO.CadastroPositivoFormTO fornTO = new AcoesCasoControllerTO.CadastroPositivoFormTO();
        fornTO.nome = 'teste';
        fornTO.numeroDocumento = '11111111111';
        fornTO.numeroRg = '000000000';
        fornTO.tipoCliente = 'PF';
        fornTO.email = 'teste@teste.com';
        fornTO.dataAniversario = '2020-03-22';
        fornTO.sexo = 'M';
        fornTO.orgaoExpedidor = 'SSP';
        fornTO.cidade = 'São Paulo';
        fornTO.endereco = 'Rua Teste,03';
        fornTO.bairro = 'Teste';
        fornTO.cep = '11111111';
        fornTO.unidadeFederativa = 'SP';
        AcoesCasoControllerTO.ReturnStatusTO retn = AcoesCasoController.reautorizacaoCadastroPositivo(JSON.serialize(fornTO));

//      System.assertEquals(true, retn != null);
    }

    @isTest
    private static void testObterStatusCadastroPositivo()
    {
        Case caso = [SELECT Id FROM Case WHERE Account.FirstName = 'Teste Account' LIMIT 1];
        AcoesCasoControllerTO.ReturnStatusTO retn = AcoesCasoController.obterStatusCadastroPositivo(String.valueOf(caso.Id));

    }

}