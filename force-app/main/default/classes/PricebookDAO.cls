public class PricebookDAO {
    private final static PricebookDAO instance = new PricebookDAO();

    private PricebookDAO() { }

    public static PricebookDAO getInstance(){
        return instance;
    }

    public List<Pricebook2> buscarAtivos(){
        return [
            SELECT
                Id,
                TipoRegistroOportunidade__c
            FROM
                Pricebook2
            WHERE
                IsActive = true
        ];
    }
}