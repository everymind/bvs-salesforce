public class UserDAO {
    private final static UserDAO instance = new UserDAO();

    private UserDAO() { }

    public static UserDAO getInstance(){
        return instance;
    }

    public User buscarPorId(Id IdUsuario){
        List<User> lstUsuario = buscarPorId(new Set<Id> { IdUsuario });

        if (lstUsuario.isEmpty()){
            return null;
        } else {
            return lstUsuario[0];
        }
    }

    public List<User> buscarPorId(Set<Id> setIdUsuario){
        return [
            SELECT
                Id,
                CPF__c,
                Name,
                Username,
                Profile.Name,
                Email
            FROM
                User
            WHERE
                Id IN :setIdUsuario
        ];
    }

    public User buscarPorIdGrupo(String userId){

        return [
            SELECT
                Grupo__c
            FROM
                User
            WHERE
                Id = :userId
        ];
    }

    public string obterUsername (string userId)
    {
        List<User> usuario = 
        [
            SELECT id,username 
            FROM user
            WHERE id =: userId
        ];

        return usuario.size() > 0 ?  usuario[0].username : null;
    }
}