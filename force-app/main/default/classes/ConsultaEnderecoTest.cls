/**
 * @Autor: Everymind
 * @Data: 04/07/2020
 * @Descrição: Classe de teste para o serviço de consulta de endereço.
 */
@IsTest
private class ConsultaEnderecoTest {

	/**
	 * @Autor: Everymind
	 * @Data: 04/07/2020
	 * @Descrição: Método que testa uma chamada de consulta de endereço com sucesso.
	 * @Rescurso: ResultadoConsultaEnderecoSucesso - Documento XML que representa um resultado de sucesso no serviço de consulta de endereço.
	 */
	@IsTest
	private static void testarServicoConsultaEnderecoComSucesso() {
		// Preenche CEP de consulta
		String cep = '01001001';

		// Monta objeto de Mock da chamada HTTP
		StaticResourceCalloutMock objMock = new StaticResourceCalloutMock();
		objMock.setStaticResource('ResultadoConsultaEnderecoSucesso');
		objMock.setStatusCode(200);
		objMock.setHeader('Content-Type', 'text/xml');

		// Atribui o objeto de Mock ao contexto de teste
		Test.setMock(HttpCalloutMock.class, objMock);

		// Buscar Custom Metadata de Consulta de Endereço
		ConfiguracaoConsultaEndereco__mdt objConfiguracao = ConsultaEnderecoController.buscarConfiguracao('Contact');

		// Realiza a chamada de consulta de endereço
		ConsultaEnderecoTO objResultado = ConsultaEnderecoService.consultar(objConfiguracao.Id, cep);

		// Valida resultados da chamada de teste
		System.assertEquals(objResultado.CodigoRetorno, '00');
		System.assertEquals(objResultado.MensagemRetorno, 'CONSULTA EFETUADA COM SUCESSO');
		System.assertEquals(objResultado.Logradouro, 'PC DA SE');
		System.assertEquals(objResultado.Bairro, 'SE');
		System.assertEquals(objResultado.Cidade, 'SAO PAULO');
		System.assertEquals(objResultado.UF, 'SP');
	}
	
	/**
	 * @Autor: Everymind
	 * @Data: 04/07/2020
	 * @Descrição: Método que testa uma chamada de consulta de endereço com sucesso.
	 * @Rescurso: ResultadoConsultaEnderecoSucesso - Documento XML que representa um resultado de erro no serviço de consulta de endereço.
	 */
	@IsTest
	private static void testarServicoConsultaEnderecoComErro() {
		// Preenche CEP de consulta
		String cep = 'CEP_INVALIDO';

		// Monta objeto de Mock da chamada HTTP
		StaticResourceCalloutMock objMock = new StaticResourceCalloutMock();
		objMock.setStaticResource('ResultadoConsultaEnderecoErro');
		objMock.setStatusCode(200);
		objMock.setHeader('Content-Type', 'text/xml');

		// Atribui o objeto de Mock ao contexto de teste
		Test.setMock(HttpCalloutMock.class, objMock);

		// Buscar Custom Metadata de Consulta de Endereço
		ConfiguracaoConsultaEndereco__mdt objConfiguracao = ConsultaEnderecoController.buscarConfiguracao('Contact');

		try {
			// Realiza a chamada
			ConsultaEnderecoService.consultar(objConfiguracao.Id, cep);
		} catch(Exception e) {
			// Valida resultado da chamada de teste
			System.assertEquals(e.getMessage(), 'CEP não cadastrado');
		}
	}
}