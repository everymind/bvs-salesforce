public with sharing class SurveyDAO {
    private final static SurveyDAO instance = new SurveyDAO();

    private SurveyDAO() { }

    public static SurveyDAO getInstance(){
        return instance;
    }

    public Survey buscarPorDeveloperName(String surveyDeveloperName){
       
       List<Survey> listSurvey = [ 
        SELECT 
            Id 
        FROM 
            Survey 
        WHERE 
            DeveloperName = : surveyDeveloperName
                ];

        if (listSurvey.size() > 0)
        {
            return listSurvey[0];
        }
        else
        {
            return null;
        }
    }
}