public with sharing class TabulacaoDAO {
    private static final TabulacaoDAO instance = new TabulacaoDAO();
    public static TabulacaoDAO getInstance() {
		return instance;
    }

    public List<TabulacaoSLA__c> buscarFila(String tipoRegistro, String motivo, String subMotivo, String subMotivo2, String meioAcesso)
    {
        return [
            SELECT 
                ProximaFila__c
            FROM
                TabulacaoSLA__c
            WHERE StatusTabulacao__c = 'ATIVO'
                AND TipoRegistro__c = :tipoRegistro 
                AND MotivoTxt__c = :motivo 
                AND SubMotivoTxt__c = :subMotivo
                AND SubMotivo2Txt__c = :subMotivo2
                AND MeioAcessoTxt__c = :meioAcesso
                AND FilaAtual__c = null
            limit 1
        ];
    }

    public boolean requerAprovacao(String tipoRegistro, String motivo, String subMotivo, String subMotivo2, String meioAcesso)
    {
        try{
            return [
                SELECT 
                    RequerAprovacao__c
                FROM
                TabulacaoSLA__c
                WHERE
                    StatusTabulacao__c = 'ATIVO'
                    AND TipoRegistro__c = :tipoRegistro 
                    AND MotivoTxt__c = :motivo 
                    AND SubMotivoTxt__c = :subMotivo
                    AND SubMotivo2Txt__c = :subMotivo2
                    AND MeioAcessoTxt__c = :meioAcesso
                limit 1
            ][0].RequerAprovacao__c;
        } catch(Exception ex){
            return(false);
        }
    }

    public TabulacaoSLA__c dadosAprovacao(String tipoRegistro, String motivo, String subMotivo, String subMotivo2, String meioAcesso)
    {
        try{
            System.debug('tipoRegistro='+tipoRegistro);
            System.debug('motivo='+motivo);
            System.debug('subMotivo='+subMotivo);
            System.debug('subMotivo2='+subMotivo2);
            System.debug('meioAcesso='+meioAcesso);
            return [
                SELECT 
                    RequerAprovacao__c,
                    Observacoes__c,
                    MotivoTxt__c, SubMotivoTxt__c, SubMotivo2Txt__c, TipoCasoTxt__c
                FROM
                TabulacaoSLA__c
                WHERE
                    StatusTabulacao__c = 'ATIVO'
                    AND TipoRegistro__c = :tipoRegistro 
                    AND MotivoTxt__c = :motivo 
                    AND SubMotivoTxt__c = :subMotivo
                    AND SubMotivo2Txt__c = :subMotivo2
                    AND MeioAcessoTxt__c = :meioAcesso
                limit 1
            ][0];
        } catch(Exception ex){
            return(null);
        }
    }

   
    public TabulacaoSLA__c buscarTempoSLA(String name){
        TabulacaoSLA__c tt =  [
            SELECT 
                TempoSLA__c
            FROM
                TabulacaoSLA__c
            WHERE 
                name = :name
            limit 1
        ];

        System.debug('========================================> ' + tt);
        return tt;
    }

    public TabulacaoSLA__c buscarTempoSLA(String tipoRegistro, String motivo, String subMotivo, String subMotivo2, String meioAcesso, String filaAtual)
    {

        system.debug('tipoRegistro ' +tipoRegistro);
        system.debug('motivo ' +motivo);
        system.debug('subMotivo ' +subMotivo);
        system.debug('subMotivo2 ' +subMotivo2);
        system.debug('meioAcesso ' +meioAcesso);
        system.debug('filaAtual ' +filaAtual);
        return [
            SELECT 
                TempoSLA__c
            FROM
                TabulacaoSLA__c
            WHERE StatusTabulacao__c = 'ATIVO'
                AND TipoRegistro__c = :tipoRegistro 
                AND MotivoTxt__c = :motivo 
                AND SubMotivoTxt__c = :subMotivo
                AND SubMotivo2Txt__c = :subMotivo2
                AND MeioAcessoTxt__c = :meioAcesso
                AND FilaAtual__c = :filaAtual
            limit 1
        ];
    }
    public TabulacaoSLA__c buscarTempoSLA(String tipoRegistro, String motivo, String subMotivo, String subMotivo2, String meioAcesso, String filaAtual, String proximaFila)
    {

        system.debug('tipoRegistro ' +tipoRegistro);
        system.debug('motivo ' +motivo);
        system.debug('subMotivo ' +subMotivo);
        system.debug('subMotivo2 ' +subMotivo2);
        system.debug('meioAcesso ' +meioAcesso);
        system.debug('filaAtual ' +filaAtual);
        return [
            SELECT 
                TempoSLA__c
            FROM
            TabulacaoSLA__c
            WHERE StatusTabulacao__c = 'ATIVO'
                AND TipoRegistro__c = :tipoRegistro 
                AND MotivoTxt__c = :motivo 
                AND SubMotivoTxt__c = :subMotivo
                AND SubMotivo2Txt__c = :subMotivo2
                AND MeioAcessoTxt__c = :meioAcesso
                AND FilaAtual__c = :filaAtual
                AND ProximaFila__c =:proximaFila
            limit 1
        ];
    }

    public List<AggregateResult> listaMotivoTipoRegistro(String tipoRegistro){
        return [
            SELECT MotivoTxt__c 
            FROM TabulacaoSLA__c 
            WHERE TipoRegistro__c =: tipoRegistro 
            GROUP BY MotivoTxt__c
            order by MotivoTxt__c
        ];
    }

    public List<AggregateResult> listaSubMotivoTipoRegistro(String tipoRegistro, String motivo){
        return [
            SELECT SubMotivoTxt__c 
            FROM TabulacaoSLA__c 
            WHERE TipoRegistro__c =: tipoRegistro  
            AND MotivoTxt__c =: motivo 
            GROUP BY SubMotivoTxt__c
            order by SubMotivoTxt__c
        ];
    }

    public List<AggregateResult> listaSubMotivo2TipoRegistro(String tipoRegistro, String motivo, String submotivo){
        return [
            SELECT SubMotivo2Txt__c 
            FROM TabulacaoSLA__c 
            WHERE TipoRegistro__c =: tipoRegistro 
            AND MotivoTxt__c =: motivo 
            AND SubMotivoTxt__c =: submotivo 
            GROUP BY SubMotivo2Txt__c
            order by SubMotivo2Txt__c
        ];
    }

    public List<AggregateResult> listaMeioAcessoTipoRegistro(String tipoRegistro, String motivo, String submotivo){
        return [
            SELECT MeioAcessoTxt__c 
            FROM TabulacaoSLA__c 
            WHERE TipoRegistro__c =: tipoRegistro 
            AND MotivoTxt__c =: motivo 
            AND SubMotivoTxt__c =: submotivo 
            GROUP BY MeioAcessoTxt__c
            order by MeioAcessoTxt__c
        ];
    }

    public List<TabulacaoSLA__c> listaTabulacao(String tipoRegistro){
        return [
            SELECT MotivoTxt__c, SubMotivoTxt__c, SubMotivo2Txt__c, MeioAcessoTxt__c, TempoSLA__c, Observacoes__c
            FROM TabulacaoSLA__c 
            Where TipoRegistro__c = :tipoRegistro
            order by MotivoTxt__c, SubMotivoTxt__c, SubMotivo2Txt__c, MeioAcessoTxt__c
        ];
    }

    public String tipoCaso(String tipoRegistro, String motivo, String subMotivo, String subMotivo2)
    {
        try{
            return [
                SELECT 
                    TipoCasoTxt__c
                FROM
                    TabulacaoSLA__c
                WHERE
                    StatusTabulacao__c = 'ATIVO'
                    AND TipoRegistro__c = :tipoRegistro 
                    AND MotivoTxt__c = :motivo 
                    AND SubMotivoTxt__c = :subMotivo
                    AND SubMotivo2Txt__c = :subMotivo2
                limit 1
            ][0].TipoCasoTxt__c;
        } catch(Exception ex){
            return(null);
        }
    }

    public List<Codigo__c> listaCodigoDeCliente(String codigoConta){

        List<Codigo__c> listaDeCodigoDeCliente = [Select Id,Name From Codigo__c Where Conta__c =: codigoConta];
        return listaDeCodigoDeCliente;

    }

    public List<TabulacaoSLA__c> buscarTempoSLAByList(String tipoRegistro, List<String> motivo, List<String> subMotivo, List<String> subMotivo2, List<String> meioAcesso, List<String> filaAtual)
    {

        system.debug('tipoRegistro ' +tipoRegistro);
        system.debug('motivo ' +motivo);
        system.debug('subMotivo ' +subMotivo);
        system.debug('subMotivo2 ' +subMotivo2);
        system.debug('meioAcesso ' +meioAcesso);
        system.debug('filaAtual ' +filaAtual);
        return [
            SELECT 
                TempoSLA__c,MotivoTxt__c,SubMotivoTxt__c,SubMotivo2Txt__c,MeioAcessoTxt__c,FilaAtual__c
            FROM
            TabulacaoSLA__c
            WHERE StatusTabulacao__c = 'ATIVO'
                AND TipoRegistro__c = :tipoRegistro 
                AND MotivoTxt__c IN:motivo 
                AND SubMotivoTxt__c IN :subMotivo
                AND SubMotivo2Txt__c IN :subMotivo2
                AND MeioAcessoTxt__c IN :meioAcesso
                AND FilaAtual__c IN :filaAtual
            
        ];
    }
    
    
}