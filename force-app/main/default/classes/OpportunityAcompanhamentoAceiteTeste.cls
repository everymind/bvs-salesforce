@isTest
public class OpportunityAcompanhamentoAceiteTeste {

    static testMethod void myUnitTest() {
     
        test.startTest();
       
        Opportunity Opp = new Opportunity();
        opp.name = 'Teste';
        opp.stageName = 'TESTe';
        opp.closeDate = System.Today();
        insert Opp; 

        OpportunityAcompanhamentoAceite.buscarCnpjCpfConta(Opp.Id);
        test.stopTest();
    }
}