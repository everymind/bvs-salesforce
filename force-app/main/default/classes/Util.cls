public class Util {
    /**
     * Realiza uma requisição HTTP Request
     *
     * @param   endPoint  - URL da requisição
     * @param   method  - Método HTTP
     * @param   headers  - Cabeçalhos da chamada 
     * @param   body  - Corpo da requisição
     * @param   timeout  - Tempo de timeout
     * @return  HttpResponse - Objeto HTTP Response
     */
    public static HttpResponse executarHttpRequest(String endPoint, String method, Map<String, String> headers, String body, Integer timeout){
        HttpRequest request = new HttpRequest();
        Http h = new Http();

        request.setEndpoint(endPoint);
        request.setMethod(method);
        request.setTimeout(timeout);

        //Gera body = '' apenas para o método POST
        if(method == 'POST' || method == 'PUT')
            request.setBody(body != null ? body : '');
        else if(body != null)
            request.setBody(body);

        for(String nomeHeader : headers.keySet()){
            request.setHeader(nomeHeader, headers.get(nomeHeader));
        }

        return h.send(request);
    }

    public static RecordTypeInfo getRecordType(String strSObject, String strRecordTypeName) {
        Map<String, RecordTypeInfo> mapRecordTypeInfo = ((SObject)Type.forName(strSObject).newInstance()).getSObjectType().getDescribe().getRecordTypeInfosByName();
        
        for (String strKey : mapRecordTypeInfo.keySet()) {
            RecordTypeInfo objRecordTypeInfo = mapRecordTypeInfo.get(strKey);

            if (objRecordTypeInfo.getDeveloperName() == strRecordTypeName){
                return objRecordTypeInfo;
            }
        }

        return null;
    }

    public static Id getRecordTypeId(String strSObject, String strRecordTypeName){
        RecordTypeInfo objRecordTypeInfo = getRecordType(strSObject, strRecordTypeName);
        
        if (objRecordTypeInfo != null){
            return objRecordTypeInfo.getRecordTypeId();
        } else {
            return null;
        }
    }

    public static String getRecordTypeDeveloperName(Id id){
        try{
            Map<Id,Schema.RecordTypeInfo> rtMap = Case.sobjectType.getDescribe().getRecordTypeInfosById();
            return(rtMap.get(id).getDeveloperName());
        } catch(Exception ex){
            return('Erro ao buscar tipo');
        }
    }

    public static String getValueOfObject(SObject objObjeto, String strPropriedade){
        List<String> lstPropriedades = strPropriedade.split('\\.');

        SObject objObjetoAtual = objObjeto;
        
        Integer intIndex;
        for (intIndex = 0; intIndex < lstPropriedades.size(); intIndex++){
            if (intIndex < (lstPropriedades.size() - 1)){
                objObjetoAtual = objObjetoAtual.getSobject(lstPropriedades[intIndex]);
            } else {
                return String.valueOf(objObjetoAtual.get(lstPropriedades[intIndex]));
            }
        }

        return null;
    }

    public static String replaceCharacterSpecial(String strTexto){
        List<String> lstSpecials = new List<String> { 'ç', 'Ç', 'á', 'é', 'í', 'ó', 'ú', 'ý', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ý', 'à', 'è', 'ì', 'ò', 'ù', 'À', 'È', 'Ì', 'Ò', 'Ù', 'ã', 'õ', 'ñ', 'ä', 'ë', 'ï', 'ö', 'ü', 'ÿ', 'Ä', 'Ë', 'Ï', 'Ö', 'Ü', 'Ã', 'Õ', 'Ñ', 'â', 'ê', 'î', 'ô', 'û', 'Â', 'Ê', 'Î', 'Ô', 'Û', '.', ',', '-', ':', '(', ')', 'ª', '|', '\\', '°' };
        List<String> lstCommon = new List<String> { 'c', 'C', 'a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y', 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U', 'a', 'o', 'n', 'a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'A', 'O', 'N', 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U', '', '', '', '', '', '', '', '', '', '' };

        for (Integer intIndex = 0; intIndex < lstSpecials.size(); intIndex++){
            strTexto = strTexto.replace(lstSpecials[intIndex], lstCommon[intIndex]);
        }

        return strTexto;
    }

    public static String formatterDatePtBr(Date dDate){
        Datetime dtDate = Datetime.newInstance(dDate.year(), dDate.month(), dDate.day(), 0, 0, 0);

        return dtDate.format('dd/MM/YYYY');
    }
}