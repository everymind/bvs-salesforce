({
    helperMethod : function(component) {
        let title = component.get('v.titulo');
        let message = component.get('v.mensagem');
        let type = component.get('v.tipo');

        this.toastMessage(title, message, type);
    },
    toastMessage: function(title, message, type, duration){
        var notification = $A.get("e.force:showToast");
        notification.setParams({
            type: type,
            title: title,
            mode: 'dismissible',
            duration: duration ? duration : 5000,
            message: message
        });
        notification.fire();
    }
})