({
    buscarUrl : function(component) {
        var self = this;

        this.callApex(
            component,
            'buscarCnpjCpfConta',
            {
                idOportunidade: component.get('v.recordId')
            },
            function (response){
                if (response.Status == 'success'){
                    component.set('v.url', 'https://dw.bvsnet.com.br/integrador/api/WebHook/677BCDDE-B837-42BE-BFFD-A96A73B8CA03/AceiteEmail?cnpj=' + response.CnpjCpf);
                } else{
                    self.showToast('error', 'Erro', response.Mensagem, 5000);
                }
            }
        );
    },

    callApex: function (component, method, params, callback) {
		let apexCall = component.get('c.' + method);
		if (params != null) {
			apexCall.setParams(params);
		}

		apexCall.setCallback(this, (response) => {
			var state = response.getState();
			if (state === "SUCCESS") {
				callback(response.getReturnValue());
			}else{
				var error = error = response.getError();
				this.showToast("Erro", error[0].message, 'error');
			}
		});

		$A.enqueueAction(apexCall);
    },

    showToast: function(type, title, message, duration){
        var notification = $A.get("e.force:showToast");
        
        notification.setParams({
            type: type,
            title: title,
            mode: 'dismissible',
            duration: duration,
            message: message
        });

        notification.fire();
    }
})