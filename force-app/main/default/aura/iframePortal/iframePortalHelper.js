({
    buscarUrl : function(component) {
		let recordId = component.get('v.recordId');
		let portal = component.get('v.portal');
		this.chamarApex(
			component,
			'buscarUrl',
			{ recordId, portal },
			(url) => { component.set('v.url', url) }
		);
	},
	
	chamarApex: function (component, method, params, callback) {
		let apexCall = component.get('c.' + method);
		
        if (params != null) {
            apexCall.setParams(params);
        }

        apexCall.setCallback(this, response => {
            var state = response.getState();

            if (state === 'SUCCESS') {
                callback(response.getReturnValue());
            } else {
                var error = (error = response.getError());
                this.notificar('Error', error[0].message, 'error', 10);
            }
        });

        $A.enqueueAction(apexCall);
	},
	
	notificar: function (title, message, type, duration) {
        var notification = $A.get("e.force:showToast");
        notification.setParams({
            type: type,
            title: title,
            mode: "dismissible",
            duration: duration ? duration : 10,
            message: message
        });
        notification.fire();
    }
})