({
    callApex: function (component, method, params, callback) {
		if (component) {
			let apexCall = component.get('c.' + method);
			if (params != null) {
				apexCall.setParams(params);
			}
			apexCall.setCallback(this, (response) => {
				var state = response.getState();
				if (state === "SUCCESS") {
					callback(response.getReturnValue());
				} else {
					let errors = response.getError();
					let mensagem;
					let type = 'error';
					let title = 'Erro na consulta';
					if (errors) {
						if (errors[0] && errors[0].message) {
							mensagem = errors[0].message;
						}
					}

					if (mensagem == null) {
						mensagem = 'Erro na consulta';
					}
					
					this.showToast(title, mensagem, type, 20);
					component.set('v.showSpinner', false);
				};
			});

			$A.enqueueAction(apexCall);
		} else {
            component.set('v.showSpinner', false);
			console.log('Erro ao efetuar a chamada do apex, porem não foi possivel pegar o motivo da exception.');
		}
    },

    showToast: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },
})