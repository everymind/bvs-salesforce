({
    onIniciarReencarteiramento : function(component, event, helper){
        component.set('v.showSpinner', true);

        helper.callApex(component, 'iniciarReencarteiramento', {}, (retorno) =>{
            component.set('v.showSpinner', false);
            helper.showToast('Sucesso', 'Processo de reencarteiramento iniciado!', 'success');
        });	
    }
})