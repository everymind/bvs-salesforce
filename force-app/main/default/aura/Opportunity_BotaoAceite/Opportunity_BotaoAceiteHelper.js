({
    enviarAceite : function(component) {
        this.callApex(
            component,
            'enviarAceite',
            {
                idOportunidade: component.get('v.recordId')
            },
            function (response){
                var notification = $A.get("e.force:showToast");

                if (response.Status == 'success'){
                    notification.setParams({
                        type: 'success',
                        title: 'Sucesso',
                        mode: 'dismissible',
                        duration: 5000,
                        message: response.Message
                    });
                } else if (response.Status == 'warning'){
                    notification.setParams({
                        type: 'warning',
                        title: 'Aviso',
                        mode: 'dismissible',
                        duration: 5000,
                        message: response.Message
                    });
                } else{
                    notification.setParams({
                        type: 'error',
                        title: 'Erro',
                        mode: 'dismissible',
                        duration: 5000,
                        message: 'Ocorreu um erro ao tentar enviar o aceite!'
                    });
                }

                notification.fire();

				$A.get("e.force:closeQuickAction").fire();
				$A.get("e.force:refreshView").fire();
            }
        );
    },

    callApex: function (component, method, params, callback) {
		let apexCall = component.get('c.' + method);
		if (params != null) {
			apexCall.setParams(params);
		}

		apexCall.setCallback(this, (response) => {
			var state = response.getState();
			if (state === "SUCCESS") {
				callback(response.getReturnValue());
			}else{
				var error = error = response.getError();
				this.showToast("Erro", error[0].message, 'error');
			}
		});

		$A.enqueueAction(apexCall);
    }
})