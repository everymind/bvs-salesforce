({
	doInit : function(component, event, helper) {
		helper.inicializar(component);
	},

	handleSearch : function(component, event, helper) {
		helper.consultarEndereco(component);
	},

	handleReset: function(component, event, helper) {
        helper.limparFormulario(component);
    },

	handleSave : function(component, event, helper) {
		helper.salvarRegistro(component);
	}
})