({
	inicializar : function(component) {
		const recordId = component.get('v.recordId');
		const objApiName = component.get('v.sObjectName');
		this.callApex(component, 'buscarConfiguracao', { objApiName }, (objConfiguracao) => {
			component.set('v.objConfiguracao', objConfiguracao);
			this.callApex(component, 'buscarRegistro', { recordId, objConfiguracao }, (objEndereco) => {
				component.set('v.cep', objEndereco.CEP);
				component.set('v.rua', objEndereco.Logradouro);
				component.set('v.bairro', objEndereco.Bairro);
				component.set('v.cidade', objEndereco.Cidade);
				component.set('v.estado', objEndereco.UF);
				component.set('v.numero', objEndereco.Numero);
				component.set('v.complemento', objEndereco.Complemento);
			});
		});
	},

	consultarEndereco : function(component) {
		const cep = component.get('v.cep');
		const configuracao = component.get('v.objConfiguracao');
		this.callApex(component, 'consultarEndereco', { configuracaoId : configuracao.Id, cep : cep }, (objResultado) => {
			component.set('v.rua', objResultado.Logradouro);
			component.set('v.bairro', objResultado.Bairro);
			component.set('v.cidade', objResultado.Cidade);
			component.set('v.estado', objResultado.UF);
		});
	},

	salvarRegistro : function(component) {
		const recordId = component.get('v.recordId');
		const cep = component.get('v.cep');
		const rua = component.get('v.rua');
		const bairro = component.get('v.bairro');
		const cidade = component.get('v.cidade');
		const estado = component.get('v.estado');
		const numero = component.get('v.numero');
		const complemento = component.get('v.complemento');
		const objConfiguracao = component.get('v.objConfiguracao');

		this.callApex(component, 'salvarRegistro', { recordId, cep, rua, bairro, cidade, estado, numero, complemento, objConfiguracao }, () => {
			this.showToast('Sucesso', 'Endereço salvo com sucesso.', 'success');
			$A.get('e.force:refreshView').fire();
			$A.get("e.force:closeQuickAction").fire();
		});
	},

	limparFormulario : function(component) {
		component.set('v.cep', null);
		component.set('v.rua', null);
		component.set('v.bairro', null);
		component.set('v.cidade', null);
		component.set('v.estado', null);
		component.set('v.numero', null);
		component.set('v.complemento', null);
	},

	showToast : function(title, message, type) {
        var showToast = $A.get('e.force:showToast');
        showToast.setParams({
            'title': title,
            'message': message,
            'type': type
        });
        showToast.fire();
	},

	callApex: function (component, method, params, callback) {
		let apexCall = component.get('c.' + method);
		if (params != null) {
			apexCall.setParams(params);
		}

		apexCall.setCallback(this, (response) => {
			var state = response.getState();
			if (state === "SUCCESS") {
				callback(response.getReturnValue());
			}else{
				var error = error = response.getError();
				this.showToast("Erro", error[0].message, 'error');
			}
		});

		$A.enqueueAction(apexCall);
	}
})