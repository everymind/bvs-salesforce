import { api, LightningElement, wire,track} from 'lwc';
import retornaDadosCaso from '@salesforce/apex/AcoesCasoController.retornaDadosCaso';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import cancelamentoCadastroPositivo from '@salesforce/apex/AcoesCasoController.cancelamentoCadastroPositivo';
import reautorizacaoCadastroPositivo from '@salesforce/apex/AcoesCasoController.reautorizacaoCadastroPositivo';
import obterStatusCadastroPositivo from '@salesforce/apex/AcoesCasoController.obterStatusCadastroPositivo';
import { getRecord  } from 'lightning/uiRecordApi'

import MOTIVO from '@salesforce/schema/Case.MotivoTxt__c';
import SUBMOTIVO from '@salesforce/schema/Case.SubMotivoTxt__c';
import SystemModstamp from '@salesforce/schema/Account.SystemModstamp';
const FIELDS = [MOTIVO, SUBMOTIVO];
export default class CasoCadastroPositivo extends LightningElement {
    @api  recordId;
    
    spinner = true;
    subMotivo = '';
    btnCadastroPositivo = true;

    semAcesso = false;
    mensagem = '';

    cancelamentoCadastroPositivoPF = false;
    cancelamentoCadastroPositivoPJ = false;
    reautorizacaoCadastroPositivoPF = false;
    reautorizacaoCadastroPositivoPJ = false;
    documentoCliente = '';
    tipoCliente = '';
    nomeCompleto = '';
    dataNascimento = '';
    rg = ''; // Será enviado somente na Integração
    orgaoExpedidor = ''; // Será enviado somente na Integração
    sexo = ''; // Será enviado somente na Integração
    email = '';
    uf = ''; // Será enviado somente na Integração
    cidade = ''; // Será enviado somente na Integração
    cep = ''; // Será enviado somente na Integração
    endereco = ''; // Será enviado somente na Integração
    bairro = ''; // Será enviado somente na Integração

    razaoSocial = '';
    dataConstituicao = '';
    
    @track 
    show = false;
    @track
    cadastrado = false;
    @track
    showMessage = 'Para esse motivo e submotivo não é permitido o Cadastro Positivo!';
    @track 
    messagemInformativa = true;
    @track
    success = false;
    
    renderedCallback() {
        const options = this.template.querySelectorAll('option')
        options.forEach((opt) => {
            const { value } = opt;
            opt.selected = value === this.uf;
        })
    }
    
    connectedCallback(){

        obterStatusCadastroPositivo({
            caseId: this.recordId
        }).then(resultStatus =>{
            console.log(resultStatus);
            this.cadastrado = resultStatus.mensagem === 'true' ? true : false;

            retornaDadosCaso({
                caseId: this.recordId
                })
                .then(result => {
                    console.log(JSON.stringify(result));
    
                    this.subMotivo = result.subMotivo;
                    this.documentoCliente = result.CPFCNPJ;
                    console.log('result.tipoCliente >> ' + result.tipoCliente);
                    console.log('result.personBirthdate >> ' + result.personBirthdate);
                    console.log('result.subMotivo >> ' + result.subMotivo);
                    this.tipoCliente = (result.tipoCliente == '' ? 'PESSOA FÍSICA' : result.tipoCliente);
                    this.nomeCompleto = result.nome;
                    this.dataNascimento = result.personBirthdate;
                    this.email = result.eMail;
                    if(this.tipoCliente === 'Pessoa jurídica')
                    {
                        this.cidade = result.cidade;
                        this.endereco = result.rua + " ";
                        this.uf = result.uf;
                        this.cep = result.cep;
                        this.bairro = result.bairro;  
                    }
                    console.log(this.subMotivo + this.cadastrado);
                        if (this.subMotivo == 'CP - CANCELAMENTO DO CADASTRO PF - RETIDO'
                            || this.subMotivo == 'CP - CANCELAMENTO DO CADASTRO PF - Ñ RETIDO'
                            || this.subMotivo == 'CP - CANCELAMENTO PF VIA FORMULÁRIO'){
                                if(this.cadastrado == true){
                                    this.cancelamentoCadastroPositivoPF = true;
                                    this.show = true;
                                }else{
                                    this.showMessage = 'Consumidor Pessoa Física já está cancelado!';
                                    this.template.querySelector('h2').style.color = "green"
                                }
    
                        }else if (this.subMotivo == 'CP - CANCELAMENTO DO CADASTRO PJ - RETIDO'
                            || this.subMotivo == 'CP - CANCELAMENTO DO CADASTRO PJ - Ñ RETIDO'){
                                if(this.cadastrado == true){
                                    this.cancelamentoCadastroPositivoPJ = true;
                                    this.show = true;
                                }else{
                                    this.showMessage = 'Consumidor Pessoa Juridica já está cancelado!';
                                    this.template.querySelector('h2').style.color = "green"
                                }
                               
                        }else if (this.subMotivo == 'CP - REAUTORIZAÇÃO DO CADASTRO PF'
                            || this.subMotivo == 'CP - REAUTORIZAÇÃO PF VIA FORMULÁRIO'){
                                if(this.cadastrado == false){
                                    this.reautorizacaoCadastroPositivoPF = true;
                                    this.show = true;
                                }else{
                                    this.showMessage = 'Consumidor Pessoa Física já está autorizado!';
                                    this.template.querySelector('h2').style.color = "green"
                                }
                              
                        }else if (this.subMotivo == 'CP - REAUTORIZAÇÃO DO CADASTRO PJ'){
                            
                            if(this.cadastrado == false){
                                this.reautorizacaoCadastroPositivoPJ = true;
                                this.show = true;
                            }else{
                                this.showMessage = 'Consumidor Pessoa Jurídica já está autorizado!';
                                this.template.querySelector('h2').style.color = "green"
                            }
                        }
                    
                    if (!this.btnCadastroPositivo){
                        this.semAcesso = true;
                        this.mensagem = 'Sem acesso a esta ferramenta.';
                    }
                    this.spinner = false;
                    
                })
                .catch(error => {
                    this.showMessageToast('Erro', error, 'error');
                    console.log(error);
                    this.spinner = false;
                })
            
        })
        
            
    } 

    handleCancelamentoCadastroPositivo() {
        this.spinner = true;
        cancelamentoCadastroPositivo({
            
            caseId: this.recordId
            })
            .then(result => {
                if(result.erro)
                {
                    this.showMessageToast('Error', result.mensagem, 'error');
                    this.show = false;
                    this.showMessage = 'Que pena! O sistema não está disponível no momento';
                    this.cancelamentoCadastroPositivoPJ=false;
                    this.cancelamentoCadastroPositivoPF=false;
                    this.spinner = false;

                }
                else
                {
                    this.showMessageToast('Sucesso', 'Consumidor cancelado com sucesso!', 'success');
                    
                    this.show = false;
                    this.showMessage = 'Consumidor cancelado com sucesso! ';
                    this.cancelamentoCadastroPositivoPJ=false;
                    this.cancelamentoCadastroPositivoPF=false;
                    this.spinner = false;
                    this.messagemInformativa = false;
                    this.success = true;
                }
            })
            .catch(error => {
                this.showMessageToast('Erro', error, 'error');
                console.log(error);
                this.spinner = false;
            })
    }

    handleReautorizacaoCadastroPositivo() {
        if (this.dataNascimento == '' || this.dataNascimento == null
            || this.nomeCompleto == '' || this.nomeCompleto == null
            || this.email == '' || this.email == null)
        {
            this.showMessageToast('Erro', 'Preencha os campos obrigatórios!', 'error');
            return;
        }

        if (!this.isValidDate(this.dataNascimento)){
            this.showMessageToast('Erro', 'A Data de Aniversário é inválida!', 'error');
            return;
        }

        this.spinner = true;

        let cadastroForm = this.preencheObjeto();
        console.log(JSON.stringify(cadastroForm));
        if(cadastroForm.sexo === '' && cadastroForm.tipoCliente === "1")
        {
            this.showMessageToast('Error', 'Campo sexo deve ser preenchido', 'error');
            this.spinner = false;
        }
        else if(cadastroForm.unidadeFederativa === ''){
            this.showMessageToast('Error', 'Campo UF deve ser preenchido', 'error');
            this.spinner = false;
        }
        else{
            reautorizacaoCadastroPositivo({
                cadastroForm: JSON.stringify(cadastroForm)
                })
                .then(result => {
                    if(!result.erro)
                    {
                        this.showMessageToast('Error', result.mensagem, 'error');
                        this.show = false;
                        this.showMessage = 'Que pena! O sistema não está disponível no momento';
                        this.reautorizacaoCadastroPositivoPJ=false;
                        this.reautorizacaoCadastroPositivoPF=false;
                        this.spinner = false;
                
                    }
                    else
                    {
                        this.showMessageToast('Sucesso', 'Consumidor cadastrado com sucesso!', 'success');
                        
                        this.show = false;
                        this.showMessage = 'Consumidor cadastrado com sucesso! ';
                        this.reautorizacaoCadastroPositivoPF=false;
                        this.reautorizacaoCadastroPositivoPJ=false;
                        this.spinner = false;
                        this.messagemInformativa = false;
                        this.success = true;
                    }
                
                    
                    
                })
                .catch(error => {
                    this.showMessageToast('Erro', error.message, 'error');
                    console.log(error);
                    this.spinner = false;
                })
            }

    }

    handleChangeNomeCompleto(event){
        this.nomeCompleto = event.target.value;
    }

    handleChangeDataNascimento(event){
        this.dataNascimento = event.target.value;
    }
    handleChangeEMail(event){
        this.email = event.target.value;
    }
    handleChangeRG(event){
        this.rg = event.target.value;
    }
    handleChangeOrgaoExpedidor(event){
        this.orgaoExpedidor = event.target.value;
    }
    handleChangeEMail(event){
        this.email = event.target.value;
    }
    handleChangeSexo(event){
        this.sexo = event.target.value;
    }
    handleChangeUf(event){
        this.uf = event.target.value;
    }
  
    handleChangeCidade(event){
        this.cidade = event.target.value;
    }
    handleChangeBairro(event){
        this.bairro = event.target.value;
    }
    handleChangeCep(event){
        this.cep = event.target.value;
    }
    handleChangeEndereco(event){
        this.endereco = event.target.value;
    }
    
    isValidDate(s) {
        var bits = s.split('-');
        var d = new Date(bits[0], bits[1] - 1, bits[1]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    preencheObjeto()
    {
        console.log('entrei');
        var cadastroForm = new Object();
        cadastroForm.numeroDocumento = this.documentoCliente;
        cadastroForm.tipoCliente = this.tipoCliente == 'Pessoa física' ? '1' : '3';
        cadastroForm.nome = this.nomeCompleto;
        cadastroForm.dataAniversario = this.dataNascimento
        cadastroForm.numeroRg = this.rg;
        cadastroForm.orgaoExpedidor = this.orgaoExpedidor;
        cadastroForm.sexo = this.sexo;
        cadastroForm.email = this.email;
        cadastroForm.cidade = this.cidade;
        cadastroForm.endereco = this.endereco;
        cadastroForm.bairro = this.bairro;
        cadastroForm.cep = this.cep;
        cadastroForm.unidadeFederativa = this.uf;
        return cadastroForm;
    }
    

    showMessageToast(title, message, variant) {
        const evt = new ShowToastEvent({
          title: title,
          message: message,
          variant: variant,
          mode: 'dismissable'
        });
        this.dispatchEvent(evt);
      }




       dateFormat(inputDate, format) {
        //parse the input date
        const date = new Date(inputDate);
    
        //extract the parts of the date
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();    
    
        //replace the month
        format = format.replace("MM", month.toString().padStart(2,"0"));        
    
        //replace the year
        if (format.indexOf("yyyy") > -1) {
            format = format.replace("yyyy", year.toString());
        } else if (format.indexOf("yy") > -1) {
            format = format.replace("yy", year.toString().substr(2,2));
        }
    
        //replace the day
        format = format.replace("dd", day.toString().padStart(2,"0"));
    
        return format;
    }


    updateRecordView() {
        setTimeout(() => {
             eval("$A.get('e.force:refreshView').fire();");
        }, 1000); 
      }
}