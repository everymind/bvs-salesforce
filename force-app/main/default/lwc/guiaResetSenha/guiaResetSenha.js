import { LightningElement, api, wire, track} from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import MOTIVO_FIELD from '@salesforce/schema/Case.MotivoTxt__c';
import SUBMOTIVO_FIELD from '@salesforce/schema/Case.SubMotivoTxt__c';
import CONTATO_FIELD from '@salesforce/schema/Case.ContactId';
import ACCOUNTID_FIELD from '@salesforce/schema/Case.AccountId';
import buscarUrl from '@salesforce/apex/IFramePortalController.buscarUrl';

const CASE_FIELDS = [MOTIVO_FIELD, SUBMOTIVO_FIELD, CONTATO_FIELD, ACCOUNTID_FIELD];

export default class GuiaResetSenha extends LightningElement {
  @api recordId;
  case;
  pid = true;
  codigoOperadorAC;
  motivo;
  submotivo;
  contact;
  @track contactId;
  contactEmail = null;
  showReenvioSenha = true;
  @ track
  publicURL;

  @wire(getRecord, { recordId: '$recordId', fields: CASE_FIELDS })
  wireRecordCaso({ error, data }){
    if(error){
      console.log('erro');
      console.log(error);
      this.showErrorToast('Erro ao buscar informações do caso', error.body, 'error');
    } else if (data){
      // debugger
      this.case = data;
      this.motivo = this.case.fields.MotivoTxt__c.value;
      this.submotivo = this.case.fields.SubMotivoTxt__c.value;
      this.contactId = this.case.fields.ContactId.value;
      this.AccountId = this.case.fields.AccountId.value;
      this.handleIframe(this.motivo, this.submotivo);
    }
  }

  handleIframe(motivo, submotivo){
    if( motivo == 'ACESSOS' &&
        (submotivo == 'SENHA CÓD. OPERADOR - ORIENTAÇÃO PORTAL' ||
        submotivo == 'SENHA CÓD. OPERADOR - EXCESSO DE TENTATIVAS' ||
        submotivo == 'SENHA CÓD. OPERADOR - E-MAIL DIFERENTE' ||
        submotivo == 'SENHA CÓD. OPERADOR - ERRO NO PORTAL' ||
        submotivo == 'SENHA CÓD. OPERADOR - NÃO RECEBEU E-MAIL' ||
        submotivo == 'SENHA CÓD. OPERADOR - NÃO RECEBEU ACESSO MIGRAÇÃO' ||
        submotivo == 'SENHA CÓD. OPERADOR - REJEITADO/DEVOLVIDO' ||
        submotivo == 'SENHA CÓDIGO DE 8')
      ){
        console.log('Antes buscar url');
        console.log('this.contactId');
        console.log(this.contactId);
        console.log('this.AccountId');
        console.log(this.AccountId);
        buscarUrl({
          recordId: this.contactId,
          portal: 'GuiaResetSenha',
          conta: this.AccountId
        })
        .then(urlMetadata => {
          console.log('depois buscar url');
          this.showReenvioSenha = true;
          console.log(urlMetadata);
          this.publicURL = urlMetadata;
        });        
    }
  }

  showErrorToast(title, message, variant) {
    const evt = new ShowToastEvent({
      title: title,
      message: message,
      variant: variant,
      mode: 'dismissable'
    });
    this.dispatchEvent(evt);
  }  
}