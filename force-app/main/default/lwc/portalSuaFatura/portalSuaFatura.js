import { LightningElement, track, api, wire } from 'lwc';
import buscarUrl from '@salesforce/apex/IFramePortalController.buscarUrl';
import ACCOUNTID from '@salesforce/schema/Case.AccountId';
import { getRecord} from 'lightning/uiRecordApi';

const FIELDS = [ACCOUNTID];

export default class PortalSuaFatura extends LightningElement {
    @api recordId;
    tipo = 'Sua Fatura';

    @track
    accountID='';

    @track url='';
    @track lu_descr='';
    @track lu_codig='';
    @track nome_cliente='';
    @track sfid='';

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredRecord({ error, data }) {
      if (error) {
        this.showErrorToast('Erro ao buscar caso', error.body.message, 'error');
      } else if (data) {
        this.accountID=data.fields.AccountId.value;
        buscarUrl({
          recordId: this.accountID,
          portal: this.tipo
        })
        .then(success => {
            let json=JSON.parse(success);
            this.url=json.url;
            this.lu_descr=json.lu_descr;
            this.lu_codig=json.lu_codig;
            this.nome_cliente=json.nome_cliente;
            this.sfid=json.sfid;
            setTimeout(()=>{
             this.template.querySelector(".formSuaFatura").submit();
            },2000);
          })
        .catch(error => {
            console.log('error', error)
        });
      }  
    }


}