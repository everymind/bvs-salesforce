import { LightningElement, api, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import pegarTipoConta from '@salesforce/apex/criarCasoController.pegarTipoConta';
import salvarCaso from '@salesforce/apex/criarCasoController.salvarCaso';
import { NavigationMixin } from 'lightning/navigation';

import ORIGEM_FIELD from '@salesforce/schema/Case.Origin';
import PRIORIDADE_FIELD from '@salesforce/schema/Case.Priority';
import AREA_FIELD from '@salesforce/schema/Case.Area__c'
import DESCRICAO_FIELD from '@salesforce/schema/Case.Description';
import CONTA_FIELD from '@salesforce/schema/Case.AccountId';
import CONTATO_FIELD from '@salesforce/schema/Case.ContactId';
import PRODUTO_FIELD from '@salesforce/schema/Case.ProductId';
import usuarioLogado from '@salesforce/apex/UserController.usuarioLogado';
import buscaoContatos from '@salesforce/apex/criarCasoController.buscaContatosRelacionados';

export default class CriarCasoLWC extends NavigationMixin (LightningElement) {

    @api recordId;
    @track recordIdCaso;
    @track origem;
    RecordTypeAtendimento = 'CSM Atendimento';
    RecordTypePosVendas = 'CSM Pós Vendas';
    @track
    spinner = false;

    @track
    options = [];

    @track
    perfil;

    @track
    tipoRegistro;
    @track
    contatos;
    @track
    carregouContatos=false;
    @track
    eposVendas=false;
    @track
    eatendimento=false;
    @track
    value;
    connectedCallback() {
        usuarioLogado().then(result => {
            this.perfil=result.Profile.Name;
            console.log('Buscou o profile:'+this.perfil);
            // Solução realizada com demasiada velocidade
            if (this.perfil=='Atendimento N1' || this.perfil=='Gestão de Acessos' || this.perfil=='Supervisor Atendimento' || this.perfil=='Atendimento N2' || this.perfil=='Atendimento N1-N2' || this.perfil=='Analista TI' || this.perfil=='Analista N2'){
                this.options = [
                    { label: this.RecordTypeAtendimento, value: this.RecordTypeAtendimento },
                ];
                this.tipoRegistro=this.RecordTypeAtendimento;
            } else if (this.perfil=='Pós-Vendas' || this.perfil=='Pós vendas - Supervisor' || this.perfil=='Pós vendas - Qualidade' || this.perfil=='Pós vendas - Gestor' || this.perfil=='Pós vendas - Analista N1-N2'){
                this.options = [
                    { label: this.RecordTypePosVendas, value: this.RecordTypePosVendas },
                ];
                this.tipoRegistro=this.RecordTypePosVendas;
            } else if (this.perfil=='Administrativo'){
                this.options = [
                    { label: this.RecordTypePosVendas, value: this.RecordTypePosVendas },
                ];
                this.tipoRegistro=this.RecordTypePosVendas;
            } else {
                this.options = [
                    { label: this.RecordTypeAtendimento, value: this.RecordTypeAtendimento },
                    { label: this.RecordTypePosVendas, value: this.RecordTypePosVendas },
                ];
                this.tipoRegistro='';
            }
            CONTA_FIELD.value = this.recordId;
            console.log("Esse é meu amigo Conta FIELD",CONTA_FIELD );
            this.atualizaTela();
            console.log(this.options)
        });          
    }

    fields;
    tipoConta;

    handleChange(event) {

        this.tipoRegistro = event.detail.value;
       
        this.atualizaTela();
    }
    
    atualizaTela(){
        if(this.tipoRegistro == this.RecordTypeAtendimento){
            CONTA_FIELD.value = this.recordId;
            this.status = 'NOVO';
            pegarTipoConta({idConta: this.recordId})

            .then((tipoConta) => {
            
                this.eatendimento=true;
                this.eposVendas = false;
                this.tipoConta = tipoConta;

            });
            buscaoContatos({
                accId: this.recordId
              }).then((contacts) => { 
                console.log('Contacts: ', JSON.stringify(contacts))
                const contatosFormatados = contacts.map((contact) => {
                  console.log(contact);
                  const { Name } = contact;
                  const { Id } = contact;
                  console.log(Name);
                  const entry = { label: Name, value: Id };
                  return entry;
                })
                console.log(contatosFormatados);
                this.contatos = contatosFormatados;
                this.carregouContatos = true;
              }).catch((reason) => {
                console.log(reason);
              });
        } else if (this.tipoRegistro == this.RecordTypePosVendas){
            console.log(this.tipoRegistro == this.RecordTypePosVendas);

            this.status = 'AGUARDANDO TRATAMENTO';
            pegarTipoConta({idConta: this.recordId})

            .then((tipoConta) => {
    
                console.log('Tipo da Conta: ' + tipoConta);
                this.tipoConta = tipoConta;
                console.log('Conta: ' + this.tipoConta);
                this.eposVendas = true;
                this.eatendimento = false;
                /*if(this.tipoConta == 'PessoaJuridica'){
                    this.fields = [
                        AREA_FIELD,
                        ORIGEM_FIELD,
                        PRIORIDADE_FIELD,
                        CONTATO_FIELD,
                        DESCRICAO_FIELD
                    ];
                } else{
                    this.fields = [
                        AREA_FIELD,
                        ORIGEM_FIELD,
                        PRIORIDADE_FIELD,
                        CONTATO_FIELD,
                        DESCRICAO_FIELD
                    ];
                }
                */
                buscaoContatos({
                    accId: this.recordId
                  }).then((contacts) => {
            
                    console.log('Contacts: ', JSON.stringify(contacts))
                    const contatosFormatados = contacts.map((contact) => {
                      console.log(contact);
                      const { Name } = contact;
                      const { Id } = contact;
                      console.log(Name);
                      const entry = { label: Name, value: Id };
                      return entry;
                    })
                    console.log(contatosFormatados);
                    this.contatos = contatosFormatados;
                    this.carregouContatos = true;
                  }).catch((reason) => {
                    console.log(reason);
            
                  });
            });
        }
    }
    
  handleChange2(event) {
    console.log('handler: ', JSON.stringify(event.detail));
    this.value = event.detail.value;
  }
    handleSubmit(event){
        event.preventDefault();
        try{
            const options = this.template.querySelectorAll('lightning-button')
            options.forEach((opt) => {
            const { value } = opt;
            opt.disabled = true;
            })
            const eventFields = event.detail.fields;
            if(this.tipoConta == 'PessoaJuridica'){
                if(this.value != null)
                {
                    if(this.perfil === 'Comercial - Aquisição' || this.perfil === 'Comercial - Aquisição' || this.perfil === 'Comercial - Gerente Remoto - aceite verbal' || this.perfil === 'Comercial - Parceiro' || this.perfil === 'Comercial - Rentabilização' || this.perfil === 'Comercial - Televendas')
                    {
                        this.origem = 'GC';
                    }
                    else
                    {
                        this.origem = this.obterValorcampo(eventFields, 'origin');
                    }

                    salvarCaso({
                        conta: this.recordId,
                        area: this.obterValorcampo(eventFields, 'area__c'),
                        origem: this.origem,
                        prioridade: this.obterValorcampo(eventFields, 'priority'),
                        status: this.status,
                        descricao: this.obterValorcampo(eventFields, 'description'),
                        produto: this.obterValorcampo(eventFields, 'ProductId'),
                        contato: this.value,
                        tipoRegistro: this.tipoRegistro,
                        }).then(result => {
                        this[NavigationMixin.Navigate]({
    
                            type: 'standard__recordPage',
                            attributes: {
                                recordId: result.Id,
                                actionName: 'view'
                            }
                        });
                        this.spinner = true;
                        const evt = new ShowToastEvent({
                            title: "Caso",
                            message: 'Caso gravado com sucesso!',
                            variant: "success"
                        });
                        this.spinner = false;
                        this.dispatchEvent(evt);
                         }).catch(error => {
                        if (error.body.pageErrors && error.body.pageErrors.length>0){
                            this.showErrorToast('Caso', error.body.pageErrors[0].message, 'error');
                            options.forEach((opt) => {
                            const { value } = opt;
                            opt.disabled = false;
                            })
                            
                        } else if (error.body.fieldErrors){
                            let separator="\n";
                            let message = Object.values(error.body.fieldErrors).map(field => {
                                return field.map(error => error.message).join(separator);
                            }).join(separator);
                            this.spinner = false;
                            this.showErrorToast('Caso', message, 'error');
                            options.forEach((opt) => {
                            const { value } = opt;
                            opt.disabled = false;
                        })
                        } else {
                            this.spinner = false;
                            this.showErrorToast('Caso', error.body.message, 'error');
                            options.forEach((opt) => {
                            const { value } = opt;
                            opt.disabled = false;
                            })
                        }
                    });
                      
                }
                else{
                    const evt = new ShowToastEvent({
                        title: "Caso",
                        message: 'Nome do contato obrigatório!',
                        variant: "error"
                    });
                    this.dispatchEvent(evt);
                    options.forEach((opt) => {
                        const { value } = opt;
                        opt.disabled = false;
                        })
                }
            }
                else{
                    if(this.value != null)
                    {
                    
                        if(this.perfil === 'Comercial - Aquisição' || this.perfil === 'Comercial - Aquisição' || this.perfil === 'Comercial - Gerente Remoto - aceite verbal' || this.perfil === 'Comercial - Parceiro' || this.perfil === 'Comercial - Rentabilização' || this.perfil === 'Comercial - Televendas')
                        {
                            this.origem = 'GC';
                        }
                        else
                        {
                            this.origem = this.obterValorcampo(eventFields, 'origin');
                        }
                        
                            salvarCaso({
                                conta: this.obterValorcampo(eventFields, 'AccountId'),
                                area: this.obterValorcampo(eventFields, 'area__c'),
                                origem: this.origem,
                                prioridade: this.obterValorcampo(eventFields, 'priority'),
                                status: this.status,
                                descricao: this.obterValorcampo(eventFields, 'description'),
                                produto: this.obterValorcampo(eventFields, 'ProductId'),
                                contato: this.value,
                                tipoRegistro: this.tipoRegistro,
                            }).then(result => {
                                this.spinner = true;
                                this[NavigationMixin.Navigate]({

                                    type: 'standard__recordPage',
                                    attributes: {
                                        recordId: result.Id,
                                        actionName: 'view'
                                    }
                                });
                        
                                const evt = new ShowToastEvent({
                                    title: "Caso",
                                    message: 'Caso gravado com sucesso!',
                                    variant: "success"
                                });
                                this.spinner = false;
                                this.dispatchEvent(evt);
                            }).catch(error => {
                                if (error.body.pageErrors && error.body.pageErrors.length>0){
                                    this.showErrorToast('Caso', error.body.pageErrors[0].message, 'error');
                                    options.forEach((opt) => {
                                        const { value } = opt;
                                        opt.disabled = false;
                                        })
                                } else if (error.body.fieldErrors){
                                    let separator="\n";
                                    let message = Object.values(error.body.fieldErrors).map(field => {
                                        return field.map(error => error.message).join(separator);
                                    }).join(separator);
                                    this.spinner = false;
                                    this.showErrorToast('Caso', message, 'error');
                                    options.forEach((opt) => {
                                        const { value } = opt;
                                        opt.disabled = false;
                                        })
                                } else {
                                    this.spinner = false;
                                    this.showErrorToast('Caso', error.body.message, 'error');
                                    options.forEach((opt) => {
                                        const { value } = opt;
                                        opt.disabled = false;
                                        })
                                }
                            });
                        }else{
                            const evt = new ShowToastEvent({
                                title: "Caso",
                                message: 'Nome do contato obrigatório!',
                                variant: "error"
                            });
                            this.dispatchEvent(evt);
                            options.forEach((opt) => {
                                const { value } = opt;
                                opt.disabled = false;
                                })
                        
                        }
                    }
                } catch(erro){
                    console.log('error:'+erro);
                    console.log(erro)
                }
                return(false);
    }

    obterValorcampo(eventFields, campo){
        try{
          for(var field in eventFields){
            if (campo.toUpperCase()==field.toUpperCase()){
              return(eventFields[field]);
            }
          }
        } catch(erro){
          console.log(erro);
        }
        return(null);
      }

    showErrorToast(title, message, variant) {
        const evt = new ShowToastEvent({
          title: title,
          message: message,
          variant: variant,
          mode: 'dismissable'
        });
        this.dispatchEvent(evt);
      }

    handleCancel() {
        console.log("entrei");
        this[NavigationMixin.Navigate]({

            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                actionName: 'view'
            }
        });
    }
}