import { LightningElement, api, track, wire } from 'lwc';
import buscarUrl from '@salesforce/apex/IFramePortalController.buscarUrl';

export default class PortalVendas extends LightningElement {

    buscarURL() {
        buscarUrl({recordId: null, portal: 'Portal de Vendas'})
          .then (result => {
              if(result != null && result != undefined){
                  console.log('result => ' + result);
                  window.open(result);
              }
          }) 
          .catch(error => {
            this.error = error;
            console.log('error => ' + error);
          })       
      }
}