import { LightningElement, track, wire, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import returnKnowledgeArticles from '@salesforce/apex/knowledgeSearchController.getKnowledgeArticles';
import assignArticleToCase from '@salesforce/apex/knowledgeSearchController.assignArticleToCase';
import {ShowToastEvent} from 'lightning/platformShowToastEvent'; // import toast message event .
// Import message service features required for subscribing and the message channel
import {
    subscribe,
    unsubscribe,
    MessageContext
} from 'lightning/messageService';
import ChannelInformation from '@salesforce/messageChannel/channelMotivoSubmotivo__c';


const columns = [
    {label: 'Title', fieldName: 'url', type: 'url',sortable : true,typeAttributes: {label: { fieldName: 'Title' }, target: '_self'}},
    { label: 'Question', fieldName: 'Question__c', type: 'text' },
    { label: 'Created Date', fieldName: 'CreatedDate', type: 'date' },
    { label: 'Associated Cases', fieldName: 'ArticleCaseAttachCount', type: 'text' },
    { label: 'Total View', fieldName: 'ArticleTotalViewCount', type: 'text' },
];
export default class KnowledgeSearch extends NavigationMixin(LightningElement) {
    @track article;  
    @track results;
    @track data = [];
    columns = columns;
    @api recordId;
    @track recordSelected=false;
    caseId;
    error;

    @track actions = [
        { label: 'Anexar Artigo', value: 'anexar'}      
    ];

    @track isModalOpen = false;


    // To pass scope, you must get a message context.
    @wire(MessageContext)
    messageContext;

    // Pass scope to the subscribe() method.
    subscribeToMessageChannel() {
        
        console.log('subscribe');

        if (!this.subscription) {
            this.subscription = subscribe(
                this.messageContext,
                ChannelInformation,
                (message) => this.handleMessage(message)
            );
        }
    }
    
    unsubscribeToMessageChannel() {
        unsubscribe(this.subscription);
        this.subscription = null;
    }

    // Handler for message received by component
    handleMessage(message) {
        
        
        console.log('Log Message :' + JSON.stringify(message));

        this.data = message.listArticles;


    }

    connectedCallback()
    {
        this.caseId=this.recordId;
        this.subscribeToMessageChannel();
    }

    disconnectedCallback() {
        this.unsubscribeToMessageChannel();
    }
    
    handleAction(event) {
        // Get the value of the selected action
        const tileAction = event.detail.action.value;
    }

    opensubtab(event){
        this.isModalOpen = true;        
    }

    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
    }
    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isModalOpen = false;
    }


    @wire(returnKnowledgeArticles, {CaseId : '$recordId'})
    wiredArticles({error, data}) {
        if (data) {
            
            console.log('Total de Artigos :' + data.length);

            this.data = data;
            this.error = undefined;
        }
        if (error) {
            this.error = error;
            this.data = undefined;
        }
    }

   
    assignFaqToCase = event => {
        var el = this.template.querySelector('lightning-datatable');
        var selectedRows = el.getSelectedRows();
        var caseArticles=[];
        for (let i = 0; i < selectedRows.length; i++){
            var caseArticle={
                "CaseId":this.caseId,
                "KnowledgeArticleId":selectedRows[i].KnowledgeArticleId
            };
            caseArticles.push(caseArticle);
        }

        assignArticleToCase({articles: caseArticles})
        .then((data,error) => {
            if (data) {
                this.error = undefined;
                this.dispatchEvent(
                    new ShowToastEvent({
                        message: 'Case Associated Successfully',
                        variant: 'success',
                    }),
                );
            } else if (error) {
                this.error = error;
                console.log('Error:'+ JSON.stringify(error));
                this.dispatchEvent(
                new ShowToastEvent({
                    message: 'Error in Case Assiciation:' + error,
                    variant: 'error'
                }), );
            }
        });

    }
    handleRowSelection = event => {
        var selectedRows=event.detail.selectedRows;
        this.recordSelected=false;
        if(selectedRows.length>0)
        {  
            this.recordSelected=true;
        }
    }
    handleCancel= event => {
        this.recordSelected=false;
    }
}