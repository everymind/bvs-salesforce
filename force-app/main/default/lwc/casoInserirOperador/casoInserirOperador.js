import { api, LightningElement } from 'lwc';
import retornaDadosCaso from '@salesforce/apex/AcoesCasoController.retornaDadosCaso';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


export default class CasoInserirOperador extends LightningElement {
    @api recordId;
    
    spinner = true;
    motivo = '';
    subMotivo = '';
    btnCriarOperador = true;
    codigo = '';
    url = '';

    semAcesso = true;
    mensagem = '';

    showToast(title, message, variant) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(event);
    }

    connectedCallback(){
        retornaDadosCaso({
            idCase: this.recordId
            })
            .then(result => {
                console.log(JSON.stringify(result));

                this.motivo = result.motivo;
                this.subMotivo = result.subMotivo;
                this.codigo = result.codigo;
                this.url = 'https://phpcorp.scpcnet.com.br/ACSPNET/Programas/RightNow/ManOperador/index.php?lk_codig=' + this.codigo;
                console.log('URL: ' + this.url);
                console.log('Motivo: ' + this.motivo);
                console.log('Submotivo: ' + this.subMotivo);
                if (this.motivo == 'ACESSOS' && this.subMotivo == 'CRIAÇÃO CÓDIGO OPERADOR CLIENTE'
                    || this.subMotivo == 'CRIAÇÃO CÓDIGO OPERADOR ENTIDADE'){
                    
                    this.btnCriarOperador = true;
                    this.semAcesso = false;
                    console.log('Entrou!!!!!!!!');
                }
                if (!this.btnCriarOperador){
                    this.semAcesso = true;
                    this.mensagem = 'Sem acesso a esta ferramenta.';
                }
                this.spinner = false;
            })
            .catch(error => {
                this.showToast('Erro', error.body.message, 'error');
                console.log(error);
                this.spinner = false;
            })
    }
}