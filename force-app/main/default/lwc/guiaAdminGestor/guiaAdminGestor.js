import { LightningElement, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import config from '@salesforce/apex/IFramePortalControllerCase.buscarConfiguracao'

const FIELDS = [
    'ConfiguracaoIFrame__mdt.Url__c',
    'ConfiguracaoIFrame__mdt.DeveloperName',
];

export default class GuiaAdminGestor extends LightningElement {
    
    recordId;
    url = '';

    connectedCallback(){
        this.handleClick();
    }
    
    handleClick(){

        config({portal: 'GuiaAdminGestor'})

        .then((idMeta) =>{

            console.log(JSON.stringify(idMeta));
            this.recordId = idMeta.Id;
        })
    }
    // recordId ='m032h0000008aJeAAI';
    
    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    async wiredRecord({ error, data }) {
        if (error) {
            console.log('Erro: ' + error);
        } else if (data) {
            this.url = data.fields.Url__c.value;
            console.log('URL: ' + this.url);
        }
    }
}