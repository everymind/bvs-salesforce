import { LightningElement, api, track, wire} from 'lwc';
import PostQuestionsKBA from '@salesforce/apex/KBAController.PostQuestionsKBA';
import PostAnswersKBA from '@salesforce/apex/KBAController.PostAnswersKBA';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class validateKba extends LightningElement {
    
    @api cpf;
    
    @track isModalOpen = true;
    @track paragrafo = "Você selecionou uma categoria que habilita o questionário do KBA, você não poderá fechar o caso até o consumidor responder todas as perguntas. Deseja Continuar?"
    @track questoes
    @track exibirPerguntas = false;
    @track titulo = "Aviso";
    @track exibaAlternativas = false;
    @track options;
    @track respostas = [];
    @track questionario;
    @track contador = 0;
    @track finaliza = "Próxima Pergunta";
    @track requisicaoFinal = {
        DocInfo: [],
        EstimatedInf: [],
        TicketId: null,
        ResultCode: 0,
        ResultMessage: null,
        Questions: []
    };

     errorCodeCadastro = [-101,-112, -116, -301, -318];

    openModal() {
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
         
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        console.log('teste' + this.requisicaoFinal);
        
        console.log('teste' + this.requisicaoFinal);
        //this.addEventListener('retornoKba',(e) => console.log(e.detail.value))
        this.isModalOpen = false;
        this.dispatchEvent(new CustomEvent('retornokba', {detail: this.requisicaoFinal}));
       
    }


    submitDetails() {
        PostQuestionsKBA ({
            CPF: this.cpf
        }).then((resultado)=> {     
            console.log("this is result" + JSON.stringify( resultado));
            if(resultado.ResultCode == 0)
            {
                this.exibirPerguntas = false;
                this.registros = resultado;
                this.paragrafo = "     " +resultado.Questions[0].Question;
                this.exibaAlternativas = true;
                this.questionario = true;
                this.titulo = "KBA";
                console.log("this is result answers" + JSON.stringify(resultado.Questions[0].Answers));
                console.log("this is result answers" + JSON.stringify(this.registros.Questions[0].Answers));
                this.options = [];
                for(let i = 0; i < this.registros.Questions[0].Answers.length; i++)
                {
                    this.options.push({
                    label: this.registros.Questions[0].Answers[i],
                    value: this.registros.Questions[0].Answers[i]
                    })
                }
            }
            else if (this.errorCodeCadastro.includes(resultado.ResultCode))
            {
                this.paragrafo = "Verificamos que ainda não há informações suficientes para comprovar sua identidade. Por favor, envie sua solicitação através do formulário de atendimento do site ou APP Consumidor Positivo, preenchendo todos os dados solicitados: Nome completo, e-mail, UF, Cidade, telefone com DDD e no campo assunto selecionar “Informações Cadastro Positivo“. Anexe a foto do seu documento original (frente e verso: RG, CNH, RNE ou RNM – Registro Nacional Migratório) e uma selfie segurando o mesmo documento";
                this.questionario = true;
                this.finaliza = "Concluir";
            }
            else if(resultado.ResultCode == -102)
            {
                this.paragrafo = "Que pena! O sistema não está disponível no momento";
                this.questionario = true;
                this.finaliza = "Concluir";
            }
            else
            {
                this.showMessageToast('Falha!', resultado.ResultMessage, 'error')
                this.closeModal();
            }
            
            
              
          })
        }

        proximaPergunta(event)
        {
            console.log("Entrei" + this.contador);
           // console.log("Entrei" + this.value[this.contador]);
            if(this.finaliza != "Concluir")
            {
                if(this.respostas[this.contador] != null)
                {

                    if(this.finaliza == "Próxima Pergunta")
                    {
                        if(this.contador < (this.registros.Questions.length - 1))
                        {
                            this.contador = this.contador + 1;
                            this.paragrafo = "     " + this.registros.Questions[this.contador].Question;
                            this.options = [];
                            for(let i = 0; i < this.registros.Questions[this.contador].Answers.length; i++)
                            {
                                this.options.push({
                                    label: this.registros.Questions[this.contador].Answers[i],
                                    value: this.registros.Questions[this.contador].Answers[i]
                                })
                            }
                            this.contador == (this.registros.Questions.length - 1) ? this.finaliza = "Enviar" : this.finaliza == "Próxima Pergunta";
                        }
                    }
                    else if (this.finaliza == "Enviar")
                    {
                        console.log("Ticket" + this.registros.TicketId);
                        PostAnswersKBA ({
                            ticket: this.registros.TicketId,
                            respostas: this.respostas
                        }).then((resultado)=> {     
                            console.log("this is result" + JSON.stringify( resultado));
                            this.requisicaoFinal = resultado;

                            if(resultado.ResultCode == 1)
                            {
                                this.showMessageToast('Sucesso!', 'KBA validado com sucesso!', 'success');
                                this.closeModal();
                            }
                            else if(resultado.ResultCode == -1 || resultado.ResultCode == -307)
                            {
                                this.exibaAlternativas = false;
                                this.titulo = "Aviso";
                                this.paragrafo = "Algumas das respostas não batem com nossos registros. Para garantir sua segurança não poderemos confirmar sua identidade agora. Você terá uma nova chance em 24 horas."
                                this.finaliza = "Ok";
                                
                            }
                            else
                            {
                                this.showMessageToast('Falha!', resultado.ResultMessage, 'error');
                                this.closeModal();
                            }
                        })
                    }
                    else if (this.finaliza == "Ok")
                    {
                        this.showMessageToast('Falha!', "KBA inválido", 'error')
                        this.closeModal();
                    }
                }
                else
                {
                    this.showMessageToast('Erro', 'Valor não selecionado', 'error');
                }
            }
            else
            {
                this.closeModal();
            }
        }
            

        handleChange(event) {
            
            console.log("vetor de respostas" + JSON.stringify(this.respostas));
            this.respostas[this.contador] = event.target.value;
          }

          showMessageToast(title, message, variant) {
            const evt = new ShowToastEvent({
              title: title,
              message: message,
              variant: variant,
              mode: 'dismissable'
            });
            this.dispatchEvent(evt);
          }

          
         

          
          
}