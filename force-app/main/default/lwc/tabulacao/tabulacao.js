import { LightningElement, api, wire, track} from 'lwc';
import { getRecord, getRecordNotifyChange  } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import salvarCaso from '@salesforce/apex/CaseController.salvarCaso';
import getCase from '@salesforce/apex/CaseController.getCase';

import MOTIVO from '@salesforce/schema/Case.MotivoTxt__c';
import SUBMOTIVO from '@salesforce/schema/Case.SubMotivoTxt__c';
import MEIO_ACESSO from '@salesforce/schema/Case.MeioAcessoTxt__c';
import SUBMOTIVO2 from '@salesforce/schema/Case.SubMotivo2Txt__c';
import TIPO_FIELD from '@salesforce/schema/Case.Type';
import RECORDTYPE_DEVELOPER_NAME from '@salesforce/schema/Case.RecordType.DeveloperName';
import STATUS from '@salesforce/schema/Case.Status';

import QUAL_REMETENTE_DESTINATARIO_EMAIL from '@salesforce/schema/Case.QualRemetenteDestinatarioEmail__c';
import QUAL_DATA_HORA_EMAIL from '@salesforce/schema/Case.QualDataHoraEmail__c';
import A_QUEM_PERTENCE_ESTE_EMAIL from '@salesforce/schema/Case.AQuemPertenceEsteEmail__c';

import QUAL_FOI_ARQUIVO_ATIVIDADE_BLOQUEADA from '@salesforce/schema/Case.QualFoiArquivoAtividadeBloqueada__c';

import EXISTE_DOCUMENTO_CONSULTA_BVS from '@salesforce/schema/Case.ExisteDocumentoConsultaBVS__c';
import INFORME_DADOS_AUTOR_FRAUDE from '@salesforce/schema/Case.InformeDadosAutorFraude__c';
import DATA_OCORRENCIA_FRAUDE from '@salesforce/schema/Case.DataOcorrenciaFraude__c';
import DATA_DENUNCIA from '@salesforce/schema/Case.DataDenuncia__c';
import CPF_VITIMA from '@salesforce/schema/Case.CPFVitima__c';
import NOME_VITIMA from '@salesforce/schema/Case.NomeVitima__c';
import NOME_EMPRESA_ENVOLVIDA_FRAUDE from '@salesforce/schema/Case.NomeEmpresaEnvolvidaFraude__c';
import DOCUMENTO_CONSULTA from '@salesforce/schema/Case.DocumentoConsulta__c';

import QUAL_URL_PARA_REMOCAO from '@salesforce/schema/Case.QualURLParaRemocao__c';
import JUSTIFICATIVA from '@salesforce/schema/Case.Justificativa__c';

import QUAL_URL_PHISHING from '@salesforce/schema/Case.QualURLPhishing__c';

import A_QUEM_PERTENCE_SOLICITACAO from '@salesforce/schema/Case.AQuemPertenceSolicitacao__c';
import OUTROS from '@salesforce/schema/Case.Outros__c';

import ATUALMENTE_UTILIZA_VDI from '@salesforce/schema/Case.AtualmenteUtilizaVDI__c';

import QUAL_SITE_VOCE_DESEJA_LIBERACAO from '@salesforce/schema/Case.QualSiteVoceDesejaLiberacao__c';
import A_QUEM_PERTENCE_ESTE_SITE from '@salesforce/schema/Case.AQuemPertenceEsteSite__c';

import CONTA from '@salesforce/schema/Case.AccountId';
import PRODUCT_ID from '@salesforce/schema/Case.ProductId';
import QUANTIDADE_CONSULTAS from '@salesforce/schema/Case.QuantidadeConsultas__c';
import DATA_INICIO from '@salesforce/schema/Case.DataInicio__c';
import DATA_FIM from '@salesforce/schema/Case.Datafim__c';
import NOME_USUARIO from '@salesforce/schema/Case.NomeUsuario__c';
import CPF from '@salesforce/schema/Case.CPF__c';
import EMAIL_USUARIO from '@salesforce/schema/Case.EmailUsuario__c';
import PARENID from '@salesforce/schema/Case.ParentId';

import NOME_OPERADOR from '@salesforce/schema/Case.NomeOperador__c';
import DATA_NASCIMENTO from '@salesforce/schema/Case.DataNascimento__c';
import EMAIL_OPERADOR from '@salesforce/schema/Case.EmailOperador__c';
import MOTIVO_REJEICAO from '@salesforce/schema/Case.MotivoRejeicao__c';

import CODIGO_DE_CLIENTE from '@salesforce/schema/Case.CodigoDeCliente__c';
import CASO_RESPOSTA_NEGATIVA from '@salesforce/schema/Case.CasoRespostaNegativa__c';


import PID_APROVADO from '@salesforce/schema/Case.PIDAprovado__c';
import ACC_BLOQUEIO_CASO from '@salesforce/schema/Case.Contact.BloquearCriacaoCasoPID__c';
import ACC_DATA_LIMITE_BLOQUEIO from '@salesforce/schema/Case.Account.DataLimiteBloqueio__c';

import returnMotivoList from '@salesforce/apex/TabulacaoController.returnMotivoList';
import returnSubMotivoList from '@salesforce/apex/TabulacaoController.returnSubMotivoList';
import returnMeioDeAcessoList from '@salesforce/apex/TabulacaoController.returnMeioDeAcessoList';
import returnSubMotivo2List from '@salesforce/apex/TabulacaoController.returnSubMotivo2List';
import retornaObservacoes from '@salesforce/apex/TabulacaoController.retornaObservacoes';
import returnPID from '@salesforce/apex/TabulacaoController.returnPID';
import obterTempoDeBloqueioCasoPID from '@salesforce/apex/MetadataDAO.ObterTempoDeBloqueioCasoPID'
import updateCaseAssunto from '@salesforce/apex/TabulacaoController.updateCaseAssunto';

import { publish, MessageContext } from 'lightning/messageService';
import ChannelInformation from '@salesforce/messageChannel/channelMotivoSubmotivo__c';
import TravaTabCasoFilho__c from '@salesforce/schema/Case.TravaTabCasoFilho__c';
import KBA_Aprovado__c from '@salesforce/schema/Case.KBA_Aprovado__c'
import buscaCPF from '@salesforce/apex/KBAController.buscaCPF';
import buscaTempoKba from '@salesforce/apex/KBAController.getBuscarTempoKBA';
import ContactId from '@salesforce/schema/Case.ContactId';
import returnCodigoDeClienteList from '@salesforce/apex/TabulacaoController.returnCodigoDeClienteList';



const FIELDS = [CODIGO_DE_CLIENTE,CONTA,MOTIVO, SUBMOTIVO, MEIO_ACESSO, TIPO_FIELD, RECORDTYPE_DEVELOPER_NAME, SUBMOTIVO2, STATUS, MOTIVO_REJEICAO, PID_APROVADO, ACC_BLOQUEIO_CASO,ACC_DATA_LIMITE_BLOQUEIO, PARENID, TravaTabCasoFilho__c,PRODUCT_ID,ContactId,KBA_Aprovado__c,CASO_RESPOSTA_NEGATIVA];
export default class Tabulacao extends LightningElement {
  @api recordId;

  @track lstMotivo;
  @track lstSubMotivo;
  @track lstMeioAcesso;
  @track lstSubMotivo2;
  @track lstCodigosDeCliente = [];

  @track valueCodigoDeCliente = '';
  @track disabledSubMotivo = true;
  @track disabledMeioDeAcesso = true;
  @track disabledSubMotivo2 = true;
  @track disabledMotivo = true;
  @track disabledButton = false;
  @track leituraMeioDeAcesso = true;
  @track leituraCodigoDeCliente = true;

  @track leituraSubMotivo2 = true;
  @track HasCodigoDeCliente = false;
  @track motivo;
  @track subMotivo;
  @track meioDeAcesso;
  @track subMotivo2;
  @track tipo;
  @track recordDeveloperName;
  @track status;
  @track TempoKba;
  @track isVisible = true;
  @track error;
  @track retornoKba = '';

  @track isITSM;
  @track observacoes='';
  @track kbaAprovado ='';
  @track modoVisualizacao='readonly';

  @track acessarPlataforma=false;
  CPF_KBA;
  bloquearCasoPIDInvalido = false;
  dataLimiteBloqueioCasoPIDInvalido;
  configuracaoTempoBloqueioCasoPID = '0';
  caseContact;
  showKBA = false;

  FIELDS_CUSTOM;
  
  mostra = false;
  origem;


  @wire(MessageContext)
  messageContext;


  @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
  wiredRecord({ error, data }) {
    console.log('RecordId: ' + this.recordId)
    if (error) {
      this.showErrorToast('Erro ao buscar valores do objeto tabulação', error.body.message, 'error');
    } else if (data) {
      this.registro=data;

      buscaCPF({ 
        id: this.registro.fields.ContactId.value
      }).then((resultado)=> {     
        console.log("this is result" + resultado);
        this.CPF_KBA = resultado
      })
      buscaTempoKba({ 
        id: this.registro.fields.ContactId.value
      }).then((resultado)=> {     
        console.log("this is result" + resultado);
        this.TempoKba = resultado
      })
      console.log("this is");
      console.log('Codigo do Cliente :' + this.registro.fields);
      this.valueCodigoDeCliente = this.registro.fields.CodigoDeCliente__c.value;
      this.caseContact = this.registro.fields.ContactId.value;

      //Verifica se existe preenchimento de codigo de cliente para pesquisar as opcoes
      if(this.valueCodigoDeCliente != undefined){

        returnCodigoDeClienteList({ 
          codigoConta: this.registro.fields.AccountId.value
        }).then(codigoDeClienteList => {
        
        let optionsCodigos = [{ label: '', value: '' }];

        //criando array de valores  
        codigoDeClienteList.forEach(codigoC => {
          optionsCodigos.push({ label: codigoC.Name.toString(), value: codigoC.Id });
        });

        this.lstCodigosDeCliente = optionsCodigos;
      
      }).catch(error => {
        console.log(error);
        this.showErrorToast('Erro ao buscar codigos do cliente', error.body.message, 'error');
      });

      }

      
      this.mostrarEdit();
      this.mostraCodigo();
      this.inicializarTela(data);
    }  
  }

  @wire(obterTempoDeBloqueioCasoPID,{})
  wiredObterTempoDeBloqueioCasoPID({error,data}) {    
    if (error) {
      this.showErrorToast('Erro ao buscar o tempo de bloqueio para a criação de novos casos com motivo PID.', error.body.message, 'error');
    } else if (data) {
      
      this.configuracaoTempoBloqueioCasoPID = data;
    }  

  }


  inicializarTela(){
    this.recordDeveloperName = this.registro.fields.RecordType.value.fields.DeveloperName.value;
    this.motivo = this.registro.fields.MotivoTxt__c.value;
    this.subMotivo = this.registro.fields.SubMotivoTxt__c.value;
    this.meioDeAcesso = this.registro.fields.MeioAcessoTxt__c.value;
    this.subMotivo2 = this.registro.fields.SubMotivo2Txt__c.value;
    this.atualizarPID();
    this.lstMotivo = [];
     console.log('this.registro.fields' +  this.registro.fields);
    if(!this.registro.fields.Account.value === 'null' || !this.registro.fields.Account.value === undefined)
    {
        this.dataLimiteBloqueioCasoPIDInvalido = this.registro.fields.Account.value.fields.DataLimiteBloqueio__c.value;
    }
    if(!this.registro.fields.Contact.value === 'null' || !this.registro.fields.Contact.value === undefined)
    {
      this.bloquearCasoPIDInvalido = this.registro.fields.Contact.value.fields.BloquearCriacaoCasoPID__c.value;
    }
    
    this.kbaAprovado = this.registro.fields.KBA_Aprovado__c.value;

    console.log('this.recordDeveloperName : ' + this.recordDeveloperName )
    

 

    returnMotivoList({ 
      recordDeveloperName: this.recordDeveloperName 
    }).then(motivoList => {
      let options = [{ label: '', value: '' }];
      motivoList.forEach(motivo => {
        options.push({ label: motivo.MotivoTxt__c, value: motivo.MotivoTxt__c });
      });
      this.lstMotivo = options;
      this.aoMudarMotivo();
    }).catch(error => {
      console.log(error);
      this.showErrorToast('Erro ao buscar valores para Motivo', error.body.message, 'error');
    });

    this.disabledSubMotivo = true;
    this.disabledMeioDeAcesso = true;
    this.disabledSubMotivo2 = true;
    this.tipo = this.registro.fields.Type.value;
    if (this.recordDeveloperName == 'ITSM') {
      this.isITSM = true;

    } else {
      this.isITSM = false;
    }
    this.disabledMotivo=!(this.registro.fields.Status.value=='AGUARDANDO CLASSIFICAÇÃO') 
    && !((this.recordDeveloperName=='CSMPosVendas' && this.registro.fields.Status.value=='AGUARDANDO TRATAMENTO')
    || (this.recordDeveloperName!='CSMPosVendas' && (!(this.motivo) || this.motivo=='' || this.motivo=='null'))) 
    && !(this.registro.fields.Status.value=='REJEITADO' && this.registro.fields.MotivoRejeicao__c.value=='DIRECIONAMENTO INCORRETO')
    && !(this.registro.fields.Type.value=='PREOUVIDORIA' && this.registro.fields.Status.value=='NOVO' && this.registro.fields.ParentId.value == 'null')
    && !(this.registro.fields.TravaTabCasoFilho__c.value);
    //Regra realizada para Caso Criado via Convite de Pesquisa de Satisfação, flag de criação de Caso Filho.
    if(this.registro.fields.CasoRespostaNegativa__c.value === true)
    {
      this.disabledMotivo = true;
    }
    console.log('DisabledMotivo' + this.disabledMotivo);
   
    if (this.disabledMotivo){
      this.modoVisualizacao='readonly';  
    } else {
      this.modoVisualizacao='view';
      console.log('Teste view');
    }
    this.disabledButton = true;

    this.mostrarCamposTabulacao();
  }

  toggleSection() {
    this.isVisible = !this.isVisible; 
  }

  aoMudarMotivo(){
    this.lstSubMotivo = [{ label: '', value: '' }];
    this.disabledSubMotivo = true;
    this.lstMeioAcesso = [{ label: '', value: '' }];
    this.lstSubMotivo2 = [{ label: '', value: '' }];
    this.disabledMeioDeAcesso = true;
    this.disabledSubMotivo2 = true;
    if(this.motivo && this.motivo!=''){
      if (this.disabledMotivo){
        this.modoVisualizacao='readonly';  
      } else {
        this.modoVisualizacao='edit';
      }
      returnSubMotivoList({ 
        recordDeveloperName: this.recordDeveloperName,
        motivo: this.motivo
      }).then(subMotivoList => {
        let options = [{ label: '', value: '' }];
        subMotivoList.forEach(subMotivo => {
          if (subMotivo.SubMotivoTxt__c && subMotivo.SubMotivoTxt__c.trim()!=''){
            options.push({ label: subMotivo.SubMotivoTxt__c, value: subMotivo.SubMotivoTxt__c });
          }
        });
        this.disabledSubMotivo = this.disabledMotivo || !(options && options.length>1);
        this.lstSubMotivo = options;
        this.aoMudarSubMotivo();
      })
      .catch(error => {
        console.log(error);
        this.showErrorToast('Erro ao buscar valores para o SubMotivo', error.body.message, 'error');
      });
    }
  }

  handleChangeMotivo(event){
    this.motivo = event.detail.value;
    this.aoMudarMotivo();
  }

  aoMudarSubMotivo(){

    console.log('aoMudarSubMotivo: ', 
    'this.recordDeveloperName:' + this.recordDeveloperName,
    'this.getMotivo:' + this.getMotivo,
    'this.getSubMotivo: ' + this.getSubMotivo
    )

   
    returnMeioDeAcessoList({
      recordDeveloperName: this.recordDeveloperName,
      motivo: this.getMotivo,
      subMotivo: this.getSubMotivo,
    }).then(meioDeAcessoList => {
      console.log(meioDeAcessoList)
      let options = [{ label: '', value: '' }];
      meioDeAcessoList.forEach(meioDeAcesso => {
        if (meioDeAcesso.MeioAcessoTxt__c && meioDeAcesso.MeioAcessoTxt__c.trim()!=''){
          options.push({ label: meioDeAcesso.MeioAcessoTxt__c, value: meioDeAcesso.MeioAcessoTxt__c });
        }
      });
      this.lstMeioAcesso = options;
      if (options.length>1){
        this.disabledMeioDeAcesso = false;
        if (this.disabledMotivo){
          this.leituraMeioDeAcesso = true;
          this.leituraCodigoDeCliente = true; 
        } else {
          this.leituraMeioDeAcesso = false;
          this.leituraCodigoDeCliente = false;
        }
    } else {
        this.disabledMeioDeAcesso = true;
      }
      this.disabledButton = this.disabledMotivo;
      this.mostrarCamposTabulacao();
    })
    .catch(error => {
      console.log(error);
      this.showErrorToast('Erro ao buscar valores para o Meio de acesso', error.body.message, 'error');
    });

    returnSubMotivo2List({
      recordDeveloperName: this.recordDeveloperName,
      motivo: this.getMotivo,
      subMotivo: this.getSubMotivo,
    }).then(subMotivo2List => {
      let options = [{ label: '', value: '' }];
      subMotivo2List.forEach(subMotivo2 => {
        if (subMotivo2.SubMotivo2Txt__c && subMotivo2.SubMotivo2Txt__c.trim()!=''){
          options.push({ label: subMotivo2.SubMotivo2Txt__c, value: subMotivo2.SubMotivo2Txt__c });
        }
      });
      this.lstSubMotivo2 = options;
      if (options.length>1){
        this.disabledSubMotivo2 = false;
        if (this.disabledMotivo){
          this.leituraSubMotivo2 = true;
        } else {
          this.leituraSubMotivo2 = false;
        }
      } else {
        this.disabledSubMotivo2 = true;
        this.subMotivo2 = '';
      }
      this.disabledButton = this.disabledMotivo;
      this.mostrarCamposTabulacao();
    })
    .catch(error => {
      console.log(error);
      this.showErrorToast('Erro ao buscar valores para o Meio de acesso', error.body.message, 'error');
    });

    if (this.registro && this.registro.fields && this.registro.fields.PIDAprovado__c && !this.registro.fields.PIDAprovado__c.value){

      console.log(`Buscar Submotivo: ${this.getMotivo} - Submotivo: ${this.getSubMotivo}`  )
      returnPID({
        caseId: this.recordId,
        motivo: this.getMotivo,
        subMotivo: this.getSubMotivo,
      }).then(pid => {
        console.log('carregando pid');
        this.atualizarPID(pid);
          

        if(this.mostrarPID)
        {
          //Checa de conta esta bloqueada
          if (this.bloquearCasoPIDInvalido && !(this.disabledMotivo)) {
            this.showErrorToast('Caso', 'Contato bloqueado para essa solicitação por falta de identificação positiva, orientar cliente confirmar os dados solicitados anteriormente e retornar após este período', 'error');
            this.motivo = null;
            this.subMotivo = null;
            this.mostrarPID = false;
            this.mostrarCamposTabulacao();
            
            return false;        
          }
        }

      })
      .catch(error => {
        console.log(error);
        this.showErrorToast('Erro ao buscar dados de PID', error.body.message, 'error');
      });
    }
  }
  
  handleChangeCodigoDeCliente(event) {
    this.valueCodigoDeCliente = event.detail.value;
  }

  mostraCodigo(){
    console.log('mostraCodigo()');
    returnCodigoDeClienteList({ 
      codigoConta: this.registro.fields.AccountId.value
    }).then(codigoDeClienteList => {
    
      let optionsCodigos = [{ label: '', value: '' }];

      //criando array de valores  
      codigoDeClienteList.forEach(codigoC => {
        optionsCodigos.push({ label: codigoC.Name.toString(), value: codigoC.Id });
      });

      this.lstCodigosDeCliente = optionsCodigos;
  
    }).catch(error => {
      console.log(error);
      this.showErrorToast('Erro ao buscar codigos do cliente', error.body.message, 'error');
    });
  }

  handleChangeSubMotivo(event){
    this.subMotivo = event.detail.value;



    //Atualiza ASSUNTO para refresh em BASE DE CONHECIMENTO
    updateCaseAssunto({caseId : this.recordId , motivo : this.getMotivo, subMotivo : this.getSubMotivo
    }).then(retornoUpdate => {

            console.log('Atualiza case : --------------- :' + this.recordId);

      const payload = { listArticles: retornoUpdate };
      publish(this.messageContext, ChannelInformation, payload);

      // Refresh all records updated by Apex (for Salesforce cache)
     // updateRecord({ fields: { Id: this.recordId } }); 

     returnCodigoDeClienteList({ 
        codigoConta: this.registro.fields.AccountId.value
      }).then(codigoDeClienteList => {
      
      console.log('------------------------ Codigos --------------- :' + codigoDeClienteList.length );
      let optionsCodigos = [{ label: '', value: '' }];

      //criando array de valores  
      codigoDeClienteList.forEach(codigoC => {
        optionsCodigos.push({ label: codigoC.Name.toString(), value: codigoC.Id });
      });

      this.lstCodigosDeCliente = optionsCodigos;
     
    }).catch(error => {
      console.log(error);
      this.showErrorToast('Erro ao buscar codigos do cliente', error.body.message, 'error');
    });



    })
    .catch(error => {
      console.log(error);
      this.showErrorToast('Erro ao atualiza o assunto do caso', error.body.message, 'error');
    });



    this.aoMudarSubMotivo();
  }

  mostrarCamposTabulacao(){
    if (this.motivo!='' && this.subMotivo!=''){
      retornaObservacoes({
        motivo: this.getMotivo,
        subMotivo : this.getSubMotivo,
        meioDeAcesso: this.getMeioDeAcesso,
        recordDeveloperName: this.recordDeveloperName
      }).then(result => {
        this.observacoes=result;
      }).catch(error => {
        console.log(error);
          this.showErrorToast('Caso - Observações', error, 'error');
      });
    }
    this.showKBA = false;
    this.FIELDS_CUSTOM= [];

    if ((this.motivo =="DÚVIDAS E INFORMAÇÕES" 
          && (this.subMotivo =="OCI_CONSULTA CONTABILIDADE" 
            || this.subMotivo =="OCI_CONSULTA JURIDICA")
        ) ||
        (this.motivo =="DOCUMENTOS E RELATÓRIOS" 
          && (this.subMotivo =="OCI_SOLICITAR ATUALIZAÇÃO DE DOCUMENTOS DO PORTAL DE PROCESSOS" 
            || this.subMotivo =="OCI_SOLICITAÇÃO DE DOCUMENTO DE CLIENTES (CONTRATO DE CLIENTES CUSTOMI")
        ) ||
        (this.motivo =="FATURAMENTO" 
          && (this.subMotivo =="OCI_ALTERAÇÃO DE CONDIÇÃO DE PAGAMENTO E" 
            || this.subMotivo =="OCI_ALTERAÇÃO DE FORMA DE PAGAMENTO"
            || this.subMotivo =="OCI_ACESSO AOS FORMULÁRIOS NO OIC"
            || this.subMotivo =="OCI_CORREÇÃO DE FATURAMENTO"
            || this.subMotivo =="OCI_SOLICITAÇÃO DE EXCEÇÃO DE COBRANÇA"
            || this.subMotivo =="OCI_GUICHE DE SERVIÇO"
            || this.subMotivo =="OCI_SC DESCONTO DE CAMPANHA"
            || this.subMotivo =="OCI_SD DESCONTO"
            || this.subMotivo =="OCI_SOLICITAÇÃO DE COBRANÇA"
            || this.subMotivo =="OCI_SOLICITAÇÃO DE ALTERAÇÃO DE FORMA DE PAGAMENTO"
            
            )
        )){
          this.acessarPlataforma=true;
    } else {
      this.acessarPlataforma=false;
    }

    if (this.recordDeveloperName == 'CSMPosVendas'){
      if(this.motivo == 'ACESSOS'
        && this.subMotivo == 'CRIAÇÃO DE CÓDIGO TESTE'){
        this.FIELDS_CUSTOM = [PRODUCT_ID, QUANTIDADE_CONSULTAS, DATA_INICIO, DATA_FIM, NOME_USUARIO, CPF, EMAIL_USUARIO];
      } else if (this.motivo == 'ACESSOS'
      && this.subMotivo == 'CRIAÇÃO LOGIN MKT SERVICES'){
        this.FIELDS_CUSTOM = [NOME_OPERADOR, CPF, DATA_NASCIMENTO, EMAIL_OPERADOR];
      }     
    } else if (this.recordDeveloperName == 'ITSM' || this.recordDeveloperName=='CSMAtendimento'){
      if (this.motivo == 'ALERTA'
        && this.subMotivo == 'TENTATIVA DE GOLPE'){
        this.FIELDS_CUSTOM = [EXISTE_DOCUMENTO_CONSULTA_BVS, INFORME_DADOS_AUTOR_FRAUDE, DATA_OCORRENCIA_FRAUDE, DATA_DENUNCIA, CPF_VITIMA, NOME_VITIMA, NOME_EMPRESA_ENVOLVIDA_FRAUDE, DOCUMENTO_CONSULTA];
      } else if (this.recordDeveloperName == 'ITSM' &&
        this.motivo == 'SEGURANÇA DA INFORMAÇÃO'){
        if(this.subMotivo == 'LIBERAÇÃO DE E-MAIL/SPAM ENVIADO/RECEBIDO'
          && this.meioDeAcesso == 'CONFIGURAÇÃO NO ANTISPAM'){
          this.FIELDS_CUSTOM = [QUAL_REMETENTE_DESTINATARIO_EMAIL, QUAL_DATA_HORA_EMAIL, A_QUEM_PERTENCE_ESTE_EMAIL];
        }else if (this.subMotivo == 'AVALIAÇÃO DE BLOQUEIOS DO ANTIVÍRUS'
            && this.meioDeAcesso == 'ANÁLISE NO ANTIVIRUS'){
          this.FIELDS_CUSTOM = [QUAL_FOI_ARQUIVO_ATIVIDADE_BLOQUEADA];
        }else if (this.subMotivo == 'REMOÇÃO DE CONTEÚDO DA INTERNET'
            && this.meioDeAcesso == 'AXUR - PEDIDO DE TAKEDOWN'){
          this.FIELDS_CUSTOM = [QUAL_URL_PARA_REMOCAO, JUSTIFICATIVA];
        }else if (this.subMotivo == 'DENÚNCIA DE PHISHING'
            && this.meioDeAcesso == 'AVALIAÇÃO DE PHISHING EM NOME DA BVS PARA CLIENTES'){
          this.FIELDS_CUSTOM = [QUAL_URL_PHISHING];
        }else if (this.subMotivo == 'AVALIAÇÃO DE CONFIGURAÇÕES DO WAF'
            && this.meioDeAcesso == 'ANÁLISE NO WAF'){
          this.FIELDS_CUSTOM = [JUSTIFICATIVA, A_QUEM_PERTENCE_SOLICITACAO, OUTROS];
        }else if (this.subMotivo == 'AUTORIZAÇÃO DE BYOD'
            && this.meioDeAcesso == 'ANÁLISE PARA INCLUSÃO NO GRUPO BYOD'){
          this.FIELDS_CUSTOM = [ATUALMENTE_UTILIZA_VDI, JUSTIFICATIVA];
        }else if (this.subMotivo == 'LIBERAÇÃO DE SITES'
            && this.meioDeAcesso == 'CONFIGURAÇÃO NO PROXY'){
          this.FIELDS_CUSTOM = [QUAL_SITE_VOCE_DESEJA_LIBERACAO, JUSTIFICATIVA, A_QUEM_PERTENCE_ESTE_SITE, OUTROS];
        }
      }

      if (this.recordDeveloperName=='CSMAtendimento'){
        if (
          (this.motivo == 'CONTATO' && this.subMotivo == 'TELEFONE DE CONTATO/ENDEREÇO - ENTIDADES')
          || (this.motivo == 'MARKETING' && this.subMotivo == 'MATERIAL PUBLICITÁRIO')
          || (this.motivo == 'MARKETING' && this.subMotivo == 'PATROCÍNIOS E EVENTOS')
          || (this.motivo == 'MARKETING' && this.subMotivo == 'SENHA FALE COM A REDE')
          || (this.motivo == 'ALERTA' && this.subMotivo == 'TENTATIVA DE GOLPE')
          || (this.motivo == 'DADOS' && this.subMotivo == 'ATUALIZAÇÃO PARTICIPANTES/SÓCIOS')
          || (this.motivo == 'EXCLUSÃO CONSULTAS ANTERIORES' && this.subMotivo == 'EXCLUSÃO CONSULTAS CLIENTE/ENTIDADE')
          || (this.motivo =='ACESSOS' && (
               this.subMotivo == 'BLOQUEIO/CANCELAMENTO CÓD. SECUNDÁRIO'
            || this.subMotivo == 'BLOQUEIO/CANCELAMENTO CÓD. OPERADOR'
            || this.subMotivo == 'CADASTRO DE IP - CLIENTES'
            || this.subMotivo == 'CRIAÇÃO CÓDIGO OPERADOR CLIENTE'
            || this.subMotivo == 'CRIAÇÃO CÓDIGO OPERADOR ENTIDADE'
            || this.subMotivo == 'CRIAÇÃO DE CÓDIGO SECUNDÁRIO'
            || this.subMotivo == 'DEMANDAS LOTE - CÓDIGO SECUNDÁRIO'
            || this.subMotivo == 'DEMANDAS LOTE - OPERADOR AC'
            || this.subMotivo == 'DEMANDAS LOTE - LIBERAÇÃO PRODUTOS'
            || this.subMotivo == 'DEMANDAS LOTE - SECUNDÁRIO/OPERADOR AC'
            || this.subMotivo == 'LIBERAÇÃO CONSULTAS WEB - OPERADOR AC'
            || this.subMotivo == 'LIBERAÇÃO MANUTENÇÃO WEB /OPERADOR AC'
            || this.subMotivo == 'LIBERAÇÃO SIA - VISUALIZAÇÃO (MENU)'
            || this.subMotivo == 'LIBERAÇÃO BOLETOS E FATURAS'
            || this.subMotivo == 'LIBERAÇÃO ALTERAÇÃO RATING/PERFIL DO GC'
            || this.subMotivo == 'LIBERAÇÃO ALTERAÇÃO HORA/DIA DE ACESSO'
            || this.subMotivo == 'REATIVAÇÃO DE CÓDIGO'
            || this.subMotivo == 'REATIVAÇÃO DE CÓD. BLOQUEADO POR INADIMPLÊNCIA'
            || this.subMotivo == 'REDE VERDE AMARELA LIBERAÇÃO PAINEL DE RESULTADOS'
            || this.subMotivo == 'SENHA CÓD. OPERADOR - ORIENTAÇÃO PORTAL'
            || this.subMotivo == 'SENHA CÓD. OPERADOR - EXCESSO DE TENTATIVAS'
            || this.subMotivo == 'SENHA CÓD. OPERADOR - E-MAIL DIFERENTE'
            || this.subMotivo == 'SENHA CÓD. OPERADOR - ERRO NO PORTAL'
            || this.subMotivo == 'SENHA CÓD. OPERADOR - NÃO RECEBEU E-MAIL'
            || this.subMotivo == 'SENHA CÓD. OPERADOR - NÃO RECEBEU ACESSO MIGRAÇÃO'
            || this.subMotivo == 'SENHA CÓD. OPERADOR - REJEITADO/DEVOLVIDO'
            || this.subMotivo == 'SENHA CÓDIGO DE 8'
            || this.subMotivo == 'SENHA EAD PORTAL REDE VERDE AMARELA'
            || this.subMotivo == 'SENHA PORTAL REDE VERDE AMARELA'
            || this.subMotivo == 'SENHA FALE COM A REDE'
            || this.subMotivo == 'USO DA MARCA'
            ))
          || (this.motivo == 'ACIONAMENTO COMERCIAL' && (
            this.subMotivo == 'ATENDIMENTO CLIENTE VIP'
            || this.subMotivo == 'RETORNO AO CLIENTE - PROP. DA CONTA'
            || this.subMotivo == 'RETORNO AO CLIENTE CLASS - INTENÇÃO CANCELAMENTO'
            || this.subMotivo == 'RETORNO AO CLIENTE CLASS - AUMENTO PACOTE'
            || this.subMotivo == 'RETORNO AO CLIENTE CLASS - REDUÇÃO PACOTE'
            || this.subMotivo == 'RETORNO AO CLIENTE CLASS - NOVOS PRODUTOS'
            || this.subMotivo == 'RETORNO AO CLIENTE CLASS - TABELA DE PREÇO'
            ))
          || (this.motivo == 'ANÁLISE DE CONSULTAS' && (
            this.subMotivo == 'PREVENÇÃO À FRAUDE - NOVA SENHA'
            || this.subMotivo == 'PREVENÇÃO À FRAUDE - NOVO SUBCÓDIGO'
            || this.subMotivo == 'PREVENÇÃO À FRAUDE - NOVO CÓD. DESMEMBRADO'
            || this.subMotivo == 'PREVENÇÃO À FRAUDE - CONTATO COMERCIAL'
            ))
          || (this.motivo == 'CADASTRO' && (
            this.subMotivo == 'CADASTRO DE IP - CLIENTES'
            || this.subMotivo == 'DEMANDAS LOTE - TEXTO CARTA DÉBITO(COS/MEN)'
            || this.subMotivo == 'ALTERAÇÃO DENOMINAÇÃO DE CÓDIGO'
            || this.subMotivo == 'ALTERAÇÃO CADASTRO CLIENTE - CNPJ PÓS BARRA'
            || this.subMotivo == 'ALTERAÇÃO CADASTRO CLIENTE - ENDEREÇO'
            || this.subMotivo == 'ALTERAÇÃO CADASTRO CLIENTE - ENDEREÇO/RAZÃO SOCIAL'
            || this.subMotivo == 'ALTERAÇÃO CADASTRO CLIENTE - RAZÃO SOCIAL'
            || this.subMotivo == 'ALTERAÇÃO DE PACOTE - AUMENTO PLANO'
            || this.subMotivo == 'ALTERAÇÃO DE PACOTE - REDUÇÃO PLANO'
            || this.subMotivo == 'ALTERAÇÃO DE PACOTE - TABELA DO PEC'
            || this.subMotivo == 'ALTERAÇÃO DADOS BANCÁRIOS - ANÁLISE'
            || this.subMotivo == 'ALTERAÇÃO DADOS BANCÁRIOS - CADASTRO'
            || this.subMotivo == 'PORTAL REDE VERDE AMARELA - PÁGINA BLOQUEADA'
            || this.subMotivo == 'PORTAL REDE VERDE AMARELA - USUÁRIO S/ VINCULO'
            ))
          || (this.motivo == 'COBRANÇA' && (
            this.subMotivo == 'PARCELAMENTO - PENDENTE DE APROVAÇÃO'
            || this.subMotivo == 'BOLETO C/ JUROS - ORIENT. SUA FATURA'
            || this.subMotivo == 'BOLETO C/ JUROS - SEM ORIENT. SUA FATURA'
            || this.subMotivo == 'BOLETO C/ JUROS - ENVIO VIA CORREIO'
            || this.subMotivo == 'BOLETO C/ JUROS - VENCIDO + 90 DIAS'
            || this.subMotivo == 'BOLETO S/ JUROS - ORIENT. SUA FATURA'
            || this.subMotivo == 'BOLETO S/ JUROS - SEM ORIENT. SUA FATURA'
            || this.subMotivo == 'BOLETO S/ JUROS - ENVIO VIA CORREIO'
            || this.subMotivo == 'PAGTO EM DUPLICIDADE - NOTA DE CRÉDITO'
            || this.subMotivo == 'PAGTO EM DUPLICIDADE - REEMBOLSO'
            || this.subMotivo == 'BOLETO - ANÁLISE COMPROVANTE DE PAGTO'
            || this.subMotivo == 'CARTA DE ANUÊNCIA /QUITAÇÃO ANUAL DE DÉBITO'
            || this.subMotivo == 'RECIBO FORMALIZAÇÃO PAGAMENTO/CANCELAMENTO'
            || this.subMotivo == 'PRORROGAÇÃO MENOR 1.000 - RECEBEU C/ ATRASO'
            || this.subMotivo == 'PRORROGAÇÃO MENOR 1.000 - RECEBEU EM BRANCO'
            || this.subMotivo == 'PRORROGAÇÃO MENOR 1.000 - RECEBEU VENCIDO'
            || this.subMotivo == 'PRORROGAÇÃO MENOR 1.000 - DIFICULDADE FINANCEIRA'
            || this.subMotivo == 'PRORROGAÇÃO MAIOR 1.000 - RECEBEU C/ ATRASO'
            || this.subMotivo == 'PRORROGAÇÃO MAIOR 1.000 - RECEBEU EM BRANCO'
            || this.subMotivo == 'PRORROGAÇÃO MAIOR 1.000 - RECEBEU VENCIDO'
            || this.subMotivo == 'PRORROGAÇÃO MAIOR 1.000 - DIFICULDADE FINANCEIRA'
            ))
          || (this.motivo == 'CONTRATO' && (
            this.subMotivo == 'CANCELAMENTO FORÇADO DE CONTRATO' 
            || this.subMotivo == 'PEMISSAS CONTRATUAIS ENTIDADES'
            || this.subMotivo == 'EMISSÃO DE CONTRATO PADRÃO'
            || this.subMotivo == 'DESCONHECE O CONTRATO'
            || this.subMotivo == '2ª VIA DE CONTRATO - CLIENTE'
            || this.subMotivo == '2ª VIA DE CONTRATO - ENTIDADE'
            || this.subMotivo == 'CANCELAMENTO ENTIDADE'
            || this.subMotivo == 'PRORROGAÇÃO MENOR 1.000 - DIFICULDADE FINANCEIRA'
            ))
          || (this.motivo == 'FATURAMENTO' && (
            this.subMotivo == 'DISCORDÂNCIA DO VALOR DO PRODUTO'
            || this.subMotivo == 'DISCORDÂNCIA DO VALOR DA FATURA CLIENTES'
            || this.subMotivo == 'CIF BLOQUEADA / NÃO PAGA'
            || this.subMotivo == 'ENVIO DA NF'
            || this.subMotivo == 'IMPOSTOS - DEDUÇÃO INDEVIDA'
            || this.subMotivo == 'DISCORDÂNCIA DO VALOR DA FATURA ENTIDADES'
            || this.subMotivo == 'INFORME DE RENDIMENTOS'
            || this.subMotivo == 'FATURA NÃO EMITIDA'
            || this.subMotivo == 'FATURA EM DUPLICIDADE'
            || this.subMotivo == 'INFORMAÇÕES FATURA EM ABERTO'
            || this.subMotivo == 'EMISSÃO CARTA CORREÇÃO NF-E'
            || this.subMotivo == 'CIF / REPASSE / COMISSÃO'
            || this.subMotivo == 'ALTERAÇÃO GRUPO FATURAMENTO/VENCTO CLIENTES'
            || this.subMotivo == 'ALTERAÇÃO GRUPO FATURAMENTO/VENCTO ENTIDADES'
            || this.subMotivo == 'EMISSÃO DESMEMBRADA POR CÓD.' 
            || this.subMotivo == 'ESCLARECIMENTO SOBRE REAJUSTE'

            ))
          || (this.motivo == 'IMPLANTAÇÃO' && (
            this.subMotivo == 'FONTES CADASTRO POSITIVO'
            || this.subMotivo == 'COMPORTAMENTAL PJ'
            || this.subMotivo == 'ENVIO DE DOCUMENTAÇÃO TÉCNICA'
            || this.subMotivo == 'PRODUTOS E/ SERVIÇOS'
            || this.subMotivo == 'PROCESSAMENTOS DE RELATÓRIOS / ANALÍTICOS EXPORÁDICOS' 
            || this.subMotivo == 'DÚVIDAS ENVIO DE ARQUIVO' 

            ))
          || (this.motivo == 'LOJA VIRTUAL' && (
            this.subMotivo == 'RECUPERAÇÃO LOGIN/SENHA - ESQUECEU LOGIN'
            || this.subMotivo == 'RECUPERAÇÃO - LOGIN/SENHA - ESQUECEU SENHA'
            || this.subMotivo == 'RECUPERAÇÃO LOGIN/SENHA - LOGIN/SENHA'
            || this.subMotivo == 'LOJA VIRTUAL REEMBOLSO DE COMPRA'
            || this.subMotivo == 'CANCELAMENTO DE PLANO LOJA VIRTUAL'
            ))
          || (this.motivo == 'PRODUTO' && (
            this.subMotivo == 'RECUPERAÇÃO - SCPC COMUNICA 1º ACIONAMENTO'
            || this.subMotivo == 'RECUPERAÇÃO - SCPC COMUNICA'
            || this.subMotivo == 'RECUPERAÇÃO - PARAMETRIZAÇÃO'
            || this.subMotivo == 'CANCELAMENTO DE PRODUTOS'
            || this.subMotivo == 'LIBERAÇÃO DE PRODUTO'
            || this.subMotivo == 'LIBERAÇÃO REGISTRO DÉBITO'
            || this.subMotivo == 'PRODUTO LIBERADO COM ERRO'
            || this.subMotivo == 'DESENVOLVIMENTO MODELOS DE SCORE'
            || this.subMotivo == 'LIBERAÇÃO GERENCIAMENTO CARTEIRA ENTIDADES'
            || this.subMotivo == 'AED CONTAGENS'
            || this.subMotivo == 'AED ARQUIVO/CARGA'
            || this.subMotivo == 'AED PARAMETRIZAÇÃO'
            || this.subMotivo == 'RECUPERAÇÃO - DÚVIDAS SIMPLES'
            || this.subMotivo == 'RECUPERAÇÃO - DÚVIDAS COMPLEXAS'
            || this.subMotivo == 'DEMANDAS LOTE - LIBERAÇÃO PRODUTOS' 

            ))
          || (this.motivo == 'RELATÓRIOS' && (
            this.subMotivo == 'RELATÓRIOS EM BRANCO'
            || this.subMotivo == 'RELATÓRIO DE CONSULTAS - INDISP./OSCILAÇÃO PORTAL'
            || this.subMotivo == 'RELATÓRIO DE CONSULTAS - LINHAS EXCEDIDAS'
            || this.subMotivo == 'RELATÓRIO DE CONSULTAS - PERÍODO ANTERIOR 2015'
            || this.subMotivo == 'RELATÓRIO RECUPERAÇÃO - CHQ040'
            || this.subMotivo == 'RELATÓRIO RECUPERAÇÃO - DPJ100'
            || this.subMotivo == 'RELATÓRIO RECUPERAÇÃO - SCPC20'
            || this.subMotivo == 'RELATÓRIO RECUPERAÇÃO - SIA617'
            || this.subMotivo == 'RELATÓRIO RECUPERAÇÃO - SIA866'
            || this.subMotivo == 'RELATÓRIO RECUPERAÇÃO - SIA935'
            || this.subMotivo == 'RELATÓRIO RECUPERAÇÃO - SPN161'
            || this.subMotivo == 'RELATÓRIO CUSTOMIZADO/DESCONTINUADO'
            || this.subMotivo == 'PROCESSAMENTO PRODUTOS – BATCH'
            ))
            || (this.motivo =='ERRO NO ARQUIVO' && (  
              this.subMotivo == 'NEGATIVAÇÃO - (INCLUSÃO/EXCLUSÃO)'
              || this.subMotivo == 'GERENCIAMENTO DE CARTEIRA'
            ))
            || (this.motivo =='LIBERAÇÃO DE IP' && (  
              this.subMotivo == 'LIBERAR IPS'
            ))
            || (this.motivo =='MIGRAÇÃO INTEGRA' && (  
              this.subMotivo == 'INTEGRA'
            ))
            || (this.motivo =='SUPORTE TÉCNICO' && (  
              this.subMotivo == 'DIVERGÊNCIA ENTRE PRODUTOS'
            ))
            || (this.motivo =='SUPORTE AO SITE/APP' && (  
            this.subMotivo == 'PORTAL REDE VERDE AMARELA - DÚVIDAS CONTEÚDOS'
            || this.subMotivo == 'PORTAL REDE VERDE AMARELA - DÚVIDAS DE PREÇO'
          ))

        )
        {       
            this.FIELDS_CUSTOM = [CODIGO_DE_CLIENTE];
            this.HasCodigoDeCliente = true;
          }
          else{
            this.HasCodigoDeCliente = false;
      }
    }
    if (this.recordDeveloperName=='CSMAtendimento')
      {
        if (this.motivo == 'PRODUTO' && (
          this.subMotivo == 'RECUPERAÇÃO - RECLAMAÇÃO DO CONSUMIDOR'
        ||this.subMotivo == 'INF. SOBRE PRODUTOS'
        ||this.subMotivo == 'INF. SOBRE PRODUTOS SCORE'
        ||this.subMotivo == 'AED CONTAGENS'
        ||this.subMotivo == 'RECUPERAÇÃO - COMUNICADO ESPELHO'
        ||this.subMotivo == 'RECUPERAÇÃO - DÚVIDAS SIMPLES'
        ||this.subMotivo == 'RECUPERAÇÃO - DÚVIDAS COMPLEXAS'
        ||this.subMotivo == 'RECUPERAÇÃO - SCPC COMUNICA 1º ACIONAMENTO'
        ||this.subMotivo == 'RECUPERAÇÃO - SCPC COMUNICA'
        ||this.subMotivo == 'RECUPERAÇÃO - CADASTRAMENTO DE PROTESTO'
        ||this.subMotivo == 'RECUPERAÇÃO - RECLAMAÇÃO DO CONSUMIDOR'))
        {
          this.FIELDS_CUSTOM = [PRODUCT_ID];
        }
      }
      if (this.recordDeveloperName=='CSMAtendimento')
      {
        console.log("Olha pra mim" + this.kbaAprovado);
        if (this.motivo == 'CADASTRO POSITIVO' && (
          this.subMotivo == 'CP - CANCELAMENTO DO CADASTRO PF - RETIDO'
          || this.subMotivo == 'CP - CANCELAMENTO DO CADASTRO PF - Ñ RETIDO'
          || this.subMotivo == 'CP - REAUTORIZAÇÃO DO CADASTRO PF') && this.kbaAprovado == null)
        {
          if(this.TempoKba >= 1)
          {
            console.log( "CPF" + this.CPF_KBA);
            this.showKBA = true;
            console.log("KBA" + this.showKBA);
          }
          else
          {
            this.showErrorToast('Contato Bloqueado', 'Excedeu o número de tentativas no prazo de 24 horas', 'error');
            this.motivo = null;
            this.subMotivo = null;
            this.showKBA = false;
            this.aoMudarMotivo();
          }
         
          
        }
      }
    }
  }
    onValidateKba(event){
      if(event.detail.ResultCode == 0)
      {
        this.motivo = null;
        this.subMotivo = null;
        this.showKBA = false;
        this.aoMudarMotivo();
      }
      else if(event.detail.ResultCode == 1)
      {
        this.kbaAprovado = "Sim";
        this.retornoKba = event.detail.ResultCode + " "+ event.detail.ResultMessage;
        this.showKBA = false;
        //this.confirmaCatalogoDeServico(new Event('submit', {detail: FIELDS_CUSTOM}));   
        //var btn = document.getElementById("btn");
        //btn.addEventListener("submit", this.confirmaCatalogoDeServico);
        try
        {
         let btn = this.template.querySelector("lightning-button");
         console.log("btn" + btn);
          btn.click();
        }catch(e){
          console.log(e);
        }
       
      }
      else
      {
        this.kbaAprovado = "Não";
        this.retornoKba = event.detail.ResultCode + " "+ event.detail.ResultMessage;
        this.showKBA = false;
        try
        {
         let btn = this.template.querySelector("lightning-button");
         console.log("btn" + btn);
          btn.click();
        }catch(e){
          console.log(e);
        }
      }
    }

  @track mostrarPID=false;
  @track mostrarPergunta1=false;
  @track mostrarPergunta2=false;
  @track mostrarPergunta3=false;
  @track mostrarPergunta4=false;
  @track mostrarPergunta5=false;
  @track mostrarPergunta6=false;
  @track mostrarPergunta7=false;
  @track mostrarPergunta8=false;

  @track corretoPergunta1=false;
  @track corretoPergunta2=false;
  @track corretoPergunta3=false;
  @track corretoPergunta4=false;
  @track corretoPergunta5=false;
  @track corretoPergunta6=false;
  @track corretoPergunta7=false;
  @track corretoPergunta8=false;
  

  @track pidMostrar;
  atualizarPID(pid){


    this.corretoPergunta1=false;
    this.corretoPergunta2=false;
    this.corretoPergunta3=false;
    this.corretoPergunta4=false;
    this.corretoPergunta5=false;
    this.corretoPergunta6=false;
    this.corretoPergunta7=false;
    this.corretoPergunta8=false;
    if (pid && pid.pergunta1 && pid.pergunta1!=''){
      this.mostrarPID=true;
      this.mostrarPergunta1=true;
      this.mostrarPergunta2=pid.pergunta2 && pid.pergunta2!='';
      this.mostrarPergunta3=pid.pergunta3 && pid.pergunta3!='';
      this.mostrarPergunta4=pid.pergunta4 && pid.pergunta4!='';
      this.mostrarPergunta5=pid.pergunta5 && pid.pergunta5!='';
      this.mostrarPergunta6=pid.pergunta6 && pid.pergunta6!='';
      this.mostrarPergunta7=pid.pergunta7 && pid.pergunta7!='';
      this.mostrarPergunta8=pid.pergunta8 && pid.pergunta8!='';
      this.pidMostrar=pid;
    } else {
      this.mostrarPID=false;
    }
  }

  handleChangePergunta1(e) {
    console.log(e);
    this.corretoPergunta1 = e.target.checked;
    console.log('this.corretoPergunta1');
    console.log(this.corretoPergunta1);
  }

  handleChangePergunta2(e) {
    this.corretoPergunta2 = e.target.checked;
  }

  handleChangePergunta3(e) {
    this.corretoPergunta3 = e.target.checked;
  }

  handleChangePergunta4(e) {
    this.corretoPergunta4 = e.target.checked;
  }

  handleChangePergunta5(e) {
    this.corretoPergunta5 = e.target.checked;
  }

  handleChangePergunta6(e) {
    this.corretoPergunta6 = e.target.checked;
  }

  handleChangePergunta7(e) {
    this.corretoPergunta7 = e.target.checked;
  }

  handleChangePergunta8(e) {
    this.corretoPergunta8 = e.target.checked;
  }

  
  handleChangeMeioDeAcesso(event){
    this.meioDeAcesso = event.detail.value;
    this.mostrarCamposTabulacao();
  }

  handleChangeSubMotivo2(event){
    this.subMotivo2 = event.detail.value;
    this.mostrarCamposTabulacao();
  }

  cancelaCatalogoDeServico(event){
    this.inicializarTela(); 
    this.HasCodigoDeCliente = false;
    this.valueCodigoDeCliente = '';

    updateRecord({ fields: { Id: this.recordId } });
    return(false);
  }

  confirmaCatalogoDeServico(event)
  {
    event.preventDefault();
    this.PIDAprovado=false;
    let validarPID = false;

         

    if (this.mostrarPID && this.pidMostrar){

      validarPID = true;

      if (this.bloquearCasoPIDInvalido) {
        this.showErrorToast('Caso', 'Contato bloqueado para essa solicitação por falta de identificação positiva, orientar cliente confirmar os dados solicitados anteriormente e retornar após este período', 'error');
       
        return false;        
      }


      let respostasCorretas=0;

      if (this.corretoPergunta1) respostasCorretas++;
      if (this.corretoPergunta2) respostasCorretas++;
      if (this.corretoPergunta3) respostasCorretas++;
      if (this.corretoPergunta4) respostasCorretas++;
      if (this.corretoPergunta5) respostasCorretas++;
      if (this.corretoPergunta6) respostasCorretas++;
      if (this.corretoPergunta7) respostasCorretas++;
      if (this.corretoPergunta8) respostasCorretas++;

      let mensagemBloqueio = `Algumas das respostas não batem com o cadastro, PID bloqueado por ${this.configuracaoTempoBloqueioCasoPID} min. Orientar cliente confirmar os dados e retornar após esse período.`;

      if (this.pidMostrar.tipo=='CRITICO'){
        if (respostasCorretas<6){          
          this.showErrorToast('Caso', mensagemBloqueio, 'error');          
        } else {
          this.PIDAprovado=true;
        }
      } else if (this.pidMostrar.tipo=='MODERADO'){
        if (respostasCorretas<3){          
          this.showErrorToast('Caso', mensagemBloqueio, 'error');
          
        } else {
          this.PIDAprovado=true;
        }
      } else if (this.pidMostrar.tipo=='MODERADO FINANCEIRO'){
        if (respostasCorretas<5){          
          this.showErrorToast('Caso', mensagemBloqueio, 'error');          
        } else {
          this.PIDAprovado=true;
        }
      }
      if(this.PIDAprovado){
        console.log('Mostrar toast');
        const evt = new ShowToastEvent({
          title: 'Caso',
          message: 'PID validado com sucesso!!',
          variant: 'success',
          mode: 'dismissable'
        });
        this.dispatchEvent(evt);
      }
    }
    try{
      const eventFields = event.detail.fields;
      console.log("event" + JSON.stringify(eventFields));
      var caso = new Object();
      caso.caseId = this.recordId;
      caso.motivo = this.getMotivo;
      caso.subMotivo = this.getSubMotivo;
      caso.subMotivo2 = this.getSubMotivo2;
      caso.meioDeAcesso = this.getMeioDeAcesso;
      caso.recordDeveloperName = this.recordDeveloperName;
      caso.centroCusto = this.obterValorcampo(eventFields, 'centroCusto__c');
      caso.justificativa = this.obterValorcampo(eventFields, 'justificativa__c');
      caso.qualRemetenteDestinatarioEmail = this.obterValorcampo(eventFields, 'qualRemetenteDestinatarioEmail__c');
      caso.qualDataHoraEmail = this.obterValorcampo(eventFields, 'qualDataHoraEmail__c');
      caso.aQuemPertenceEsteEmail = this.obterValorcampo(eventFields, 'aQuemPertenceEsteEmail__c');
      caso.qualFoiArquivoAtividadeBloqueada = this.obterValorcampo(eventFields, 'qualFoiArquivoAtividadeBloqueada__c');
      caso.existeDocumentoConsultaBVS = this.obterValorcampo(eventFields, 'existeDocumentoConsultaBVS__c');
      caso.informeDadosAutorFraude = this.obterValorcampo(eventFields, 'informeDadosAutorFraude__c');
      caso.dataOcorrenciaFraude = this.obterValorcampo(eventFields, 'dataOcorrenciaFraude__c');
      caso.dataDenuncia = this.obterValorcampo(eventFields, 'dataDenuncia__c');
      caso.cPFVitima = this.obterValorcampo(eventFields, 'cPFVitima__c');
      caso.nomeVitima = this.obterValorcampo(eventFields, 'nomeVitima__c');
      caso.nomeEmpresaEnvolvidaFraude = this.obterValorcampo(eventFields, 'nomeEmpresaEnvolvidaFraude__c');
      caso.documentoConsulta = this.obterValorcampo(eventFields, 'documentoConsulta__c');
      caso.qualURLParaRemocao = this.obterValorcampo(eventFields, 'qualURLParaRemocao__c');
      caso.qualURLPhishing = this.obterValorcampo(eventFields, 'qualURLPhishing__c');
      caso.aQuemPertenceSolicitacao = this.obterValorcampo(eventFields, 'aQuemPertenceSolicitacao__c');
      caso.outros = this.obterValorcampo(eventFields, 'outros__c');
      caso.atualmenteUtilizaVDI = this.obterValorcampo(eventFields, 'atualmenteUtilizaVDI__c');       
      caso.qualSiteVoceDesejaLiberacao = this.obterValorcampo(eventFields, 'qualSiteVoceDesejaLiberacao__c');       
      caso.aQuemPertenceEsteSite = this.obterValorcampo(eventFields, 'aQuemPertenceEsteSite__c');

        console.log('entrou onde a gente queria');
        caso.productId = this.obterValorcampo(eventFields, 'ProductId');
        console.log(caso.productId);
        console.log(caso.productId);
        console.log(caso.productId);

      caso.quantidadeConsultas = this.obterValorcampo(eventFields, 'QuantidadeConsultas__c');
      caso.dataInicio = this.obterValorcampo(eventFields, 'DataInicio__c');
      caso.datafim = this.obterValorcampo(eventFields, 'Datafim__c');
      caso.nomeUsuario = this.obterValorcampo(eventFields, 'NomeUsuario__c');
      caso.cPF = this.obterValorcampo(eventFields, 'CPF__c');
      caso.emailUsuario = this.obterValorcampo(eventFields, 'EmailUsuario__c');
      caso.nomeOperador = this.obterValorcampo(eventFields, 'NomeOperador__c');
      caso.dataNascimento = this.obterValorcampo(eventFields, 'DataNascimento__c');
      caso.emailOperador = this.obterValorcampo(eventFields, 'EmailOperador__c');
      caso.codigoDeCliente = this.valueCodigoDeCliente;
      caso.acessarPlataforma = this.acessarPlataforma;
      caso.PIDAprovado = this.PIDAprovado;
      caso.bloquearCriacaoCasoPID = this.bloquearCasoPIDInvalido;
      caso.dataLimiteBloqueioCasoPID = this.dataLimiteBloqueioCasoPIDInvalido;
      caso.validouPID = validarPID;
      caso.contato = this.caseContact;
      caso.retornoKba = this.retornoKba;
      caso.kbaAprovado = this.kbaAprovado;

      console.log('Objeto', JSON.stringify(caso));
      console.log(caso);
      salvarCaso({
        casoWrapper: caso,
      }).then(result => {
        getRecordNotifyChange([{recordId: this.recordId}]);
        this.updateRecordView();
      }).catch(error => {
          if (error.body.pageErrors && error.body.pageErrors.length>0){
              this.showErrorToast('Caso', error.body.pageErrors[0].message, 'error');
          } else if (error.body.fieldErrors){
              let separator="\n";
              let message = Object.values(error.body.fieldErrors).map(field => {
                  return field.map(error => error.message).join(separator);
              }).join(separator);

              this.showErrorToast('Caso', message, 'error');
          } else {
              this.showErrorToast('Caso', error.body.message, 'error');
          }
      });
    } catch(erro){
      console.log('error:'+erro);
      console.log(erro);
    }
    return(false);
  }

  obterValorcampo(eventFields, campo){
    try{
      for(var field in eventFields){
        console.log(campo);
        console.log(field);

        if (campo.toUpperCase()==field.toUpperCase()){
          return(eventFields[field]);
        }
      }
    } catch(erro){
      console.log(erro);
    }
    return(null);
  }

  showErrorToast(title, message, variant) {
    const evt = new ShowToastEvent({
      title: title,
      message: message,
      variant: variant,
      mode: 'dismissable'
    });
    this.dispatchEvent(evt);
  }

  formViaWeb(){
    console.log('formViaWeb()');
    try {
      this.HasCodigoDeCliente = false;

      this.disabledSubMotivo = false;
      this.disabledMotivo = false;
      this.disabledSubMotivo2 = false;
      this.disabledMeioDeAcesso = false;
    } catch (error) {
      console.log('formViaWeb ERROR: ' + error);
    }
  }
  
  mostrarEdit(){
    console.log('mostrarEdit()');
    console.log('Id: ' + this.recordId);

    getCase({
      caseId: this.recordId
    })
    .then(result=>{
      console.log('getCase()');
      if (result.CategorizacaoAutomatica__c == true && this.disabledMotivo == true){ 
            
        console.log('ENTROU NO IF');
        this.mostra = true;
      }else{
        this.mostra = false;
      }
    })
    .catch(error=>{
      console.log('getCase ERROR: ' + error.body.message);
    });
    
    setTimeout(function(){
      console.log('ORIGIN: ' + this.origem);
      
    },5000);
  }

  @api
  get getMotivo(){
    return this.motivo;
  }

  @api
  get getSubMotivo(){
    return this.subMotivo;
  }

  @api
  get getMeioDeAcesso(){
    return this.meioDeAcesso;
  }

  @api
  get getSubMotivo2(){
    return this.subMotivo2;
  }

  @api
  get temObservacoes(){
    return this.observacoes && this.observacoes!='';
  }
  updateRecordView() {
    setTimeout(() => {
         eval("$A.get('e.force:refreshView').fire();");
    }, 1000); 
  }

  
}