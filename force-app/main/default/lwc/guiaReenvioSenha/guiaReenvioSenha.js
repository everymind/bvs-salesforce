import { LightningElement, api, wire, track } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import MOTIVO_FIELD from '@salesforce/schema/Case.MotivoTxt__c';
import SUBMOTIVO_FIELD from '@salesforce/schema/Case.SubMotivoTxt__c';
import CPFCNPJ_FIELD from '@salesforce/schema/Case.Account.CnpjCpf__c';
import EMAILCONTATO_FIELD from '@salesforce/schema/Case.ContactEmail';
import CONTATO_FIELD from '@salesforce/schema/Case.ContactId';
import CONTA_FIELD from '@salesforce/schema/Case.AccountId';
import buscarUrl from '@salesforce/apex/IFramePortalController.buscarUrl';
import buscarUrlcomIdContato from '@salesforce/apex/IFramePortalController.buscarUrlcomIdContato';
import buscaoContatos from '@salesforce/apex/GuiaReenvioSenhaController.buscaContatosRelacionados';
const FIELDS = [MOTIVO_FIELD, SUBMOTIVO_FIELD, CPFCNPJ_FIELD, EMAILCONTATO_FIELD, CONTATO_FIELD, CONTA_FIELD];
export default class GuiaReenvioSenha extends LightningElement {

  @track contatos

  value

  @api recordId;
  case;
  pid = true;
  publicURL;
  motivo;
  submotivo;
  @track
  nomelabel
  @track
  showReenvioSenha = false;
  @track acessarUrl = false;
  urlMetadata;
  cpf_cnpj;
  email;
  contactId;
  accountId = ' ';
  operadorId = ' ';
  idConta;
  idContatoRelacionado;
  carregouContatos = false;
  get opcoes() {
    //this.showContactsFromAccount();
    return this.contatos;
  }

  @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
  wireRecord({ error, data }) {
    if (error) {
      this.showErrorToast('Erro ao buscar informações do caso', error.body, 'error');
    } else if (data) {
      this.case = data;
      this.motivo = this.case.fields.MotivoTxt__c.value;
      this.submotivo = this.case.fields.SubMotivoTxt__c.value;
      this.contactId = this.case.fields.ContactId.value;
      this.idConta = this.case.fields.AccountId.value;
      this.cpf_cnpj = this.case.fields.Account.value.fields.CnpjCpf__c.value;
      this.email = this.case.fields.ContactEmail.value;
      this.handleIframe(this.motivo, this.submotivo);
      buscaoContatos({
        accId: this.case.fields.AccountId.value
      }).then((contacts) => {

        console.log('Contacts: ', JSON.stringify(contacts))
        const contatosFormatados = contacts.map((contact) => {
          console.log(contact);
          const { Name } = contact;
          const { Id } = contact;
          const {OperadorAC__c} = contact;
          console.log(Name);
          this.nomelabel =  OperadorAC__c === undefined ? Name + '': Name + ' - '+OperadorAC__c;

          const entry = { label: this.nomelabel, value: Id };
          return entry;
        })
        console.log(contatosFormatados);
        this.contatos = contatosFormatados;
        this.carregouContatos = true;
      }).catch((reason) => {
        console.log(reason);

      });
    }

  }



  handleIframe(motivo, submotivo) {
    console.log(motivo, submotivo);
    if (motivo == 'ACESSOS' && this.pid &&
      (submotivo == 'SENHA CÓD. OPERADOR - ORIENTAÇÃO PORTAL' ||
        submotivo == 'SENHA CÓD. OPERADOR - EXCESSO DE TENTATIVAS' ||
        submotivo == 'SENHA CÓD. OPERADOR - E-MAIL DIFERENTE' ||
        submotivo == 'SENHA CÓD. OPERADOR - ERRO NO PORTAL' ||
        submotivo == 'SENHA CÓD. OPERADOR - NÃO RECEBEU E-MAIL' ||
        submotivo == 'SENHA CÓD. OPERADOR - NÃO RECEBEU ACESSO MIGRAÇÃO' ||
        submotivo == 'SENHA CÓD. OPERADOR - REJEITADO/DEVOLVIDO' ||
        submotivo == 'SENHA CÓDIGO DE 8')
    ) {
      console.log('handle iframe')
      this.showReenvioSenha = true;
      console.log('value', this.value)
      //buscar URL com ID COntato
    }
  }

  handleAccessClick(event) {
    console.log(this.acessarUrl);
    console.log(event);
  
    this.acessarUrl = true;
    
    buscarUrlcomIdContato({
      recordId: this.contactId,
      portal: 'GuiaReenvioSenha',
      conta: this.idConta,
      recordIdContato: this.value
    })
      .then(urlMetadata => {
        console.log(urlMetadata);
        this.publicURL = urlMetadata;
        console.log('URL ABAIXO');
        console.log(this.publicURL);
      })
      .catch((reason) => {
        console.log(reason);

      });
  }

  showUrlComId() {
    console('acessaURL=', this.acessarUrl)
    console('acessaURL=', this.acessarUrl)
    console('acessaURL=', this.acessarUrl)
    this.acessarUrl = true;
    console('acessaURL=', this.acessarUrl)
    console('acessaURL=', this.acessarUrl)
    console('acessaURL=', this.acessarUrl)
    console('acessaURL=', this.acessarUrl)
  }

  showErrorToast(title, message, variant) {
    const evt = new ShowToastEvent({
      title: title,
      message: message,
      variant: variant,
      mode: 'dismissable'
    });
    this.dispatchEvent(evt);
  }

  handleChange(event) {
    console.log('handler: ', JSON.stringify(event.detail));
    this.value = event.detail.value;
  }
}