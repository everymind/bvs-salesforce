import { LightningElement } from 'lwc';
import BaseChatHeader from 'lightningsnapin/baseChatHeader';

export default class ChatDisclaimerLGPD extends BaseChatHeader {
    redirectHome(){
        var strDom = window.location.toString();
        var strSplit = strDom.split("/s/");
        var dominio = strSplit[0];

        window.open(dominio, '_blank');
    }
}