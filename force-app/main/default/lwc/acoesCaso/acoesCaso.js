import { api, LightningElement, track } from 'lwc';
import retornaDadosCaso from '@salesforce/apex/AcoesCasoController.retornaDadosCaso';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import SystemModstamp from '@salesforce/schema/Account.SystemModstamp';

export default class AcoesCaso extends LightningElement {
    @api recordId;
    //clickedButtonLabel;
    isModalOpen = false;
    isIFrame = false;
    spinner = true;
    subMotivo = '';
    mensagemSemAcao = false;
    btnCriarOperador = false;
    @track URL = '';


    handleCriarOperadorClick(event) {
        //this.clickedButtonLabel = event.target.label;
        this.isModalOpen = true;
        this.isIFrame = true;
    }

    handleClickCancel(event){
        console.log('handleClickCancel');
        this.isModalOpen = false;
    }

    handleVoltarClick(event){
        this.isModalOpen = false;
        this.isIFrame = false;
    }

    showToast(title, message, variant) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(event);
    }

    connectedCallback(){
        retornaDadosCaso({
            idCase: this.recordId
            })
            .then(result => {
                console.log(JSON.stringify(result));
                this.URL = "https://phpcorp.scpcnet.com.br/ACSPNET/Programas/RightNow/ManOperador/index.php?lk_codig=" + result.codigo;
                console.log('URL' + this.URL);
                this.subMotivo = result.subMotivo;
                if (this.subMotivo == 'CRIAÇÃO CÓD. OPERADOR CLIENTE'
                    || this.subMotivo == 'CRIAÇÃO CÓD. OPERADOR ENTIDADE'){
                    
                    this.btnCriarOperador = true;
                }
                if (!this.btnCriarOperador){
                    this.mensagemSemAcao = true;
                }
                this.spinner = false;
            })
            .catch(error => {
                this.showToast('Erro', error.body.message, 'error');
                console.log(error);
                this.spinner = false;
            })
    }
}