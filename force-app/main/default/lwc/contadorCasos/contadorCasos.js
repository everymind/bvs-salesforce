import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import buscarCasos from '@salesforce/apex/contadorCasosController.buscarCasosPorCliente';
import buscarCasosContact from '@salesforce/apex/contadorCasosController.buscarCasosPorClienteContato';
import montarListaMotivoSubMotivo from '@salesforce/apex/contadorCasosController.montarListaMotivoSubMotivo';

export default class ContadorCasos extends NavigationMixin(LightningElement) {
    @api recordId;
    @track hasCases = false;
    @track listCases;
    @track contador;
    @track tituloCard;
    @track listMotivoSubMotivo = [];


    connectedCallback(){
        if(this.recordId.slice(0,3) == '001')
        {
            buscarCasos({contaId: this.recordId})
            .then(result => {
                this.listCases = result;
                this.hasCases = true;
                this.openModal = false;
                this.tituloCard = 'Casos (' + this.listCases.length + ')';
                montarListaMotivoSubMotivo({casos: this.listCases})
                .then(result => {
                    for (let key in result) {
                        this.listMotivoSubMotivo.push({value:result[key], key:key});
                    }
                })
            })
            .catch(result => {
                this.openModal = false;
                this.hasCases = false;
                this.tituloCard = 'Casos (0)';
            })
        }
        else
        {
            buscarCasosContact({contatoId: this.recordId})
            .then(result => {
                this.listCases = result;
                this.hasCases = true;
                this.openModal = false;
                this.tituloCard = 'Casos (' + this.listCases.length + ')';
                montarListaMotivoSubMotivo({casos: this.listCases})
                .then(result => {
                    for (let key in result) {
                        this.listMotivoSubMotivo.push({value:result[key], key:key});
                    }
                })
            })
            .catch(result => {
                this.openModal = false;
                this.hasCases = false;
                this.tituloCard = 'Casos (0)';
            })
        }
        
    }
    recordPage(event){
        let text = event.target.innerHTML;
        let listaFiltrada = this.listCases.filter(caso => caso.CaseNumber == text);
        let id = listaFiltrada[0].Id;
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: id,
                actionName: 'view'
            }
        });   
    }
    
}