/**
 * @Autor: Everymind
 * @Data: 18/07/2020
 * @Descrição: Trigger que orquestra os eventos de DML do objeto Opportunity.
 */
trigger OpportunityTrigger on Opportunity (before insert, after insert, before update, after update) {
	new OpportunityTriggerHandler().run();
}