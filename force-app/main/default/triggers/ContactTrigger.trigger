/**
 * @Autor: Maycon
 * @Data: 13/07/2022
 * @Descrição: Trigger que orquestra os eventos de DML do objeto Contact.
 */
trigger ContactTrigger on Contact (after update) {
	new ContactTriggerHandler().run();
}