/**
 * @Autor: Everymind
 * @Data: 21/08/2020
 * @Descrição: Trigger que orquestra os eventos de DML do objeto Account.
 */
trigger AccountTrigger on Account (after insert, after update) {
	new AccountTriggerHandler().run();
}