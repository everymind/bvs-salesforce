trigger FilaAnterior on Case (before update) {

 for(Case caso : Trigger.New) {
       
      Id fila;
      if(caso.FilaAnterior__c != null){
      fila = [SELECT Id, Name FROM Group WHERE Type = 'Queue' AND Name = :caso.FilaAnterior__c].Id;
      ProcessInstance pi = [SELECT Id, Status FROM ProcessInstance WHERE TargetObjectId =: caso.Id];
      if(pi != null && pi.Status == 'Rejected' && fila != null){
            caso.OwnerId = fila;
        }
      }  
  }
 }