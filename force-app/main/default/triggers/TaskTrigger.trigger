/**
 * @Autor: Everymind
 * @Data: 20/07/2020
 * @Descrição: Trigger que orquestra os eventos de DML do objeto Task.
 */
trigger TaskTrigger on Task (after update,before delete) {
	new TaskTriggerHandler().run();
}