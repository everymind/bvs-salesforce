/**
 * @Autor: Everymind
 * @Data: 14/10/2020
 * @Descrição: Trigger que orquestra os eventos de DML do objeto Event.
 */
trigger EventTrigger on Event (after insert, after update) {
	new EventTriggerHandler().run();
}