trigger SLAAtivar on Case (before update) {
    for(Case atual : Trigger.New) {
      for(Case antes : Trigger.Old) {
		if (antes.Id==atual.Id){
			if (antes.Fila__c<>atual.Fila__c){
				//Mantendo a fila anterior, sempre que mudar a fila
				atual.FilaAnterior__c=antes.Fila__c;
				//Mantendo o proprietário anterior do caso
				atual.OwnerIdAnterior__c=antes.OwnerId;
			}
				
			if (antes.SLAAtivo__c<>atual.SLAAtivo__c){
				if (atual.SLAAtivo__c==false){
					//Quando o campo SLAAtivo__c for desmarcado(false), pausa o tempo decorrido, somando com o decorrido atual
					List<CaseMilestone> cmsToUpdate = [select Id, ElapsedTimeInMins 
					from CaseMilestone cm
					where caseId = :atual.Id 
					and completionDate = null limit 1];
					for (CaseMilestone cm : cmsToUpdate){
						if (atual.TempoDecorridoSLACliente__c==null){
							atual.TempoDecorridoSLACliente__c=cm.ActualElapsedTimeInMins;
						} else {
							atual.TempoDecorridoSLACliente__c=atual.TempoDecorridoSLACliente__c + cm.ActualElapsedTimeInMins;
						}
					}
				}
			}
			
			if (antes.SLAFinalizado__c<>atual.SLAFinalizado__c){
				//Quando o caso for marcado como finalizado, encerra o marco
				List<CaseMilestone> cmsToUpdate = [select Id, completionDate
					from CaseMilestone cm
					where caseId = :atual.Id
					and completionDate = null limit 1];
					
				if (cmsToUpdate.isEmpty() == false){
					DateTime completionDate = System.now(); 
					for (CaseMilestone cm : cmsToUpdate){
						cm.completionDate = completionDate;
					}
					update cmsToUpdate;
				}	
			}
		}
      }
    }
}