trigger OpportunityLineItemTrigger on OpportunityLineItem (before insert, after insert, after delete, before delete) {
	new OpportunityLineItemTriggerHandler().run();
}