/**
 * @Autor: Everymind
 * @Data: 21/08/2020
 * @Descrição: Trigger que orquestra os eventos de DML do objeto UserTerritory2Association.
 */
trigger UserTerritory2AssociationTrigger on UserTerritory2Association (after insert, after delete) {
	new UserTerritory2AssociationTriggerHandler().run();
}