/*
@Autor: Everymind
@Data: 18/05/2020
@Descrição: Trigger responsável por registrar os campos que foram alterados na conta.
Para vizualizar é necessário ir até "Registros de depuração" na caixa de busca do Salesforce,
alterar a data de expiração do "AccountDebugChanges" e então será gravado os logs
quando houver alguma alteração na conta.
*/

trigger AccountChangeTrigger on AccountChangeEvent (after insert) {

    Set<String> acctId = new Set<String>();

    for (AccountChangeEvent event : Trigger.New) {
        //Captura o Id da Conta alterada
        List<String> recordIds = event.ChangeEventHeader.getRecordIds();
        acctId.addAll(recordIds);

        EventBus.ChangeEventHeader header = event.ChangeEventHeader;

      // For update Operação, captura a lista de todos os campos alterados
      if (header.changetype == 'UPDATE') {
        System.debug('ID da Conta: ' + acctId);
        System.debug('---------LISTA DE TODOS OS CAMPOS ALTERADOS---------');

          for (String field : header.changedFields) {
              if (null == event.get(field)) {
                  System.debug('Deleted field value (set to null): ' + field);
              } else {
                  System.debug('CAMPO ALTERADO: ' + field + ' / NOVO VALOR: '
                      + event.get(field));
              }
          }
      }
    } 

}